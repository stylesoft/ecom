<?php
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'Login';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************

  if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }
    
    /*
    if (isset($_GET['activation'])) {
    	$activation = $_GET['activation'];
    	$id = $_GET['id'];
    }
    */
    

    
    $selectedPageInfo = $pageObject->getPage($pageId);
    $pageId = $selectedPageInfo->id;
        $pageTitle = "Edit Account";
        $pageKeywords = "Edit Account";
        $pageDescription = "Edit Account";
     
        
        


$CONTENT = '';
$CONTENT = FRONT_LAYOUT_VIEW_PATH . "customer/editaccount.tpl.php";
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>
