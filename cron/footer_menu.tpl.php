<div id="footer">
	<div class="footer_bg_img"></div>
    <div class="footer_innerdiv">
    	<div class="footer_condiv">
        	<div class="footer_colum">
                <h1>Information</h1>
                <ul>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/56/contact-us.html">Contact</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/69/delivery.html">Delivery</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/70/legal-notice.html">Legal Notice</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/64/privacy-policy.html">Terms and conditions of use</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/55/about-us.html">About us</a></li>
                </ul>
            </div>	
            <div class="footer_colum">
                <h1>Our offers</h1>
                <ul>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/53/products.html?k=new-products">New products</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/53/products.html?k=top-sellers">Top sellers</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>page/53/products.html?k=specials">Specials</a></li>
                </ul>
            </div>	
            <div class="footer_colum">
                <h1>Your Account</h1>
                <ul>
                    <li><a href="<?php print(SITE_BASE_URL);?>myaccount.html">Your Account</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>myaccount.html">Personal information</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>myaccount.html?myaccp=addresses">Addresses</a></li>
                    <li><a href="#">Discount</a></li>
                    <li><a href="<?php print(SITE_BASE_URL);?>myaccount.html?myaccp=Orders-History">Orders history</a></li>
                </ul>
            </div>	
            <div class="clear"></div>
        </div>
    </div> 
     
</div>
  <div class="sub_footer">
        	<div  class="sub_footer_inner">
                <div class="footer_copyright">Copyright © Cowper Auto Paints All rights reserved. </div>                       
                <div class="footer_pluspro"><a href="http://www.pluspro.com" target="_blank"><img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/plus-logo.png" width="185" height="68" alt="" border="0" /></a></div>
                <div class="clear"></div>
           </div>
         </div> 