<ul>
    <?php
    $objMenuPages = new Page();
    $menuPages = $objMenuPages->getAllByStatus('Published');
    ?>
    <li><a href="<?php print(SITE_BASE_URL); ?>" class="a_visited">HOME</a></li>
    <?php if ($menuPages) { ?>
        <?php foreach ($menuPages As $menuObj) { ?>
            <li><a href="<?php print(SITE_BASE_URL); ?>page/<?php print($menuObj->id); ?>/<?php print($objMenuPages->formateUrlString($menuObj->name)); ?>.html"><?php print($menuObj->name); ?></a></li>
        <?php } ?>
    <?php } ?>

</ul>