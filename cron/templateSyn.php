<?php
$localDir  = "/home/theendha/public_html/demo/admin/templates/tpl/";
$remoteFileLoc = "https://secure.pluspro.com/globaladmin2/tpl/";

$arrFileNames = array('footer.tpl','header.tpl','dashboard.tpl','login_layout.tpl','lost_password_layout.tpl','side_menu.tpl','settings.tpl','pages/form.tpl','pages/index.tpl','pages/sec_sub_pages.tpl','pages/sub_pages.tpl');


define('LOCAL_DIR',$localDir);
define('REMOTE_FILE_LOC',$remoteFileLoc);


foreach($arrFileNames As $fIndex=>$fileName){
    
    $remoteFileUrl = REMOTE_FILE_LOC.$fileName;
    print($fileName);
    print("\n");
    $remoteFileSize = getRemoteFileSize($remoteFileUrl);
    print("Remote File Size : ".$remoteFileSize);
    print("\n");
    print("\n");
    
    $localFilePath = LOCAL_DIR.$fileName;
    //print($localFilePath);
    print("\n");
    $downloadFile = false;
    if (file_exists($localFilePath)) {
        $localFileSize = filesize($localFilePath);
        
        if($localFileSize == $remoteFileSize){
            $downloadFile = false;
        } else {
            $downloadFile = true;
        }
        
        echo "The file $fileName exists";
        print("\n");
        echo "The file size : ".$localFileSize;
    } else {
       $downloadFile = true;
    }
    
    
    if($downloadFile == true){
        $fileDownloaded = downloadFile($remoteFileUrl, $localFilePath);
        
        if($fileDownloaded){
            print("The file ".$fileName." downloaded");
        }
        
    }
    
     print("\n");
    
    
    
    
    
}


function downloadFile($remoteFileUrl, $localFileSource){
    set_time_limit(0);
    $fp = fopen ($localFileSource, 'w+');//This is the file where we save the    information
    $ch = curl_init($remoteFileUrl);//Here is the file we are downloading
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_FILE, $fp); // here it sais to curl to just save it
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp); 
    return true;
}


function getRemoteFileSize($remoteFile){
    $ch = curl_init($remoteFile);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //not necessary unless the file redirects (like the PHP example we're using here)
    $data = curl_exec($ch);
    curl_close($ch);
    if ($data === false) {
    echo 'cURL failed';
    exit;
    }

    $contentLength = 'unknown';
    $status = 'unknown';
    if (preg_match('/^HTTP\/1\.[01] (\d\d\d)/', $data, $matches)) {
    $status = (int)$matches[1];
    }
    if (preg_match('/Content-Length: (\d+)/', $data, $matches)) {
    $contentLength = (int)$matches[1];
    }

    return $contentLength;
}
?>
