<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');


$member_id  =   "";
$firstname  =   "";
$lastname   =   "";
$login      =   "";
$passwd     =   "";
$isadmin    =   "";
        
$action = "";
$usersResult  = "";

$objUser = new User();

if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit') {

        if (isset($_GET['userId'])) {

            $userId = $_GET['userId'];

            $userInfo = $objUser->getUser($userId);


            $member_id  =   $userInfo->member_id;
            $firstname  =   $userInfo->firstname;
            $lastname   =   $userInfo->lastname;
            $login      =   $userInfo->login;
            $passwd     =   $userInfo->passwd;
            $isadmin    =   $userInfo->isadmin;
        }
    } elseif ($action == 'delete') {
         $userId = $_GET['userId'];
        $objUserData = new stdClass();
        $objUserData->member_id = $userId;
        $isRecordDeleted = $objUser->deleteUser($objUserData);
        header('Location: users.html');
    
    }
    
    
    
} else {
    $usersResult = $objUser->getAll();
}


if ($_POST) {

    $action = $_POST['txtAction'];

    $member_id = ($action == 'add') ? "" : $_POST['txtId'];
    $firstname  =   $_POST['txtFirstName'];
    $lastname   =   $_POST['txtLastName'];
    $login      =   $_POST['txtEmail'];
    $passwd     =   $_POST['password'];
    $isadmin    =   $_POST['cmbIsAdmin'];
    
    $objUserData = new stdClass();
    $objUserData->member_id = $member_id;
    $objUserData->firstname = $firstname;
    $objUserData->lastname = $lastname;
    $objUserData->login = $login;
    if($passwd){
        $objUserData->passwd = md5($passwd);
    }
    $objUserData->isadmin = $isadmin;
    

    if ($action == 'add') {
        $lastInsertedId = $objUser->addUser($objUserData);
    } else if ($action == 'edit') {
        $isRecordUpdated = $objUser->editUser($objUserData);
    }

    $newPageUrl = "users.html?action=add";
    $mainPageUrl = "users.html";

    if ($lastInsertedId) {

	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("User <span class='red'>$firstname</span> has been added!<br /><br /><br />Want to add another user now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
	/*
        echo "<div id='coverit'></div><div id='message'>User <span class='red'>$firstname</span> has been added!<br /><br /><br />Want to add another user now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	*/

    } else if ($isRecordUpdated) {

	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("User <span class='red'>$firstname</span> has been updated!<br /><br /><br />Want to add another user now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
	/*
        echo "<div id='coverit'></div><div id='message'>User <span class='red'>$firstname</span> has been updated!<br /><br /><br />Want to add another user now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	*/
	
    }
}


$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";

if($action == 'add' || $action == 'edit'){
    $CONTENT = ADMIN_LAYOUT_PATH."tpl/user/form.tpl";
} else {
    $CONTENT = ADMIN_LAYOUT_PATH."tpl/user/index.tpl";
}
	
require_once $LAYOUT;
?>
