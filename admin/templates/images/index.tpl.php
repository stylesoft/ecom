<?php error_reporting(E_ALL);
ini_set('display_errors', 1);?>
<div id="search_main_wrapper">
<h2>Upload/Manage Images for <?php print($contentTitle);?></h2>
</div>
<span style="color:red;"><b>Note :</b> Please upload images greater than or equal to 600px in width and greater than or equal to 600px in height</span>
 <div id="table_main_wrapper">



        <div id="dashboard" style="background-color:#FFF;">
            <div>
            <form  action="" method="post">
            <div id="two">
            <fieldset style="margin-top:35px;">

                <div id="add_images_content" style="width: 810px;">

<link href="<?php print(ADMIN_BASE_URL);?>templates/contents/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css" rel="stylesheet" type="text/css" />
<!-- Load plupload and all it's runtimes and finally the jQuery queue widget -->
<script type="text/javascript" src="<?php print(ADMIN_BASE_URL);?>templates/contents/plupload/plupload.full.js"></script>
<script type="text/javascript" src="<?php print(ADMIN_BASE_URL);?>templates/contents/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="<?php print($_JS_LIBS_PATH);?>dragdrop/images.js"></script>


<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
$(function() {
	// Setup flash version
	$("#flash_uploader").pluploadQueue({
		// General settings
		runtimes : 'flash',
		url : '<?php print(ADMIN_BASE_URL);?>includes/imageUpload.php?m=<?php print($module);?>&c=<?php print($controller);?>&id=<?php print($objectId);?>',
		max_file_size : '10mb',
		unique_names : true,
		multiple_queues : true,
		preinit : attachCallbacks,

		// Resize images on clientside if we can
		resize : {quality : 90},

		// Specify what files to browse for
		filters : [
			{title : "Image files", extensions : "jpg,gif,png"}
			
		],

		// Flash settings
		flash_swf_url : '<?php print(ADMIN_BASE_URL);?>templates/contents/plupload/plupload.flash.swf'
	});
  });
function attachCallbacks(Uploader) {

Uploader.bind('FileUploaded', function(Up, File, Response) {

  if( (Uploader.total.uploaded + 1) == Uploader.files.length)
  {
    window.location.reload();
  }
})};


</script>


<div id="flash_uploader">You browser doesn't have Adobe Flash installed.</div>

 <div id="carcontentTop">
                <div  style="margin-left: 10px;">
                    <div id="contentTop">
Drag the images into the order that you want them to appear on your website <br> To delete a page drag it over the waste basket.

</div>
                </div>
            </div>


<div id="carcontentTop">
<div id="carcontentLeft"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/placehere.jpg" width="120" height="120" border="0" /></div>

<div id="carcontentRight">Instructions</div>
</div>
<div style="clear:both"></div>

<div id="carcontentWrap">

    
<div id="carimagesDiv">
    <ul id="carnamelist">

<?php 
foreach($imageRec As $rIndex=>$imageObj){?>
 <li id="recordsArray_<?php print($imageObj->recordId);?>">
 		<img src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($imageObj->recordText);?>&width=120&height=120" />
 </li>
<?php } ?>
 
</ul>
</div>



</div>

<div id="cardeleteArea"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/trash_basket.jpg" width="120" height="120" /></div>
   



</div>
                
                 </fieldset>
                </div>
                
                <div style="width:500px; margin-top:40px;"><div style="width:220px; float:left;">
<a href="<?php print(ADMIN_BASE_URL.$backUrl);?>" id="testbutton"><<< Back </a>
</div></div> 
                
                
            </form>
            </div>
            <div class="clear"></div>
        </div>
        <div style="height:10px;"></div>
        
        
        
    </div>