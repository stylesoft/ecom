// JavaScript Document

// gets car models from db based on car makes dropdown selection
function populatemodelName() {
    $.getJSON('modules/car_sales/getcarmodels.php', {makeName:$('#makeName').val()}, function(data) {

        var select = $('#modelName');
        var options = select.attr('options');
        $('option', select).remove();

        $.each(data, function(index, array) {
            options[options.length] = new Option(array['MODEL']);
			
        });
$(select).append($('<option />').val('other').html('Other'));
    });

}

$(document).ready(function() {
	
	//populatemodelName();
	$('#makeName').change(function() {
		populatemodelName();
	});
	
});

<!--
// disables text box
function disable_text(status)
{
status=status;	
document.two.price.disabled = status;
}
//-->

	tinyMCE.init({
		// General options
		
		 mode : "exact",
		theme : "advanced",
		elements : "description",
		plugins : "paste",

		// Theme options
		        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,undo,redo,separator,bullist,numlist,separator,pastetext,pasteword,selectall,formatselect",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
		theme_advanced_blockformats : "p,h1,h2",
	
		
	});