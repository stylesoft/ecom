<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php print(SITE_NAME); ?> - Content Management</title>
<link rel="shortcut icon" href="/favicon.ico" />
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jquery/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/pluspro.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jNice.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jquery/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jquery/jquery-1.8.2-ui.min.js"></script>
<script type="text/javascript" 	src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/jquery/ajax/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/easing.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/collapse.js"></script>
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/stylev4.css" rel="stylesheet" type="text/css" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/jNice.css" rel="stylesheet" type="text/css" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/main.css" rel="stylesheet" type="text/css" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/forms.css" rel="stylesheet" type="text/css" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/dragdrop.css" rel="stylesheet" type="text/css" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/order_details.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript">
	$(document).ready(
		function()
		{
			$('#sideNav').jqcollapse({
				   slide: true,
				   speed: 1000,
				   easing: 'easeOutCubic'
			});
		}
	);
	</script>						

</head>

<body>


<div id="wrapper">

<?php
/*
$ch = curl_init();
// set the url to fetch
curl_setopt($ch, CURLOPT_URL, 'https://secure.pluspro.com/globaladmin2/tpl/header.tpl');
curl_setopt($ch, CURLOPT_HEADER, 0);
$head = curl_exec($ch);
$header = eval($head);
curl_close($ch);
echo $header;
*/  
 ?>
<?php include('tpl/header.tpl'); ?>     
    
    
    <div class="clear"></div>
    
       <div id="content_wrapper">
		<div id="page_path"></div> 
    
 
 
 
 <!--- start side_bar ---->
<?php include('tpl/side_menu.tpl'); ?>
<!-- end side_bar --->
 
 
 <!--  contents area  -->
 
<div id="mid_content_wrapper">
        
       <?php include($CONTENT); ?>   
          	
</div>

<!--  ends contents area -->


<div class="clear"></div> 
</div></div>


</div>
<div class="clear"></div> 
</div>

<?php
/*
$ch = curl_init();
// set the url to fetch
curl_setopt($ch, CURLOPT_URL, 'https://secure.pluspro.com/globaladmin2/tpl/footer.tpl');
curl_setopt($ch, CURLOPT_HEADER, 0);
$content = curl_exec($ch);
include ($content);
curl_close($ch);
*/
 ?>
<?php include('tpl/footer.tpl'); ?>  

</body>
</html>
