<?php if($_SESSION['SESS_IS_ADMIN'] == 1){?>
<div id="search_main_wrapper">

            <h2>The User Details</h2>     
</div>
          
          <div id="table_main_wrapper">
       	  <div class="table_wrapper2" id="side_panel_box_5_content">
          	<div id="dashboard"></div>
          </div>
        
          <div class="table_wrapper2" id="side_panel_box_5_content">
          		<div id="dashboard">
          	<table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody><tr>
                <td align="center">
                
                <div style="margin-top:45px;"><a href="?action=add&pageType=mainPage" id="testbutton">Add a New User</a></div>
                
                <div style="clear:both;"></div>
                
                <div id="tablecontents"> 
                    
                    
                    
                   <table id="users">
    <thead> 
		<tr height="12px">
		  <td  width="118">First Name</td>
        <td  width="118">Last Name</td>
        <td width="305">Email</td>
        <td width="72">Admin</td>
        <td width="172">&nbsp;</td>
       
        </tr>
        </thead> 

        <tbody> 
	
        </tbody>
        
        
        <?php if($usersResult){?>
        <?php foreach($usersResult As $uIndex=>$user){?>
        
		<tr <?php if($uIndex%2 == 0){?> class="odd" <?php } ?> id="<?php print($user->member_id); ?>">
	    <td  ><?php print($user->firstname); ?></td>
            <td><?php print($user->lastname); ?></td>
            <td><?php print($user->login); ?></td>
	    <td><?php if($user->isadmin == 1){?> Yes <?php } else {?> No <?php } ?></td>
	    <td class="action">
                 <a href="users.html?action=edit&userId=<?php print($user->member_id); ?>" class="edit">Edit</a>
                 <a href="users.html?action=delete&userId=<?php print($user->member_id); ?>" class="delete">Delete</a>
            </td>
	   
                
         <?php } ?>
           <?php } ?>      
       
		
		
	</table>
                
                
                
                
                
                </div>
        


                    
                  
	
                
                </td>
              </tr>
            </tbody></table>
			</div>
		  </div>
           <div style="height:10px;"></div>
</div>
<?php } else {?>
<div id="search_main_wrapper">

            <h2>You are not authorized to view this page</h2>     
</div>

<div id="table_main_wrapper" style="margin-top:45px;">
          <div class="table_wrapper2" id="side_panel_box_5_content">
          		<div id="dashboard">
          	Please contact the administrator for more help. 
			</div>
		  </div>
           <div style="height:10px;"></div>
</div>
<?php } ?>

            
          