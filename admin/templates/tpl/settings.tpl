<script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImgLogo');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImgLogo").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImgLogo").attr("src",new_image);
	                                        $("#txtSmallFileNameLogo").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       
                        
		}
	);
	</script>
        
        
        <script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImgFavIcon');
			new AjaxUpload(btnUpload, {
				action: 'uploadFavFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|ico)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImgFavIcon").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>"+response;
	                                        $("#btnImgFavIcon").attr("src",new_image);
	                                        $("#txtSmallFileNameIcon").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       
                        
		}
                
                 
	);
	</script>
        
        
        
         <script type="text/javascript">
	$(document).ready(function(){
	
                $('.slidingDiv').hide();
                $('.show_hide').click(function(){
                    $('#slidingDiv').slideToggle();
                });
        });
	</script>



<?php if($_SESSION['SESS_IS_ADMIN'] == 1){?>
<div id="search_main_wrapper">


		
			<h2>Edit Site Settings</h2>
	<?php 
$selected = SITE_DEFAULT_CURRENCY_ID;
$objCurrency = new Currency();
$objCurrency->tb_name = 'tbl_currency';
$objCurrency->status = 'Enabled';
$currencyResult = $objCurrency->search();
?>		

	
</div>


<div style="clear:both;"></div>

<div id="table_main_wrapper">



	<div id="dashboard" style="background-color: #FFF;">


		<form action="" method="post">
			
<div>
<div  id="two">
    
     <h2></h2>
     <div>
				<fieldset>

				


					<div id="separator">
						<label for="site_name">Site Name : </label> <input
							name="site_name" type="text" id="site_name"
                                                        value="<?php print($websiteName); ?>" size="64" maxlength="64" tabindex="1" />
					</div>
					<div id="separator">
						<label for="site_url">Admin URL: </label> <input name="site_url"
							type="text" id="site_url" value="<?php print($websiteAdminBaseUrl); ?>" size="64"
							maxlength="64" tabindex="2" />
					</div>
					<div id="separator">
						<label for="base_url">Site URL: </label> <input name="base_url"
							type="text" id="base_url" value="<?php print($websiteBaseUrl); ?>"
							size="64" maxlength="64" tabindex="3" />
					</div>
					<div id="separator">
						<label for="site_email">Site Email : </label> <input
							name="site_email" type="text" id="site_email"
							value="<?php print($websiteEmail); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
					<div id="separator">
						<label for="site_template">Template : </label> <select
							name="site_template" id="site_template" tabindex="5">
							<?php
							$curr_dir = getcwd();
							echo $curr_dir;
							foreach(glob('../includes/templates/*', GLOB_ONLYDIR) as $dir) {
								$dir = str_replace('../includes/templates/', '', $dir);
								$isSelected = "";
								if($websiteTemplate == $dir){
									$isSelected = " selected='selected' ";	
								}
								echo "<option value=".$dir." ".$isSelected.">".$dir."</option>";
							}
							?>

						</select>
					</div>
					
					
						<div id="separator">
						<label for="site_template1">Select Defult Currency : </label> <select
							name="site_currency" id="site_currency" tabindex="5">
							<?php
                             foreach ($currencyResult as $value) {
							?>
                           <option value="<?php echo $value->id; ?>" <?php if ($selected == $value->id) {?> selected="selected" <?php }?>> <?php  echo $value->code; ?> </option>
                           <?php }?>
						</select>
					</div>
                                        <div id="separator" style="display: none;">
						<label for="sidebox">Sidebars : </label> <select name="sidebox"
							id="sidebox" tabindex="6">
							<option value="0" <?php if($websiteSideBox == '0'){?> selected="selected" <?php } ?>>None</option>
							<option value="1" <?php if($websiteSideBox == '1'){?> selected="selected" <?php } ?>>Left</option>
							<option value="2" <?php if($websiteSideBox == '2'){?> selected="selected" <?php } ?>>Right</option>
						</select>
					</div>
                                         <div id="separator">
						<label for="site_name">Logo : </label> 
                                                <img src="<?php print($siteLogoImage);?>" height="50" width="50" name="btnImgLogo" id="btnImgLogo"/>
                                                <input type="hidden" id="txtSmallFileNameLogo" name="txtSmallFileNameLogo" value="<?php print($siteLogo);?>" />
                                                <span style="color:red;font-size: 10px;">Clcik on image to change</span>
					</div>
                                         <div id="separator">
						<label for="site_name">Fav Icon : </label> 
                                                <img src="<?php print($siteFavIconImage);?>" height="50" width="50" name="btnImgFavIcon" id="btnImgFavIcon"/>
                                                <input type="hidden" id="txtSmallFileNameIcon" name="txtSmallFileNameIcon" value="<?php print($siteFavIcon);?>" />
                                                <span style="color:red;font-size: 10px;">Clcik on image to change</span>
					</div>
				</fieldset>
         </div>
				<p>
                                    
                                    
                                <h2>Advanced ( <span class="show_hide">Click to display</span>)</h2>         
                                <div id="slidingDiv" style="display: none;">     
                        

                                
                                <h2>NewsFeed API's</h2>
                <div>   
                    <fieldset>

                       <h2>Facebook</h2>

                           <div id="separator">

                            <label for="pagetitle">App ID : </label>
                            <input name="txtFacebookAppId" type="text" id="txtFacebookAppId" tabindex="7" size="64" style="width:600px;" value="<?php print($facebook_app_id);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Secret key : </label> 
                            <input name="txtFacebookSecretkey" type="text" id="txtFacebookSecretkey" tabindex="8" style="width:600px;" value="<?php print($facebook_secret_key);?>"/>
                        </div>


 <h2>Twitter</h2>
                        <div id="separator">

                            <label for="pagetitle">Consumer key : </label>
                            <input name="txtTwitterConsumerkey" type="text" id="txtTwitterConsumerkey" tabindex="9" size="64" style="width:600px;" value="<?php print($consumer_key);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Consumer secret : </label> 
                            <input name="txtTwitterConsumersecret" type="text" id="txtTwitterConsumersecret" tabindex="10" style="width:600px;" value="<?php print($consumer_secret);?>"/>
                        </div>

<div id="separator">

                            <label for="pagetitle">App Access token : </label>
                            <input name="txtTwitterAppAccesstoken" type="text" id="txtTwitterAppAccesstoken" tabindex="9" size="64" style="width:600px;" value="<?php print($user_token);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">App token secret : </label> 
                            <input name="txtTwitterAppAccesstokensecret" type="text" id="txtTwitterAppAccesstokensecret" tabindex="10" style="width:600px;" value="<?php print($user_secret);?>"/>
                        </div>


                         

                    </fieldset>



                </div>
                                
                                
                       <h2>XML Feeds</h2>        
                        <div>
                           
                            <fieldset>
                            
                            
                            <div id="separator">

                            <label for="pagetitle">Feed XML : </label>
                            <input name="txtTermsConditions" type="text" id="txtTermsConditions" tabindex="7" size="64" style="width:600px;" value="<?php print($terms_conditions);?>"/>
                            </div>
                                
                                
                            <div id="separator">

                            <label for="pagetitle">Sitemap XML : </label>
                            <input name="txtPrivacyPolicy" type="text" id="txtPrivacyPolicy" tabindex="7" size="64" style="width:600px;" value="<?php print($privacy_policy);?>"/>
                            </div>
                            
                            
                            </fieldset>
                        
                        
                        </div>            
                                
                                
                                
                                
                                
                                </div>
                                    
</div>
					  <input class="testbutton"  id="testbutton" type="submit" value="submit" name="Submit"  onClick="sendRequest()"/> 
                                          <input type="hidden" name="txthomeBannerHeight" id="txthomeBannerHeight" value="<?php print($homeBannerHeight);?>" />
                                          <input type="hidden" name="txthomeBannerWidth" id="txthomeBannerWidth" value="<?php print($homeBannerWidth);?>" />
                                          <input type="hidden" name="txtMobileSite" id="txtMobileSite" value="<?php print($mobileSite);?>" />
                                          
  <input id="resetbutton" type="reset"  />
<div style="clear:both;margin-top:10px;"></div>
				</p>
				<div id="show"></div>
		</div>
		</form>

	</div>








</div>
<?php } else {?>
<div id="search_main_wrapper">

            <h2>You are not authorized to view this page</h2>     
</div>

<div id="table_main_wrapper" style="margin-top:45px;">
          <div class="table_wrapper2" id="side_panel_box_5_content">
          		<div id="dashboard">
          	Please contact the administrator for more help. 
			</div>
		  </div>
           <div style="height:10px;"></div>
</div>
<?php } ?>
