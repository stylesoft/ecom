<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "";
$order          = "";
$orderStr       = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            // get the search query....
            if(isset($_GET['q'])){
                $searchQ   = $_GET['q'];
            }
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }
            
            if(isset($_GET['order'])){
                $order = $_GET['order'];
            }
            
            if($orderBy != '' && $order != ''){
            	$orderStr       = $orderBy." ".$order;
            } else {
            
            	$orderStr       = 'id'." ".'Asc';
            }

          
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}



//--------------------------------------------

$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;

$objSubcribe = new PaymentMethod();
$objSubcribe->tb_name = 'tbl_paymentmethod';

$objSubcribe->searchStr = $searchQ;
$objSubcribe->limit = $recLimit;
$objSubcribe->listingOrder = $orderStr;
$subcribeResult = $objSubcribe->search();
//print_r($subcribeResult);

//--------------------------------------------

?>

	<script type="text/javascript">
    $(document).ready(function(){ 	
        function slideout(){
            setTimeout(function(){
                $("#response").slideUp("slow", function () {
                });
    
            }, 2000);}
	
        $("#response").hide();
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
                    var order = $(this).sortable("serialize") + '&update=update'; 
                    $.post("<?php print(ADMIN_BASE_URL); ?>templates/tpl/payment/updateListingOrder.php", order, function(theResponse){
                        $("#response").html(theResponse);
                        $("#response").slideDown('slow');
                        slideout();
                    }); 															 
                }								  
            });
        });

    });	

    function deleterow(id){
        if (confirm('Are you sure want to delete?')) {
            $.post('<?php print(ADMIN_BASE_URL); ?>templates/tpl/payment/deletepayment.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                return false;
                //$(".message").delay(2000).fadeOut(1000);
            });
        }
    }

</script>




<div id="list">
    <div id="table_main_div" >
        <div id="response"> </div>
        <div class="row_heding">

            <div class="colum" style="width:100px;">
                <strong>ID</strong>
            	<a href="<?php print(ADMIN_BASE_URL);?>paymentmethod.html?q=<?php print($searchQ);?>&orderby=id&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?>paymentmethod.html?q=<?php print($searchQ);?>&orderby=id&order=asc">&darr;</a>
            </div>
            
                 <div class="colum" style="width:330px;">
                <strong> Payment method Name</strong>
        <a href="<?php print(ADMIN_BASE_URL);?>paymentmethod.html?q=<?php print($searchQ);?>&orderby=name&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?>paymentmethod.html?q=<?php print($searchQ);?>&orderby=name&order=asc">&darr;</a> 
            </div>
            
            <div class="colum" style="width:115px;">
                <strong>Status</strong>
                
            </div>

            <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
        </div>
        <div class="clear"></div>

        <ul>
          <?php  if(count($subcribeResult) >0){
      foreach($subcribeResult As $rowIndex=>$subcribeData) {?> 
                    <li id="arrayorder_<?php print($subcribeData->id); ?>">
                        <?php if ($rowIndex % 2 != 0) { ?>
                            <div class="row1">
                            <?php } else { ?>
                                <div class="row2">
                                <?php } ?>
                                <div class="colum" style="width:100px;"><?php print($subcribeData->id); ?></div>
                                <div class="colum" style="width:330px;"><?php print($subcribeData->name); ?></div>
                                <div class="colum" style="width:115px;"><?php print($subcribeData->status);?></div>
                                <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">

                                    <a href="<?php print(ADMIN_BASE_URL);?>paymentmethod.html?action=edit&id=<?php print($subcribeData->id); ?>">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                    <a href="#" onclick="deleterow(<?php echo $subcribeData->id; ?>);">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                    </li>

                <?php }
            } else {
                ?>
                <li>
                    <div id="listings" style="background:#eee;">No Matching Records Found </div>
                </li>
<?php } ?>

        </ul>



    </div>

</div>