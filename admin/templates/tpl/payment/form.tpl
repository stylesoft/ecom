<script type="text/javascript">
	
    $(document).ready(function(){
		
        //Display Loading Image
        function Display_Load()
        {
            $("#manageloading").fadeIn(900,0);
            $("#manageloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        //Hide Loading Image
        function Hide_Load()
        {
            $("#manageloading").fadeOut('slow');
        };
	
        //Default Starting Page Results
   
        $("#pagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
	
        Display_Load();
	
        $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>templates/tpl/payment/pagination_data.php?q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=paymentmethod", Hide_Load());

        //Pagination Click
        $("#pagination li").click(function(){
			
            Display_Load();
		
            //CSS Styles
            $("#pagination li")
            .css({'border' : 'solid #dddddd 1px'})
            .css({'color' : '#0063DC'});
		
            $(this)
            .css({'color' : '#FF0084'})
            .css({'border' : 'none'});

            //Loading Data
            var pageNum = this.id;
		
            $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>templates/tpl/payment/pagination_data.php?page=" + pageNum+"&m=paymentmethod&q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($_REC_PER_PAGE); ?>", Hide_Load());
        });

    });
</script>



<div id="table_main_wrapper">

<div id="search_main_wrapper"  style="margin-bottom:55px;">
            <h2>Add Edit Site Payment methods Details</h2>     
</div>

    <div id="dashboard" style="background-color: #FFF;">

            <form name="form1" id="form1" action="" method="post">
            
                     <div id="two">
                         <fieldset style="margin-top: 20px;">
                   
                    
                    <div id="separator">
                        <label for="headline">Payment  Name: </label>
                        <input name="name" type="text" id="pagename" value="<?php echo $name; ?>" size="65"  />
                    </div>
                   
                    <div id="separator">
                        <label for="headline">Payment Method Code: </label>
                        <input name="code" type="text" id="contact" value="<?php echo $code; ?>" size="65" />
                    </div>
                    
                     
                    
                 
                    
                    <div id="separator">
                        <label for="pagename">Status: </label>

                        <select name="status" id="status" style="width: 200px;">
                            <option value="">--Select--</option>
                            <option value="Enabled" <?php if ($status == 'Enabled') { ?> selected="selected" <?php } ?>>Enabled</option>
                            <option value="Disabled" <?php if ($status == 'Disabled') { ?> selected="selected" <?php } ?>>Disabled</option>
                        </select>
                    </div>
                   
                </fieldset>
                     </div>
                <p>
					<input type="hidden" name="txtAction" id="txtAction" value="<?php if($action ==''){echo "add";}else{ print($action);}?>"/>
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                    </p>

            </form>

     <br>
 <br> <br> <br> <br> <br> <br> <br>
  <br> <br> <br> <br> <br> <br> <br>
   <br> <br> <br> <br> <br> <br> <br>
    <div class="table_wrapper2" id="side_panel_box_5_content">

            <table border="0" cellpadding="0" cellspacing="0" width="100%">

                <tbody>
<!--  <div id="search_main_wrapper">
          <h2> Payment Methods Listing</h2>     
</div>-->
                    <tr>
                          <td align="center">
                           <div style="margin-top:45px;"></div>
  
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <form action="" method="get" name="form1" id="search">
                                <div align="center"><label for="searchtext">Search:</label>
                                    <input type="text" name="q" id="q" />
                                    
                                    <input type="submit" id="searchbutt" value="" name="search" />
                                </div>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:5px">	
                                <table width="750px">
                                    <tr>
                                        <td width="30">Pages:</td>
                                        <td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td>
                                        <td width="90">Rows per page :</td>
                                        <td width="30">
                                            <select onchange="javascript:goToPage(options[selectedIndex].value)" id="cmbRowsPerPage" name="cmbRowsPerPage" style="width: 50px;">
                                                <option value=""></option>

                                                <?php for ($row_num = 10; $row_num <= 200; $row_num = $row_num + 10) { ?>
                                                    <option value="paymentmethod.html?q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($row_num); ?>" <?php if($_GET['rows']==$row_num)echo "selected=selected"?>><?php print($row_num); ?></option>
                                                <?php } ?>
                                            </select> 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">




                            <div id="manageloading" ></div>
                            <div id="managecontent" ></div>




                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:40px">	<table width="800px">
                                    <tr><td width="30">Pages:</td><td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td></tr></table></div>

                        </td>
                    </tr>

                </tbody></table>
        
    </div>

   

</div>



</div>



