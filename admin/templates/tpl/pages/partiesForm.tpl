<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/pluspro_validation/pages.js"></script>


<script type="text/javascript">
	$(document).ready(
		function()
		{
                        <?php for($uploaderX=0;$uploaderX<=5; $uploaderX++){?>
                        // the small image button
			var btnUpload=$('#btnImg<?php print($uploaderX);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg<?php print($uploaderX);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg<?php print($uploaderX);?>").attr("src",new_image);
	                                        $("#txtSmallFileName<?php print($uploaderX);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                        <?php } ?>
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper"  style="margin-bottom:55px;">
            <?php if($action == 'add'){?>
           <h2>Add New Page</h2> 
            <?php } else {?>
            <h2>Update Page</h2> 
            <?php } ?>
</div>

         <div id="error">
            Please fill in all the required fields and re-submit the form .<br>
        </div>

<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">
        <div>
            <form  action="" method="post" name="pageForm" id="pageForm">

<div id="two">
                
               

                <div  <?php if($pageType == 'homePage'){?> style="display: none;" <?php } ?>>
                    <fieldset>

                        
                        <div id="separator">
                            <label for="pagename">Page Name : </label> 
                            <input name="pagename" type="text" id="pagename" tabindex="1" size="30" maxlength="30" value="<?php print($NAME);?>"/>
                        </div>




                     <div id="separator">
                            <label for="status">Page Status : </label> 
                            <select name="status" id="status" tabindex="2">
                                <option value="Published" <?php if($LIVE == 'Published'){?> selected="selected" <?php } ?>>Published</option>
                                <option value="Exclude_from_menu" <?php if($LIVE == 'Exclude_from_menu'){?> selected="selected" <?php } ?>>Exclude from menu</option>
                                <option value="Draft" <?php if($LIVE == 'Draft'){?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                      </div>



                        <div id="separator" style="display: none;">
                            <label for="contact">Add Contact: </label> 
                            <select name="contact" id="contact" tabindex="3">
                                <option value="Yes" <?php if($ISCONTACT == 'Yes'){?> selected="selected" <?php } ?>>Yes</option>
                                <option value="No" <?php if($ISCONTACT == 'No'){?> selected="selected" <?php } ?>>No</option>
                            </select>
                            <span>Select Yes if this page is a contact us page.</span>
                            
                        </div>
                    </fieldset>
                </div>
               

<h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                           <div id="separator">

                            <label for="pagetitle">Page Title : </label>
                            <input name="pagetitle" type="text" id="pagetitle" tabindex="4" size="64" style="width:600px;" value="<?php print($TITLE);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Keywords : </label> 
                            <input name="keywords" type="text" id="keywords" tabindex="5" style="width:600px;" value="<?php print($KEYWORDS);?>"/>
                        </div>




                          <div id="separator">


                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description" tabindex="6"><?php print($DESCRIPTION);?></textarea>
                        </div>  

                    </fieldset>



                </div>









<div style="display: none;">
  <h2>Page Content</h2>

                <fieldset>
                 
                    <div id="separator">
                        <textarea name="mainbody"  id="mainbody" tabindex="7"><?php print($BODY);?></textarea>
                    </div>
                </fieldset>
</div>




<h2>Top Contents</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($x = 0;$x<=3;$x++){
                 $homePageColumnsContent = "";
                 $homePageColumnsContent = $topContentsDetails[$x];
                 $contentImage = "";
                 if($homePageColumnsContent){
                    if($homePageColumnsContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$homePageColumnsContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
         
                <div id="separator" style="display: none;">
                
                    <label for="lcol_title">Staff Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg<?php print($x);?>" id="btnImg<?php print($x);?>"/>
                    <input type="hidden" id="txtSmallFileName<?php print($x);?>" name="txtSmallFileName[]" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentMainImage);?>" <?php } ?> />
                    
                </div>
                
                <div id="separator">

                            <label for="lcol_title"><?php if($x==0){?> Top Content Heading <?php } elseif($x==1){ ?> Left Box Heading : <?php } elseif($x==2){ ?>  Middle Box Heading <?php } else {?> Right Box Heading  <?php } ?></label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TOP_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->title);?>" <?php } ?>/>
                        </div>


                        <div id="separator" style="display: none;">


                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentLink);?>" <?php } ?>/>
                        </div> 


                          <div id="separator" <?php if($x==0){?>  style="display: none;" <?php } ?>>


                            <label for="lcol_description" style="display: none;">The Contents : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="description<?php print($x);?>" class="ckeditor"><?php if($homePageColumnsContent){?> <?php print($homePageColumnsContent->contents);?> <?php } ?></textarea>
                        </div>  
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div>   
                        
                        
                        
         
                        
                              <h2>Bottom Box Contents</h2>      
        <div>
        
        
        <fieldset>
        
       




                          
                          
                           
                            <?php for($t = 0;$t<=3;$t++){
                                
                                $testamonialContent = "";
                                $testamonialContent = $testamonials[$t];
                                ?>
                                
                               
                                
                            <div id="separator">

                            <label for="left_test_title"> <?php if($t==0){?> Bottom Content Heading <?php } elseif($t==1){ ?> Left Box Heading : <?php } elseif($t==2){ ?>  Middle Box Heading <?php } else {?> Right Box Heading  <?php } ?> </label> 
                            <input name="pageContentsTitle[]" type="text" id="left_test_title" tabindex="3" style="width:600px;" <?php if($testamonialContent){?> value="<?php print($testamonialContent->title);?>" <?php } ?> />
                            
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($testamonialContent){?> value="<?php print($testamonialContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TESTAMONIAL"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($t);?>"/>
                            <input type="hidden" id="txtSmallFileName<?php print($t);?>" name="txtSmallFileName[]" <?php if($testamonialContent){?> value="<?php print($testamonialContent->contentMainImage);?>" <?php } ?> />
                            
                            </div>
                            
                            
                            <div id="separator" style="display: none;">


                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($testamonialContent){?> value="<?php print($testamonialContent->contentLink);?>" <?php } ?>/>
                        </div> 

            
                            <div id="separator" <?php if($t == 0){?> style="display: none;" <?php } ?>>

                            <label for="left_test_description" style="display: none;">The Contents : </label> 
                                <textarea name="pageContents[]" style="width:600px; height:80px;" id="left_test_description" class="ckeditor">
<?php if($testamonialContent){?> <?php print($testamonialContent->contents);?> <?php } ?>
                            </textarea>
                            </div>  
    
                            <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                            <?php } ?>   
        
                        


                                
                        
 
                        
       
        
        
        </fieldset>  
                
                
   </div> 
                        
                        
                        
                        
  
  
  
</div>
                    <input id="testbutton" class="testbutton" type="submit" value="submit" name="Submit" tabindex="8"/> 
                    <input id="resetbutton" type="reset" tabindex="9" />
                    <input type="hidden" name="txtPageId" id="txtPageId" value="<?php print($pageId);?>" />
                    <input type="hidden" name="txtParentpageId" id="txtParentpageId" value="<?php print($parentpageId);?>" />
                    <input type="hidden" name="txtSecParentpageId" id="txtSecParentpageId" value="<?php print($secParentpageId);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    <input type="hidden" name="txtPageType" id="txtPageType" value="<?php print($pageType);?>" />
                     <input type="hidden" name="txtListingId" id="txtListingId" value="<?php print($ListingID);?>" />
                      <input type="hidden" name="txtAllowToDelete" id="txtAllowToDelete" value="<?php print($ALLOWDELETE);?>" />
                </p>

                <div id="show"></div>
            </form>
        </div>
        <div class="clear">
        </div> 
    </div>
</div>
