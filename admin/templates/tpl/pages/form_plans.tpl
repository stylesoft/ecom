<link rel="stylesheet" href="<?php print(ADMIN_BASE_URL);?>templates/contents/js/redactor/css/redactor.css" />
	<script src="<?php print(ADMIN_BASE_URL);?>templates/contents/js/redactor/redactor.js"></script>

        
        <script type="text/javascript">
	$(document).ready(
		function()
		{
			$('#description0').redactor();
                        $('#description1').redactor();
                        $('#description2').redactor();
                         $('#description3').redactor();
                          $('#description4').redactor();
                           $('#mainbody').redactor();
		}
	);
	</script>



<script type="text/javascript">
	$(document).ready(
		function()
		{
                        <?php for($uploaderX=0;$uploaderX<=4; $uploaderX++){?>
                        // the small image button
			var btnUpload=$('#btnImg<?php print($uploaderX);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadContentFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg<?php print($uploaderX);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg<?php print($uploaderX);?>").attr("src",new_image);
	                                        $("#txtSmallFileName<?php print($uploaderX);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                        <?php } ?>
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper"  style="margin-bottom:55px;">
            <?php if($action == 'add'){?>
           <h2>Add New Page</h2> 
            <?php } else {?>
            <h2>Update Page</h2> 
            <?php } ?>
</div>

         <div id="error">
            Please fill in all the required fields and re-submit the form .<br>
        </div>

<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">
        <div>
            <form  action="" method="post" name="pageForm" id="pageForm">

<div id="two">
                
               

                <div  <?php if($pageType == 'homePage'){?> style="display: none;" <?php } ?>>
                    <fieldset>

                        
                        <div id="separator">
                            <label for="pagename">Page Name : </label> 
                            <input name="pagename" type="text" id="pagename" tabindex="1" size="30" maxlength="30" value="<?php print($NAME);?>"/>
                        </div>




                     <div id="separator">
                            <label for="status">Page Status : </label> 
                            <select name="status" id="status" tabindex="2">
                                <option value="Published" <?php if($LIVE == 'Published'){?> selected="selected" <?php } ?>>Published</option>
                                <option value="Exclude_from_menu" <?php if($LIVE == 'Exclude_from_menu'){?> selected="selected" <?php } ?>>Exclude from menu</option>
                                <option value="Draft" <?php if($LIVE == 'Draft'){?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                      </div>



                        <div id="separator" style="display: none;">
                            <label for="contact">Add Contact: </label> 
                            <select name="contact" id="contact" tabindex="3">
                                <option value="Yes" <?php if($ISCONTACT == 'Yes'){?> selected="selected" <?php } ?>>Yes</option>
                                <option value="No" <?php if($ISCONTACT == 'No'){?> selected="selected" <?php } ?>>No</option>
                            </select>
                            <span>Select Yes if this page is a contact us page.</span>
                            
                        </div>
                    </fieldset>
                </div>
               

<h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                           <div id="separator">

                            <label for="pagetitle">Page Title : </label>
                            <input name="pagetitle" type="text" id="pagetitle" tabindex="4" size="64" style="width:600px;" value="<?php print($TITLE);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Keywords : </label> 
                            <input name="keywords" type="text" id="keywords" tabindex="5" style="width:600px;" value="<?php print($KEYWORDS);?>"/>
                        </div>




                          <div id="separator">


                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description" tabindex="6"><?php print($DESCRIPTION);?></textarea>
                        </div>  

                    </fieldset>



                </div>






  <h2>Our Plans</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($x = 0;$x<=3;$x++){
                 $homePageColumnsContent = "";
                 $dataX = $x;
                 $homePageColumnsContent = $topContentsDetails[$dataX];
                 $contentImage = "";
                 if($homePageColumnsContent){
                    if($homePageColumnsContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$homePageColumnsContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
         
                <div id="separator">
                
                    <label for="lcol_title">Plan Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg<?php print($x);?>" id="btnImg<?php print($x);?>"/>
                    <input type="hidden" id="txtSmallFileName<?php print($x);?>" name="txtSmallFileName[]" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentMainImage);?>" <?php } ?> />
                    
                </div>
                
                    <div id="separator">

                            <label for="lcol_title">Plan Description : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TOP_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->title);?>" <?php } ?>/>
                        </div>


                        <div id="separator" style="display: none;" >


                            <label for="lcol_description">Pop-up Title : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentLink);?>" <?php } ?>/>
                        </div> 


                          <div id="separator">


                              <label for="lcol_description" style="display: none;">Plan Features : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:280px;" id="description<?php print($x);?>"><?php if($homePageColumnsContent){?> <?php print($homePageColumnsContent->contents);?> <?php } ?></textarea>
                        </div>  
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div>  
                        
                        
                        
                        
                         <h2>Extra Information</h2>      
          
        
        
       
        
       

<?php for($x = 4;$x<=4;$x++){
                  $testamonialContent = "";
                  $contentIndex = $x - 4;
                 $testamonialContent = $testamonials[$contentIndex];
                 
                 
                 $contentImage = "";
                 if($testamonialContent){
                    if($testamonialContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$testamonialContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
          <div>
                 <fieldset>
                <div id="separator" style="display: none;">
                
                    <label for="lcol_title">Page Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg<?php print($x);?>" id="btnImg<?php print($x);?>"/>
                    <input type="hidden" id="txtSmallFileName<?php print($x);?>" name="txtSmallFileName[]" <?php if($testamonialContent){?> value="<?php print($testamonialContent->contentMainImage);?>" <?php } ?> />
                    
                </div>
                
                <div id="separator" style="display: none;">

                            <label for="lcol_title">Content Heading : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($testamonialContent){?> value="<?php print($testamonialContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TESTAMONIAL"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($testamonialContent){?> value="<?php print($testamonialContent->title);?>" <?php } ?>/>
                        </div>


                        


                          <div id="separator">

                       
                            
                            <textarea name="pageContents[]" style="width:600px; height:280px;" id="description<?php print($x);?>"><?php if($testamonialContent){?> <?php print($testamonialContent->contents);?> <?php } ?></textarea>
                        </div>  
                        <div class="clear"></div>
                        
                         <div id="separator" style="display: none;">
                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($testamonialContent){?> value="<?php print($testamonialContent->contentLink);?>" <?php } ?>/>
                        </div> 
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
            
                </fieldset>  
                
                
   </div> 
         <?php } ?>      
                        
                        
                        
                        
                        
                        <h2>About Us</h2>
<div>
                <fieldset>
                 
                    <div id="separator">
                        <textarea name="mainbody"  id="mainbody" tabindex="7" style="height: 550px;"><?php print($BODY);?></textarea>
                    </div>
                </fieldset>
    
    </div>
  
  
  
  
  
    
    
                <p>
</div>
                    <input id="testbutton" class="testbutton" type="submit" value="submit" name="Submit" tabindex="8"/> 
                    <input id="resetbutton" type="reset" tabindex="9" />
                    <input type="hidden" name="txtPageId" id="txtPageId" value="<?php print($pageId);?>" />
                    <input type="hidden" name="txtParentpageId" id="txtParentpageId" value="<?php print($parentpageId);?>" />
                    <input type="hidden" name="txtSecParentpageId" id="txtSecParentpageId" value="<?php print($secParentpageId);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    <input type="hidden" name="txtPageType" id="txtPageType" value="<?php print($pageType);?>" />
                     <input type="hidden" name="txtListingId" id="txtListingId" value="<?php print($ListingID);?>" />
                      <input type="hidden" name="txtAllowToDelete" id="txtAllowToDelete" value="<?php print($ALLOWDELETE);?>" />
                </p>

                <div id="show"></div>
            </form>
        </div>
        <div class="clear">
        </div> 
    </div>
</div>




<script>


CKEDITOR.replace('mainbody' , {
//extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
//contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 650
});


<?php 
for($y = 0;$y<=4;$y++){
?>
CKEDITOR.replace('description<?php print($y);?>' , {
//extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
//contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 250
});
<?php } ?>
</script>