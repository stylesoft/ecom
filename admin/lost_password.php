<?php
require_once '../bootstrap.php';

$isEmailNotFound = false;
$isUpdatePasswordError = false;
$isUpdated= false;

if($_POST){
	
	$email = $_POST['email'];
	
	$objUser = new User();
	$userInfo  = $objUser->getUserByEmail($email);

	$memberId  = $userInfo->id; 
	$email_address = $email; 
	
	
	if(!$userInfo){
		$isEmailNotFound = true;
	} else {
		
		$random_password = makeRandomPassword(); 
    	$db_password = md5($random_password); 
		
    	$objUserData = new stdClass();
    	$objUserData->member_id = $memberId;
    	$objUserData->passwd = $db_password;
    	
    	// update password...
    	$isPasswordUpdated = $objUser->changePassword($objUserData);
    
    	if($isPasswordUpdated){
    		
    		//print("password updated".$email_address); exit;
    		$admin_baseUrl = ADMIN_BASE_URL;
    		$subject = "Your password at ".$admin_baseUrl; 
    $message = "Hi, we have reset your password. 
    
	Login: $email_address
	New Password: $random_password 
	
        Login Page: $admin_baseUrl
     
     
    Thanks! 
    Site admin 
     
    This is an automated response, please do not reply!"; 
     
         
    mail($email_address, $subject, $message, "From: PlusPro<no_reply@pluspro.com>"); 
    
    $isUpdated= true;
    		
    	} else {
    		$isUpdatePasswordError = true;
    	}

     
    
    	
    	
		
	}
	
}

function makeRandomPassword() { 
          $salt = "abchefghjkmnpqrstuvwxyz0123456789"; 
          srand((double)microtime()*1000000);  
          $i = 0; 
          while ($i <= 7) { 
                $num = rand() % 33; 
                $tmp = substr($salt, $num, 1); 
                $pass = $pass . $tmp; 
                $i++; 
          } 
          return $pass; 
    } 

$LAYOUT = ADMIN_LAYOUT_PATH . "tpl/lost_password_layout.tpl";

require_once $LAYOUT;
?>