<?php
/** '
 * @desc : load site contact data
 * @author : Gayan Chathuranga <gayan@monara.com>
 */
require_once '../bootstrap.php';
require_once('includes/auth.php');

$id = '';
$contact_name = '';
$address_line_1 = '';
$address_line_2 = '';
$address_line_3 = '';
$contact_number_1 = '';
$contact_number_2 = '';
$google_map_code = '';

$siteId = 1;
$objSiteContact = new SiteContact();
$arrSiteContactInfo = $objSiteContact->getSiteContact($siteId);

$id = $arrSiteContactInfo->id;
$contact_name = $arrSiteContactInfo->contact_name;
$address_line_1 = $arrSiteContactInfo->address_line_1;
$address_line_2 = $arrSiteContactInfo->address_line_2;
$address_line_3 = $arrSiteContactInfo->address_line_3;
$contact_number_1 = $arrSiteContactInfo->contact_number_1;
$contact_number_2 = $arrSiteContactInfo->contact_number_2;
$google_map_code = $arrSiteContactInfo->google_map_code;
$contact_type = $arrSiteContactInfo->contact_type;

if ($_POST) {

    
    $id = $siteId;
    $contact_name = mysql_real_escape_string($_POST['contact_name']);
    $address_line_1 = mysql_real_escape_string($_POST['address_line_1']);
    $address_line_2 = mysql_real_escape_string($_POST['address_line_2']);
    $address_line_3 = mysql_real_escape_string($_POST['address_line_3']);
    $contact_number_1 = mysql_real_escape_string($_POST['contact_number_1']);
    $contact_number_2 = mysql_real_escape_string($_POST['contact_number_2']);
    $google_map_code = mysql_real_escape_string($_POST['google_map_code']);
    $contact_type    = $_POST['contact_type'];

    $objSiteContact->id = $websiteId;
    $objSiteContact->contact_name = $contact_name;
    $objSiteContact->address_line_1 = $address_line_1;
    $objSiteContact->address_line_2 = $address_line_2;
    $objSiteContact->address_line_3 = $address_line_3;
    $objSiteContact->contact_number_1 = $contact_number_1;
    $objSiteContact->contact_number_2 = $contact_number_2;
    $objSiteContact->google_map_code = $google_map_code;
    $objSiteContact->contact_type = $contact_type;

    $objSiteContact->editSiteContact($id);

    $mainPageUrl = 'site-contacts.html';

    print("<div id='boxes'>");
    print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
    //The message comes here..
    print("Your <span class='red'>Site Contact</span> has been updated!<br /><br /><br /><br /><a href='$mainPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
    print("</div>");
    print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
    print("</div>");
    /*
      echo "<div id='coverit'></div><div id='message'>Your <span class='red'>Settings</span> has been updated!<br /><br /><br /><br /><a class='myButton' href='$mainPageUrl'>Continue</a></div>";
     */
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH . "tpl/contact/form.tpl";

require_once $LAYOUT;
?>
