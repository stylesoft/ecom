<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');

$objCurrency = new Currency();
$objCurrency->tb_name = 'tbl_currency';

$objCurrencyD = new Currency();
$objCurrencyD->tb_name = 'tbl_currency';

$siteId = 1;
$objSetting         		= new Setting();
$siteSettingsInfo       	= $objSetting->getSetting($siteId);
$websiteId                  = $siteSettingsInfo->id;
$websiteAdminBaseUrl        = $siteSettingsInfo->url;
$websiteName                = $siteSettingsInfo->siteName;
$websiteEmail               = $siteSettingsInfo->siteEmail;
$websiteTemplate            = $siteSettingsInfo->template;
$websiteSideBox             = $siteSettingsInfo->sideBox;
$websiteBaseUrl             = $siteSettingsInfo->baseUrl;
$websiteNeedPing            = $siteSettingsInfo->needPing;
$facebook_app_id            = $siteSettingsInfo->facebook_app_id;
$facebook_secret_key        = $siteSettingsInfo->facebook_secret_key;
$consumer_key               = $siteSettingsInfo->consumer_key;
$consumer_secret            = $siteSettingsInfo->consumer_secret;
$user_token                 = $siteSettingsInfo->user_token;
$user_secret                = $siteSettingsInfo->user_secret;
$terms_conditions           = $siteSettingsInfo->terms_conditions;
$privacy_policy             = $siteSettingsInfo->privacy_policy;
$max_num_pages              = $siteSettingsInfo->max_num_pages;
$siteLogo                   = $siteSettingsInfo->logo;
$siteFavIcon                = $siteSettingsInfo->favIcon;
$homeBannerHeight           = $siteSettingsInfo->homeBannerHeight;
$homeBannerWidth            = $siteSettingsInfo->homeBannerWidth;
$mobileSite                 = $siteSettingsInfo->mobileSite;


 $siteLogoImage = ""; 
 if($siteLogo != ''){
        $siteLogoImage = SITE_BASE_URL.'imgs/'.$siteLogo;
  } else {
      $siteLogoImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
  }
  
  $siteFavIconImage = ""; 
 if($siteFavIcon != ''){
        $siteFavIconImage = SITE_BASE_URL.$siteFavIcon;
  } else {
      $siteFavIconImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
  }
  
  

if($_POST){
	
	$settingDataObject = new stdClass();
	
	$websiteId                  = $siteId;
	$websiteAdminBaseUrl        = $_POST['site_url'];
	$websiteName                = $_POST['site_name'];
	$websiteEmail               = $_POST['site_email'];
	$websiteTemplate            = $_POST['site_template'];
	$websiteSideBox             = $_POST['sidebox'];
	$websiteBaseUrl             = $_POST['base_url'];
	$websiteNeedPing            = '';
        $facebook_app_id            = $_POST['txtFacebookAppId'];
        $facebook_secret_key        = $_POST['txtFacebookSecretkey'];
       	$consumer_key               = $_POST['txtTwitterConsumerkey'];
	$consumer_secret            = $_POST['txtTwitterConsumersecret'];
	$user_token                 = $_POST['txtTwitterAppAccesstoken'];
	$user_secret                = $_POST['txtTwitterAppAccesstokensecret'];
        $terms_conditions           = $_POST['txtTermsConditions'];
        $privacy_policy             = $_POST['txtPrivacyPolicy'];
        $max_num_pages              = $_POST['txtMaxPages'];
        $siteLogo                   = $_POST['txtSmallFileNameLogo'];
        $siteFavIcon                = $_POST['txtSmallFileNameIcon'];
        $homeBannerHeight           = $_POST['txthomeBannerHeight'];
        $homeBannerWidth            = $_POST['txthomeBannerWidth'];
        $mobileSite                 = $_POST['txtMobileSite'];
        $defultCurrency             = $_POST['site_currency'];   
        
	
	$settingDataObject->id				=	$websiteId;
	$settingDataObject->url                         =	$websiteAdminBaseUrl;
        $settingDataObject->siteName                    =	$websiteName;
        $settingDataObject->siteEmail                   =	$websiteEmail;
        $settingDataObject->template                    =	$websiteTemplate;
        $settingDataObject->sideBox                     =	$websiteSideBox;
        $settingDataObject->baseUrl                     =	$websiteBaseUrl;
        $settingDataObject->needPing                    =	$websiteNeedPing;
        $settingDataObject->facebook_app_id		=	$facebook_app_id;
        $settingDataObject->facebook_secret_key		=	$facebook_secret_key;
        $settingDataObject->consumer_key 		= 	$consumer_key;
        $settingDataObject->consumer_secret 		= 	$consumer_secret;
        $settingDataObject->user_token 			= 	$user_token;
        $settingDataObject->user_secret 		= 	$user_secret;
        $settingDataObject->terms_conditions 	        = 	$terms_conditions;
        $settingDataObject->privacy_policy 		= 	$privacy_policy;
        $settingDataObject->max_num_pages 		= 	$max_num_pages;
        $settingDataObject->logo                        = 	$siteLogo;
        $settingDataObject->favIcon                     = 	$siteFavIcon;
        $settingDataObject->homeBannerHeight            = 	$homeBannerHeight;
        $settingDataObject->homeBannerWidth             = 	$homeBannerWidth;
        $settingDataObject->mobileSite                  = 	$mobileSite;
	

       $currencyInfo = $objCurrency->getCurrency(SITE_DEFAULT_CURRENCY_ID);
        $id = $currencyInfo->id;
        $name = $currencyInfo->name;
        $code = $currencyInfo->code;
        $codeStr = $currencyInfo->codeStr;
        $isDefault = $currencyInfo->isDefault;
        $status = $currencyInfo->status;
        
        $objCurrency->id = $id;
        $objCurrency->name = $name;
        $objCurrency->code = $code;
        $objCurrency->codeStr = $codeStr;
        $objCurrency->isDefault = "No";
        $objCurrency->status = $status;
      
        
        $isRecordUpdated = $objCurrency->editCurrency();
        
        $defultCurrency = $objCurrencyD->getCurrency($defultCurrency);
        
        $idD = $defultCurrency->id;
        $nameD = $defultCurrency->name;
        $codeD = $defultCurrency->code;
        $codeStrD = $defultCurrency->codeStr;
        $isDefaultD = $defultCurrency->isDefault;
        $statusD = $defultCurrency->status;
        
        $objCurrencyD->id = $idD;
        $objCurrencyD->name = $nameD;
        $objCurrencyD->code = $codeD;
        $objCurrencyD->codeStr = $codeStrD;
        $objCurrencyD->isDefault = "Yes";
        $objCurrencyD->status = $statusD;
        
        
        $isRecordUpdated1 = $objCurrencyD->editCurrency();
        if ($isRecordUpdated1 == true ) {
        	unset($_SESSION['selected_currency_id']);;
        }
        


	$objSetting->editSetting($settingDataObject);
	
	$mainPageUrl = 'pages.html';

	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Your <span class='red'>Settings</span> has been updated!<br /><br /><br /><br /><a href='$mainPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
	/*
	echo "<div id='coverit'></div><div id='message'>Your <span class='red'>Settings</span> has been updated!<br /><br /><br /><br /><a class='myButton' href='$mainPageUrl'>Continue</a></div>";
	*/
}

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH."tpl/settings.tpl";
	
require_once $LAYOUT;
?>
