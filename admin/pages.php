<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');

// declare variables....
$pageId             = "";
$parentpageId       = "";
$secParentpageId    = "";
$action             = "";
$pageType           = "";


$ID                 = "";
$TITLE              = "";
$KEYWORDS           = "";
$DESCRIPTION        = "";
$BODY               = "";
$ListingID          = "";
$NAME               = "";
$LIVE               = "Draft";
$ISCONTACT          = "No";
$ALLOWDELETE        = "";
$MAIN_PAGE_ID       = "";

$selectedPageInfo   = "";
$lastInsertedId     = "";
$isRecordUpdated    = "";
$pagesInfo          = "";

$newPageUrl        = "";
$mainPageUrl        = "";

// the page contents...
$pageContentId              = "";
$pageContentTitle           = "";
$pageContentDisplayOrder    = "";
$pageContentContents        = "";
$pageContentType            = "";
$pageContentPageId          = "";
$pageContentMainImage       = "";
$pageContentLink            = "";


$pContentId              = "";
$pContentTitle           = "";
$pContentDisplayOrder    = "";
$pContentContents        = "";
$pContentType            = "";
$pContentPageId          = "";
$pContentMainImage       = "";
$pContentLink            = "";

$topContentDetails        = "";
$homePageColumnsContents  = "";
$testamonials             = "";
$topContentsDetails       = "";


//--------------------------------------------
$pageObject = new Page();
$homePageObject = new HomePage();
$contactPageObject = new ContactPage();
$subPageObject = new SubPage();
$secSubPageObject = new SecSubPage();
$pageContentObject = new PageContent();
//-------------------------------------------

if($_GET){
    // get the page id
    if(isset($_GET['pageId'])) {
        $pageId             = $_GET['pageId'];
    }
    
    // get the page type...
    if(isset($_GET['pageType'])) {
            $pageType             = $_GET['pageType'];
    }
    
    // get the parent page id
    if(isset($_GET['parentpageId'])) {
        $parentpageId               = $_GET['parentpageId'];
        
        if($pageType == 'sub_pages'){
             $pagesInfo                  = $subPageObject->getAllByMainPageId($parentpageId);
        } elseif($pageType == 'sec_sub_pages'){
            $pagesInfo                  = $secSubPageObject->getAllByMainPageId($parentpageId);
        }
    }
    
    // get the secondary  parent page id
    if(isset($_GET['secParentpageId'])) {
        $secParentpageId             = $_GET['secParentpageId'];
    }
    
    // get the mode
    if(isset($_GET['action'])) {
            $action             = $_GET['action'];
    }
    
    
    
    if($pageId != ""){
        
        if($pageType == 'mainPage'){
            $selectedPageInfo = $pageObject->getPage($pageId);
       
            if($selectedPageInfo){

                $ID                 = $selectedPageInfo->id;
                $TITLE              = $selectedPageInfo->title;
                $KEYWORDS           = $selectedPageInfo->keywords;
                $DESCRIPTION        = $selectedPageInfo->description;
                $BODY               = $selectedPageInfo->body;
                $ListingID          = $selectedPageInfo->listingId;
                $NAME               = $selectedPageInfo->name;
                $LIVE               = $selectedPageInfo->live;
                $ISCONTACT          = $selectedPageInfo->isContact;
                $ALLOWDELETE        = $selectedPageInfo->allowToDelete;  
            }
        } else if($pageType == 'homePage') {
            
            $selectedPageInfo = $homePageObject->getHomePage($pageId);
            
            if($selectedPageInfo){

                $ID                 = $selectedPageInfo->id;
                $TITLE              = $selectedPageInfo->title;
                $KEYWORDS           = $selectedPageInfo->keywords;
                $DESCRIPTION        = $selectedPageInfo->description;
                $BODY               = $selectedPageInfo->body;
                $ListingID          = null;
                $NAME               = $selectedPageInfo->name;
                $LIVE               = null;
                $ISCONTACT          = null;
                $ALLOWDELETE        = null;  
            }
          
            
        } else if($pageType == 'contactUsPage') {
  
            $selectedPageInfo = $contactPageObject->getContactPage($pageId);
            if($selectedPageInfo){

                $ID                 = $selectedPageInfo->id;
                $TITLE              = $selectedPageInfo->title;
                $KEYWORDS           = $selectedPageInfo->keywords;
                $DESCRIPTION        = $selectedPageInfo->description;
                $BODY               = $selectedPageInfo->body;
                $ListingID          = null;
                $NAME               = $selectedPageInfo->name;
                $LIVE               = null;
                $ISCONTACT          = null;
                $ALLOWDELETE        = null;  
            }
            
            
        } else if($pageType == 'sub_pages') {
            
            $selectedPageInfo = $subPageObject->getSubPage($pageId);
       
            if($selectedPageInfo){

                $ID                 = $selectedPageInfo->id;
                $TITLE              = $selectedPageInfo->title;
                $KEYWORDS           = $selectedPageInfo->keywords;
                $DESCRIPTION        = $selectedPageInfo->description;
                $BODY               = $selectedPageInfo->body;
                $ListingID          = $selectedPageInfo->listingId;
                $NAME               = $selectedPageInfo->name;
                $LIVE               = $selectedPageInfo->live;
                $ISCONTACT          = $selectedPageInfo->isContact;
                $ALLOWDELETE        = $selectedPageInfo->allowToDelete;  
            }
            
        } else if($pageType == 'sec_sub_pages') {
            
            $selectedPageInfo = $secSubPageObject->getSecSubPage($pageId);
       
            if($selectedPageInfo){

                $ID                 = $selectedPageInfo->id;
                $TITLE              = $selectedPageInfo->title;
                $KEYWORDS           = $selectedPageInfo->keywords;
                $DESCRIPTION        = $selectedPageInfo->description;
                $BODY               = $selectedPageInfo->body;
                $ListingID          = $selectedPageInfo->listingId;
                $NAME               = $selectedPageInfo->name;
                $LIVE               = $selectedPageInfo->live;
                $ISCONTACT          = $selectedPageInfo->isContact;
                $ALLOWDELETE        = $selectedPageInfo->allowToDelete;  
            }
            
        }
        
        
        $pageContentObject = new PageContent();
        $pageContentObject->tb_name = 'tbl_page_contents';
        $topContentsDetails        = $pageContentObject->getAllByPageIdAndType($pageId, 'TOP_CONTENT',$pageType);
        
        // middle contents....
        $pageContentObject = new PageContent();
        $pageContentObject->tb_name = 'tbl_page_contents';
        $middleContentsDetails        = $pageContentObject->getAllByPageIdAndType($pageId, 'MIDDEL_CONTENT',$pageType);
        
        
        // bottom contents ....
        // middle contents....
        $pageContentObject = new PageContent();
        $pageContentObject->tb_name = 'tbl_page_contents';
        $bottomContentsDetails        = $pageContentObject->getAllByPageIdAndType($pageId, 'BOTTOM CONTENT',$pageType);
        
        

        $pageContentObject = new PageContent();
            $pageContentObject->tb_name = 'tbl_page_contents';
        $homePageColumnsContents  = $pageContentObject->getAllByPageIdAndType($pageId, 'COLUMN_CONTENT',$pageType);
        $pageContentObject = new PageContent();
            $pageContentObject->tb_name = 'tbl_page_contents';
        $testamonials             = $pageContentObject->getAllByPageIdAndType($pageId, 'TESTAMONIAL',$pageType);
        
        $pageContentObject->tb_name      = 'tbl_page_contents';
        $leftColumnsContents             = $pageContentObject->getAllByPageIdAndType($pageId, 'COLUMN_CONTENT_LEFT',$pageType);
        
        $pageContentObject->tb_name      = 'tbl_page_contents';
        $rightColumnsContents             = $pageContentObject->getAllByPageIdAndType($pageId, 'COLUMN_CONTENT_RIGHT',$pageType);
        
        

        // get all page images.....
        $imageObject                    = new Image(); // the image object..
        $pageImages                     = $imageObject->getAllByImageObject('Page',$pageId);
        

    }
    
    
} else {
    $pagesInfo = $pageObject->getAll();
}

$objPageData  = new stdClass();

if($_POST){
    
    
    $pageId             = ($_POST['txtPageId'] != '' ? $_POST['txtPageId'] : "");//$_POST['txtPageId'];
    $parentpageId       = ($_POST['txtParentpageId'] != '' ? $_POST['txtParentpageId'] : "");//$_POST['txtParentpageId'];
    $secParentpageId    = ($_POST['txtSecParentpageId'] != '' ? $_POST['txtSecParentpageId'] : "");// $_POST['txtSecParentpageId'];
    $action             = ($_POST['txtAction'] != '' ? $_POST['txtAction'] : "");// $_POST['txtAction'];
    $pageType           = ($_POST['txtPageType'] != '' ? $_POST['txtPageType'] : "");// $_POST['txtPageType'];


    $ID                 = $pageId;
    $TITLE              = ($_POST['pagetitle'] != '' ? $_POST['pagetitle'] : "");// $_POST['pagetitle'];
    $KEYWORDS           = ($_POST['keywords'] != '' ? $_POST['keywords'] : "");// $_POST['keywords'];
    $DESCRIPTION        = ($_POST['description'] != '' ? $_POST['description'] : "");// $_POST['description'];
    $BODY               = ($_POST['mainbody'] != '' ? $_POST['mainbody'] : "");// $_POST['mainbody'];
    $NAME               = ($_POST['pagename'] != '' ? $_POST['pagename'] : "");// $_POST['pagename'];
    $LIVE               = ($_POST['status'] != '' ? $_POST['status'] : "");// $_POST['status'];
    $ISCONTACT          = ($_POST['contact'] != '' ? $_POST['contact'] : "");// $_POST['contact'];
    
    $ListingID          = ($_POST['txtListingId'] != '' ? $_POST['txtListingId'] : "");// $_POST['txtListingId'];
    $ALLOWDELETE        = ($_POST['txtAllowToDelete'] != '' ? $_POST['txtAllowToDelete'] : "");// $_POST['txtAllowToDelete'];
    
    $MAIN_PAGE_ID       = ($_POST['txtParentpageId'] != '' ? $_POST['txtParentpageId'] : "");// $_POST['txtParentpageId'];
    
    
    $pageExtraImages    = ($_POST['ptxtSmallFileName'] != '' ? $_POST['ptxtSmallFileName'] : "");// $_POST['txtParentpageId'];
    
    // post the page contents...
    if(isset($_POST['pageContentId'])){
        $pageContentId              = ($_POST['pageContentId'] != '' ? $_POST['pageContentId'] : "");// $_POST['pageContentId'];
        $pageContentTitle           = ($_POST['pageContentsTitle'] != '' ? $_POST['pageContentsTitle'] : "");// $_POST['pageContentsTitle'];
        $pageContentDisplayOrder    = ($_POST['contentDisplayOrder'] != '' ? $_POST['contentDisplayOrder'] : "");// $_POST['contentDisplayOrder'];
        $pageContentContents        = ($_POST['pageContents'] != '' ? $_POST['pageContents'] : "");// $_POST['pageContents'];
        $pageContentType            = ($_POST['contentType'] != '' ? $_POST['contentType'] : "");// $_POST['contentType'];
        $pageContentMainImage       = ($_POST['txtSmallFileName'] != '' ? $_POST['txtSmallFileName'] : "");// $_POST['txtSmallFileName'];
        $pageContentLink            = ($_POST['pageContentsLink'] != '' ? $_POST['pageContentsLink'] : "");//  $_POST['pageContentsLink'];
    }
    
    $pageObject = new Page();
        $objPageData->id = $pageId;
        $objPageData->title = mysql_real_escape_string($TITLE);
        $objPageData->keywords = mysql_real_escape_string($KEYWORDS);
        $objPageData->description = mysql_real_escape_string($DESCRIPTION);
        $objPageData->body = mysql_real_escape_string($BODY);
        $objPageData->listingId = $ListingID;
        $objPageData->name = mysql_real_escape_string($NAME);
        $objPageData->live = $LIVE;
        $objPageData->isContact = $ISCONTACT;
        $objPageData->allowToDelete = $ALLOWDELETE;
        $objPageData->mainPageId = $MAIN_PAGE_ID;
        
        
        
        if($LIVE == 'Published'){
            /*
            $existingNumberOfPublishedPages = $pageObject->countByStatus('Published');
            if($existingNumberOfPublishedPages >= (SITE_MAX_PAGES - 1)){
                header("Location: error.html?msg=pagelimit");
                exit;
            }
             * 
             */
        }
        
        
    if($action == 'add'){
        $objPageData->id = '';
        if($pageType == 'mainPage'){
            $lastInsertedId = $pageObject->addPage($objPageData);
        } else if($pageType == 'homePage'){
            $lastInsertedId = $homePageObject->addPage($objPageData);
        } else if($pageType == 'contactUsPage'){
            $lastInsertedId = $contactPageObject->addContactPage($objPageData);      
        }  else if($pageType == 'sub_pages'){
            $lastInsertedId = $subPageObject->addSubPage($objPageData);
        }else if($pageType == 'sec_sub_pages'){
            $lastInsertedId = $secSubPageObject->addSecSubPage($objPageData);
        }
    } else if($action == 'edit'){
        if($pageType == 'mainPage'){
           $isRecordUpdated  = $pageObject->editPage($objPageData);
        } else if($pageType == 'homePage'){
            $NAME = $TITLE;
            $isRecordUpdated  = $homePageObject->editHomePage($objPageData);
        } else if($pageType == 'contactUsPage'){
            $isRecordUpdated  = $contactPageObject->editContactPage($objPageData);
        }else if($pageType == 'sub_pages'){
            $isRecordUpdated  = $subPageObject->editSubPage($objPageData);
        }else if($pageType == 'sec_sub_pages'){
            $isRecordUpdated  = $secSubPageObject->editSecSubPage($objPageData);
        }
    }
    
    if($pageType == 'sub_pages'){
        $newPageUrl = "sub_pages.html?action=add&pageType=sub_pages&parentpageId=".$MAIN_PAGE_ID;
        $mainPageUrl        = "sub_pages.html?pageType=sub_pages&parentpageId=".$MAIN_PAGE_ID; 
    }elseif($pageType == 'sec_sub_pages'){
        $newPageUrl = "sec_sub_pages.html?action=add&pageType=sec_sub_pages&parentpageId=".$MAIN_PAGE_ID;
        $mainPageUrl        = "sec_sub_pages.html?pageType=sec_sub_pages&parentpageId=".$MAIN_PAGE_ID; 
    } else {
        $newPageUrl = "pages.html?action=add&pageType=mainPage";
        $mainPageUrl        = "pages.html";
    }
    if($lastInsertedId){
        
        
        foreach($pageContentContents  As $cIndex=>$pageContent){
            
            
            $pContentId              = $pageContentId[$cIndex];
            $pContentTitle           = $pageContentTitle[$cIndex];
            $pContentDisplayOrder    = $pageContentDisplayOrder[$cIndex];
            $pContentContents        = $pageContent;
            $pContentType            = $pageContentType[$cIndex];
            $pContentPageId          = $lastInsertedId;
            $pContentMainImage       = $pageContentMainImage[$cIndex];
            $pContentLink            = $pageContentLink[$cIndex];
            $pageContentObject                      = new PageContent();
            $pageContentObject->tb_name             = 'tbl_page_contents';
            $pageContentObject->id                  = $pContentId;
            $pageContentObject->title               = mysql_real_escape_string($pContentTitle);
            $pageContentObject->displayOrder        = $pContentDisplayOrder;
            $pageContentObject->contents            = mysql_real_escape_string($pContentContents);
            $pageContentObject->contentType         = $pContentType;
            $pageContentObject->pageId              = $pContentPageId;
            $pageContentObject->contentMainImage    = $pContentMainImage;
            $pageContentObject->contentLink         = $pContentLink; // ,$pageType
            $pageContentObject->pageType            = $pageType;
            $pageContentObject->addPageContent();
            
            
        }
        
	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Page <span class='red'>$NAME</span> has been created!<br /><br /><br />Create another page now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        /*
        echo "<div id='coverit'></div><div id='message'>Page <span class='red'>$NAME</span> has been created!<br /><br /><br />Create another page now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	*/


    } else if($isRecordUpdated){
        
        
         foreach($pageContentContents  As $cIndex=>$pageContent){
            
             
            
            $pContentId              = $pageContentId[$cIndex];
            $pContentTitle           = $pageContentTitle[$cIndex];
            $pContentDisplayOrder    = $pageContentDisplayOrder[$cIndex];
            $pContentContents        = $pageContent;
            $pContentType            = $pageContentType[$cIndex];
            $pContentPageId          = $pContentPageId;
            $pContentMainImage       = $pageContentMainImage[$cIndex];
            $pContentLink            = $pageContentLink[$cIndex];
            

            
            $pageContentObject = new PageContent();
            $pageContentObject->tb_name = 'tbl_page_contents';
            $pageContentObject->id   = $pContentId;
            $pageContentObject->title = mysql_real_escape_string($pContentTitle);
            $pageContentObject->displayOrder = $pContentDisplayOrder;
            $pageContentObject->contents = mysql_real_escape_string($pContentContents);
            $pageContentObject->contentType = $pContentType;
            $pageContentObject->pageId      = $pageId;
            $pageContentObject->contentMainImage    = $pContentMainImage;
            $pageContentObject->contentLink         = $pContentLink;
            $pageContentObject->pageType            = $pageType;
        
            if($pContentId != ''){
                $pageContentObject->editPageContent();
            } else {
                $pageContentObject->addPageContent();
            }
            
            // add the extra images to the site 
            
             if(isset($pageExtraImages)){
                 // delete all the existing image before u add it
                 $imageObject                    = new Image(); // the image object..
                 $imageObject->deleteImageByObject('Page', $pageId); 
                foreach($pageExtraImages As $imgIndex=>$pageExtraImage){
                    $imageObject                    = new Image(); // the image object..
                    $imageObject->recordId          = '';
                    $imageObject->recordText        = $pageExtraImage;
                    $imageObject->recordListingId   = $imgIndex + 1;
                    $imageObject->imageObject       = 'Page';
                    $imageObject->image_title       = '';
                    $imageObject->image_description = '';
                    $imageObject->image_link        = '';
                    $imageObject->specialTag        = '';
                    $imageObject->imageObjectKeyId  = $pageId;
                    $imageObject->addImage();
                }
             }
            
            
        }
      
	/*        
         echo "<div id='coverit'></div><div id='message'>Page <span class='red'>$NAME</span> has been updated!<br /><br /><br />Create another page now?<br /><br /><a href='$newPageUrl'  class='testbutton_sm' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  id='resetbutton_sm'>No</a></div>";
	*/
	  print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Page <span class='red'>$NAME</span> has been updated!<br /><br /><br />Create another page now?<br /><br /><a href='$newPageUrl'  id='testbutton_sm' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  id='resetbutton_sm'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");

    }
    
}



$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";

if($action == 'add' || $action == 'edit'){
    
    if($pageType == 'homePage') {
        $CONTENT = ADMIN_LAYOUT_PATH."index/homepage.tpl";
    } elseif($pageType == 'contactUsPage'){
        $CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/form.tpl";
    }
    elseif($pageId == '9') {
    $CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/form_custom2.tpl";
    }
    
    elseif($pageId == '4' or $pageId == '7' ) {
    	$CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/form_custom2.tpl";
    }
    

    else {
    	$CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/form_custom.tpl";
    }
    
} else {
    if($pageType == 'sub_pages'){
        $CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/sub_pages.tpl";
    } elseif($pageType == 'sec_sub_pages'){ 
        $CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/sec_sub_pages.tpl";
    }else {
        $CONTENT = ADMIN_LAYOUT_PATH."tpl/pages/index.tpl";
    }
}
	
require_once $LAYOUT;
?>
