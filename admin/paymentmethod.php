<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');

$id = '';
$name  = '';
$code = '';
$status = '';



// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrpaymentMethod= array();

$objPaymentMethod = new PaymentMethod();
$objPaymentMethod->tb_name = 'tbl_paymentmethod';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                //$arrSubcribe = $objSubcribe->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {             
            $paymentMethodInfo = $objPaymentMethod->getPaymentMethod($id);

            $id = $paymentMethodInfo->id;
            $name = $paymentMethodInfo->name;
            $code = $paymentMethodInfo->code;
            $status = $paymentMethodInfo->status;
            
   
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}

if ($_POST) {
//print_r($_POST);
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $name = $_POST['name'];
    $code = $_POST['code'];
    $status = $_POST['status'];
    
    
    
   

    //validation
    if($name == ""){
            array_push($objPaymentMethod->error, 'The Name is required');
            $invalid = true;
        }

    $objPaymentMethod->id = $id;
    $objPaymentMethod->name = $name;
    $objPaymentMethod->code = $code;
    $objPaymentMethod->status = $status;
    
    if ($action == 'add' && !$invalid) {
    	
          $lastInsertedId = $objPaymentMethod->addPaymentMethod();
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objPaymentMethod->editPaymentMethod();
    }

    $newPageUrl = "paymentmethod.html?action=add";
    $mainPageUrl = "paymentmethod.html";

    if ($lastInsertedId) {
    	
    	
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Payment Method <span class='red'>$name</span> has been added!<br /><br /><br />Want to enter more Payment method?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
          
        
    } else if ($isRecordUpdated) {
        
       
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Payment Method <span class='red'>$name</span> has been updated!<br /><br /><br />Want to enter more Payment method?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
       
    }
} else {
	
 if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }
       
        if(isset($_GET['orderby'])){
        	$orderBy = $_GET['orderby'];
        }
        
        if(isset($_GET['order'])){
        	$order = $_GET['order'];
        }
        
        if($orderBy != '' && $order != ''){
        	$orderStr       = $orderBy." ".$order;
        } else {
        
        	$orderStr       = 'id'." ".'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }


    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objPaymentMethod->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objPaymentMethod->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}




$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH . "tpl/payment/form.tpl";



require_once $LAYOUT;
?>