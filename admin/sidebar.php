<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');

//blog post attribures

$id = '';
$content_title = ''; 	
$content_description = '';
$contentObject = '';
$contentObjectId = '';
$contentListing = '';
$content_status = ''; 
    

    $session_user = $_SESSION['SESS_FIRST_NAME']; //blog post user
    $post_author = $session_user;
// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrPost = array();

$objSiderBar = new SiderContent();
$objSiderBar->tb_name = 'tbl_sidebar_content';


$objSiderAvailable = new SiderContentAvailable();
$objPage = new Page();
$objPage->tb_name = 'pages';
$arrPages = $objPage->getAll();

$backUrl = 'sidebar.html';

if (isset($_GET['action'])) {
    $action = $_GET['action'];

}

if($action == 'edit'){
    //get the post data to the id
    $id = $_GET['id'];
    
    $siderBarData = $objSiderBar->getSiderContent($id);
                
            $id = $siderBarData->id;
            $content_title = $siderBarData->content_title;
            $content_description = $siderBarData->content_description;
            $contentObject = $siderBarData->contentObject;
            $contentObjectId = $siderBarData->contentObjectId;
            $contentListing = $siderBarData->contentListing;
            $content_status = $siderBarData->content_status;
            
            $available = $objSiderAvailable->getAllAvailable($id);
}

if ($_POST) {

 
    $action = $_POST['txtAction'];
    

    $id = ($action == 'add') ? "" : $_POST['id'];
    $content_title = $_POST['content_title'];
    $content_description = addslashes($_POST['content_description']);
    $contentObject = $_POST['contentObject'];
    $contentObjectId = '1';
    $contentListing = $_POST['contentListing'];
    $content_status = $_POST['content_status']; // $post_author = $_POST['post_author'];


    
    //assign values to the object
    $objSiderBar->id = $id;
    $objSiderBar->content_title = $content_title;
    $objSiderBar->content_description = $content_description;
    $objSiderBar->contentObject = $contentObject;
    $objSiderBar->contentObjectId = $contentObjectId;
    $objSiderBar->contentListing = $contentListing;
    $objSiderBar->content_status = $content_status;
    
    
    if ($action == 'add' && !$invalid) {
          $lastInsertedId = $objSiderBar->addSiderContent();
          //insert to the available list
          if(count($_POST['availble']) >0){
              foreach($_POST['availble'] as $data){
                  $objSiderAvailable->id = '';
                  $objSiderAvailable->page_id = $data;
                  $objSiderAvailable->content_id = $lastInsertedId;
                  $objSiderAvailable->addSiderContentAvailable();
              }

              }
    }elseif ($action == 'edit' && !$invalid) {
          $isRecordUpdated = $objSiderBar->editSiderContent($id);
          $objSiderAvailable->deleteSiderContent($id);
          if (count($_POST['availble']) > 0) {
            foreach ($_POST['availble'] as $data) {
                $objSiderAvailable->id = '';
                $objSiderAvailable->page_id = $data;
                $objSiderAvailable->content_id = $id;
                $objSiderAvailable->addSiderContentAvailable();
            }
        }
    }

    $newPageUrl = "sidebar.html?action=add";
    $mainPageUrl = "sidebar.html";

    if ($lastInsertedId) {
        echo "<div id='coverit'></div><div id='message'>Sider Content has been added!<br /><br />Add Another Content?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        echo "<div id='coverit'></div><div id='message'>Sider Content has been updated!<br /><br />Edit Content ?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "content_title";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'ASC';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objSiderBar->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objSiderBar->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
    $CONTENT = ADMIN_LAYOUT_PATH . "sidebar/form.tpl.php";
    
}else {
    $CONTENT = ADMIN_LAYOUT_PATH . "sidebar/index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
