<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');


// get all the opening time fields....
$objFields = new Fields();
$openingTimeRelatedFields = $objFields->getAllByType('Opening_Times'); 

if($_POST){
    
    $arrPostFieldsIds       = $_POST['field_id'];
    $arrPostFieldsValues    = $_POST['field_value'];
    $arrPostFieldsValueIds  = $_POST['field_value_id'];
    
    
    foreach($arrPostFieldsIds As $pfIndex=>$arrPostFieldsId){
        
        $objFieldValue  = new FieldValue();
        $objFieldValue->tb_name = 'tbl_field_value';
        $id             = $arrPostFieldsValueIds[$pfIndex];
        $field_id       = $arrPostFieldsId;
        $sub_field_id   = "";
        $field_value    = $arrPostFieldsValues[$pfIndex];
        
        $isFieldIdExists = $objFieldValue->isFieldIdExists($field_id);
        
        $objFieldValue->id = $id;
        $objFieldValue->fieldsId = $field_id;
        $objFieldValue->subFieldsId = "";
        $objFieldValue->fieldValue = $field_value;
        $objFieldValue->fieldAttributeValue1 = "";
        
        if($isFieldIdExists == true){
            $objFieldValue->editFieldValue();
        } else {
            $objFieldValue->addFieldValue();
        }
    }
   
    $newPageUrl = "opening_times.html";

     print("<div id='boxes'>");
     print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
     print("The record has been updated!<br /><br /><br /><a href='$newPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
     print("</div>");
     print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
     print("</div>");
    //print("The post came here....."); exit;
    
    
    
}


$CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";


require_once $LAYOUT;
?>
