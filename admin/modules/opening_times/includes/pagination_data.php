<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$orderBy        = "";
$order          = "";
$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "contentListing";
$order          = "Asc";
$orderStr       = "";
$contentType    = '';
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            // get the search query....
            if(isset($_GET['q'])){
                $searchQ   = $_GET['q'];
            }
            
            if (isset($_GET['type'])) {
                $contentType = $_GET['type'];

            }
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                if($_GET['orderby']!=''){
                    $orderBy = $_GET['orderby'];
                }
            }
            
            if(isset($_GET['order'])){
                if($_GET['order'] != ''){
                    $order = $_GET['order'];
                }
                
            }

            if($orderBy != '' && $order != ''){
                $orderStr       = $orderBy." ".$order;                
            } 
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}



$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;
//--------------------------------------------

$objSiderBar = new SiderContent();
$objSiderBar->tb_name = 'tbl_sidebar_content';
 
$objSiderBar->content_type = $contentType;
$objSiderBar->searchStr    = $searchQ;
$objSiderBar->limit        = $recLimit;
$objSiderBar->listingOrder = $orderStr;
$siderBarResult            = $objSiderBar->search();
?>
<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			 $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/updateListingOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

</script>


<div id="list">
<div id="table_main_div" >
     <div id="response"> </div>
                    	<div class="row_heding">
                        	
                                <div class="colum" style="width:70px;">
                                    <strong>ID&nbsp;</strong>
                                    <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                                    <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
                                </div>
                                <div class="colum" style="width:340px;">
                                    <strong>Title</strong>
                                    <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=content_title&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                                    <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=content_title&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
                                </div>
                                
                                <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
                            </div>
                            <div class="clear"></div>
                    	
                            <ul>
                                 <?php 
                                    if(count($siderBarResult) >0){
                                        foreach($siderBarResult As $rowIndex=>$contentData) {?>   
                                <li id="arrayorder_<?php print($contentData->id);?>">
                            <?php if($rowIndex%2 != 0){?>
                            <div class="row1">
                             <?php } else {?>
                                 <div class="row2">
                              <?php } ?>
                        	<div class="colum" style="width:70px;"><?php print($contentData->id);?></div>
                            <div class="colum" style="width:340px;"><?php print($contentData->content_title);?></div>
                          
                            <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                
                                <a href="<?=$module?>.html?action=edit&id=<?php print($contentData->id);?>">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                               
                                <a href="javascript:deleterow(<?php echo $contentData->id; ?>);">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                </a>
                            </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                                
                                <?php } }else{?>
                            <li>
                                <div id="listings" style="background:#eee;">No Matching Records Found </div>
                            </li>
                            <?php }?>
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>






<script type="text/javascript" >
    function deleterow(id){
        if (confirm('Are you sure want to delete?')) {
            $.post('../modules/sidebar/includes/deletelisting.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                return false;
            });
        }
    }

</script>

  


  