<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id = "";
$album_title = "";
$status = "";
$display_order = "";
$keywords = "";
$description = "";
$album_page = '';
$albumImage   = "";


$imageTitle = '';
$imageDescription = '';
$imageLink = '';
$recordId = '';
$recordText = '';
$image_object = '';
$txtAction = '';
$listingId = '';
$image_object_keyId = '';
$specialTag = '';


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$controller = '';

$objAlbum = new Album();
$objAlbum->tb_name = 'tbl_album';

$objImage = new Image();
$objImage->tb_name = 'tbl_images';

$objPage = new Page();
$objPage->tb_name = 'pages';
$arrPages = $objPage->getAll();


$module = '';
if (isset($_GET['m'])) {
    $module = $_GET['m'];
}
if (isset($_GET['c'])) {
    $controller = $_GET['c'];
   
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit' && $module != 'gallery') {

        if (isset($_GET['id'])) {

            $albumId = $_GET['id'];

            $albumInfo = $objAlbum->getAlbum($albumId);

            $id = $albumInfo->id;
            $album_title = $albumInfo->albumTitle;
            $keywords = $albumInfo->albumKeywords;
            $description = $albumInfo->albumDescription;
            $display_order = $albumInfo->albumOrder;
            $status = $albumInfo->albumStatus;
            $album_page = $albumInfo->album_page;
            $albumImage   = $albumInfo->albumImage;
        }
    }
}

if ($_POST) {
    $action = $_POST['txtAction'];


    if (isset($_POST['module']) && $_POST['module'] == 'gallery') {

        $imageTitle = $_POST['imageTitle'];
        $imageDescription = $_POST['imageDescription'];
        $imageLink = $_POST['imageLink'];
        $recordId = $_POST['recordId'];
        $specialTag = $_POST['rdSpecialTag'];
        // $recordText = $_POST['recordText'];
        //  $image_object = $_POST['imageObject'];
        $txtAction = $_POST['txtAction'];
        // $listingId = $_POST['listingId'];
         $image_object_keyId = $_POST['image_object_keyId'];

        if ($action == 'edit') {
           // print_r($objImage);
            $objImage->image_title = $imageTitle;
            $objImage->image_description = $imageDescription;
            $objImage->image_link = $imageLink;
            $objImage->specialTag = $specialTag;
           

            $isRecordUpdated = $objImage->editImage($recordId);
            $mainPageUrl = 'album.html';
            if ($isRecordUpdated) {
                
                print("<div id='boxes'>");
                print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
                //The message comes here..
                print("Page <span class='red'>$imageTitle</span> has been updated!<br /><br /><br />Go Gallery Listing?<br /><br /><a href='album.html?m=gallery&c=Image_Gallery&id=$image_object_keyId'  id='testbutton_sm' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  id='resetbutton_sm'>No</a>");
                print("</div>");
                print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
                print("</div>");
               }

        }
    } else {
      
        $id = ($action == 'add') ? "" : $_POST['id'];
        $album_title = $_POST['albumTitle'];
        $status = $_POST['status'];
        $display_order = $_POST['listingId'];
        $keywords = $_POST['albumKeywords'];
        $description = $_POST['description'];
        $album_page = $_POST['album_page'];
        $albumImage = $_POST['txtSmallFileName'];


        $objAlbum->id = $id;
        $objAlbum->albumTitle = $album_title;
        $objAlbum->albumKeywords = $keywords;
        $objAlbum->albumDescription = $description;
        $objAlbum->albumOrder = $display_order;
        $objAlbum->albumStatus = $status;
        $objAlbum->album_page = $album_page;
        $objAlbum->albumImage = $albumImage;



        if ($action == 'add') {
            $lastInsertedId = $objAlbum->addAlbum();
        } else if ($action == 'edit') {
            $isRecordUpdated = $objAlbum->editAlbum();
        }

        $newPageUrl = "album.html?action=add";
        $mainPageUrl = "album.html";
        if ($lastInsertedId) {
            print("<div id='boxes'>");
            print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
            //The message comes here..
            print("Page <span class='red'>$album_title</span> has been updated!<br /><br /><br />Upload Images? <br /><br /><a href='../images.php?m=album&c=Image_Gallery&id=$lastInsertedId'  id='testbutton_sm' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  id='resetbutton_sm'>No</a>");
            print("</div>");
            print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
            print("</div>");
           
        } elseif ($isRecordUpdated) {
            
           print("<div id='boxes'>");
            print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
            //The message comes here..
            print("Page <span class='red'>$album_title</span> has been updated!<br /><br /><br />Upload Images? <br /><br /><a href='../images.php?m=album&c=Image_Gallery&id=$id'  id='testbutton_sm' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  id='resetbutton_sm'>No</a>");
            print("</div>");
            print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
            print("</div>");
            
        }
    }
} else {

    if ($_GET) {

    
        if (isset($_GET['m'])) {
            $module = $_GET['m'];
        }
        
        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else if($module == 'gallery') {
            $ORDER_BY = "recordListingId";
        }else{$ORDER_BY = "display_order";}

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ", "", trim($_SEARCH_QUERY));
    // get all the news details
    $objSearchParam = new stdClass();
    $objAlbum->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objAlbum->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
    
    if($module == 'gallery'){
        
         
        $objImage->searchStr = ''; //serch not yet implemnted
        $objImage->imageObject = $controller;
        $objImage->imageObjectKeyId = $id;
        $totalNumberOfMenus = $objImage->countRec();
        $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
    }
}

if ($action == 'add' || $action == 'edit' && $module != 'gallery') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}elseif ($module == 'gallery') {
    $CONTENT = MODULE_TEMPLATES_PATH . "gallery/gallery.tpl.php";

    if ($action == 'edit') {

        $imgDtls = $objImage->getImage($id);

        $recordId = $imgDtls->recordId;
        $recordText = $imgDtls->recordText;
        $recordListingId = $imgDtls->recordListingId;
        $image_object = $imgDtls->imageObject;
        $image_title = $imgDtls->image_title;
        $image_description = $imgDtls->image_description;
        $image_link = $imgDtls->image_link;
        $image_object_keyId = $imgDtls->imageObjectKeyId;
        $specialTag = $imgDtls->specialTag;

        $CONTENT = MODULE_TEMPLATES_PATH . "gallery/form.tpl.php";
    }
} else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>