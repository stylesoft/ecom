<script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: "../uploadFile.php",
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#txtSmallFileName").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                    
                        
                        
		}
	);
	</script>


<div id="search_main_wrapper">

    <h2><?php if ($action == 'add') { ?> Add <?php } else { ?> Edit <?php } ?> Album</h2>
</div>

<div id="table_main_wrapper">



    <div id="dashboard" style="background-color:#FFF;">







        <form action="" method="post" name="form1" id="form1">
            <div  id="two">
                <div>

                    <h2>Album Info</h2>
                    <fieldset style="margin-top:35px;">

                        <div id="separator" style="display: none;">
                            <label for="pagetitle">Album Page : </label>                            
                            <select name="album_page" id="album_page" style="width:220px;" tabindex="3">
                                <option value="1" <?php if ($album_page == 1) { ?> selected="selected" <?php } ?>>Home</option>
                                <?php foreach ($arrPages as $key => $pageData) { ?>
                                <option value="<?php print($pageData->id);?>" <?php if ($pageData->id == $album_page) { ?> selected="selected" <?php } ?>><?php print(ucfirst($pageData->title));?></option>
                                <?php }?>
                                
                            </select>
                        </div>
                        
                        
                        <?php 
                        $contentImage = '';
                        
                        if($albumImage != ''){
                            $contentImage = SITE_BASE_URL.'imgs/'.$albumImage;
                        } else {
                            $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                        }
                        
                        ?>
                        
                        <div id="separator">
                            <label for="pagetitle">Album Image : </label>
                            <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg" id="btnImg"/>
                    <input type="hidden" id="txtSmallFileName" name="txtSmallFileName" value="<?php print($albumImage);?>" />
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Album Title : </label>
                            <input name="albumTitle" type="text" id="albumTitle" tabindex="0"  value="<?php print($album_title); ?>" size="64" style="width:600px;"/>
                        </div>
                        
                        
                        
                        <div id="separator">
                            <label for="keywords">Keywords/Tags : </label> 
                            <input name="albumKeywords" type="text" id="albumKeywords" tabindex="1"  value="<?php print($keywords); ?>" size="64" style="width:600px;"/>
                        </div>



                        <div id="separator">
                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description" tabindex="2"><?php print($description); ?></textarea>
                        </div>
                        <div style="clear:both;"> </div>


                        <div id="separator">
                            <label for="status">Album Status : </label> 
                            <select name="status" id="status" style="width:220px;" tabindex="3">
                                <option value=""></option>
                                <option value="1" <?php if ($status == 1) { ?> selected="selected" <?php } ?>>Published</option>
                                <option value="0" <?php if ($status == 0) { ?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                        </div>



                    </fieldset>
                </div>







                <p>
                    <input type="hidden" name="id" id="id" value="<?php print($id); ?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action); ?>" />
                    <input type="hidden" name="listingId" id="listingId" value="<?php print($display_order); ?>" />
            </div>
            <input id="testbutton" type="submit" value="submit" name="Submit" tabindex="4"/> 
            <input id="resetbutton" type="reset"  />
            </p>
            <div id="show"></div>


        </form>

        <div style="clear:both;">        </div>



        <div style="height:10px;"></div>
    </div>	
</div>
