<?php
/** '
 * @desc : list all the images to the album 
 */
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$orderBy = "";
$order = "";
$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

$controller = '';
if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }
    
    if(isset($_GET['c'])){
        $controller = $_GET['c'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'recordListingId' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}




$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objAlbumImage = new Image();
$objAlbumImage->tb_name = 'tbl_images';

$objAlbumImage->searchStr = $searchQ;
$objAlbumImage->limit = $recLimit;
$objAlbumImage->listingOrder = $orderStr;
$objAlbumImage->imageObject = $controller;
$objAlbumImage->imageObjectKeyId = $id;
$albumImageResult = $objAlbumImage->search();
?>



<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			 $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/updateAlbumImageOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

function deleterow(id){
if (confirm('Are you sure want to delete this Album and all its Images?')) {
$.post('<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/deletealbumimg.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder_"+id).fadeOut("slow");
//$(".message").delay(2000).fadeOut(1000);
return false;
});
}
}

</script>




<div id="list">
<div id="table_main_div" >
     <div id="response"> </div>
                    	<div class="row_heding">
                        	
                                <div class="colum" style="width:70px;">
                                    <strong>ID&nbsp;</strong>
                                    <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?m=gallery&c=Image_Gallery&id=<?php print($id); ?>&q=<?php print($searchQ); ?>&orderby=recordId&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                                    <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?m=gallery&c=Image_Gallery&id=<?php print($id); ?>&q=<?php print($searchQ); ?>&orderby=recordId&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
                                </div>
                                <div class="colum" style="width:340px;">
                                    <strong>Title</strong>
                                    <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?m=gallery&c=Image_Gallery&id=<?php print($id); ?>&q=<?php print($searchQ); ?>&orderby=image_title&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                                    <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?m=gallery&c=Image_Gallery&id=<?php print($id); ?>&q=<?php print($searchQ); ?>&orderby=image_title&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
                                </div>
                                
                                <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
                            </div>
                            <div class="clear"></div>
                    	
                            <ul>
                            <?php
                            if (count($albumImageResult) > 0) {
                                foreach ($albumImageResult As $rowIndex => $albumData) {
                                    ?> 
                                
                                <li id="arrayorder_<?php print($albumData->recordId); ?>">
                            <?php if($rowIndex%2 != 0){?>
                            <div class="row1">
                             <?php } else {?>
                                 <div class="row2">
                              <?php } ?>
                        	<div class="colum" style="width:70px;">
                                    <img src="<?php print(SITE_BASE_URL); ?>includes/extLibs/mythumb.php?file=../../imgs/<?php print($albumData->recordText); ?>&width=48&height=48" />
                                </div>
                            <div class="colum" style="width:340px;"><?php print($albumData->image_title); ?></div>
                          
                            <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                
                                <a href="<?= $module ?>.html?action=edit&m=gallery&id=<?php print($albumData->recordId); ?>">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#" onclick="deleterow(<?php echo $albumData->recordId;?>)">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                </a>
                            </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                                
                                <?php } }else{?>
                            <li>
                                <div id="listings" style="background:#eee;">No Matching Records Found </div>
                            </li>
                            <?php }?>
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>


