<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$id = $_POST['id'];

$objAlbum = new Album();
$objAlbum->tb_name = 'tbl_album';

if ($_POST['id'] != ''){
    $objAlbum->id = $id;
    if($objAlbum->deleteAlbum()){
        
        $objImage = new Image();
        $objImage->tb_name = 'tbl_images';
        
        $objKeyId = $objAlbum->id;
        $imgObject = 'Image_Gallery';
         //get all the images for the album
        $imageData = $objImage->getAllByImageObject($imgObject, $objKeyId);

        if($objImage->deleteImageByObject($imgObject,$objKeyId)){
            foreach ($imageData as $data){
                 unlink('../../../../imgs/' . $data->recordText);
            }
        }
        //TODO: delete listing images from the album
        //header('Location: ../../../news.html');
    }
	
}
?>
