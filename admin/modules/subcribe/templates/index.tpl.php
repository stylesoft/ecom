<script type="text/javascript">
	
    $(document).ready(function(){
		
        //Display Loading Image
        function Display_Load()
        {
            $("#manageloading").fadeIn(900,0);
            $("#manageloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        //Hide Loading Image
        function Hide_Load()
        {
            $("#manageloading").fadeOut('slow');
        };
	
        //Default Starting Page Results
   
        $("#pagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
	
        Display_Load();
	
        $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/subcribe/includes/pagination_data.php?q=<?php print($_SEARCH_QUERY); ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=subcribe&orderby=<?php print($orderBy); ?>&order=<?php print($order); ?>", Hide_Load());

        //Pagination Click
        $("#pagination li").click(function(){
			
            Display_Load();
		
            //CSS Styles
            $("#pagination li")
            .css({'border' : 'solid #dddddd 1px'})
            .css({'color' : '#0063DC'});
		
            $(this)
            .css({'color' : '#FF0084'})
            .css({'border' : 'none'});

            //Loading Data
            var pageNum = this.id;
		
            $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/subcribe/includes/pagination_data.php?page=" + pageNum+"&rows=<?php print($_REC_PER_PAGE); ?>&m=subcribe&orderby=<?php print($orderBy); ?>&order=<?php print($order); ?>", Hide_Load());
        });

    });
</script>

<div id="search_main_wrapper">
    <div class="search_wrapper2">
        <div class="search_L"></div>
        <div class="search_bg">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr>
                        <td height="33" width="67%"><h2>The Subcribe Listing</h2></td>
                        <td align="right" width="27%">&nbsp;</td>
                        <td width="0%"></td>
                        <td width="3%">&nbsp;</td>
                        <td width="2%"></td>
                        <td valign="top" width="1%"></td>
                        <td width="0"></td>
                    </tr>
                </tbody></table>              
        </div>
        <div class="search_R"></div>
    </div>
</div>

<div id="table_main_wrapper">
    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard"></div>
    </div>

    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                          <td align="center">
                           <div style="margin-top:45px;"><a href="<?php print(ADMIN_BASE_URL); ?>subcribe/subcribe.html?action=add" id="testbutton">Add New Subcribe</a></div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <form action="" method="get" name="form1" id="search">
                                <div align="center"><label for="searchtext">Search:</label>
                                    <input type="text" name="q" id="q" />
                                    <input type="submit" id="searchbutt" value="" name="search" />
                                </div>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:5px">	
                                <table width="750px">
                                    <tr>
                                        <td width="30">Pages:</td>
                                        <td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td>
                                        <td width="90">Rows per page :</td>
                                        <td width="30">
                                            <select onchange="javascript:goToPage(options[selectedIndex].value)" id="cmbRowsPerPage" name="cmbRowsPerPage" style="width: 50px;">
                                                <option value=""></option>

                                                <?php for ($row_num = 10; $row_num <= 200; $row_num = $row_num + 10) { ?>
                                                    <option value="subcribe.html?q=<?php print($_SEARCH_QUERY); ?>&rows=<?php print($row_num); ?>" <?php if($_GET['rows']==$row_num)echo "selected=selected"?>><?php print($row_num); ?></option>
                                                <?php } ?>
                                            </select> 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">




                            <div id="manageloading" ></div>
                            <div id="managecontent" ></div>




                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:40px">	<table width="800px">
                                    <tr><td width="30">Pages:</td><td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td></tr></table></div>

                        </td>
                    </tr>

                </tbody></table>
        </div>
    </div>
    <div style="height:10px;"></div>
</div>
