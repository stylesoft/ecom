<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');
require_once('../../../application/classes/Subcribe.php');

$id = '';
$email  = '';
$type = '';



// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrSubcribe= array();

$objSubcribe = new Subcribe();
$objSubcribe->tb_name = 'tbl_subscribers';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                //$arrSubcribe = $objSubcribe->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {             
            $subcribeInfo = $objSubcribe->getSubcribe($id);

            $id = $subcribeInfo->id;
            $email = $subcribeInfo->email;
            $type = $subcribeInfo->type;
            
   
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}

if ($_POST) {
//print_r($_POST);
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $email = $_POST['email'];
    $type = $_POST['status'];
    
    
    
   

    //validation
    if($email == ""){
            array_push($objSubcribe->error, 'The email is required');
            $invalid = true;
        }

    $objSubcribe->id = $id;
    $objSubcribe->email = $email;
    $objSubcribe->type = $type;
    
    if ($action == 'add' && !$invalid) {
    	
          $lastInsertedId = $objSubcribe->addSubcribe();
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objSubcribe->editSubcribe();
    }

    $newPageUrl = "subcribe.html?action=add";
    $mainPageUrl = "subcribe.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Subcribe <span class='red'>$email</span> has been added!<br /><br /><br />Want to enter more Email?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Subcribe <span class='red'>$email</span> has been updated!<br /><br /><br />Want to enter more Email?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {
	
 if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }
       
        if(isset($_GET['orderby'])){
        	$orderBy = $_GET['orderby'];
        }
        
        if(isset($_GET['order'])){
        	$order = $_GET['order'];
        }
        
        if($orderBy != '' && $order != ''){
        	$orderStr       = $orderBy." ".$order;
        } else {
        
        	$orderStr       = 'id'." ".'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }


    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objSubcribe->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objSubcribe->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}




if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
