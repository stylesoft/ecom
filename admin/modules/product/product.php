<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id                 = ''; 	
$productName = ''; 	
$productSmallDescription = ''; 	
$unitPrice  = '';	
$discountPrice  = '';	
$priceCurrency = ''; 	
$stockInHand = ''; 	
$reorderLevel = ''; 	
$category  = ''; 	
$addedOn  = '';	
$addedBy = ''; 	
$status = ''; 	
$featured  = '';	 
$productDescription = ''; 	
$keywords = ''; 	
$displayOrder = '';
$isSpecialOffer = '';
$weight_unit = '';
$product_weight = '';
$productCode   = '';
$productBrand  = '';

//product Display Settings...
$brand_display_style = '';
$category_display_style = '';
$Comparestatus = '';

// product Settings...
$VATValue = '';
$guest_amount = '';

$firstParentCategoryID = "";
$parentCategoryID      = "";
$firstParentSubCategories  = "";
$secondParentSubCategories = "";



// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$currencies = "";


// stock 

$objStock = new Stock();
$objStock->tb_name = 'tbl_stock';

// Menu field 
$objMenuField = new MenuField();
$objMenuField->tb_name = 'tbl_menu_data';

//Menu 

$objMenu = new Menu();
$objMenu->tb_name = 'tbl_menu';



// get all currencies....
$objCurrency = new Currency();
$currencies = $objCurrency->getAll();


$objProduct = new Product();
$objProduct->tb_name = 'tbl_product';


$objCategory = new Category();
$objCategory->tb_name = 'tbl_category';
$objCategory->searchStr    = '';
$objCategory->limit        = '';
$objCategory->listingOrder = '';
$categoryResult            = $objCategory->getAllParentCategory('Enabled');


$objBrand = new Brand();
$objBrand->tb_name = 'tbl_brand';
$objBrand->status = 'Enabled';
$brandsInfo  = $objBrand->search();

//Product Display Settings..
$objDisplaySettings = new ProductDisplaySettings();
$objDisplaySettings->tb_name = 'tbl_product_display_settings';

//Product Settings..
$objProductSettings  = new ProductSettings();
$objProductSettings->tb_name = 'tbl_product_settings';



if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit') {

        if (isset($_GET['id'])) {

            $productId = $_GET['id'];

            $productInfo = $objProduct->getProduct($productId);

            $id = $productInfo->id;
            $productName = $productInfo->productName; 	
            $productSmallDescription = $productInfo->productSmallDescription; 	
            $unitPrice  = $productInfo->unitPrice;	
            $discountPrice  = $productInfo->discountPrice;	
            $priceCurrency = $productInfo->priceCurrency; 	
            $stockInHand = $productInfo->stockInHand; 	
            $reorderLevel = $productInfo->reorderLevel; 	
            $category  = $productInfo->category; 	
            $addedOn  = $productInfo->addedOn;	
            $addedBy = $productInfo->addedBy; 	
            $status = $productInfo->status; 	
            $featured  = $productInfo->isFeatured;	 
            $productDescription = $productInfo->productDescription; 	
            $keywords = $productInfo->keywords; 	
            $displayOrder = $productInfo->displayOrder;
            $isSpecialOffer = $productInfo->isSpecialOffer;
            $weight_unit = $productInfo->weightUnit;
            $product_weight = $productInfo->productWeight;
            $productCode   = $productInfo->productCode;
            $Comparestatus = $productInfo->Comparestatus;
            if($productInfo->productBrand){
                $productBrand   = $productInfo->productBrand->id;
            } else {
                $productBrand   = null;
            }
            $productSize   = $productInfo->productSize;
            

            $productCategoryID      = $category->id;
            $parentCategoryID      = $category->parent_category;
            if($parentCategoryID == 0){
                $parentCategoryID = $productCategoryID;
            }
            
            //$firstParentCategoryID = $objCategory->getParentCategory($parentCategoryID);
            $firstParentSubCategories = $objCategory->getAllSubCategory($parentCategoryID);
            $secondParentSubCategories = $objCategory->getAllSubCategory($parentCategoryID,'Enabled');
            
            
            
        }
    } elseif($action == 'addToFeatured'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isFeatured = 'yes';	 
        $isRecordUpdated = $objProduct->updateFeatured();
        header('Location: product.html?action=featured');
    }elseif($action == 'removeFeatured'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isFeatured = 'no';	 
        $isRecordUpdated = $objProduct->updateFeatured();
        header('Location: product.html?action=featured');
    }elseif($action == 'addToSpecialOffer'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isSpecialOffer = 'Yes';	 
        $isRecordUpdated = $objProduct->updateSpecialOffer();
        header('Location: product.html?action=specialOffers');
    }elseif($action == 'removeSpecialOffer'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isSpecialOffer = 'No';	 
        $isRecordUpdated = $objProduct->updateSpecialOffer();
        header('Location: product.html?action=specialOffers');
    }
    
}

if ($_POST) {
    //print_r($_POST); exit;
    
    $err_array = array();
    $tstamp = time();
        $tstamp = gmdate("Y-m-d H:i:s", $tstamp);
        $stockInHand = $_POST['txtStockInHand'];

        $stock = $_POST['stock'];
        
        $Editsotck = $_POST['Editstock'];
        $updatestock = $_POST['stockID'];
      // print_r($updatestock); exit;
        
        if($action == 'add'){
        
        if(!count($stock)){
        	array_push($err_array, "Please select a product Option");
        	
        
        }
        
        foreach ($stock as $stockInd => $stockData){
        	 
        	$totalstock = $stockData+$totalstock;
        	 
        }
        
        }
        
        
        if($action == 'edit'){
        	
        	//print_r($Editsotck);
        	//exit();
        	if(count($Editsotck)){

        		foreach ($Editsotck as $index => $updatedstock){
        		
        		
        		$id = $updatestock[$index];
        		
        		
        		$objStock->stock = $Editsotck[$index];
        			
        		
        		$isRecordUpdated = $objStock -> Updatestock($id);
        		
        		}
        	}
       
        	
        	
        	if (!count($stock) && !count($Editsotck) ){
        		array_push($err_array, "Please select Minimum 1 product option");
        	}
        	
        	
        	if(!count($stock)){
        	
        		foreach ($Editsotck as $updatedstock){
        			
        			$totalstock = $updatedstock+$totalstock;
        		
        		}

        	} else {
        		
        		foreach ($stock as $stockInd => $stockData){
        		
        			$totalstock = $stockData+$totalstock;
        		
        		}
        		
        		if(count($Editsotck)){
        			
        			foreach ($Editsotck as $updatedstock){
        				 
        				$Curenttotalstock = $updatedstock+$Curenttotalstock;
        			
        			}
        			$Curenttotalstock;
        			$totalstock = $totalstock+$Curenttotalstock;
        			
        		}
        		
        	}
        	
        	
        	
        	$isRecordUpdated = $objProduct-> Updatestock();
        	
        }
        
        
        
        $parentMenu_id = $_POST['parentMenu_id'];
        
        $super_parentMenu = $_POST['SuperparentMenu_id'];
        
       
        
    
        

    $action = $_POST['txtAction'];
    
    //Product Settings..
    
    $VATValue = $_POST['VATValue'];
    $guest_amount = $_POST['guest_amount'];
    
    //Product display settings....
    $brand_display_style = $_POST['brand_display_style'];
    $category_display_style = $_POST['category_display_style'];
    
    //Product Compare..
    $Comparestatus = $_POST['Comparestatus']  ; 
       

    $id = ($action == 'add') ? "" : $_POST['id'];
    $productName = mysql_real_escape_string($_POST['txtProductName']); 
    //validation..
    if ($productName == ''){
        array_push($err_array, "Product Name is Required");
    }
    
    
    
    
    $productSmallDescription = mysql_real_escape_string($_POST['description']); 	
    $unitPrice  = $_POST['txtUnitPrice'];
    
    if(!is_numeric($unitPrice)){
         array_push($err_array, "Please enter a valid number for Unit Price");
    }
    
    $discountPrice  = $_POST['txtUnitPriceD'];	
    $priceCurrency = $_POST['cmbPriceCurrency']; 	
    
    //validation. (Do not validate the $stockInHand the stock will be added dyanamically )
    
    
    
   // if(!is_numeric($stockInHand)){
       //  array_push($err_array, "Please enter a valid number for Stock in Hand");
   // }
    
    

    $reorderLevel = $_POST['txtReorderLevel']; 	
    if($_POST['cmbProductSubCategory'] && $_POST['cmbProductSubCategory'] != ""){
        $category  = $_POST['cmbProductSubCategory']; 
    } else {
        $category  = $_POST['cmbProductCategory']; 
    }
    
    $addedOn  = $tstamp;	
    $addedBy = '1'; 	
    $status = $_POST['status']; 	
    $featured  = $_POST['cmbIsFeatured'];	 
    $productDescription = mysql_real_escape_string($_POST['mainbody']); 	
    $keywords = $_POST['keywords']; 	
    $displayOrder = $_POST['displayOrder'];
    $isSpecialOffer = $_POST['cmbIsSpecialOffer'];
    $weight_unit = $_POST['cmbProductWeightU'];
    $product_weight = $_POST['txtProductWeight'];
    $productCode   = ($_POST['txtProductCode'] == '') ? "" : $_POST['txtProductCode'];//$_POST['txtProductCode'];
    
    $productBrand   = ($_POST['cmbProductBrand'] == '') ? "" : $_POST['cmbProductBrand'];//$_POST['txtProductCode'];
    $productSize   = ($_POST['cmbProductSize'] == '') ? "" : $_POST['cmbProductSize'];//$_POST['txtProductCode'];

    $objProduct->id = $id;
    $objProduct->productName = $productName; 	
    $objProduct->productSmallDescription = $productSmallDescription; 	
    $objProduct->unitPrice = $unitPrice;	
    $objProduct->discountPrice = $discountPrice;	
    $objProduct->priceCurrency = $priceCurrency; 	
    $objProduct->stockInHand = $totalstock; 	
    $objProduct->reorderLevel = $reorderLevel; 	
    $objProduct->category = $category; 	
    $objProduct->addedOn = $addedOn;	
    $objProduct->addedBy = $addedBy; 	
    $objProduct->status = $status; 	
    $objProduct->isFeatured = $featured;	 
    $objProduct->productDescription = $productDescription; 	
    $objProduct->keywords = $keywords; 	
    $objProduct->displayOrder = $displayOrder;
    $objProduct->isSpecialOffer = $isSpecialOffer;
    $objProduct->weightUnit = $weight_unit;
    $objProduct->productWeight = $product_weight;
    $objProduct->productCode = $productCode;
    $objProduct->Comparestatus = $Comparestatus;
    
    $objProduct->productBrand = $productBrand;
    $objProduct->productSize = $productSize;
    

    $currentProductId = '';
    
   
    if(empty($err_array)){
    if ($action == 'add') {
    	
    
       $lastInsertedId = $objProduct->addProduct();

  $currentProductId = $lastInsertedId;
        
// adding stock 
  foreach ($stock as $stockInd => $stockData){
  
  
  
  
  	$objStock->id='';
  	$objStock->menu_id=$parentMenu_id[$stockInd];
  	$objStock->product_id=$currentProductId;
  	$objStock->stock=$stockData;
  	$objStock->parent_menu_id =$super_parentMenu[$stockInd];
  
  
  	$lastInsertedId = $objStock->addStock();
  
  
  }
   

  //$isRecordUpdated = $objProduct-> Updatestock();
  

        
        
        
        
        
        
        
        

        $currentProductId = $lastInsertedId;

    } else if ($action == 'edit') {
        $currentProductId = $id;
    $isRecordUpdated = $objProduct->editProduct();
    
 
    
    if (count($stock)) {
    	
    	
    	
    	
    	
    	foreach ($stock as $stockInd => $stockData){
    	
    //	echo $parentMenu_id[$stockInd];
    	//echo $currentProductId;
    	//echo $stockData;
    	//echo $super_parentMenu[$stockInd];
    		
    		
    		
    	//exit();
    		$objStock->id='';
    		$objStock->menu_id=$parentMenu_id[$stockInd];
    		$objStock->product_id=$currentProductId;
    		$objStock->stock=$stockData;
    		$objStock->parent_menu_id =$super_parentMenu[$stockInd];
    		
    		$lastInsertedId = $objStock->addStock();
    	
    	}
      }
   
      //exit();
   
    
    
    
    
    
    
    
    
    }
    }
    
    if ($action == 'display_settings') {
        $objDisplaySettings->brand_display_style=  $brand_display_style;
        $objDisplaySettings->category_display_style=$category_display_style;
        $isSettingsUpdated = $objDisplaySettings->editProductDisplaySettings();
            
    }
    
    if ($action == 'product_settings') {
        
        $objProductSettings->VATValue = $VATValue;
        $objProductSettings->guest_amount = $guest_amount;
        $isProductSettingsUpdated  = $objProductSettings->editProductSettings();
        }
    
    

    
    
    
    
    $newPageUrl = "../images.php?m=product&c=Product&id=".$currentProductId;
    $mainPageUrl = "product.html";
    $menu_url = "../menu/menu.html?action=addmenu&Pid=$lastInsertedId";
    $menu_url_edit ="../menu/menu.html?action=addmenu&Pid=$id";

    if ($lastInsertedId) {
        
           print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Product <span class='red'>$productName</span> has been added!<br /><br /><br />Select a option below <br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Add Images</a><a href='$mainPageUrl'  class='mySmallButton'>Product List</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        
        
       // echo "<div id='coverit'></div><div id='message'>Product <span class='red'>$productName</span> has been added!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Product <span class='red'>$productName</span> has been added!<br /><br /><br />Select a option below <br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Add Images</a>
        		<a href='$mainPageUrl'  class='mySmallButton'>Product List</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Menu <span class='red'>$productName</span> has been updated!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }elseif($isSettingsUpdated){
        
      $PageUrl = "product.html?action=display_settings";

     print("<div id='boxes'>");
     print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
     print("The record has been updated!<br /><br /><br /><a href='$PageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
     print("</div>");
     print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
     print("</div>");
    //print("The post came here....."); exit;
    
    }elseif($isProductSettingsUpdated){
        
      $PageUrl = "product.html?action=product_settings";

     print("<div id='boxes'>");
     print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
     print("The record has been updated!<br /><br /><br /><a href='$PageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
     print("</div>");
     print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
     print("</div>");
    //print("The post came here....."); exit;
    
    }
    
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }
        
        if(isset($_GET['brand'])){
                $productBrand = $_GET['brand'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "display_order";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the product details
    $objSearchParam = new stdClass();
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->productBrand = $productBrand;
    $totalNumberOfMenus = $objProduct->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
} elseif($action == 'featured'){
    
    // count featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isFeatured = 'Yes';
    $totalNumberOfProducts = $objProduct->countRec();
    $featuredProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
    
    // count non featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isFeatured = 'No';
    $totalNumberOfProducts = $objProduct->countRec();
    $nonfeaturedProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
   
    
     $CONTENT = MODULE_TEMPLATES_PATH . "featured.tpl.php";
     
     
     
  } elseif($action == 'specialOffers'){
    
    // count featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isSpecialOffer = 'Yes';
    $totalNumberOfProducts = $objProduct->countRec();
    $specialOfferProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);

    
    // count non featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isSpecialOffer = 'No';
    $totalNumberOfProducts = $objProduct->countRec();
    $nonSpecialOfferProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
    
    
     $CONTENT = MODULE_TEMPLATES_PATH . "specialOffers.tpl.php";   
     
     
     
}elseif($action == 'display_settings'){
   
    $ProductDisplayInfo = $objDisplaySettings->getProductDisplaySettings();
    
    $brand_display_style= $ProductDisplayInfo->brand_display_style;
    $category_display_style = $ProductDisplayInfo->category_display_style;
        
    $CONTENT = MODULE_TEMPLATES_PATH . "display_settings.tpl.php";   

}elseif($action == 'product_settings'){
   
    $ProductSettingsInfo = $objProductSettings->getProductSettings();
    
    $VATValue= $ProductSettingsInfo->VATValue;
    $guest_amount = $ProductSettingsInfo->guest_amount;
    
        
    $CONTENT = MODULE_TEMPLATES_PATH . "product_settings.tpl.php";   

}
else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}


$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>