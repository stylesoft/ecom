<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id = '';
$name  = '';
$code  = '';
$codeStr  = '';
$isDefault  = ''; 
$status  = 'Disabled'; //Enabled', 'Disabled
//$displayOrder = "";


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrCurrency = array();

$objCurrency = new Currency();
$objCurrency->tb_name = 'tbl_currency';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                $arrCurrency = $objCurrency->getCurrency($id);
                
    if ($action == 'edit') {             
            $currencyInfo = $objCurrency->getCurrency($id);

            $id = $currencyInfo->id;
            $name = $currencyInfo->name;
            $code = $currencyInfo->code;
            $codeStr = $currencyInfo->codeStr;
            $isDefault = $currencyInfo->isDefault;
            $status = $currencyInfo->status;
            //$displayOrder = $categoryInfo->displayorder;
   
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}

if ($_POST) {

    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $name = $_POST['name'];
    $code = $_POST['code'];
    $codeStr = $_POST['codeStr'];
    $isDefault = $_POST['isDefault'];
    //$displayOrder = $_POST['txtDisplayOrder'];
  
    
    
    
    if(isset($_POST['status'])) $status = $_POST['status'];

    //validation
    if($name == ""){
            array_push($objCurrency->error, 'The Currency name is required');
            $invalid = true;
        }

    $objCurrency->id = $id;
    $objCurrency->name = $name;
    $objCurrency->code = $code;
    $objCurrency->codeStr = $codeStr;
    $objCurrency->isDefault = $isDefault;
    $objCurrency->status = $status;
    //$objCurrency->displayorder = $displayOrder;
    
    if ($action == 'add' && !$invalid) {
          $lastInsertedId = $objCurrency->addCurrency();
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objCurrency->editCurrency();
    }

    $newPageUrl = "currency.html?action=add";
    $mainPageUrl = "currency.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Currency <span class='red'>$name</span> has been added!<br /><br /><br />Want to add a new Currency?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Currency <span class='red'>$name</span> has been updated!<br /><br /><br />Want to add a new Currency?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "id";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'ASC';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objCurrency->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objCurrency->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
