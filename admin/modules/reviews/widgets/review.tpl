<ul class="reveiw">
<?php
if (count($reviewResult) != 0) {
    foreach ($reviewResult As $indexre => $reviewRes) {
        if (isset($_SESSION['SESS_CUSTOMER_INFO'])) {
         $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
        $customerId_list = $customerInfo->id;
    }else{
        $customerId_list = 'Guest';
    }

        $customerid = $reviewRes->added_by;
        $customerInfo = $objCustomer->getCustomer($customerid);
        ?>
<!--            <li <?php //if($reviewRes->highlighted =='Yes' && $customerId_list == $reviewRes->added_by) {?>style="border: 1px solid #524583"<?php ?>>-->
    <li>
                <h4 class="title"><?php print($customerInfo->firstName) ?></h4>said&nbsp;<?php print($reviewRes->added_on) ?>
                <br/>
            <fieldse class="rating">

                <input type="radio"  <?php if ($reviewRes->rating == '5') { ?>checked="checked"<?php } ?>/><label for="star5" title="5 stars">5 stars</label>
                <input type="radio"  <?php if ($reviewRes->rating == '4') { ?>checked="checked"<?php } ?>/><label for="star4" title="4 stars">4 stars</label>
                <input type="radio"  <?php if ($reviewRes->rating == '3') { ?>checked="checked"<?php } ?>/><label for="star3" title="3 stars">3 stars</label>
                <input type="radio"  <?php if ($reviewRes->rating == '2') { ?>checked="checked"<?php } ?>/><label for="star2" title="2 stars">2 stars</label>
                <input type="radio"  <?php if ($reviewRes->rating == '1') { ?>checked="checked"<?php } ?>/><label for="star1" title="1 stars">1 star</label>
            </fieldse>






            <span class="reveiwdetails"><b><?php print($reviewRes->reviews_subject) ?></b><br/><?php print($reviewRes->reviews_description) ?> </span>

        </li>
    <?php }
} ?>
</ul>

<?php
if (isset($_SESSION['SESS_CUSTOMER_INFO'])) {
    

    if (count($visited_client) == 0) {
        $action = 'add';
    } else {
        $action = 'edit';
    }

    ?>
    <form class="form-vertical" id="review-submit-form" method = "post" action="<?php print(ADMIN_BASE_URL); ?>modules/reviews/reviews_request.php">

        <fieldse class="rating">

            <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="5 stars">5 stars</label>
            <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="4 stars">4 stars</label>
            <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="3 stars">3 stars</label>
            <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="2 stars">2 stars</label>
            <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="1 stars">1 star</label>
        </fieldse>
        <div id="rate-error" style="color:red; margin-left: 300px; display:none">Please rate this  product</div>


        <h3>Write a Review</h3>  

        <fieldset>
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                    <input type="text" class="span3" name="reviews_subject"><div id="title-error" style="color:red;  display:none">title is required</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                    <textarea rows="3"  class="span3" name="reviews_description"></textarea>
                </div>
                
                <div id="des-error" style="color:red;  display:none">description is required</div>
                
<!--                <label class="control-label"></label>
                <div class="controls">
                    <input type="checkbox" name="highlighted" value ="Yes" />
                    Highlight this Review in the Review List.
                </div>-->
                
                <input type="hidden" name="ip" id="ip" value="<?php echo $ip; ?>"/>
                 
    <?php if ($action == 'edit') {
       
        foreach ($visited_client As $vis) {
            ?>
                        <input type="hidden" name="id" id="id" value="<?php echo $vis->id; ?>"/>
        <?php }
    } ?>
                <input type = "hidden" name ="url" value="<?php echo $url ?>" />
                <input type="hidden" name="product_id" id="product_id" value="<?php echo $productId; ?>"/>
                <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action ?>"/>
                <input type="hidden" name="added_by" id="customerName" value="<?php echo $customerId ?>"/>

            </div>
        </fieldset>
        <input type="submit" class="btn btn-inverse" value="continue" name="submit" id="submit" >
    </form><?php }else{?>
Please <a href="<?php print(SITE_BASE_URL); ?>login.html?product_url=<?php echo $url?>">Login </a>for make a Review here.
    <?php }?>

<script>
    $(document).ready(function() {
        $("#review-submit-form").validate({
            //Submit Handler
            submitHandler: function() {
                $.ajax({
                    type: "POST",
                    url: "<?php print(ADMIN_BASE_URL); ?>modules/reviews/reviews_request.php",
                    data: $("#review-submit-form").serialize(),
                    success: function(Res) {

                        console.log(Res);
                        if (Res == "failure") {
                            $("#review-submit-form").hide('slow').after("<h1 style='color:#F10505;'> failure!</h1>");
                        } else if (Res == "add-success") {
                            //console.log('yes');
                            $("#review-submit-form").hide().after("<h1 style='color:#000;'>Review has been sent!</h1>");
                        } else if (Res == "edit-success") {
                            //console.log('yes');
                            $("#review-submit-form").hide().after("<h1 style='color:#000;'>Successfully updated!</h1>");
                        } else if (Res == "rating-failure") {

                            $("#rate-error").show();

                        } else if (Res == "title-failure") {

                            $("#title-error").show();

                        } else if (Res == "des-failure") {

                            $("#des-error").show();
                        }
                        //$(window).scrollTop(0);

                    },
                    error: function(Res) {
                        $("#review-submit-form").hide('slow').after("<h1 style='color:#000;'>Please submit again!</h1>");
                    }
                });
                return false;
            }
            //Submit Handler
        });
    });
</script>   


<!--
//FORNT END VIEW INTEGRATION CODE

<div id="review"></div>
<script type="text/javascript">
        $.ajax({
              type: "POST",
              url: "<?php print(ADMIN_BASE_URL); ?>modules/reviews/reviews_front.php?r=review",
              dataType: 'json',
              data: 'view=true',
              success: function(R) {
                   $('#review').append(R);
              },
              error: function(Res) {
                   $('#review').append('<div class="alert alert-warning">Error Rendering Review Widget!!</div>');
              }
          });
</script>
-->