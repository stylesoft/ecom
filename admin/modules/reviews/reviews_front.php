<?php
/** '
 * Module : Payment
 * File: Payment.php
 * @author Gayan Chathuranga <gayan.c@pluspro.com>
 */
require_once '../../../bootstrap.php';
require_once(DOC_ROOT.'/admin/modules/reviews/includes/Reviews.class.php');

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";

$controller =  $_GET['r'];

$objReviews = new Reviews();

switch ($controller) {
	
	case 'review':	    
		$objReviews->getFrontWidgetReview();
		break;
	
	default:
		# code...
		break;
}
require_once $LAYOUT;
?>