<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require "class.phpmailer.php";

class Reviews extends Core_Database {

    //news propoerties
    public $id;
    public $reviews_subject;
    public $reviews_description;
    public $reviews_status;
    public $added_by;
    public $added_on;
    public $modified_by ;
    public $modified_on ;
    public $url;
    public $IP ;
    public $rating ;
    public $product_id;
    
    
    public $error = array();
    public $data_array = array();
    
    
    
    

    //constructor
   public function Reviews() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addReviews
     * @param        :   ReviewsegoryObject
     * Description   :   The function is to Reviewsegory details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */


    
    public function addReviews() {
        $recordId = null;
        try {
            $id = $this->id;
            $reviews_subject = $this->reviews_subject;
            $reviews_description = $this->reviews_description;
            $reviews_status = $this->reviews_status;
            $added_by = $this->added_by;
            $added_on = $this->added_on;
            $modified_by = $this->modified_by;
            $modified_on = $this->modified_on;
            $rating     = $this->rating;
            $ipclient = $this->IP;
            $url = $this->url;
            $product_id = $this->product_id;
//            $highlighted = $this->highlighted;
            //print_r($this);
             //$this->tb_name;
                $inserted = $this->insert($this->tb_name, array($id,$added_on,$added_by,$reviews_subject,$reviews_description,$product_id,$ipclient,$url,$rating,$modified_by,$modified_on,$reviews_status));
                
                if ($inserted) {
                	//exit();
                    $recordId = $this->getLastInsertedId();
            }
            //exit();
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }  
    
    
    

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editrReviews() {
        $isUpdated = false;
        try {
            $id = $this->id;
            $reviews_subject = $this->reviews_subject;
            $reviews_description = $this->reviews_description;
            $reviews_status = $this->reviews_status;
            $added_by = $this->added_by;
            $added_on = $this->added_on;
            $modified_by = $this->modified_by;
            $modified_on = $this->modified_on;
            $rating     = $this->rating;
            $url       = $this->url;
//            $highlighted       = $this->highlighted;
                $arrayData = array(
                    'id' => $id,
                    'subject' => $reviews_subject,
                    'description' => $reviews_description,
                    'is_approved' => $reviews_status,
                    'added_by' => $added_by,
                    'added_on' => $added_on,
                    'rating'       => $rating,
                    'modified_by'=>$modified_by,
                    'modified_on'=>$modified_on,
                    'url'  =>$url
                   
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteReviews() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getReviews($ReviewsId) {
        $objReviews = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $ReviewsId;
                $this->select('tbl_reviews', $colums, $where);
                $ReviewsInfo = $this->getResult();
                $objReviews->id = $ReviewsInfo['id'];
                $objReviews->reviews_subject = $ReviewsInfo['subject'];
                $objReviews->reviews_description = $ReviewsInfo['description'];
                $objReviews->reviews_status = $ReviewsInfo['is_approved'];
                $objReviews->added_by = $ReviewsInfo['added_by'];
                $objReviews->added_on = $ReviewsInfo['added_on'];
                $objReviews->modified_by = $ReviewsInfo['modified_by'];
                $objReviews->modified_on = $ReviewsInfo['modified_on'];
                $objReviews->IP = $ReviewsInfo['IP'];
                $objReviews->url = $ReviewsInfo['url'];
                $objReviews->rating = $ReviewsInfo['rating'];
//                $objReviews->highlighted = $ReviewsInfo['highlighted'];
              
                 
            }
            return $objReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all Reviewsegory details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrReviews = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $reviewResult = $this->getResult();
                foreach ($reviewResult As $ReviewsRow) {
                    $ReviewsId = $ReviewsRow['id'];
                    $ReviewsInfo = $this->getReviews($ReviewsId);
                    array_push($arrReviews, $ReviewsInfo);
                }
            }

            return $arrReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all Reviewsegory details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrReviews = array();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = "is_approved = '" . $status . "'";
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $ReviewsRes = $this->getResult();
                
               // echo "yes";
               //print_r($ReviewsRes);
                foreach ($ReviewsRes As $nIndex => $ReviewsRow) {
                    $ReviewsId = $ReviewsRow['id'];
                    $ReviewsInfo = $this->getReviews($ReviewsId);
                   // print_r($ReviewsInfo);
                    array_push($arrReviews, $ReviewsInfo);
                }
            }
           // print_r($arrReviews);
            return $arrReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all Reviewsegory details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByIP($IP) {
        $arrReviews = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "IP = '" . $IP . "'";
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $ReviewsRes = $this->getResult();
                //print_r($ReviewsRes);
                foreach ($ReviewsRes As $nIndex => $ReviewsRow) {
                    $ReviewsId = $ReviewsRow['id'];
                    $ReviewsInfo = $this->getReviews($ReviewsId);
                    array_push($arrReviews, $ReviewsInfo);
                }
            }
            return $arrReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all Reviewsegory details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

//    public function getAllParentByStatus($status,$Reviews_id) {
//        $arrReviews = array();
//        try {
//            if ($this->connect()) {
//                $colums = 'id';
//                $where = "id != '".$Reviews_id."' AND status = '" . $status . "'";
//                $orderBy = "brand_name ASC";
//                $this->select($this->tb_name, $colums, $where, $orderBy);
//                $ReviewsRes = $this->getResult();
//
//                foreach ($ReviewsRes As $nIndex => $ReviewsRow) {
//                    $ReviewsId = $ReviewsRow['id'];
//                    $ReviewsInfo = $this->getReviews($ReviewsId);
//                    array_push($arrReviews, $ReviewsInfo);
//                }
//            }
//            return $arrReviews;
//        } catch (Exception $e) {
//            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
//        }
//    }
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateReviews(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$reviews_status 			= $this->reviews_status;                               
				$arrayData          = array('is_approved'=>$reviews_status);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all Reviewsegory details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

//    public function getAllByReviewsObject($param = '') {
//        $arrReviews = array();
//        try {
//
//            $colums = 'id';
//            $where = "id != '" . $Reviews_id . "' AND status = '" . $status . "'";
//            $orderBy = "brand_name ASC";
//            $this->select($this->tb_name, $colums, $where, $orderBy);
//            $ReviewsRes = $this->getResult();
//
//            foreach ($ReviewsRes As $nIndex => $ReviewsRow) {
//                $ReviewsId = $ReviewsRow['id'];
//                $ReviewsInfo = $this->getReviews($ReviewsId);
//                array_push($arrReviews, $ReviewsInfo);
//            }
//
//            return $arrReviews;
//        } catch (Exception $e) {
//            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
//        }
//    }
//    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active Reviewsegories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   12-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "brand_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $ReviewsRes = $this->getResult();
            $totalNumberOfRec = count($ReviewsRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   Reviewsegory_name
     * @desc         :   The function is to search  Reviewsegory details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrReviews = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "subject LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

	    
            if ($this->IP != '') {
                    array_push($arrWhere, "IP = '" . $this->IP . "'");
            }
            if ($this->product_id != '') {
                    array_push($arrWhere, "product_id = '" . $this->product_id . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder != '') {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }
            else{
                $SQL.= ' ORDER BY id ASC ';
            }

            if ($this->limit != '') {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $ReviewsRes = $this->getResult();
           
           
            foreach ($ReviewsRes As $ReviewsRow) {
               
                 $id = $ReviewsRow['id'];
                $ReviewsInfo = $this->getReviews($id);
                array_push($arrReviews, $ReviewsInfo);
            }
            return $arrReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    public function searchApproved() {
        $arrReviews = array();
        $arrWhere = array();
     
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "subject LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

	    if ($this->reviews_status != '') {
                    array_push($arrWhere, "is_approved = '" . $this->reviews_status . "'");
            }
            
            if ($this->product_id != '') {
                    array_push($arrWhere, "product_id = '" . $this->product_id . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder != '') {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }
            else{
                $SQL.= ' ORDER BY id ASC ';
            }

            if ($this->limit != '') {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $ReviewsRes = $this->getResult();
           
           
            foreach ($ReviewsRes As $ReviewsRow) {
               
                 $id = $ReviewsRow['id'];
                $ReviewsInfo = $this->getReviews($id);
                array_push($arrReviews, $ReviewsInfo);
            }
            return $arrReviews;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
//   public function getip()
//{
//    if ( $_SERVER["HTTP_CLIENT_IP"] && strcasecmp($_SERVER["HTTP_CLIENT_IP"], "unknown") )
//    {
//        $ip = $_SERVER["HTTP_CLIENT_IP"];
//    }
//    else if ( $_SERVER["HTTP_X_FORWARDED_FOR"] && strcasecmp($_SERVER["HTTP_X_FORWARDED_FOR"], "unknown") )
//    {
//        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
//    }
//    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
//    {
//        $ip = getenv("REMOTE_ADDR");
//    }
//    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
//    {
//        $ip = $_SERVER['REMOTE_ADDR'];
//    }
//    else
//    {
//        $ip = "unknown: ".var_dump($_SERVER, true);
//    }
//    
//   
//    return($ip);
//}
    
             public function get_client_ip() {
            $ipaddress = '';
            if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
            else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
            else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
            else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
            else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
            else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
            else
            $ipaddress = 'UNKNOWN';

            return $ipaddress;
            }
    
    
     /*get widget checkout
    */
    public function getFrontWidgetReview($productId){
        /*http://php.net/manual/en/function.ob-get-clean.php
        */


        $this->tb_name = 'tbl_reviews';
        $ip = $this->get_client_ip();
        
        
        $productId = $this->product_id;
        $url = $this->url;
        
        
        $this->reviews_status = 'Approved';;
        $this->product_id = $productId;
        $reviewResult = $this->searchApproved();
        
        if (isset($_SESSION['SESS_CUSTOMER_INFO'])) {
        $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
        $customerName = $customerInfo->firstName;
        $customerId = $customerInfo->id;
        
       

        $this->product_id = $productId;
        $this->IP = $ip;
        $this->added_by = $customerId;
        $visited_client = $this->search();
        }
        
        
        $objCustomer = new Customer();
        $objCustomer->tb_name = ' tbl_customer';
        
        ob_start();
        //$payment_options = $this->getActivePaymentOptions();
        $view = include('widgets/review.tpl');
        $view = ob_get_clean();
        echo json_encode($view);exit();
    }
    
 
        
        public function ReviewAdminMail(){
            
            $siteEmailAddress = SITE_EMAIL;
          if (isset($_SESSION['SESS_CUSTOMER_INFO'])) {
            $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
            $customerEmail= $customerInfo->email;
            $customerName = $customerInfo->firstName;
        }
          
            $msg = SITE_NAME .'&nbsp;'. 'Administrator,' . '<br /><br/><br/>
            You have recently received a customer review for one of your store product.	<br />
            To review and publish it, please follow the link below:	<br />
                  <a href="http://shop.plusprodemos.com/admin/reviews/reviews.html"> ' . ADMIN_BASE_URL . 'reviews/reviews.html</a><br />
            <br /><br />

            © 2014 '.SITE_NAME.'.,  <br/><br/>Note : This is system generated email please do not reply'

            ;

           sleep(3);
            $mail = new PHPMailer();
            $mail->IsMail();

            $mail->AddReplyTo($customerEmail, $customerName);
            $mail->AddAddress($siteEmailAddress);
            $mail->SetFrom($customerEmail, $customerName);
            $mail->Subject = "A new Review from " . $customerName ;

            $HTML = $mail->MsgHTML($msg);

            $mailsend = $mail->Send();
            /*if($mailsend){
            echo "2";
            exit();
            }*/

            //echo "tesqd";
            //exit();
            if($_POST['ajax'])
            {
                    echo '1';
            }
            else
            {
            //    echo '1';
            //    exit();
                    $_SESSION['sent']=1;


            }

            function checkLen($str,$len=2)
            {
                    return isset($_POST[$str]) && mb_strlen(strip_tags($_POST[$str]),"utf-8") > $len;
            }

            function checkEmail($str)
            {
                    return preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $str);
            }  


            
            
            
        }
        
        
         /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function UpdateSettings() {
        $isUpdated = false;
        try {
            $id = $this->id;
            $status = $this->status;
           
            
                $arrayData = array(
                   
                    'status' => $status
                    
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '1'");
                $isUpdated = $this->update('tbl_review_settings', $arrayData, $arrWhere);
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteSubFields
     * @param        :   SubFieldsObject
     * Description   :   The function is to delete SubFields details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    /*public function deleteSubFields() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }*/

    /*     * '
     * @name         :   getSubFields
     * @param        :   Integer (SubFields ID)
     * Description   :   The function is to get a SubFields details
     * @return       :   SubFields Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getSettings() {
        $objSettings = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = 1';
                $this->select('tbl_review_settings', $colums, $where);
                $SettingsInfo = $this->getResult();
               
                    
                    
                 
                   $objSettings->status = $SettingsInfo['status'];
                   
                    
                    
                 
            }
            return $objSettings;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>
