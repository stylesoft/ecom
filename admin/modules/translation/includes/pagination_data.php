<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$orderBy = "";
$order = "";
$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }   

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'name' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}


$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objTranslation = new Translation();
$objTranslation->tb_name = 'tbl_lang_fields';


$objTranslation->searchStr = $searchQ;
$objTranslation->limit = $recLimit;
$objTranslation->listingOrder = $orderStr;
$translationResult = $objTranslation->search();
?>
<script type="text/javascript"> 
        function deleterow(id){
            if (confirm('Are you sure want to delete?')) {
                $.post('../modules/<?= $module ?>/includes/deletemaintext.php', {id: +id, ajax: 'true' },
                function(){
                    $("#row_"+id).fadeOut("slow");
                    $(".message").fadeIn("slow");
                    $(".message").delay(2000).fadeOut(1000);
                });
            }
        }
        
        function deletePhrRow(id){
            if (confirm('Are you sure want to delete?')) {
                $.post('../modules/<?= $module ?>/includes/deletephrase.php', {id: +id, ajax: 'true' },
                function(){
                    $("#row_"+id).fadeOut("slow");
                    $(".message").fadeIn("slow");
                    $(".message").delay(2000).fadeOut(1000);
                });
            }
        }
</script>
      
      
<div id="list">

   

    <div id="listings">
        <div class="defult">
            ID&nbsp;
            <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=asc">&darr;</a>
        </div>
        <div class="albums">
            Title&nbsp;
            <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=name&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=name&order=asc">&darr;</a>
        </div>
        <div class="blank_div"></div>
        <div class="options">Options</div>
        <div class="clear"></div>
    </div>    
    <ul>

        <?php
        if (count($translationResult) > 0) {
            foreach ($translationResult As $rowIndex => $phrData) {
                ?>  
                <li id="arrayorder_<?php print($phrData->id); ?>">

                    <div id="listings" <?php if (($rowIndex % 2) == 0) { ?>  style="background:#eee;" <?php } else { ?> style="background:#eee;" <?php } ?>>
                        <div class="defult"><?php print($phrData->id); ?></div>
                        <div class="albums"><?php print($phrData->name); ?></div>
                        <div class="blank_div"></div>
                        <div class="options">
                            <div id="icons">
                                <div id="icons_edit"><a href="<?= $module ?>.html?action=edit&id=<?php print($phrData->id); ?>"><img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/edit.png" alt="Edit Listing" width="16" height="16" border="0" /></a></div>
                                <div id="icons_view"><a href="<?= $module ?>.html?action=translate&id=<?php print($phrData->id); ?>"><img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/translate.jpeg" alt="Translate" width="16" height="16" border="0" /></a></div>
                                <div id="icons_delete"><a href=""  onclick="deleterow(<?php echo $phrData->id; ?>)"><img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/trash.png" alt="Delete Listing" width="16" height="16" border="0" /></a></div></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <?php
                        foreach ($phrData->Trans as $rowIndex => $phrTrans) {
                            ?>

                            <li id="arrayorder_<?php print($phrTrans->id); ?>">   
                                <div id="listings" <?php if (($rowIndex % 2) == 0) { ?>  style="background:#fff;" <?php } else { ?> style="background:#fff;" <?php } ?>>
                                    <div class="defult"></div>
                                    <div class="albums" style="font-weight: normal;"><?php print($phrTrans->phrase_text); ?></div>
                                    <div class="blank_div"></div>
                                    <div class="options">
                                        <div id="icons">
                                            <div id="icons_edit"></div>
                                            <div id="icons_images"></div>
                                            <div id="icons_delete"><a href=""  onclick="deletePhrRow(<?php echo $phrTrans->id; ?>)"><img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/trash.png" alt="Delete Listing" width="16" height="16" border="0" /></a></div></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div>     </div>

                                <div class="clear"></div>
                            </li>
                        <?php }
                        ?>
                    </div>

                    <div class="clear"></div>
                </li>

            <?php }
        } else {
            ?>
            <li>
                <div id="listings" style="background:#eee;">No Matching Records Found </div>
            </li>
<?php } ?>
    </ul>
</div>