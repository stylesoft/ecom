<?php

require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$id = $_POST['id'];

$objTrans = new Translation();
$objTrans->tb_name = 'tbl_lang_fields';

if ($_POST['id'] != '') {
    $objTrans->id = $id;


    if ($objTrans->deleteFieldText()) {

        $objPhrTrans = new PhraseTranslation();
        $objPhrTrans->tb_name = 'tbl_phrases';

        if ($objPhrTrans->deletePhraseByContentId($id)) {
            header('Location: ../../../translation.html');
        }
    }
}
?>
