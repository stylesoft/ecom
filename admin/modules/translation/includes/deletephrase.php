<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$id = $_POST['id'];

$objPhrTrans               = new PhraseTranslation();
$objPhrTrans->tb_name = 'tbl_phrases';

if ($_POST['id'] != ''){
    $objPhrTrans->id = $id;
    if($objPhrTrans->deleteTranslation()){
        header('Location: ../../../translation.html');
        exit;
    }
	
}
?>
