<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id = "";
$name = '';
$pid = ''; 
$lang_id = ''; 
$phrase_type = ''; 
$phrase_name = ''; 
$content_table_name = '';
$content_id = ''; 
$phrase_text = '';
$lang_name = '';


$disabled = '';
// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";
$addPhrase = '';
$updateTrans = '';


$action = '';
$status = '';

$objTranslation = new Translation();
$objTranslation->tb_name = 'tbl_lang_fields';

if (isset($_GET['action'])) {

    $action = $_GET['action'];
    if (isset($_GET['id']))$id = $_GET['id'];
    if (isset($_GET['pid']))$pid = $_GET['pid'];
    if (isset($_GET['status']))$status = $_GET['status'];
    //$pid = $_GET['pid'];
    
    $objLanguage = new Language();
    $objLanguage->tb_name = 'tbl_lang';

    $languageRow = $objLanguage->getAllLanguages('Yes');

    if ($action == 'edit') {

        if (isset($_GET['id'])) {

            $fId = $_GET['id'];

            $phrInfo = $objTranslation->getPhrase($fId);

            $id = $phrInfo->id;
            $name = $phrInfo->name;
        }
    }

    if ($action == 'translate') {
        if (isset($_GET['id'])) {

            
            
            if($status=='edit' && isset($pid)){
                //get the phrase details for edit
                
                $disabled = "disabled=disabled";
                $objPhrTrans = new PhraseTranslation();
                $objPhrTrans->tb_name = 'tbl_phrases';
                $phrTrans  = $objPhrTrans->getPhraseTranslation($pid);
                    if (property_exists($phrTrans, "id")) {    $pid = $phrTrans->id;   }
                    if (property_exists($phrTrans, "phrase_text")) {    $name = $phrTrans->phrase_text;}
                    if (property_exists($phrTrans, "lang_name")) {    $phrTrans->lang_name; }
            }
            
            
            //$phrInfo = $objTranslation->getPhrase($id);

            //$lang_id = $phrInfo->id;
            $phrase_type = 'tbl_lang_fields';
            $phrase_name = 'name';
            $phrase_text = '';
            $content_table_name = 'tbl_lang_fields';
            $content_id = $id;

            $content_tbl_name = 'tbl_lang_fields';
            $objPhrTrans = new PhraseTranslation();
            $objPhrTrans->tb_name = 'tbl_phrases';

            $translatedRows = $objPhrTrans->getAllPhraseTranslation($id, $content_tbl_name);
        }
    }
}

if ($_POST) {

    
    if (isset($_POST['action'])) $action = $_POST['action'];
    if (isset($_POST['status'])) $status = $_POST['status'];

    if (isset($_POST['pid'])) $pid = $_POST['pid']; 
    if (isset($_POST['lang_id'])) $lang_id = $_POST['lang_id']; 
    if (isset($_POST['phrase_type'])) $phrase_type = $_POST['phrase_type']; 
    if (isset($_POST['phrase_name'])) $phrase_name = $_POST['phrase_name']; 
    if (isset($_POST['name'])) $name = $_POST['name']; 
    if (isset($_POST['content_table_name'])) $content_table_name = $_POST['content_table_name'];
    if (isset($_POST['content_id'])) $content_id = $_POST['content_id']; 


    //validation
    if ($name == "") {
        array_push($objTranslation->error, 'The phrase name is required');
        $invalid = true;
    }


    $objTranslation->id = '';
    $objTranslation->fieldText = $name;
    
    
    $objPhrTrans->id = '';
    $objPhrTrans->lang_id = $lang_id;
    $objPhrTrans->phrase_type = $phrase_type;
    $objPhrTrans->phrase_name = $phrase_name;
    $objPhrTrans->phrase_text = $name;
    $objPhrTrans->content_tbl_name = $content_table_name;
    $objPhrTrans->content_id = $content_id;


    if ($action == 'add' && !$invalid) {
        $addPhrase = $objTranslation->addFieldText();
       // $reAddUrl    = 'translation.html?action=add';
    } else if ($action == 'edit' && !$invalid) {
        
        $objTranslation->id = $id;
        $isRecordUpdated = $objTranslation->editFieldText();
        
        
    } elseif ($action == 'translate' && $status != 'edit'&& !$invalid) {
        //TODO : add translation to the phrase         
        $lastInsertedId = $objPhrTrans->addSiteTranslation();
        
    }elseif($status == 'edit' && !$invalid){
        $objPhrTrans->id = $pid;
        $objPhrTrans->phrase_text = $name;
        $updateTrans = $objPhrTrans->editSiteTranslation();
    }

    $newPageUrl = "translation.html?action=translate&id='$id'";
    $mainPageUrl = "translation.html";
    
    
    
    if ($lastInsertedId) {
        echo "<div id='coverit'></div><div id='message'Phrase <span class='red'>$name</span> has been added!<br /><br /><br />Add another Phrase?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } elseif ($addPhrase) {
        echo "<div id='coverit'></div><div id='message'Phrase <span class='red'>$name</span> has been added!<br /><br /><br />Add another Phrase?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        echo "<div id='coverit'></div><div id='message'>Menu <span class='red'>$name</span> has been updated!<br /><br /><br />Edit Phrase?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }elseif($updateTrans){
         echo "<div id='coverit'></div><div id='message'>Menu <span class='red'>$name</span> has been updated!<br /><br /><br />Edit Phrase?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "name";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ", "", trim($_SEARCH_QUERY));
    // get all the news details
    $objTranslation->searchStr = $_SEARCH_QUERY;
    $totalNumberOfPhr = $objTranslation->countRec();
    $pageNumbers = ceil($totalNumberOfPhr / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
} elseif ($action == 'translate') {
    $CONTENT = MODULE_TEMPLATES_PATH . "phrtrans.tpl.php";
} else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
require_once $LAYOUT;
?>
