<script type="text/javascript"> 
        function deleterow(id){
            if (confirm('Are you sure want to delete?')) {
                $.post('../modules/translation/includes/deletephrtrans.php', {id: +id, ajax: 'true' },
                function(){
                    $("#row_"+id).fadeOut("slow");
                    $(".message").fadeIn("slow");
                    $(".message").delay(2000).fadeOut(1000);
                });
            }
        }
</script>
<div id="mid_content_wrapper">
    <div id="search_main_wrapper">
        <div class="search_wrapper2">
            <div class="search_L"></div>

            <div class="search_bg">
                <h2>Translate Phrase</h2><button class="toggle" id="side_panel_box_5"></button>

            </div>
            <div class="search_R"></div>
        </div>
    </div>
    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">


            <table id="listings">
                <thead> 
                    <tr height="12px">
                        <td  width="100px">Language</td>
                        <td  width="100px">Phrase Name</td>
                        <td  width="100px">Action</td>
                    </tr>
                </thead> 
                <tbody> 
                    <?php 
                    if(count($translatedRows)>0){
                    foreach ($translatedRows As $rIndex => $transData) {?>
                        <tr height="12px" id="row_1"  class="odd">
                            <td  width="100px"><?php print(ucfirst($transData->lang_name)); ?></td>
                            <td  width="100px"><?php print($transData->phrase_text); ?></td>

                            <td  class="options">
                                <div id="icons">
                                    <div id="icons_edit">
                                        <a href="?action=translate&id=<?=$id?>&status=edit&pid=<?php print($transData->id); ?>">
                                            <img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/edit.png" alt="Edit" width="16" height="16" border="0" />
                                        </a>
                                    </div>

                                    <div id="icons_delete">
                                        <a href=""  onclick="deleterow(<?php print($transData->id); ?>)">
                                            <img src="<?php print(ADMIN_BASE_URL); ?>templates/contents/images/icons/trash.png" alt="Delete" width="16" height="16" border="0" />
                                        </a>
                                    </div></div>
                            </td>

                        </tr>
                    <?php } }else{?>
                        <tr height="12px" id="row_1"  class="odd"><td colspan="3">No Translation Found</td></tr>
                        <?php }?>
                </tbody>

            </table>

            <div style="height:10px;"></div>

        </div>

    </div>
    
    <div id="table_main_wrapper">
        <div id="dashboard" style="background-color:#FFF;">

            <?php if (count($objPhrTrans->error) > 0) { ?>
                <div class="table_wrapper2" id="side_panel_box_4_content">

                    <div class="notification failure canhide">
                        <p>
                            <strong>Error: </strong>
                            Error occurred, when updating the record
                        </p>
                    </div>
                    <div class="notification required canhide">
                        <p>
                        <ul>
                            <?php foreach ($objPhrTrans->error As $index => $eror) { ?>
                                <li>
                                    <?php print($eror); ?>
                                </li> 
                            <?php } ?>

                        </ul>
                        </p>
                    </div> 

                </div>
            <?php } ?>


            <form id="two" action="" method="post">
                <div>
                    <fieldset>
                        
                        <legend>Phrase Details</legend>                           
                            
                            
                        <div id="separator">
                            <label for="contact">Language: </label> 
                            <select name="lang_id" id="lang_id" <?=$disabled?>>
                                
                                <option value=""></option>
                                <?php foreach($languageRow as $rowIndex => $langData) { ?>
                                <option value="<?php print($langData->id); ?>" <?php if ($lang_name == $langData->lang_name) { ?> selected="selected"  <?php } ?>><?php print(ucwords($langData->lang_name)); ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                            
                            
                        <div id="separator">
                            <label for="pagename">Phrase Name : </label> <input 
                                name="name" type="text" id="name"
                                tabindex="1" value="<?php print($name); ?>" size="30" maxlength="30" />                                 
                        </div>

                        <input name="content_table_name" type="hidden" id="content_table_name"   value="<?php print($content_table_name); ?>" size="64" maxlength="64" />
                        <input name="content_id" type="hidden" id="content_id"   value="<?php print($id); ?>" size="64" maxlength="64" />
                        <input name="phrase_type" type="hidden" id="phrase_type"   value="<?php print($phrase_type); ?>" size="64" maxlength="64" />
                        <input name="phrase_name" type="hidden" id="phrase_name"   value="<?php print($phrase_name); ?>" size="64" maxlength="64" />
                        <input name="action" type="hidden" id="action"   value="<?=$action ?>" />
                        <input name="pid" type="hidden" id="pid"   value="<?php print($pid); ?>" size="64" maxlength="64" />
                        <input name="status" type="hidden" id="status"   value="<?=$status ?>" />
                           
                    </fieldset>
                        
                        
                </div>

                <p>                    
                    <input id="button1" type="submit" value="submit" name="Submit"
                           onClick="sendRequest()" /> <input id="button2" type="reset" />
                </p>
                <div id="show"></div>
            </form>
        </div>
        <div style="height:10px;"></div>
    </div>	
</div>
</div>
<div class="clear">
</div> 
</div>