<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$orderBy = "";
$order = "";
$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'display_order' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}




$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objCategory = new Category();
$objCategory->tb_name = 'tbl_category';

$objCategory->searchStr = $searchQ;
$objCategory->limit = $recLimit;
$objCategory->listingOrder = $orderStr;
$categoryResult = $objCategory->getAllParentCategory();

//print('<pre>');print_r($categoryResult);print('</pre>');
?>
<style type="text/css">
    .row3{
        background: none repeat scroll 0 0 #E5F1FF;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
        
    }
    .row3:hover{
        opacity: 2.0;
    }
    .row4{
        background: none repeat scroll 0 0 #E5F1FF;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity:  0.7;
        transition: opacity 0.8s;
    }
        .row4:hover{
        opacity: 2.0;
    }
    
    
       .row5{
        background: none repeat scroll 0 0 #F7F4DC;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
        
    }
    .row5:hover{
        opacity: 2.0;
    }
    .row6{
        background: none repeat scroll 0 0 #F7F4DC;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
    }
        .row6:hover{
        opacity: 2.0;
    } 
    
</style>
<script type="text/javascript">
    $(document).ready(function(){ 	
        function slideout(){
            setTimeout(function(){
                $("#response").slideUp("slow", function () {
                });
    
            }, 2000);}
	
        $("#response").hide();
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
                    var order = $(this).sortable("serialize") + '&update=update'; 
                    $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/updateListingOrder.php", order, function(theResponse){
                        $("#response").html(theResponse);
                        $("#response").slideDown('slow');
                        slideout();
                    }); 															 
                }								  
            });
        });

    });	

</script>

<script type="text/javascript" >
    function deleterow(id){
        if (confirm('This will delte with all its sub categories. Are you sure delete this?')) {
            $.post('../modules/category/includes/deletecategory.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                $(".message").fadeIn("slow");
                $(".message").delay(2000).fadeOut(1000);
                return false;
            });
        }
    }

</script>

<script type="text/javascript">
 
    $(document).ready(function(){
 
        $(".slidingDiv").hide();
        $(".show_hide").show();
 
 
        $("input[name=showHide]").click(function(){
            $(".cls"+this.id).slideToggle();
        });
        
 
    });
 
</script>


<div id="list">
    <div id="table_main_div" >
        <div id="response"> </div>
        <div class="row_heding">

            <div class="colum" style="width:70px;">
                <strong>ID&nbsp;</strong>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
            </div>
            <div class="colum" style="width:280px;">
                <strong>Category Name</strong>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=category_name&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=category_name&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
            </div>

            <div class="colum_right" align="right" style="margin-top:2px; width:185px;"></div>
        </div>
        <div class="clear"></div>

        <ul>
            <?php
            if (count($categoryResult) > 0) {
                foreach ($categoryResult As $rowIndex => $categoryData) {
                    ?> 

                    <li id="arrayorder_<?php print($categoryData->id); ?>">
                        <?php if ($rowIndex % 2 != 0) { ?>
                            <div class="row1">
                            <?php } else { ?>
                                <div class="row2">
                                <?php } ?>
                                <div class="colum" style="width:70px;"><?php print($categoryData->id); ?></div>
                                <div class="colum" style="width:280px;"><?php print($categoryData->category_name); ?></div>

                                <div class="colum_right" align="right" style="margin-top:2px;  width:185px;">
                                    <input type="checkbox" name="showHide" id="<?php print($categoryData->id); ?>"> Sub Categories &nbsp;

                                    <a href="<?= $module ?>.html?action=edit&id=<?php print($categoryData->id); ?>">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                    <a href="javascript:deleterow(<?php echo $categoryData->id; ?>);">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                    </a>
                                </div>
                            </div>
                            <div class="cls<?php print($categoryData->id); ?>" style="
                                 display: none; 
                                 background-color: #cccc;
                                 padding: 0px 0px 10px 50px;
                                 margin-top:5px;
                                 margin-bottom:5px;
                                 border-bottom:3px solid #3399FF;">
                                <ul>
                                        <?php
                                        if (count($categoryData->subCategory)) {
                                            foreach ($categoryData->subCategory As $subIndex => $subCategoryData) {
                                        ?> 
                                    <li id="arrayorder_<?php print($subCategoryData->id); ?>">
                                        <?php if ($subIndex % 2 != 0) { ?> <div class="row3" id="row_<?php print($subCategoryData->id); ?>"> <?php } else { ?> <div class="row4" id="row_<?php print($subCategoryData->id); ?>"> <?php } ?>
                                               
                                                <div class="colum" style="width:350px;"><?php print($subCategoryData->category_name); ?></div>
                                                    <div class="colum_right" align="right" style="margin-top:2px;  width:185px;"> 
                                                         <input type="hidden" name="showHide" id="<?php print($subCategoryData->id); ?>"> Sub Categories &nbsp;
                                                        <a href="<?= $module ?>.html?action=edit&id=<?php print($subCategoryData->id); ?>">
                                                            <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                                        <a href="javascript:deleterow(<?php echo $subCategoryData->id; ?>);">
                                                            <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                                        </a>
                                                    </div>

                                               
                                                </div>
                                            
                                            
                                                    <!-- level 2 subcategories-->
                                                    <div class="cls<?php print($subCategoryData->id); ?>" style="
                                                    display: none; 
                                                    background-color: #cccc;
                                                    padding: 0px 0px 10px 50px;
                                                    margin-top:5px;
                                                    margin-bottom:5px;
                                                    border-bottom:3px solid #A8980A;">
                                                        
                                                    <ul >
                                                            <?php
                                                            if (count($subCategoryData->subCategory)) {
                                                                foreach ($subCategoryData->subCategory As $subL2Index => $subL2CategoryData) {
                                                            ?> 
                                                        <li id="arrayorder_<?php print($subL2CategoryData->id); ?>">
                                                            <?php if ($subL2Index % 2 != 0) { ?> <div class="row5" id="row_<?php print($subL2CategoryData->id); ?>"> <?php } else { ?> <div class="row6" id="row_<?php print($subL2CategoryData->id); ?>"> <?php } ?>

                                                                    <div class="colum" style="width:150px;"><?php print($subL2CategoryData->category_name); ?></div>
                                                                        <div class="colum_right" align="right" style="margin-top:2px;  width:185px;"> 
                                                                           
                                                                            <a href="<?= $module ?>.html?action=edit&id=<?php print($subL2CategoryData->id); ?>">
                                                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                                                            </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                                                            <a href="javascript:deleterow(<?php echo $subL2CategoryData->id; ?>);">
                                                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                                                            </a>
                                                                        </div>



                                                                    </div>
                                                                
                                                        </li>
                                                        
                                                        <?php } }?>

                                                    </ul>                            
            </div>
                                                            
                                                <!-- eof level 2 subcategories-->
                                               <div class="clear"></div>
    
                                      </li>
                                     <?php } }?>
                                
                               
                                 </ul>                            
                            </div>

                            <div class="clear"></div>
                    </li>

                <?php }
            } else { ?>
                <li>
                    <div id="listings" style="background:#eee;">No Matching Records Found </div>
                </li>
<?php } ?>

        </ul>



    </div>

</div>
