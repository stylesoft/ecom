<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id = '';
$category_name  = '';
$category_description  = '';
$sub_category_description  = '';
$parent_category  = ''; 
//$display_style   = ''; 
$status  = 'Disabled'; //Enabled', 'Disabled
$displayOrder = "";


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrCategory = array();

$objCategory = new Category();
$objCategory->tb_name = 'tbl_category';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                $arrCategory = $objCategory->getAllParentCategory();
                
    if ($action == 'edit') {             
            $categoryInfo = $objCategory->getCategory($id);

            $id = $categoryInfo->id;
            $category_name = $categoryInfo->category_name;
            $category_description = $categoryInfo->category_description;
            $sub_category_description = $categoryInfo->sub_category_description;
            $parent_category = $categoryInfo->parent_category;
            $status = $categoryInfo->status;
            $displayOrder = $categoryInfo->displayorder;
             //$display_style = $categoryInfo->display_style;
   
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}

if ($_POST) {

    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $category_name = $_POST['category_name'];
    $category_description = $_POST['category_description'];
    $sub_category_description = $_POST['sub_category_description'];
    $parent_category = $_POST['parent_category'];
    //$display_style   = $_POST['display_style'];
    $displayOrder = $_POST['txtDisplayOrder'];
    if($displayOrder == ''){
        $displayOrder = '200';
    }
    
    
    
    if(isset($_POST['status'])) $status = $_POST['status'];

    //validation
    if($category_name == ""){
            array_push($objCategory->error, 'The Category name is required');
            $invalid = true;
        }

    $objCategory->id = $id;
    $objCategory->category_name = $category_name;
    $objCategory->category_description = $category_description;
    $objCategory->sub_category_description = $sub_category_description;
    $objCategory->parent_category = $parent_category;
    //$objCategory->display_style   = $display_style;
    $objCategory->status = $status;
    $objCategory->displayorder = $displayOrder;
    
    if ($action == 'add' && !$invalid) {
          $lastInsertedId = $objCategory->addCategory();
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objCategory->editCategory();
    }

    $newPageUrl = "category.html?action=add";
    $mainPageUrl = "category.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Category <span class='red'>$category_name</span> has been added!<br /><br /><br />Want to add a new category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Category <span class='red'>$category_name</span> has been updated!<br /><br /><br />Want to add a new category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "category_name";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'ASC';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objCategory->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objCategory->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
