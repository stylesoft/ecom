<div id="search_main_wrapper">

                <h2>Add/Edit Category</h2>
    </div>


    

    <div id="table_main_wrapper">



        <div id="dashboard" style="background-color:#FFF;">

            <form name="form1" id="form1" action="" method="post">
            
                     <div id="two">
                         <fieldset style="margin-top: 20px;">
                   
                    
                    <div id="separator">
                        <label for="headline">Category Name: </label>
                        <input name="category_name" type="text" id="category_name" value="<?php echo $category_name; ?>" size="65" />
                    </div>
                    <div id="separator">
                        <label for="headline">Description: </label>
                        <input name="category_description" type="text" id="category_description" value="<?php echo $category_description; ?>" size="65" />
                    </div>
                    
                     <div id="separator">
                        <label for="headline">Sub Description: </label>
                        <input name="sub_category_description" type="text" id="sub_category_description" value="<?php echo $sub_category_description; ?>" size="65" />
                    </div>
                    
                    <div id="separator">
                        <label for="headline">Parent Category: </label>
                        <select id="parent_category"  name="parent_category" style="width: 200px;">
                            <option value="">--SELECT--</option>
                            <?php foreach ($arrCategory as $index => $categoryData): ?>
                                <option value="<?= $categoryData->id ?>" <?php if ($parent_category == $categoryData->id) echo "selected='selected'" ?>><?= $categoryData->category_name ?></option>
                            <?php endforeach; ?>  
                        </select>
                    </div>
                             
                    <div id="separator">
                        <label for="style">Display Style: </label>
                        <select id="display_style"  name="display_style" style="width: 200px;">
                           
                            
                                <option value="Full Width Category" <?php if ($display_style ==  'Full Width Category'){ ?> selected='selected' <?php }?>>Full width Category</option>
                                <option value="Category with Side Bar" <?php if ($display_style ==  'Category with Side Bar'){ ?> selected='selected' <?php }?>>Category with Side Bar</option>
                            
                        </select>
                    </div>
                    
                    <div id="separator">
                        <label for="pagename">Status: </label>

                        <select name="status" id="status" style="width: 200px;">
                            <option value="">--Select--</option>
                            <option value="Enabled" <?php if ($status == 'Enabled') { ?> selected="selected" <?php } ?>>Enabled</option>
                            <option value="Disabled" <?php if ($status == 'Disabled') { ?> selected="selected" <?php } ?>>Disabled</option>
                        </select>
                    </div>

                </fieldset>
                     </div>
                <p>
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action; ?>"/>
                    <input type="hidden" name="txtDisplayOrder" id="txtDisplayOrder" value="<?php echo $displayOrder; ?>"/>
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>

            </form>

<div class="clear">
        </div> 

        </div>



        <div style="height:10px;"></div>
    </div>	