<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

 // personal details ...........
$customerId                     = '';
$id                             = '';
$title                          = '';
$firstName                      = '';
$lastName                       = '';
$email                          = '';
$password                       = '';
$status                         = '';
$addedDate                      = '';
$lastLoginDate                  = '';
$lastLoginFrom                  = '';

// Billing Address .................
$billingAddressId               = '';
$billingAddressTitle            = '';
$billingAddressFirstName        = '';
$billingAddressLastName         = '';
$billingAddressCompanyName      = '';
$billingAddressPhoneNumber      = '';
$billingAddressAddress1         = '';
$billingAddressAddress2         = '';
$billingAddressTown             = '';
$billingAddressCounty           = '';
$billingAddressPostal           = '';
$billingAddressCountry          = '';
$billingAddressInfo             = '';


// Shipping Address .................
$shippingAddressId               = '';
$shippingAddressTitle            = '';
$shippingAddressFirstName        = '';
$shippingAddressLastName         = '';
$shippingAddressCompanyName      = '';
$shippingAddressPhoneNumber      = '';
$shippingAddressAddress1         = '';
$shippingAddressAddress2         = '';
$shippingAddressTown             = '';
$shippingAddressCounty           = '';
$shippingAddressPostal           = '';
$shippingAddressCountry          = '';
$shippingAddressInfo             = '';

// listings.....
$_REC_PER_PAGE                  = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER                     = "";
$_REC_PAGES                     = "";
$_SEARCH_QUERY                  = "";
$ORDER_BY                       = "";
$ORDER                          = "";

$invalid                        = false;

$lastInsertedId                 = "";
$isRecordUpdated                = "";

$action                         = '';


$objCustomer = new Customer();
$objCustomer->tb_name = 'tbl_customer';




if (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action == 'edit' || $action == 'view') {

        if (isset($_GET['id'])) {

            $customerId = $_GET['id'];
            $id         = $customerId; 
            $objCustomer = new Customer();
	    $objCustomer->tb_name = ' tbl_customer';
	    $customerInfo = $objCustomer->getCustomer($customerId);
            
            $billingAddress             = $customerInfo->addressBillingDtls;
            $shippingAddress             = $customerInfo->addressShippingDtls;
            
            $billingAddressInfo             = $billingAddress[0];
            $shippingAddressInfo            = $shippingAddress[0];
            
            
            //--------------------
            $orderStr       = 'order_date'." ".'Asc';  
            $objOrder = new Order();
            $objOrder->tb_name = 'tbl_order';
            $objOrder->customerId = $customerId;

            $objOrder->listingOrder   = $orderStr;
            $orderResult              = $objOrder->search();
            
            
            //--------------------
        }
    } 
}

if ($_POST) {
   
    $tstamp = time();
        $tstamp = gmdate("Y-m-d H:i:s", $tstamp);
        $action = $_POST['txtAction'];
                                $id = ($action == 'add') ? "" : $_POST['id'];
        
        // personal details ...........
                                $title                          = $_POST['cmbTitle'];
                                $firstName                      = $_POST['txtFirstName'];
                                $lastName                       = $_POST['txtLastName'];
                                $email                          = $_POST['txtEmail'];
                                $password                       = $_POST['txtPassword'];
				$addedDate                      = getCurrentDateTime();
				$lastLoginDate                  = '';
				$lastLoginFrom                  = '';
                                
                                // Billing Address .................
                                if($_POST['txtBillingAddressId'] != ''){
                                    $billingAddressId               = $_POST['txtBillingAddressId'];
                                } else {
                                    $billingAddressId               = "";
                                }
                                $billingAddressTitle            = $_POST['cmbBillingTitle'];
                                $billingAddressFirstName        = $_POST['txtBillingFirstName'];
                                $billingAddressLastName         = $_POST['txtBillingLastName'];
                                $billingAddressCompanyName      = $_POST['txtBillingCompanyName'];
                                $billingAddressPhoneNumber      = $_POST['txtBillingPhone'];
                                $billingAddressAddress1         = $_POST['txtBillingAddress_1'];
                                $billingAddressAddress2         = $_POST['txtBillingAddress_2'];
                                $billingAddressTown             = $_POST['txtBillingTownCity'];
                                $billingAddressCounty           = $_POST['txtBillingCountyState'];
                                $billingAddressPostal           = $_POST['txtBillingPostalZIP'];
                                $billingAddressCountry          = $_POST['txtBillingCountry'];
                                
                                
                                // Shipping Address .................
                                
                                if($_POST['txtShippingAddressId'] != ''){
                                    $shippingAddressId               = $_POST['txtShippingAddressId'];
                                } else {
                                    $shippingAddressId            = "";
                                }
                                
                                $shippingAddressTitle            = $_POST['cmbShippingTitle'];
                                $shippingAddressFirstName        = $_POST['txtShippingFirstName'];
                                $shippingAddressLastName         = $_POST['txtShippingLastName'];
                                $shippingAddressCompanyName      = $_POST['txtShippingCompanyName'];
                                $shippingAddressPhoneNumber      = $_POST['txtShippingPhone'];
                                $shippingAddressAddress1         = $_POST['txtShippingAddress_1'];
                                $shippingAddressAddress2         = $_POST['txtShippingAddress_2'];
                                $shippingAddressTown             = $_POST['txtShippingTownCity'];
                                $shippingAddressCounty           = $_POST['txtShippingCountyState'];
                                $shippingAddressPostal           = $_POST['txtShippingPostalZIP'];
                                $shippingAddressCountry          = $_POST['txtShippingCountry'];
        

                                if ($action == 'add') {

                                                        $objCustomer = new Customer();
                                                        $objCustomer->tb_name = 'tbl_customer';
                                                        $objCustomer->id = $id;
                                                        $objCustomer->title = $title;
                                                        $objCustomer->firstName = $firstName;
                                                        $objCustomer->lastName = $lastName;
                                                        $objCustomer->email = $email;
                                                        $objCustomer->password = md5($password);
                                                        $objCustomer->status = $status;
                                                        $objCustomer->addedDate = $addedDate;
                                                        $objCustomer->lastLoginDate = $lastLoginDate;
                                                        $objCustomer->lastLoginFrom = $lastLoginFrom;

                                                        $customerId = $objCustomer->addCustomer();

                                                        $lastInsertedId = $customerId;

                                                        if($customerId){

                                                            // save the Your Billing Address
                                                            $objCustomerAddress = new Address();
                                                            $objCustomerAddress->id = $billingAddressId;
                                                            $objCustomerAddress->title = $billingAddressTitle;
                                                            $objCustomerAddress->firstName = $billingAddressFirstName;
                                                            $objCustomerAddress->lastName = $billingAddressLastName;
                                                            $objCustomerAddress->houseNumber = $billingAddressCompanyName;
                                                            $objCustomerAddress->phone  = $billingAddressPhoneNumber;
                                                            $objCustomerAddress->address = $billingAddressAddress1;
                                                            $objCustomerAddress->region  = $billingAddressAddress2;
                                                            $objCustomerAddress->city    = $billingAddressTown;
                                                            $objCustomerAddress->state   = $billingAddressCounty;
                                                            $objCustomerAddress->postcode = $billingAddressPostal;
                                                            $objCustomerAddress->country  = $billingAddressCountry;
                                                            $objCustomerAddress->membersId = $customerId;
                                                            $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                                            $objCustomerAddress->addressType = 'BILLING';
                                                            $objCustomerAddress->addAddress();


                                                            // save the Your Shipping Address
                                                            $objCustomerAddress = new Address();
                                                            $objCustomerAddress->id = $shippingAddressId;
                                                            $objCustomerAddress->title = $shippingAddressTitle;
                                                            $objCustomerAddress->firstName = $shippingAddressFirstName;
                                                            $objCustomerAddress->lastName = $shippingAddressLastName;
                                                            $objCustomerAddress->houseNumber = $shippingAddressCompanyName;
                                                            $objCustomerAddress->phone  = $shippingAddressPhoneNumber;
                                                            $objCustomerAddress->address = $shippingAddressAddress1;
                                                            $objCustomerAddress->region  = $shippingAddressAddress2;
                                                            $objCustomerAddress->city    = $shippingAddressTown;
                                                            $objCustomerAddress->state   = $shippingAddressCounty;
                                                            $objCustomerAddress->postcode = $shippingAddressPostal;
                                                            $objCustomerAddress->country  = $shippingAddressCountry;
                                                            $objCustomerAddress->membersId = $customerId;
                                                            $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                                            $objCustomerAddress->addressType = 'SHIPPING';
                                                            $objCustomerAddress->addAddress();
                                                        }


                                } elseif ($action == 'edit') {
                                    
                                                        $customerId  = $id;
                                    
                                                        $objCustomer = new Customer();
                                                        $objCustomer->tb_name = 'tbl_customer';
                                                        $objCustomer->id = $id;
                                                        $objCustomer->title = $title;
                                                        $objCustomer->firstName = $firstName;
                                                        $objCustomer->lastName = $lastName;
                                                        $objCustomer->email = $email;
                                                        $encPassword = '';
                                                        if($password != ''){
                                                            $encPassword = md5($password);
                                                        }
                                                        $objCustomer->password = $encPassword;
                                                        $objCustomer->status = $status;
                                                        $objCustomer->addedDate = $addedDate;
                                                        $objCustomer->lastLoginDate = $lastLoginDate;
                                                        $objCustomer->lastLoginFrom = $lastLoginFrom;

                                                        $objCustomer->editCustomer();
                                                        
                                                        
                                                         // save the Your Billing Address
                                                            $objCustomerAddress = new Address();
                                                            $objCustomerAddress->id = $billingAddressId;
                                                            $objCustomerAddress->title = $billingAddressTitle;
                                                            $objCustomerAddress->firstName = $billingAddressFirstName;
                                                            $objCustomerAddress->lastName = $billingAddressLastName;
                                                            $objCustomerAddress->houseNumber = $billingAddressCompanyName;
                                                            $objCustomerAddress->phone  = $billingAddressPhoneNumber;
                                                            $objCustomerAddress->address = $billingAddressAddress1;
                                                            $objCustomerAddress->region  = $billingAddressAddress2;
                                                            $objCustomerAddress->city    = $billingAddressTown;
                                                            $objCustomerAddress->state   = $billingAddressCounty;
                                                            $objCustomerAddress->postcode = $billingAddressPostal;
                                                            $objCustomerAddress->country  = $billingAddressCountry;
                                                            $objCustomerAddress->membersId = $customerId;
                                                            $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                                            $objCustomerAddress->addressType = 'BILLING';
                                                            if($billingAddressId != ''){
                                                                $objCustomerAddress->updateAddress();
                                                            } else {
                                                                $objCustomerAddress->addAddress();
                                                            }

                                                            // save the Your Shipping Address
                                                            $objCustomerAddress = new Address();
                                                            $objCustomerAddress->id = $shippingAddressId;
                                                            $objCustomerAddress->title = $shippingAddressTitle;
                                                            $objCustomerAddress->firstName = $shippingAddressFirstName;
                                                            $objCustomerAddress->lastName = $shippingAddressLastName;
                                                            $objCustomerAddress->houseNumber = $shippingAddressCompanyName;
                                                            $objCustomerAddress->phone  = $shippingAddressPhoneNumber;
                                                            $objCustomerAddress->address = $shippingAddressAddress1;
                                                            $objCustomerAddress->region  = $shippingAddressAddress2;
                                                            $objCustomerAddress->city    = $shippingAddressTown;
                                                            $objCustomerAddress->state   = $shippingAddressCounty;
                                                            $objCustomerAddress->postcode = $shippingAddressPostal;
                                                            $objCustomerAddress->country  = $shippingAddressCountry;
                                                            $objCustomerAddress->membersId = $customerId;
                                                            $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                                            $objCustomerAddress->addressType = 'SHIPPING';
                                                            if($shippingAddressId != ''){
                                                                $objCustomerAddress->updateAddress();
                                                            } else {
                                                                $objCustomerAddress->addAddress();
                                                            }
                                                            $isRecordUpdated = true;

                                }

    
    
    
    
    //$newPageUrl = "../images.php?m=customer&c=customer&id=".$currentProductId;
    $mainPageUrl = "customer.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Customer <span class='red'>$firstName</span> has been added!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton'>Ok</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Product <span class='red'>$productName</span> has been added!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
          print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Customer <span class='red'>$firstName</span> record has been updated!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton'>Ok</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Menu <span class='red'>$productName</span> has been updated!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "addedDate";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));


    $objCustomer = new Customer();
    $objCustomer->tb_name = ' tbl_customer';
    $objCustomer->searchStr = $_SEARCH_QUERY;
    $totalNumberOfCustomers = $objCustomer->countRec();
    $pageNumbers = ceil($totalNumberOfCustomers / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";     
} elseif($action == 'view') {
    $CONTENT = MODULE_TEMPLATES_PATH . "view.tpl.php";     
} else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}


$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
