<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  





    $('#chkSameasbilling').change(function() {
        var checked = $('input[type=checkbox]').is(':checked');
       //  console.log(checked);
        
        if($(this).is(":checked")) {
             console.log(checked);
           $('#cmbShippingTitle1').val($('#cmbBillingTitle1').val())
           $('#txtShippingFirstName1').val($('#txtBillingFirstName1').val())
           $('#txtShippingLastName1').val($('#txtBillingLastName1').val())
           $('#txtBillingCompanyName1').val($('#txtBillingCompanyName1').val())
           $('#txtShippingPhone1').val($('#txtBillingPhone1').val())
           $('#txtShippingAddress_11').val($('#txtBillingAddress_1').val())
           $('#txtShippingAddress_21').val($('#txtBillingAddress_2').val())
           $('#txtShippingTownCity1').val($('#txtBillingTownCity1').val())
           $('#txtShippingCountyState1').val($('#txtBillingCountyState1').val())
           $('#txtShippingPostalZIP1').val($('#txtBillingPostalZIP1').val())
            $('#txtShippingCountry1').val($('#txtBillingCountry1').val())

                
             
        }else{
            $('#cmbShippingTitle1').val('');
            $('#txtShippingFirstName1').val('');
            $('#txtShippingLastName1').val('');
            $('#txtBillingCompanyName1').val('');
            $('#txtShippingPhone1').val('');
            $('#txtShippingAddress_11').val('');
            $('#txtBillingAddress_21').val('');
            $('#txtShippingTownCity1').val('');
            $('#txtShippingCountyState1').val('');
            $('#txtShippingPostalZIP1').val('');
            $('#txtShippingCountry1').val('');
            
            
            
        }
        
    });
    
});


</script>

<div id="search_main_wrapper">

                <h2>Add/Edit Customer</h2>
    </div>

<?php 
$objCountry = new Country();
$countryList = $objCountry->getAll();

?>

    

    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">


            <form name="form1" id="form1"  action="" method="post">
                <div id="two">
                    
                      <h2>Customer's Personal Details</h2>


                    <fieldset>

                         <div id="separator">
                            <label for="pagetitle">Title : </label>
                            
                             <select name="cmbTitle" id="cmbTitle" style="width: 190px;">
                                <option value=""></option>
                                <option value="Mr" <?php if($customerInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($customerInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($customerInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($customerInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($customerInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                            </select>
                            
                        </div>
                       
                        <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtFirstName" id="txtFirstName" type="text" value="<?php print($customerInfo->firstName);?>"/>
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtLastName" id="txtLastName" type="text" value="<?php print($customerInfo->lastName);?>"/>
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Email : </label>
                            <input name="txtEmail" id="txtEmail" type="text" value="<?php print($customerInfo->email);?>"/>
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Password : </label>
                            <input name="txtPassword" id="txtPassword" type="password" style="width: 170px;" />
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Confirm Password : </label>
                            <input name="txtCPassword" id="txtCPassword" type="password" style="width: 170px;" />
                        </div>

                    </fieldset>
                      
                      
                      <h2>Customer's Billing Address</h2>
                
                      
                      <fieldset>
                      <div id="separator">
                            <label for="pagetitle">Title : </label>
                            <select class="select" id="cmbBillingTitle1" name="cmbBillingTitle" style="width: 190px;">
                            <option value=""></option>
                                <option value="Mr" <?php if($billingAddressInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($billingAddressInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($billingAddressInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($billingAddressInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($billingAddressInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtBillingFirstName" id="txtBillingFirstName1" type="text" value="<?php print($billingAddressInfo->firstName);?>"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtBillingLastName" id="txtBillingLastName1" type="text" value="<?php print($billingAddressInfo->lastName);?>"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Company Name : </label>
                            <input name="txtBillingCompanyName" id="txtBillingCompanyName1" type="text" value="<?php print($billingAddressInfo->houseNumber);?>"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Phone : </label>
                            <input name="txtBillingPhone" id="txtBillingPhone1" type="text" value="<?php print($billingAddressInfo->phone);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 1 : </label>
                            <input name="txtBillingAddress_1" id="txtBillingAddress_1" type="text" value="<?php print($billingAddressInfo->address);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 2 : </label>
                            <input name="txtBillingAddress_2" id="txtBillingAddress_2" type="text" value="<?php print($billingAddressInfo->region);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Town/City : </label>
                            <input name="txtBillingTownCity" id="txtBillingTownCity1" type="text" value="<?php print($billingAddressInfo->city);?>"/>
                        </div>
 
                          <div id="separator">
                            <label for="pagetitle">County/State : </label>
                            <input name="txtBillingCountyState" id="txtBillingCountyState1" type="text" value="<?php print($billingAddressInfo->state);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Postal/ZIP code : </label>
                            <input name="txtBillingPostalZIP" id="txtBillingPostalZIP1" type="text" value="<?php print($billingAddressInfo->postcode);?>"/>
                        </div>
   
                          <div id="separator">
                            <label for="pagetitle">Country : </label>
                            <select id="txtBillingCountry1" name="txtBillingCountry" style="width: 190px;">
                                 <option>Select Country</option>   
                                     <?php foreach ($countryList as $index=>$country) { ?>
			                         <option value="<?php echo $country->id;?>" <?php if($country->id == $billingAddressInfo->country) { ?> selected="selected" <?php } ?>><?php echo $country->name;?></option>
				                    <?php } ?>
                            </select>
                        </div>
   
                      </fieldset>
                      
                      
                      
                      
                      <h2>Customer's Shipping Address &nbsp;&nbsp;<input type="checkbox"  name="chkSameasbilling" id="chkSameasbilling" /> <span style="color: #646464;font-size: 14px;">(Same as Billing Address)</span></h2>

                      
                      
                      
                      <div id="checkecusdetails"  >
                      <fieldset>
                         
                      <div id="separator">
                            <label for="pagetitle">Title : </label>
                            <select class="select" id="cmbShippingTitle1" name="cmbShippingTitle" style="width: 190px;">
                            <option value=""></option>
                            <option value="Mr" >Mr</option>
                                <option value="Mr" <?php if($shippingAddressInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($shippingAddressInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($shippingAddressInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($shippingAddressInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($shippingAddressInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtShippingFirstName" id="txtShippingFirstName1" type="text" value="<?php print($shippingAddressInfo->firstName);?>" />
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtShippingLastName" id="txtShippingLastName1" type="text" value="<?php print($shippingAddressInfo->lastName);?>" />
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Company Name : </label>
                            <input name="txtShippingCompanyName" id="txtShippingCompanyName1" type="text" value="<?php print($shippingAddressInfo->houseNumber);?>"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Phone : </label>
                            <input name="txtShippingPhone" id="txtShippingPhone1" type="text" value="<?php print($shippingAddressInfo->phone);?>" />
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 1 : </label>
                            <input name="txtShippingAddress_1" id="txtShippingAddress_11" type="text" value="<?php print($shippingAddressInfo->address);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 2 : </label>
                            <input name="txtShippingAddress_2" id="txtShippingAddress_21" type="text" value="<?php print($shippingAddressInfo->region);?>" />
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Town/City : </label>
                            <input name="txtShippingTownCity" id="txtShippingTownCity1" type="text" value="<?php print($shippingAddressInfo->city);?>"/>
                        </div>
 
                          <div id="separator">
                            <label for="pagetitle">County/State : </label>
                            <input name="txtShippingCountyState" id="txtShippingCountyState1" type="text" value="<?php print($shippingAddressInfo->state);?>"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Postal/ZIP code : </label>
                            <input name="txtShippingPostalZIP" id="txtShippingPostalZIP1" type="text" value="<?php print($shippingAddressInfo->postcode);?>"/>
                        </div>
   
                          <div id="separator">
                            <label for="pagetitle">Country : </label>
                            <select id="txtShippingCountry1" name="txtShippingCountry" style="width: 190px;">
                              <option>Select Country</option>   
                                  <?php foreach ($countryList as $index=>$country) { ?>
			                    <option value="<?php echo $country->id;?>" <?php if($country->id == $shippingAddressInfo->country) { ?> selected="selected" <?php } ?>><?php echo $country->name;?></option>
				                <?php } ?>
                            </select>
                        </div>
   
                      </fieldset>
   
                      </div>
   
   
                </div>
                
                <p>
                    <input type="hidden" name="id" id="id" value="<?php print($customerId);?>" />
                    <input type="hidden" name="txtBillingAddressId" id="txtBillingAddressId" value="<?php print($billingAddressInfo->id);?>" />
                    <input type="hidden" name="txtShippingAddressId" id="txtShippingAddressId" value="<?php print($shippingAddressInfo->id);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>
                <div id="show"></div>
            </form>



        </div>

<div class="clear">
        </div> 

        <div style="height:10px;"></div>
    </div>	
<script>
CKEDITOR.replace( 'mainbody' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/main.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 600
});
</script>