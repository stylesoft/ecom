
<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>
<div id="search_main_wrapper">

                <h2>View Customer</h2>
    </div>

<?php 
$objCountry = new Country();
$countryList = $objCountry->getAll();

?>
    

    

    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">


            <form name="form1" id="form1"  action="" method="post">
                <div id="two">
                    
                      <h2>Customer's Personal Details</h2>


                    <fieldset>

                         <div id="separator">
                            <label for="pagetitle">Title : </label>
                            
                            <select name="cmbTitle" id="cmbTitle" style="width: 190px;" disabled>
                                <option value=""></option>
                                <option value="Mr" <?php if($customerInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($customerInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($customerInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($customerInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($customerInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                            </select>
                            
                        </div>
                       
                        <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtFirstName" id="txtFirstName" type="text" value="<?php print($customerInfo->firstName);?>" readonly="readonly"/>
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtLastName" id="txtLastName" type="text" value="<?php print($customerInfo->lastName);?>" readonly="readonly"/>
                        </div>
                        
                        <div id="separator">
                            <label for="pagetitle">Email : </label>
                            <input name="txtEmail" id="txtEmail" type="text" value="<?php print($customerInfo->email);?>" readonly="readonly"/>
                        </div>
                  

                    </fieldset>
                      
                      
                      <h2>Customer's Billing Address</h2>
                
                      
                      <fieldset>
                      <div id="separator">
                            <label for="pagetitle">Title : </label>
                            <select class="select" id="cmbBillingTitle" name="cmbBillingTitle" style="width: 190px;" disabled>
                            <option value=""></option>
                                <option value="Mr" <?php if($billingAddressInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($billingAddressInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($billingAddressInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($billingAddressInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($billingAddressInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtBillingFirstName" id="txtBillingFirstName" type="text" value="<?php print($billingAddressInfo->firstName);?>" readonly="readonly"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtBillingLastName" id="txtBillingLastName" type="text" value="<?php print($billingAddressInfo->lastName);?>" readonly="readonly"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Company Name : </label>
                            <input name="txtBillingCompanyName" id="txtBillingCompanyName" type="text" value="<?php print($billingAddressInfo->houseNumber);?>" readonly="readonly"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Phone : </label>
                            <input name="txtBillingPhone" id="txtBillingPhone" type="text" value="<?php print($billingAddressInfo->phone);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 1 : </label>
                            <input name="txtBillingAddress_1" id="txtBillingAddress_1" type="text" value="<?php print($billingAddressInfo->address);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 2 : </label>
                            <input name="txtBillingAddress_2" id="txtBillingAddress_2" type="text" value="<?php print($billingAddressInfo->region);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Town/City : </label>
                            <input name="txtBillingTownCity" id="txtBillingTownCity" type="text" value="<?php print($billingAddressInfo->city);?>" readonly="readonly"/>
                        </div>
 
                          <div id="separator">
                            <label for="pagetitle">County/State : </label>
                            <input name="txtBillingCountyState" id="txtBillingCountyState" type="text" value="<?php print($billingAddressInfo->state);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Postal/ZIP code : </label>
                            <input name="txtBillingPostalZIP" id="txtBillingPostalZIP" type="text" value="<?php print($billingAddressInfo->postcode);?>" readonly="readonly"/>
                        </div>
   
                          <div id="separator">
                            <label for="pagetitle">Country : </label>
                            <select id="txtBillingCountry" name="txtBillingCountry" style="width: 190px;" disabled>
                                <?php foreach ($countryList as $index=>$country) { ?>
			                         <option value="<?php echo $country->id;?>" <?php if($country->id == $billingAddressInfo->country) { ?> selected="selected" <?php } ?>><?php echo $country->name;?></option>
				                    <?php } ?>
                            </select>
                        </div>
   
                      </fieldset>
                      
                      
                      
                      
                      <h2>Customer's Shipping Address &nbsp;&nbsp;<input type="checkbox" name="chkSameasbilling" id="chkSameasbilling" /> <span style="color: #646464;font-size: 14px;">(Same as Billing Address)</span></h2>


                      <fieldset>
                      <div id="separator">
                            <label for="pagetitle">Title : </label>
                            <select class="select" id="cmbShippingTitle" name="cmbShippingTitle" style="width: 190px;" disabled>
                            <option value=""></option>
                            <option value="Mr" <?php if($shippingAddressInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                                <option value="Mrs" <?php if($shippingAddressInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                                <option value="Ms" <?php if($shippingAddressInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                                <option value="Miss" <?php if($shippingAddressInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                                <option value="Dr" <?php if($shippingAddressInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">First Name : </label>
                            <input name="txtShippingFirstName" id="txtShippingFirstName" type="text" value="<?php print($shippingAddressInfo->firstName);?>" readonly="readonly"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Last Name : </label>
                            <input name="txtShippingLastName" id="txtShippingLastName" type="text" value="<?php print($shippingAddressInfo->lastName);?>" readonly="readonly" />
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Company Name : </label>
                            <input name="txtShippingCompanyName" id="txtShippingCompanyName" type="text" value="<?php print($shippingAddressInfo->houseNumber);?>" readonly="readonly"/>
                        </div>
                          
                          
                          <div id="separator">
                            <label for="pagetitle">Phone : </label>
                            <input name="txtShippingPhone" id="txtShippingPhone" type="text" value="<?php print($shippingAddressInfo->phone);?>" readonly="readonly" />
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 1 : </label>
                            <input name="txtShippingAddress_1" id="txtShippingAddress_1" type="text" value="<?php print($shippingAddressInfo->address);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Address 2 : </label>
                            <input name="txtShippingAddress_2" id="txtShippingAddress_2" type="text" value="<?php print($shippingAddressInfo->region);?>" readonly="readonly" />
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Town/City : </label>
                            <input name="txtShippingTownCity" id="txtShippingTownCity" type="text" value="<?php print($shippingAddressInfo->city);?>" readonly="readonly"/>
                        </div>
 
                          <div id="separator">
                            <label for="pagetitle">County/State : </label>
                            <input name="txtShippingCountyState" id="txtShippingCountyState" type="text" value="<?php print($shippingAddressInfo->state);?>" readonly="readonly"/>
                        </div>
                          
                          <div id="separator">
                            <label for="pagetitle">Postal/ZIP code : </label>
                            <input name="txtShippingPostalZIP" id="txtShippingPostalZIP" type="text" value="<?php print($shippingAddressInfo->postcode);?>" readonly="readonly"/>
                        </div>
   
                          <div id="separator">
                            <label for="pagetitle">Country : </label>
                            <select id="txtShippingCountry" name="txtShippingCountry" style="width: 190px;" disabled>
                                <?php foreach ($countryList as $index=>$country) { ?>
			                    <option value="<?php echo $country->id;?>" <?php if($country->id == $shippingAddressInfo->country) { ?> selected="selected" <?php } ?>><?php echo $country->name;?></option>
				                <?php } ?>
                            </select>
                        </div>
   
                      </fieldset>
                      
                      
                       <h2>Customer's Orders</h2>


                       <fieldset>
                           
                           
                           
                           <div id="list">
<div id="table_main_div" >
    
        <div class="row_heding">

            <div class="colum" style="width:100px;">
                <strong>Order No&nbsp;</strong>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
            </div>
            <div class="colum" style="width:120px;">Date On&nbsp;</div>
            <div class="colum" style="width:140px;">Ordered By&nbsp;</div>
            <div class="colum" style="width:95px;">Pay By</div>
            <div class="colum" style="width:70px;">Amount&nbsp;</div>                               
            <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
        </div>
                            <div class="clear"></div>
                    	
                            <ul>
                             <?php 
                                if(count($orderResult) >0){
                                foreach($orderResult As $rowIndex=>$orderData) {?>  
                                <li id="arrayorder_<?php print($orderData->id);?>">
                                <?php 
                                $orderDtls = $objOrder->getOrder($orderData->id);
                                
                                $prd_pricCurrency = $orderDtls->orderItemDetails['0']->productDtls->priceCurrency->code;
                                
                                ?>
                            <?php if($rowIndex%2 != 0){?>
                            <div class="row1">
                             <?php } else {?>
                                 <div class="row2">
                              <?php } ?>
                                     
                                 <?php //print_r($orderData);?>    
                        	<div class="colum" style="width:100px;"><?php print($orderData->id);?></div>
                                <div class="colum" style="width:120px;"><?php print($orderData->orderDate);?></div>
                                <div class="colum" style="width:140px;"><?php print($orderData->cutomerDetails->firstName);?>&nbsp;<?php print($orderData->cutomerDetails->lastName);?></div>
                                <div class="colum" style="width:95px;"><?php print($orderData->paymentMethod);?></div>
                                <div class="colum" style="width:70px;"><?php echo $prd_pricCurrency;?> <?php print($orderData->grandTotal);?></div>                                                         
                            
                                       <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                            
                                            <a href="<?php print(ADMIN_BASE_URL); ?>order/order.html?action=edit&id=<?php print($orderData->id); ?>">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                            </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="<?php print(ADMIN_BASE_URL); ?>order/order.html?action=view&id=<?php print($orderData->id); ?>">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/view.png" alt="" width="16" height="16" border="0"  title="View"/>
                                            </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="javascript:deleterow(<?php echo $orderData->id; ?>);">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                            </a>
                                        </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                                
                                <?php } }else{?>
                            <li>
                                <div id="listings" style="background:#eee;">No Matching Records Found </div>
                            </li>
                            <?php }?>
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>
                           
                           
                           
                       </fieldset>
   
   
   
   
                </div>
                
                <p>
                    <input type="hidden" name="id" id="id" value="<?php print($customerId);?>" />
                    <input type="hidden" name="txtBillingAddressId" id="txtBillingAddressId" value="<?php print($billingAddressInfo->id);?>" />
                    <input type="hidden" name="txtShippingAddressId" id="txtShippingAddressId" value="<?php print($shippingAddressInfo->id);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>
                <div id="show"></div>
            </form>



        </div>

<div class="clear">
        </div> 

        <div style="height:10px;"></div>
    </div>	
<script>
CKEDITOR.replace( 'mainbody' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/main.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 600
});
</script>