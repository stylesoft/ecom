<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "";
$order          = "";
$orderStr       = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$SERCH_STR = array(); //search array
if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            if(isset($_GET['orderno'])){
                $SERCH_STR['orderno'] = $_GET['orderno'];
            } 
            if(isset($_GET['order_date'])){
                $SERCH_STR['order_date'] = $_GET['order_date'];
            } 
            if(isset($_GET['method_paid'])){
                $SERCH_STR['method_paid'] = $_GET['method_paid'];
            } 
            if(isset($_GET['status'])){
                $SERCH_STR['status'] = $_GET['status'];
            } 
                $objOrder->id = $_GET['orderno'];
                $objOrder->orderDate = $_GET['order_date'];
                $objOrder->paymentMethod = $_GET['method_paid'];
                $objOrder->orderStatus = $_GET['status'];
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }
            
            if(isset($_GET['order'])){
                $order = $_GET['order'];
            }

            if($orderBy != '' && $order != ''){
                $orderStr       = $orderBy." ".$order;                
            } else {
                
                $orderStr       = 'order_date'." ".'Asc';  
            }
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}




$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;
//--------------------------------------------

$objOrder = new Order();
$objOrder->tb_name = 'tbl_order';
 
$objOrder->id = $SERCH_STR['orderno'];
$objOrder->orderDate = $SERCH_STR['order_date'];
$objOrder->paymentMethod = $SERCH_STR['method_paid'];
$objOrder->oderStatus = $SERCH_STR['status'];

$objOrder->limit          = $recLimit;
$objOrder->listingOrder   = $orderStr;
$orderResult              = $objOrder->search();

//print_r($orderResult);
?>
<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			$.post("<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/updateListingOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

function deleterow(id){
if (confirm('Are you sure want to delete?')) {
$.post('<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/deletelisting.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder_"+id).fadeOut("slow");
return false;
//$(".message").delay(2000).fadeOut(1000);
});
}
}

</script>




<div id="list">
<div id="table_main_div" >
     <div id="response"> </div>
        <div class="row_heding">

            <div class="colum" style="width:100px;">
                <strong>Order No&nbsp;</strong>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                <a href="<?php print(ADMIN_BASE_URL); ?><?= $module ?>/<?= $module ?>.html?q=<?php print($searchQ); ?>&orderby=id&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
            </div>
            <div class="colum" style="width:120px;">Date On&nbsp;</div>
            <div class="colum" style="width:140px;">Ordered By&nbsp;</div>
            <div class="colum" style="width:95px;">Pay By</div>
            <div class="colum" style="width:70px;">Amount&nbsp;</div>                               
            <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
        </div>
                            <div class="clear"></div>
                    	
                            <ul>
                             <?php 
                                if(count($orderResult) >0){
                                foreach($orderResult As $rowIndex=>$orderData) {?>  
                                <?php
                                 
                                $orderDtls = $objOrder->getOrder($orderData->id);
                              
                               $prd_pricCurrency = $orderDtls->orderItemDetails['0']->productDtls->priceCurrency->code;
                               /*
                               $prd_pricCurrency1 = $orderDtls->orderItemDetails['0']->id;
                               
                               $objorderItem = new OrderItem();
                               $getorderItemProduct = $objorderItem->getOrderItem($prd_pricCurrency1);
                               $orderPid = $getorderItemProduct->productId;
                               $objProduct = new Product();
                               $objProduct->tb_name = 'tbl_product';
                               
                               $productInfo = $objProduct->getProduct($orderPid);
                               $priceCurrency = $productInfo->priceCurrency;
                               //$prd_pricCurrency =  $priceCurrency->code;
                              //echo "<pre>";
                              // //print_r($priceCurrency);
                            // echo "</pre>";
                             // exit;
                                */
                                ?>
                                <li id="arrayorder_<?php print($orderData->id);?>">
                            <?php if($rowIndex%2 != 0){?>
                            <div class="row1">
                             <?php } else {?>
                                 <div class="row2">
                              <?php } ?>
                                     
                                 <?php //print_r($orderData);?>    
                        	<div class="colum" style="width:100px;"><?php print($orderData->id);?></div>
                                <div class="colum" style="width:120px;"><?php print($orderData->orderDate);?></div>
                                <div class="colum" style="width:140px;"><?php print($orderData->cutomerDetails->firstName);?>&nbsp;<?php print($orderData->cutomerDetails->lastName);?></div>
                                <div class="colum" style="width:95px;"><?php print($orderData->paymentMethod);?></div>
                                <div class="colum" style="width:70px;"><?php print ($prd_pricCurrency);?> <?php print($orderData->grandTotal);?></div>                                                         
                            
                                       <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                            
                                            <a href="<?= $module ?>.html?action=edit&id=<?php print($orderData->id); ?>">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                            </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="<?= $module ?>.html?action=view&id=<?php print($orderData->id); ?>">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/view.png" alt="" width="16" height="16" border="0"  title="View"/>
                                            </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="javascript:deleterow(<?php echo $orderData->id; ?>);">
                                                <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                            </a>
                                        </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                                
                                <?php } }else{?>
                            <li>
                                <div id="listings" style="background:#eee;">No Matching Records Found </div>
                            </li>
                            <?php }?>
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>
    
   