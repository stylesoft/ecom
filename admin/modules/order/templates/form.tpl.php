<div id="search_main_wrapper">

    <h2>Edit Order</h2>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        function Display_Load()
        {
            $("#cloading").fadeIn(900,0);
            $("#cloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        
        function Hide_Load()
        {
            $("#cloading").fadeOut('slow');
        };
        //Pagination Click
        $("#orderStatus").change(function(){
            
            Display_Load();
            
            var ord_stat = $('#orderStatus').val();
            $.post('<?php print(ADMIN_BASE_URL); ?>modules/order/includes/update_status.php', {ord_stat: +ord_stat, ord_id: + <?php print($order_id);?>, ajax: 'true' },
            function(){
                $("#cloading").html("All Chnages Saved");
                $("#cloading").fadeOut('slow');
                return false;
                //$(".message").delay(2000).fadeOut(1000);
            });
            
            //$("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/order/includes/pagination_data.php?page=" + pageNum+"&m=order", Hide_Load());
        });
        
        $("#paymentStatus").change(function(){
            
            Display_Load();
            
            var pay_stat = $('#paymentStatus').val();
            $.post('<?php print(ADMIN_BASE_URL); ?>modules/order/includes/update_status.php', {pay_stat: + pay_stat, ord_id: + <?php print($order_id);?>, ajax: 'true' },
            function(){
                $("#cloading").html("All Chnages Saved");
                $("#cloading").fadeOut('slow');
                return false;
                //$(".message").delay(2000).fadeOut(1000);
            });
            
            //$("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/order/includes/pagination_data.php?page=" + pageNum+"&m=order", Hide_Load());
        });
        
    });
    
  
</script>

<div id="table_main_wrapper">

    <div id="dashboard" style="background-color:#FFF;">


        <form name="form1" id="form1"  action="" method="post">
            <div id="cloading" style="margin-left: 10px; margin-top: 40px; margin-bottom: 0px; background-color: greenyellow; display: none; width: 810px; height: 25px; text-align: center; ">sss</div>
            <div id="two">

                <h2>Order Info</h2>
                           
                <fieldset >

                    <div id="separator">
                        <label for="pagetitle">Order Id : </label>
                        <input name="txtProductName" type="text" id="txtProductName" tabindex="2"  value="<?php print($order_id); ?>" size="64" style="width:600px;" readonly="readonly"/>
                    </div>

                    <div id="separator">
                        <label for="keywords">Order On : </label> 
                        <input name="keywords" type="text" id="keywords" tabindex="3"  value="<?php print($order_date); ?>" size="64" style="width:600px;" maxlength="256" readonly="readonly" />
                    </div>
                    <div id="separator">
                        <label for="description">Order Status : </label> 
                       <select name="orderStatus" id="orderStatus" style="width: 200px;">
                            <option value="1" <?php if ($order_status == 'SHIPPED') { ?> selected="selected" <?php } ?>>Shipped</option>
                            <option value="2" <?php if ($order_status == 'PENDING') { ?> selected="selected" <?php } ?>>Pending</option>
                        </select>
                    </div>


                    <div style="clear:both;"> </div>


                     <div id="separator">
                        <label for="description">Payment Status : </label> 
                       <select name="paymentStatus" id="paymentStatus" style="width: 200px;">
                            <option value="1" <?php if ($payment_status == 'PENDING') { ?> selected="selected" <?php } ?>>Pending</option>
                            <option value="2" <?php if ($payment_status == 'COMPLETED') { ?> selected="selected" <?php } ?>>Completed</option>
                        </select>
                    </div>

                    <div style="clear:both;"> </div>

                    <div id="separator">
                        <label for="priceCurrency">Sub Total: </label> 
                        <select name="priceCurrency" id="priceCurrency" style="width: 55px;" disabled="disabled">
                            <option value="1" <?php if ($prd_pricCurrency == '£') { ?> selected="selected" <?php } ?>>£</option>
                            <option value="2" <?php if ($prd_pricCurrency == '$') { ?> selected="selected" <?php } ?>>$</option>
                        </select>
                        <input name="subTotal" type="text" id="subTotal" tabindex="2"  value="<?php print($sub_total); ?>" size="64" style="width:120px;" maxlength="64" readonly="readonly"/>
                    </div>

                    <div style="clear:both;"> </div>

                    <div id="separator">
                        <label for="priceCurrency">Shipping Amount: </label> 
                        <select name="priceCurrency" id="priceCurrency" style="width: 55px;" disabled="disabled">
                            <option value="1" <?php if ($prd_pricCurrency == '£') { ?> selected="selected" <?php } ?>>£</option>
                            <option value="2" <?php if ($prd_pricCurrency == '$') { ?> selected="selected" <?php } ?>>$</option>
                        </select>
                        <input name="shippingAmount" type="text" id="shippingAmount" tabindex="2"  value="<?php print($shipping_amount); ?>" size="64" style="width:120px;" maxlength="64" readonly="readonly"/>
                    </div>
                    
                    <div style="clear:both;"> </div>

                    <div id="separator">
                        <label for="priceCurrency">Grand Total: </label> 
                        <select name="priceCurrency" id="priceCurrency" style="width: 55px;" disabled="disabled">
                            <option value="1" <?php if ($prd_pricCurrency == '£') { ?> selected="selected" <?php } ?>>£</option>
                            <option value="2" <?php if ($prd_pricCurrency == '$') { ?> selected="selected" <?php } ?>>$</option>
                        </select>
                        <input name="grandTotal" type="text" id="grandTotal" tabindex="2"  value="<?php print($grand_total); ?>" size="64" style="width:120px;" maxlength="64" readonly="readonly"/>
                    </div>
                    

                    <div id="separator">
                        <label for="priceCurrency">Payment Method: </label> 
                        <input name="paymentMethod" type="text" id="paymentMethod" tabindex="2"  value="<?php print($payment_method); ?>" size="64" style="width:120px;" maxlength="64" readonly="readonly"/>
                    </div>
                    
                </fieldset>


              
            </div>

            <p>
                <input type="hidden" name="id" id="id" value="<?php print($id); ?>" />
                <input type="hidden" name="added_date" id="added_date" value="<?php print($added_date); ?>" />
                <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action); ?>" />
                <input type="hidden" name="displayOrder" id="displayOrder" value="<?php print($displayOrder); ?>" />

                <a href="order.html"> <input id="testbutton" type="button" value="Back to list" name="Submit" /> </a>
            </p>
            <div id="show"></div>
        </form>



    </div>

    <div class="clear">
    </div> 

    <div style="height:10px;"></div>
</div>	
