    <div id="table_main_wrapper" style="margin-top: 10px;">

        <div id="search_main_wrapper">
    <h2>The Order Details</h2>     
</div>
        
        
        
        
        
        <div id="main">
                	<div class="invoice_left">
                    	<h3>Order ID : <span></b> <?php print($order_id); ?></span></h3>
                        
                       
                    </div>
                    <div class="invoice_right">
                    	<div align="right" class="pending"><span>Order Date  :</span><?php print($order_date); ?></div>
                        <div align="right" class="pending"><span>Shipping Status  :</span><?php print(ucfirst(strtolower($order_status))); ?></div>
                        <div align="right" class="pending"><span>Payment Status  :</span><?php print(ucfirst(strtolower($payment_status)));?></div>
                        <div align="right" class="pending"><span>Payment Method  :</span><?php print(ucfirst($payment_method)); ?></div>
                    </div> 
                   <div class="clear"></div>
                   <div><hr /></div>
                   <div class="invoice_left">
                   
                        
                        <h4>Shipping Details</h4>
                        <ul>
                            <li><span> Full Name&nbsp; : </span><?php print($shp_title.'&nbsp;'.$shp_firstName.'&nbsp;'.$shp_lastName);?></li>
                        <li><span>Company Name&nbsp; :</span><?php print($shp_houseNumber);?></li>
                        <li><span> Phone&nbsp; :</span><?php print($shp_phone);?></li>
                        <li><span> Address &nbsp; :</span><?php print($shp_address);?></li>
                        <li><span>City&nbsp; :</span><?php print($shp_city);?></li>
                        <li><span>State&nbsp; :</span><?php print($shp_state);?></li>
                        <li><span>Postal Code&nbsp; :</span><?php print($shp_postCode);?></li>
                        <li><span>Country&nbsp; : </span><?php print($shp_country);?></li>
                    </ul>
                    </div>
                    <div class="invoice_right">
                    	 <h4>Billing Details</h4>
                        
                         <ul>
                <li><span> Full Name&nbsp; : </span><?php print($bil_title.'&nbsp;'.$shp_firstName.'&nbsp;'.$shp_lastName);?></li>
                <li><span>Company Name&nbsp; :</span><?php print($bil_houseNumber);?></li>
                <li><span> Phone&nbsp; :</span><?php print($bil_phone);?></li>
                <li><span> Address &nbsp; :</span><?php print($bil_address);?></li>
                <li><span>City&nbsp; :</span><?php print($bil_city);?></li>
                <li><span>State&nbsp; :</span><?php print($bil_state);?></li>
                <li><span>Postal Code&nbsp; :</span><?php print($bil_postCode);?></li>
                <li><span>Country&nbsp; : </span><?php print($bil_country);?></li>
            </ul>
                         
                    </div>
                     <div class="clear"></div>
                      <div><hr /></div>
                      <div class="invoice">
                      
                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td width="68" >ID</td>
                                      <td width="428" >Product</td>
                                      <td width="68" >Price</td>
                                    <td width="68" >Quantity</td>
                                    
                                    <td width="104">Sub Total</td>
                               
                                  </tr>
                                  
                                  <?php 
                                  $orderCurrency = '';
                                  $shippingCost  = 12;
                                  $finalAmount = '';
                                  $totalAmount = 0;
                foreach ($orderDtls->orderItemDetails as $orderIndex=>$orderProduct) {
                    $qty = $orderProduct->productQty;
                    $product_name = $orderProduct->productDtls->productName;
                    $price = $orderProduct->productPrice;
                    
                    $total += $grand_total;
                    $tot_subTotal += $sub_total;
                   $totalAmount = $totalAmount + $sub_total;
                    $orderCurrency = $orderProduct->productDtls->priceCurrency->code;
                    
               ?>
                                   <tr>
                                    <td class="in_row1"><?php print($orderProduct->productDtls->id); ?></td>
                                     <td class="in_row1"><?php print($product_name); ?></td>
                                     <td width="68" class="in_row1"><?php print($price);?></td>
                                    <td <?php if($orderIndex%2 == 0){?> class="in_row1" <?php } else {?> class="in_row2" <?php } ?>><?php print($qty); ?></td> 
                                   
                                    <td class="in_row1"><?php print($orderCurrency); ?> <?php print($sub_total); ?></td>
                                    
                                  </tr>
                                  <?php }?>
                                  
                                   <tr>
                                    <td  class="in_row3">&nbsp;</td> 
                                    <td  class="in_row3"></td>
                                    <td  class="in_row3" colspan="2">Sub Total</td>
                                   
                                    <td  class="in_row3"><?php print($orderCurrency); ?> <?php print(priceFormateNumber($totalAmount));?></td>
                                  </tr> 
                                  <tr>
                                    <td  class="in_row3">&nbsp;</td> 
                                    <td  class="in_row3"></td>
                                    <td  class="in_row3" colspan="2">Shipping Amount</td>
                                   
                                    <td  class="in_row3"> <?php print($orderCurrency); ?> <?php print(priceFormateNumber($shippingCost));?></td>
                                  </tr>
                                    <tr>
                                    <td  class="in_row">&nbsp;</td> 
                                    <td  class="in_row"></td>
                                    <td  class="in_row4" colspan="2"> TOTAL</td>
                                   
                                    <td  class="in_row4"> <?php 
                                    $finalAmount = $totalAmount + $shippingCost;
                                    print($orderCurrency); ?> <?php print(priceFormateNumber($finalAmount));?></td>
                                  </tr>
                                </table>


                      </div>
                </div>
                <!-- // #main -->
        
        
        
        


    </div>