
<script type="text/javascript">
	
    $(document).ready(function(){
		
        //Display Loading Image
        function Display_Load()
        {
            $("#manageloading").fadeIn(900,0);
            $("#manageloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        //Hide Loading Image
        function Hide_Load()
        {
            $("#manageloading").fadeOut('slow');
        };
	
        //Default Starting Page Results
   
        $("#pagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
	
        Display_Load();
	
        $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/order/includes/pagination_data.php?order_date=<?php echo $order_date; ?>&method_paid=<?php echo $method_paid; ?>&status=<?php echo $status; ?>&orderno=<?php echo $orderno; ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=order&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>", Hide_Load());

        //Pagination Click
        $("#pagination li").click(function(){
			
            Display_Load();
		
            //Loading Data
            var pageNum = this.id;
		
            $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/order/includes/pagination_data.php?page=" + pageNum+"&m=order&order_date=<?php echo $order_date; ?>&method_paid=<?php echo $method_paid; ?>&status=<?php echo $status; ?>&orderno=<?php echo $orderno; ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=order&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>", Hide_Load());
        });
        
        
        
    });
    
  
</script>
<?php 

?>
<div id="search_main_wrapper">
    <h2>View Order List</h2>     
</div>

<script type="text/javascript" src="<?php print(ADMIN_BASE_URL); ?>/modules/order/js/jquery-1.3.2.js"></script>        
<link type="text/css" href="<?php print(ADMIN_BASE_URL); ?>/modules/order/css/flick/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
<script type="text/javascript" src="<?php print(ADMIN_BASE_URL); ?>/modules/order/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript">
    $(function(){
        var today       = new Date();
        
        $( "#order_date" ).datepicker({
            showOn: "button",
            buttonImage: "<?php print(ADMIN_BASE_URL); ?>/modules/order/css/imgs/calendar.gif",
            buttonImageOnly: true
            //minDate: new Date(today)
        });
       /* 
        $( "#end_date" ).datepicker({                               
            showOn: "button",
            buttonImage: "<?php print(ADMIN_BASE_URL); ?>/modules/testimonial/images/calendar.gif",
            buttonImageOnly: true
            //minDate: new Date(today)
        });*/
        
    });
</script>
<div id="table_main_wrapper">
    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard"></div>
    </div>

    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                          <!--  <div style="margin-top:45px;"><a href="<?php print(ADMIN_BASE_URL); ?>order/order.html?action=add" id="testbutton">Add New Product</a></div> -->
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <div style="padding-top: 75px; padding-left: 20px;">
                            <form method="post" action="order.html" >
                                            
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td colspan="2"></td></tr>
                                                <tr>
                                                    <td>Order Date</td>
                                                    <td>
                                                        <input type="text" id="order_date" name="order_date" value="<?php echo $order_date; ?>" size="10" readonly="readonly"/>			
                                                    </td>
                                                    <td>Payment Method</td>
                                                    <td>
                                                        <select name="method_paid" id="method_paid">
                                                            <option value="">--select--</option>
                                                            <option value="PayPal" <?php if ($method_paid == 'paypal') echo 'selected=selected'; ?>>Pay Pal</option>
                                                            <option value="SecureTrading" <?php if ($method_paid == 'securetrad') echo 'selected=selected'; ?>>Secure Trading</option>
                                                            <option value="cash" <?php if ($method_paid == 'cash') echo 'selected=selected'; ?>>cash</option>
                                                            <option value="sage" <?php if ($method_paid == 'sage') echo 'selected=selected'; ?>>sage</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="4" height ="10px"></td></tr>
                                                <tr>
                                                    <td>Order Status</td>
                                                    <td> <select name="status" id="status">
                                                            <option value="">--select--</option>
                                                            <option value="PENDING" <?php if ($status == 'PENDING') echo 'selected=selected'; ?>>Pending</option>
                                                            <option value="SHIPPED" <?php if ($status == 'SHIPPED') echo 'selected=selected'; ?>>Shipped</option>
                                                        </select>
                                                    </td>
                                                    <td>Invoice No</td>
                                                    <td><input type="text" name="orderno" id="orderno" value="<?php echo $orderno; ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td><!--Reservation ID--></td>
                                                    <td><!--<input type="text" name="reser_id" value="<?php //echo $reser_id   ?>" />--></td>
                                                    <td></td>
                                                    <td><input type="hidden" name="page" value="<?php echo $page; ?>" /><input type="submit" name="searchtext" value="Search" />&nbsp;<input type="button" name="rst" value="Reset" onclick="rst_fields(this.form)"/></td>
                                                </tr>
                                            </table>
                                                
                                        </form>
                            </div>

                        </td>
                    </tr>
                     <tr>
                        <td align="center" height ="20px;">
                          
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:5px">	
                                <table width="765px">
                                    <tr>
                                        <td width="30">Pages:</td>
                                        <td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td>
                                        <td width="90">Rows per page :</td>
                                        <td width="30">
                                            <select onchange="javascript:goToPage(options[selectedIndex].value)" id="cmbRowsPerPage" name="cmbRowsPerPage" style="width: 50px;">
                                                <option value=""></option>

                                                <?php for ($row_num = 10; $row_num <= 200; $row_num = $row_num + 10) { ?>
                                                    <option value="order.html?q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($row_num); ?>" <?php if ($_GET['rows'] == $row_num) echo "selected=selected" ?>><?php print($row_num); ?></option>
                                                <?php } ?>
                                            </select> 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">




                            <div id="manageloading" ></div>
                            <div id="managecontent" ></div>




                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:40px">	
                                <table width="800px">
                                    <tr><td width="30">Pages:</td><td>
                                            <ul id="pagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $pageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td></tr>
                                </table>
                            </div>

                        </td>
                    </tr>

                </tbody></table>
        </div>
    </div>
    <div style="height:10px;"></div>
</div>