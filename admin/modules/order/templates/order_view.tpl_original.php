    <div id="table_main_wrapper" style="margin-top: 10px;">
<style type="text/css">
    
    table {
        font: Arial;
        font-size: '2';
    }
</style>
<div style="background: #555555;">
<table width="85%" align="center">
    <tr>
        <td>
            <img src="http://www.cowperautopaints.co.uk/includes/templates/cowper/contents/images/logo.png" />
            <div id="separator"></div><div id="separator"></div>
            <table width=100% border="0" cellspacing="0" cellpadding="1">
                <tr>
                    <td><font face="" size=2><b>Order ID: </b> <?php print($order_id); ?></td>
                    <td align=right><b>INVOICE</b>&nbsp;&nbsp;&nbsp;<br/>
                        <b><font size=2 color=BROWN><?php if($paid) echo 'UN PAID ORDER';?>&nbsp;&nbsp;&nbsp;</font></b>
                    </td>
                </tr>
                <tr>
                    <td><font face="" size=2></td>
                    <td align=right><font face="" size=2 color=white><?php print($order_status); ?></font>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr><td colspan="2"><b>Date:</b> <font face="" size=2><?php print($order_date); ?></td></tr> 
                <tr><td colspan="2"><b>Payment by:</b><?php print(ucfirst($payment_method)); ?></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
              
                <tr><td colspan="2">&nbsp;</td></tr>
            </table>
            
        </td>
    </tr>
    <tr><td height="20px;"></td></tr>
    <tr>
        <td>

            <table width=100% border=0 cellpadding=3 cellspacing=0>
                <tr>
                    <td width=12% align=center bgcolor=#00000><font face="" size=2 color=white><b>Quantity</b></td>
                    <td width=20% bgcolor=#00000><font face="" size=2 color=white><b>Product</b></td>
                    <td width=24% align=right bgcolor=#00000><font face="" size=2 color=white><b>Sub Total</b></td>
                    <td width=24% align=right bgcolor=#00000><font face="" size=2 color=white><b>Shipping Amount</b></td>
                    <td width=34% align=right bgcolor=#00000><font face="" size=2 color=white><b>Grand Total</b></td>
                </tr>

            

                <tr><td colspan="5"><hr size=1></td></tr>
                
                <?php 
                foreach ($orderDtls->orderItemDetails as $orderProduct) {
                    $qty = $orderProduct->productQty;
                    $product_name = $orderProduct->productDtls->productName;
                    $price = $orderProduct->productPrice;
                    
                    $total += $grand_total;
                    $tot_subTotal += $sub_total;
                    $tot_shipAmnt += $shipping_amount;
               ?>
              
                <tr>
                    <td width=12% align=center ><font face="" size=2 color=white><?php print($qty); ?></td>
                    <td width=20% ><font face="" size=2 color=white><?php print($product_name); ?></td>
                    <td width=24% align=right><font size=2 color=white><?php print($sub_total); ?></td>
                    <td width=24% align=right><font size=2 color=white><?php print($shipping_amount); ?></td>
                    <td width=34% align=right><font  size=2 color=white><?php print(number_format($grand_total,2)); ?></td>
                </tr>
                
                <?php }?>
                <tr><td colspan="5" height="5px;"></td></tr>
                <tr>
                    <td colspan=3></td>
                    <td align=left><b>Sub Total</b></td>
                    <td align=right><?php print(number_format($tot_subTotal,2)); ?></td>
                </tr>
                <tr>
                    <td colspan=3></td>
                    <td align=left><b>Shipping Amount</b></td>
                    <td align=right><?php print(number_format($tot_shipAmnt,2)); ?></td>
                </tr>

                <tr><td colspan=5><hr size=1></td></tr>

                <tr>
                    <td colspan=3></td>
                    <td align=left><b>TOTAL</b></td>
                    <td align=right><?php print(number_format($total,2)); ?></td>
                </tr>

            </table>	

        </td>
    </tr>
    <tr><td align=center bgcolor=#00000><h3 color=white>Shipping Details</h3></td></tr>
    <tr>
        <td>

            <ul>
                <li> Full Name&nbsp; : <?php print($shp_title.'&nbsp;'.$shp_firstName.'&nbsp;'.$shp_lastName);?></li>
                <li>Company Name&nbsp; :<?php print($shp_houseNumber);?></li>
                <li> Phone&nbsp; :<?php print($shp_phone);?></li>
                <li> Address &nbsp; :<?php print($shp_address);?></li>
                <li>City&nbsp; :<?php print($shp_city);?></li>
                <li>State&nbsp; :<?php print($shp_state);?></li>
                <li>Postal Code&nbsp; :<?php print($shp_postCode);?></li>
                <li>Country&nbsp; : <?php print($shp_country);?></li>
            </ul>

        </td>
    </tr>
    <tr><td height="10px;"></td></tr>
    <tr><td align=center bgcolor=#00000><h3 color=white>Billing Details</h3></td></tr>
    <tr>
        <td>

            <ul>
                <li> Full Name&nbsp; : <?php print($bil_title.'&nbsp;'.$shp_firstName.'&nbsp;'.$shp_lastName);?></li>
                <li>Company Name&nbsp; :<?php print($bil_houseNumber);?></li>
                <li> Phone&nbsp; :<?php print($bil_phone);?></li>
                <li> Address &nbsp; :<?php print($bil_address);?></li>
                <li>City&nbsp; :<?php print($bil_city);?></li>
                <li>State&nbsp; :<?php print($bil_state);?></li>
                <li>Postal Code&nbsp; :<?php print($bil_postCode);?></li>
                <li>Country&nbsp; : <?php print($bil_country);?></li>
            </ul>

        </td>
    </tr>
    </tr>
</table>
</div>
    </div>