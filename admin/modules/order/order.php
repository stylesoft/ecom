<?php
/** '
 * @desc : order management
 * @author : Gayan Chathuranga <gayan@monra.com>
 */
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

//object properties
$id = '';
$customerId = '';
$order_date = '';
$order_status = '';
$payment_status = '';
$sub_total = '';
$shipping_amount = '';
$grand_total = '';
$payment_method = '';


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';


$objOrder = new Order();
$objOrder->tb_name = 'tbl_order';

                                                /*
                                                $objCategory = new Category();
                                                $objCategory->tb_name = 'tbl_category';
                                                $objCategory->searchStr = '';
                                                $objCategory->limit = '';
                                                $objCategory->listingOrder = '';
                                                $categoryResult = $objCategory->search();
                                                */

if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit' || $action == 'view') {

        if (isset($_GET['id'])) {

            $id = $_GET['id'];
            $orderDtls = $objOrder->getOrder($id);
            
            //print_r($orderDtls->orderItemDetails);
            
            $order_id = $orderDtls->id;
            $customerId = $orderDtls->customerId;
            $order_date = $orderDtls->orderDate;
            $order_status = $orderDtls->orderStatus;
            $payment_status = $orderDtls->paymentStatus;
            $sub_total = $orderDtls->subTotal;
            $shipping_amount = $orderDtls->shippingAmount;
            $grand_total = $orderDtls->grandTotal;
            $payment_method = $orderDtls->paymentMethod;
            
            //customer details
            $cus_title = $orderDtls->cutomerDetails->title;
            $cus_firstName = ucfirst($orderDtls->cutomerDetails->firstName);
            $cus_lastName = ucfirst($orderDtls->cutomerDetails->lastName);
            $cus_email = $orderDtls->cutomerDetails->email; //pwd is there
            $cus_status =  $orderDtls->cutomerDetails->status;
            $cus_addedDate = $orderDtls->cutomerDetails->addedDate;
            
            
            //get order item details
            $itm_orderId = $orderDtls->orderItemDetails->orderId;
            $itm_productId = $orderDtls->orderItemDetails->orderId;
            $itm_prdctQty = $orderDtls->orderItemDetails->productQty;
            $itm_prdctPrice = $orderDtls->orderItemDetails->productPrice;
            
            
           // print_r($orderDtls->orderItemDetails);
            
            //get item product details
            $prd_prdctName = $orderDtls->orderItemDetails->productDtls->productName;
            $prd_smallDesc = $orderDtls->orderItemDetails->productDtls->productSmallDescription;
            $prd_unitPrice = $orderDtls->orderItemDetails->productDtls->unitPrice;
            $prd_pricCurrency = $orderDtls->orderItemDetails->productDtls->priceCurrency;
            $prd_weightunit = $orderDtls->orderItemDetails->productDtls->weightUnit;
            $prd_prdctWeight = $orderDtls->orderItemDetails->productDtls->productWeight;

            $prd_category = $orderDtls->orderItemDetails->productDtls->category;
            $prd_description = $orderDtls->orderItemDetails->productDtls->productDescription;
            $prd_defaultImg = $orderDtls->orderItemDetails->productDtls->productDefaultImage;
            $prd_code = $orderDtls->orderItemDetails->productDtls->productCode;
            
            //customer billingdtls
          //  print_r($orderDtls->cutomerDetails->addressBillingDtls);
            $bil_address = $orderDtls->cutomerDetails->addressBillingDtls[0]->address;

            $bil_region  = $orderDtls->cutomerDetails->addressBillingDtls[0]->region;
            $bil_state = $orderDtls->cutomerDetails->addressBillingDtls[0]->state;
            $bil_city  = $orderDtls->cutomerDetails->addressBillingDtls[0]->city;
            $bil_postCode = $orderDtls->cutomerDetails->addressBillingDtls[0]->postcode;
            $bil_country = $orderDtls->cutomerDetails->addressBillingDtls[0]->country;
            $bil_objType = $orderDtls->cutomerDetails->addressBillingDtls[0]->addressObjectType;
            $bil_houseNumber = $orderDtls->cutomerDetails->addressBillingDtls[0]->houseNumber;
            $bil_title = $orderDtls->cutomerDetails->addressBillingDtls[0]->title;
            $bil_firstName = $orderDtls->cutomerDetails->addressBillingDtls[0]->firstName;
            $bil_lastName = $orderDtls->cutomerDetails->addressBillingDtls[0]->lastName;
            $bil_phone = $orderDtls->cutomerDetails->addressBillingDtls[0]->phone;
            
            //customer shipping details            
            $shp_address = $orderDtls->cutomerDetails->addressShippingDtls[0]->address;
            $shp_region  = $orderDtls->cutomerDetails->addressShippingDtls[0]->region;
            $shp_state = $orderDtls->cutomerDetails->addressShippingDtls[0]->state;
            $shp_city  = $orderDtls->cutomerDetails->addressShippingDtls[0]->city;
            $shp_postCode = $orderDtls->cutomerDetails->addressShippingDtls[0]->postcode;
            $shp_country = $orderDtls->cutomerDetails->addressShippingDtls[0]->country;
            $shp_objType = $orderDtls->cutomerDetails->addressShippingDtls[0]->addressObjectType;
            $shp_houseNumber = $orderDtls->cutomerDetails->addressShippingDtls[0]->houseNumber;
            $shp_title = $orderDtls->cutomerDetails->addressShippingDtls[0]->title;
            $shp_firstName = ucfirst($orderDtls->cutomerDetails->addressShippingDtls[0]->firstName);
            $shp_lastName = ucfirst($orderDtls->cutomerDetails->addressShippingDtls[0]->lastName);
            $shp_phone = $orderDtls->cutomerDetails->addressShippingDtls[0]->phone;
            
            
            
        }
    }
}

if ($_POST) {


    //advanced search
    if ($_POST['searchtext']) {
        $order_date = $_POST['order_date'];
        $method_paid = $_POST['method_paid'];
        $status = $_POST['status'];
        $orderno = $_POST['orderno'];
        $page = $_POST['page'];
        
        $_SEARCH_QUERY = str_replace(" ", "", trim($_SEARCH_QUERY));
        // get all the product details
        $objSearchParam = new stdClass();
        $objProduct->searchStr = $_SEARCH_QUERY;
        $totalNumberOfMenus = $objOrder->countRec();
        $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
        
    }

    $action = $_POST['txtAction'];
    
    //TO DO : update payment status and order status
   
    $currentOrderId = '';

    if ($action == 'add') {
        $lastInsertedId = $objProduct->addProduct();
        $currentOrderId = $lastInsertedId;
    } else if ($action == 'edit') {
        //$currentOrderId = $id;
        //$isRecordUpdated = $objProduct->editProduct();
    }

} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "order_date";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ", "", trim($_SEARCH_QUERY));
    // get all the product details
    $objSearchParam = new stdClass();
    $objProduct->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objOrder->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}elseif($action == 'view'){
    $CONTENT = MODULE_TEMPLATES_PATH . "order_view.tpl.php";
} else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}
$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
