<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');
require_once('../../../application/classes/Brand.php');


 $id;
 $brand_name;
 $brand_description;
 $status;
 $displayorder;
 $brand_image;
 $Cattype;

// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
//$arrBrand= array();

$objBrand = new Brand();
$objBrand->tb_name = 'tbl_brand';
//$result = $objBrand->search();
//print_r($result); exit;

$objCategory = new Category();
$objCategory->tb_name = 'tbl_category';
$objCategory->searchStr    = '';
$objCategory->limit        = '';
$objCategory->listingOrder = '';
$categoryResult            = $objCategory->getAllParentCategory('Enabled');

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                //$arrBrand = $objBrand->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {             
            $BrandInfo = $objBrand->getBrand($id);

            $id = $BrandInfo->id;
            $brand_name = $BrandInfo->brand_name;
            $Cattype = $BrandInfo->Cattype;
            $brand_description = $BrandInfo->brand_description;
            $status = $BrandInfo->status;
            $displayorder = $BrandInfo->displayorder;
            $brand_image = $BrandInfo->brand_image;
   
        
    }
    
   
}

if ($_POST) {
//print_r($_POST); exit;
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $brand_name = $_POST['brand_name'];
    $brand_description = $_POST['brand_description'];
    $status = $_POST['status'];
    $displayorder = $_POST['display_order'];
    $brand_image = $_POST['brand_image'];
    $Cattype = $_POST['cmbBrandCategory'];


    //validation
    if($brand_name == ""){
            array_push($objBrand->error, 'The Brand Name is required');
            $invalid = true;
        }
        

    $objBrand->id = $id;
    $objBrand->brand_name = $brand_name;
    $objBrand->brand_description = mysql_real_escape_string($brand_description);
    $objBrand->status = $status;
    $objBrand->displayorder = $displayorder;
    $objBrand->brand_image = $brand_image;
    $objBrand->Cattype =$Cattype;
    
    if ($action == 'add' && !$invalid) {
    	 $objBrand->id = '';
          $lastInsertedId = $objBrand->addBrand();
        
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objBrand->editBrand();
    }

    
    $newPageUrl = "brand.html?action=add";
    $mainPageUrl = "brand.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Brand <span class='red'>$brand_name</span> has been added!<br /><br /><br />Want to enter more Brand?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Brand <span class='red'>$brand_name</span> has been updated!<br /><br /><br />Want to enter more Brand?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {
	
 if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
        	$ORDER_BY = $_GET['orderby'];
        } else {
        	$ORDER_BY = "display_order";
        }
        
        if (isset($_GET['order'])) {
        	$ORDER = $_GET['order'];
        } else {
        	$ORDER = 'Asc';
        }

        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }


    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objBrand->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objBrand->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}




if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
