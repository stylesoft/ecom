<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>

<script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: "../uploadFile.php",
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#txtSmallFileName").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                    
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper">

                <h2>Add/Edit Brand</h2>
    </div>


    

    <div id="table_main_wrapper">



            <div id="dashboard" style="background-color:#FFF;">

            <form name="form1" id="form1" action="" method="post">
            
                     <div id="two">
                     
                       <?php 
                        $contentImage = '';
                        
                        if($brand_image != ''){
                            $contentImage = SITE_BASE_URL.'imgs/'.$brand_image;
                        } else {
                            $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                        }
                        
                        ?>      
                        
                        
                        <fieldset style="margin-top:35px;">
                        
                        <div id="separator">
                       
                            <label for="pagetitle">Image : </label>
                            <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg" id="btnImg"/>
                           
                            <input type="hidden" id="txtSmallFileName" name="brand_image" value="<?php print($brand_image);?>" />
                        </div>
                        
                        </fieldset>
                        
                        
                         <fieldset style="margin-top: 20px;">
                   
                    
                    <div id="separator">
                        <label for="headline">Brand Name: </label>
                        <input name="brand_name" type="text" id="brand_name" value="<?php echo $brand_name; ?>" size="65" />
                    </div>
                   
 
                    <div id="separator">
                        <label for="pagename">Status: </label>

                        <select name="status" id="status" style="width: 200px;">
                            <option value="">--Select--</option>
                            <option value="Enabled" <?php if ($status == 'Enabled') { ?> selected="selected" <?php } ?>>Enabled</option>
                            <option value="Disabled" <?php if ($status == 'Disabled') { ?> selected="selected" <?php } ?>>Disabled</option>
                        </select>
                    </div>
                        <div id="separator">
                            <label for="status">Brand Category : </label> 
                            <select name="cmbBrandCategory" id="cmbBrandCategory" style="width: 200px;">
                                
                                <?php foreach($categoryResult As $catIndex=>$categoryData){
                                    if($categoryData->id != '-1'){
                                    
                                    ?>
    
                                <option value="<?php print($categoryData->id);?>" <?php  if($Cattype == $categoryData->id){?> selected="selected" <?php  } ?>><?php print($categoryData->category_name);?></option>
                                <?php }} ?>
                            </select>
                        </div>     
                    
                      <div id="separator">


                            <label for="lcol_description"> Description : </label> 
                            <textarea name="brand_description" style="width:600px; height:80px;" id="descriptionB"> <?php print($brand_description);?> </textarea>
                        </div>  

                </fieldset>
                     </div>
                <p>
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action; ?>"/>
                    <input type="hidden" name="display_order" id="display_order" value="<?php echo $displayorder; ?>"/>
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>

            </form>

<div class="clear">
        </div> 

        </div>



        <div style="height:10px;"></div>
    </div>	
    
    
    
    
    
    
    
    <script>
CKEDITOR.replace( 'descriptionB' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 350
});
</script>