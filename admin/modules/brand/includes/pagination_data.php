<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "";
$order          = "";
$orderStr       = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            // get the search query....
            if(isset($_GET['q'])){
                $searchQ   = $_GET['q'];
            }
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }
            
            if(isset($_GET['order'])){
                $order = $_GET['order'];
            }

            if($orderBy != '' && $order != ''){
                $orderStr       = $orderBy." ".$order;                
            } else {
                
                $orderStr       = 'display_order'." ".'Asc';  
            }
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}



//--------------------------------------------

$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;


$objBrand = new Brand();
$objBrand->tb_name = 'tbl_brand';

$objBrand->searchStr = $searchQ;
$objBrand->limit = $recLimit;
$objBrand->listingOrder = $orderStr;
$brandResult = $objBrand->search();


//--------------------------------------------

?>
<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			$.post("<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/updateListingOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

function deleterow(id){
if (confirm('Are you sure want to delete?')) {
	 $.post('<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/deletebrand.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder"+id).fadeOut("slow");

//$(".message").delay(2000).fadeOut(1000);
});
}
}

</script>

<div id="tablecontents"> 
<table id="users">
    <thead> 
		<tr height="12px">
		  <td  width="70">
		  	ID
		  	<a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=asc">&darr;</a>
		  </td>
        <td  width="518">
        Brand Name
        <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=brand_name&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=brand_name&order=asc">&darr;</a>
        </td>
     
        <td width="172">&nbsp;</td>
       
        </tr>
        </thead> 

        <tbody> 
	
        </tbody>
        
        
         <?php 
      if(count($brandResult) >0){
      foreach($brandResult As $rowIndex=>$brandData) {?> 
        	<tr <?php if($rowIndex%2 == 0 && $brandData->id != '-1' ){?> class="odd" <?php } ?> id="arrayorder<?php print($brandData->id);?>">
        	<?php if ($brandData->status == "Enabled" && $brandData->id != '-1') {?>
	    	<td height="29"><?php print($brandData->id);?></td>
            <td height="29"><?php print($brandData->brand_name);?></td>
            
	    <td class="action">
                 <a href="<?php print($module);?>.html?action=edit&id=<?php print($brandData->id);?>" class="edit">Edit</a>
                 <a href="#" onclick="deleterow(<?php echo $brandData->id; ?>)" class="delete">Delete</a>
            </td>
	   
                
         <?php }} ?>
           <?php } else { ?>      
       
		
	</table>
	 <div id="listings" style="background:#eee;">No Matching Records Found </div>
		<?php }?>
	</div>