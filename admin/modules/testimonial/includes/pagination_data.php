<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'listingId' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}




$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objTestimonial = new Testimonial();
$objTestimonial->tb_name = 'tbl_testimonials';

$objTestimonial->searchStr = $searchQ;
$objTestimonial->limit = $recLimit;
$objTestimonial->listingOrder = $orderStr;
$testmContent = $objTestimonial->search();
?>
<script type="text/javascript">
    $(document).ready(function(){ 	
        function slideout(){
            setTimeout(function(){
                $("#response").slideUp("slow", function () {
                });
    
            }, 2000);}
	
        $("#response").hide();
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
                    var order = $(this).sortable("serialize") + '&update=update'; 
                    $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?php print($module); ?>/includes/updateListingOrder.php", order, function(theResponse){
                        $("#response").html(theResponse);
                        $("#response").slideDown('slow');
                        slideout();
                    }); 															 
                }								  
            });
        });

    });	

    function deleterow(id){
        if (confirm('Are you sure want to delete?')) {
            $.post('<?php print(ADMIN_BASE_URL); ?>modules/<?php print($module); ?>/includes/deletelisting.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                return false;
                //$(".message").delay(2000).fadeOut(1000);
            });
        }
    }

</script>




<div id="list">
    <div id="table_main_div" >
        <div id="response"> </div>
        <div class="row_heding">

            <div class="colum" style="width:430px;">
                <strong>Title</strong>
                <a href="<?php print(ADMIN_BASE_URL); ?>testimonial/testimonials.html?q=<?php print($searchQ); ?>&orderby=tes_title&order=desc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-up.png" alt="" width="12" height="7" border="0" /></a>
                <a href="<?php print(ADMIN_BASE_URL); ?>testimonial/testimonials.html?q=<?php print($searchQ); ?>&orderby=tes_title&order=asc"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/arrow-down.png" alt="" width="12" height="7" border="0" /></a>
            </div>

            <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
        </div>
        <div class="clear"></div>

        <ul>
            <?php
            if (count($testmContent) > 0) {
                foreach ($testmContent As $rowIndex => $testmData) {   
                   // print($testmData->image[0]->recordText);
                    ?>   
                    <li id="arrayorder_<?php print($testmData->id); ?>">
                        <?php if ($rowIndex % 2 != 0) { ?>
                            <div class="row1">
                            <?php } else { ?>
                                <div class="row2">
                                <?php } ?>
                                <div class="colum" style="width:430px;"><?php print(substr($testmData->tes_title,0,50)); ?></div>

                                <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">

                                    <a href="testimonials.html?action=edit&id=<?php print($testmData->id); ?>">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                    <a href="#" onclick="deleterow(<?php echo $testmData->id; ?>);">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>
                    </li>

                <?php }
            } else {
                ?>
                <li>
                    <div id="listings" style="background:#eee;">No Matching Records Found </div>
                </li>
<?php } ?>

        </ul>



    </div>

</div>