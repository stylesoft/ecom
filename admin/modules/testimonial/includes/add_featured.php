<?php
ob_start();
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$id = $_GET['new'];

$objTestimonial = new Testimonial();
$objTestimonial->tb_name = 'tbl_testimonials';


if($objTestimonial->addFeatured($id)){
    header("Location: ../../../testimonial/testimonials.html?c=featured");
    exit;
}
ob_flush();
?>
