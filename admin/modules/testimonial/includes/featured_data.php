<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'listingId' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}




$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objTestimonial = new Testimonial();
$objTestimonial->tb_name = 'tbl_testimonials';

$objTestimonial->searchStr = $searchQ;
$objTestimonial->limit = $recLimit;
$objTestimonial->listingOrder = $orderStr;
$testmContent = $objTestimonial->getAllFeatured();
?>
<script type="text/javascript">
    $(document).ready(function(){ 	
        function slideout(){
            setTimeout(function(){
                $("#response").slideUp("slow", function () {
                });
    
            }, 2000);}
	
        $("#response").hide();
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
                    var order = $(this).sortable("serialize") + '&update=update'; 
                    $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?php print($module); ?>/includes/updateListingOrder.php", order, function(theResponse){
                        $("#response").html(theResponse);
                        $("#response").slideDown('slow');
                        slideout();
                    }); 															 
                }								  
            });
        });

    });	

    function deleterow(id){
        if (confirm('Are you sure want to delete?')) {
            $.post('<?php print(ADMIN_BASE_URL); ?>modules/<?php print($module); ?>/includes/deletelisting.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                return false;
                //$(".message").delay(2000).fadeOut(1000);
            });
        }
    }

</script>




<div id="list">
    <div id="table_main_div" >
        <div class="row_heding">

            <div class="colum" style="width:210px;">
                <strong>Project&nbsp;</strong>
                </div>
            <div class="colum" style="width:220px;">
                <strong>Title</strong>              
                
            </div>

            <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
        </div>
        <div class="clear"></div>

        <ul>
            <?php
            if (count($testmContent) > 0) {
                foreach ($testmContent As $rowIndex => $testmData) {   
                   // print($testmData->image[0]->recordText);
                    ?>   
                    <li id="arrayorder_<?php print($testmData->id); ?>">
                        <?php if ($rowIndex % 2 != 0) { ?>
                            <div class="row1">
                            <?php } else { ?>
                                <div class="row2">
                                <?php } ?>
                                 <div class="colum" style="width:120px;"><img src="<?php print(SITE_BASE_URL); ?>includes/extLibs/mythumb.php?file=../../imgs/<?php print($testmData->image[0]->recordText); ?>&width=48&height=40" /></div>
                                <div class="colum" style="width:70px;"><?php print($testmData->project_id); ?></div>
                                <div class="colum" style="width:220px;"><?php print($testmData->tes_title); ?></div>

                                <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">

                                    <a href="../modules/testimonial/includes/remove_featured.php?current=<?php print($testmData->id); ?>">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Edit" />
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <div class="clear"></div>
                    </li>

                <?php }
            } else {
                ?>
                <li>
                    <div id="listings" style="background:#eee;">No Matching Records Found </div>
                </li>
<?php } ?>

        </ul>



    </div>

</div>