<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$id = $_GET['current'];

$objTestimonial = new Testimonial();
$objTestimonial->tb_name = 'tbl_testimonials';


if($objTestimonial->removeFeatured($id)){
    header("Location: ../../../testimonial/testimonials.html?c=featured");
}

?>
