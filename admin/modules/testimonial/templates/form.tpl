<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php print(ADMIN_BASE_URL); ?>/modules/order/js/jquery-1.3.2.js"></script>        
<link type="text/css" href="<?php print(ADMIN_BASE_URL); ?>/modules/order/css/flick/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
<script type="text/javascript" src="<?php print(ADMIN_BASE_URL); ?>/modules/order/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript">
    $(function(){
        var today       = new Date();
        
        $( "#designation" ).datepicker({
            showOn: "button",
            buttonImage: "<?php print(ADMIN_BASE_URL); ?>/modules/order/css/imgs/calendar.gif",
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd'
            //minDate: new Date(today)
        });
       /* 
        $( "#end_date" ).datepicker({                               
            showOn: "button",
            buttonImage: "<?php print(ADMIN_BASE_URL); ?>/modules/testimonial/images/calendar.gif",
            buttonImageOnly: true
            //minDate: new Date(today)
        });*/
        
    });
</script>


<script type="text/javascript">
	$(document).ready(
		function()
		{
                        
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: '../uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg<?php print($uploaderX);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#features").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper">

    <h2>Testimonials</h2>
</div>
<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">


        <form  name="testmForm" id="testmForm" action="" method="post">
            <div id="two">
                

                <h2>Testimonial</h2>
                <fieldset style="margin-top:40px;">
                    
                    
                        <?php
                        if($features != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$features;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                    ?>
                    <div id="separator" style="display: none;">
                
                    <label for="lcol_title"> Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg" id="btnImg"/>
                    <input type="hidden" id="features" name="features" value="<?php print($features);?>" />
                    
                </div>
                    
                    
                    <div id="separator" style="display: none;">
                        <label for="headline">Portfolio: </label>
                        <select name="project_id" id="project_id">
                            <option value="">--SELECT--</option>
                             <option value="1" <?php if($project_id == 1) echo "selected=selected";?>>Solar Panels</option>
                             <option value="2" <?php if($project_id == 2) echo "selected=selected";?>>Double Glazing</option>
                        </select>
                         <div style="clear:both;"> </div>
                    </div>
                    
                    <div id="separator">
                        <label for="headline">Title: </label>
                        <input name="tes_title" type="text" style="width:600px;" size="64" maxlength="100" id="tes_title" value="<?php print($tes_title); ?>" />
                    </div>
                    <div style="clear:both;"> </div>

                    <div id="separator">
                        <label for="headline">Quoted By: </label>
                        <input name="quote_by" type="text" style="width:600px;" size="64" maxlength="64" id="quote_by" value="<?php print($quote_by); ?>"/>
                    </div>
                    <div style="clear:both;"> </div>
                    <div id="separator"  style="display: none;">
                        <label for="headline">Date On: </label>
                        <input name="designation" type="text" style="width:200px;" size="64" maxlength="64" id="designation" value="<?php print($designation); ?>"/>
                    </div>
                    <div style="display: none;">
                    <div style="clear:both;"> </div>
                            <div id="separator">
                                <label for="headline">Featured: </label>
                                <input type="radio" name="featured" value="1" id="featured" <?php
                                    if ($featured == '1') {
                                        echo "checked='checked'";
                                    }
        ?>/> Yes  <input type="radio" name="featured" value="0" id="featured" <?php
                                    if ($featured == '0') {
                                        echo "checked='checked'";
                                    }
        ?>/> no
                            </div>
                    </div>
                </fieldset>

                <h2>Description</h2>
                <span id="tes_descInfo" style="color:red;"></span>
                <fieldset>
                    <div id="separator">                        
                        <textarea name="tes_desc"  id="tes_desc" style="width:98%; height:200px;"><?php print($tes_desc); ?></textarea>
                    </div>

                </fieldset>
 
            </div>
            <p>
                <input type="hidden" name="listingId" id="listingId" value="<?php print($listingId); ?>" />
                <input type="hidden" name="id" id="id" value="<?php print($id); ?>" />
                <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action); ?>" />
                <input id="testbutton" type="submit" value="Continue" name="Submit"  onClick="sendRequest()"/> 
                <input  id="resetbutton" type="reset"  />
            </p>
            <div id="show"></div>
        </form>

    </div>

    <div style="clear:both;">
    </div> 
    <div style="height:10px;"></div>
</div>
<script>
CKEDITOR.replace('tes_desc' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 300
});
</script>