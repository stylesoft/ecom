<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');


$id = '';
$project_id = '';

$status = '';
$tes_title = '';
$tes_desc = '';
$features = '';
$quote_by = '';
$designation = '';
$featured = "0";
$listingId = '';


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";


$action = '';
$controller = '';
$project_list = array();


$objTestimonial = new Testimonial();
$objTestimonial->tb_name = 'tbl_testimonials';




if (isset($_GET['c'])) {
    $controller = $_GET['c'];
}
if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit') {

        if (isset($_GET['id'])) {

            $id = $_GET['id'];

            $testimonialInfo = $objTestimonial->getTestimonial($id);

                $id = $testimonialInfo->id;
                $project_id = $testimonialInfo->project_id;
                $tes_title = $testimonialInfo->tes_title;
                $tes_desc = $testimonialInfo->tes_desc;
                $features = $testimonialInfo->features;
                $quote_by = $testimonialInfo->quote_by;
                $designation = $testimonialInfo->designation;
                $featured = $testimonialInfo->featured;
                $listingId = $testimonialInfo->listingId;
        }
    }
}

if ($_POST) {

    $action = $_POST['txtAction'];
  
 
        $id = ($action == 'add') ? "" : $_POST['id'];
        $project_id= $_POST['project_id'];

        
        $tes_title = $_POST['tes_title'];
        $tes_title = preg_replace("/[^a-zA-Z0-9\s]/", "", $tes_title);
        $tes_desc = mysql_real_escape_string($_POST['tes_desc']);
        $quote_by = $_POST['quote_by'];
        $features = $_POST['features'] ;
        
        $designation = $_POST['designation'];
        $featured = ($_POST['featured'] == '') ? "0" : $_POST['featured'];
        $listingId = $_POST['listingId'];
    

                    $objTestimonial->id = $id;
                    $objTestimonial->project_id = $project_id;
                    $objTestimonial->status = $status;
                    $objTestimonial->tes_title = $tes_title;
                    $objTestimonial->tes_desc = $tes_desc;
                    $objTestimonial->features = mysql_real_escape_string($features);
                    $objTestimonial->quote_by = $quote_by;
                    $objTestimonial->designation = $designation;
                    $objTestimonial->featured = $featured;
                    $objTestimonial->listingId = $listingId;

    if ($action == 'add') {
          // print_r($_POST);exit;
        $lastInsertedId = $objTestimonial->addTestimonial();
         $contentId = $lastInsertedId;
    } else if ($action == 'edit') {
        $isRecordUpdated = $objTestimonial->editTestimonial();
        $contentId = $id;
    }

    $newPageUrl = "../images.php?m=testimonial&c=Testimonial&id=$contentId";
    $mainPageUrl = "testimonials.html";

    if ($lastInsertedId) {
        
            print("<div id='boxes'>");
            print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
            //The message comes here..
            print("Testimonial<span class='red'>$tes_title</span> has been added!<br /><br /><br /><a href='$mainPageUrl'  id='resetbutton_sm'>Ok</a>");
            print("</div>");
            print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
            print("</div>");
        //echo "<div id='coverit'></div><div id='message'>Site Content <span class='red'>$name</span> has been added!<br /><br /><br />Create another page now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
            print("<div id='boxes'>");
            print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
            //The message comes here..
            print("Testimonial <span class='red'>$tes_title</span> has been updated!<br /><br /><br /><a href='$mainPageUrl'  id='resetbutton_sm'>Ok</a>");
            print("</div>");
            print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
            print("</div>");
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "start_date";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the siteContent details
    $objSearchParam = new stdClass();
    $objSiteContent->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objTestimonial->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl";
} elseif($controller == 'featured') {
     $CONTENT = MODULE_TEMPLATES_PATH . "feature.tpl";
}else{
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
