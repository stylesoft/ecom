$(document).ready(function(){
	
    //email = $("#txtEmail");
    errornotice = $("#error");
    // The text to show up within a field when it is incorrect
    emptyerror = "This field is required.";
    emailerror = "Please enter a valid e-mail.";


    $("#testmForm").submit(function() {
                            
                            				
        // validate the pagename....
        var project_id = $('#project_id').val();
        if (project_id.length == 0 || $(project_id).val() == "" || project_id == emptyerror) {
            $('#project_id').addClass("error");
            $('#project_id').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            $('#project_id').removeClass("error");
        }
                                                                
        // validate the pagename....
        var tes_title = $('#tes_title').val();
        if ((tes_title == "") || (tes_title == emptyerror)) {
            $('#tes_title').addClass("error");
            $('#tes_title').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            $('#tes_title').removeClass("error");
        }
                                                                
        // validate the description
        var tes_desc = $('#tes_desc').val();
        if ((tes_desc == "") || (tes_desc == emptyerror)) {
            $('#tes_descInfo').addClass("error");
            $('#tes_descInfo').text('* This Field Required');
            errornotice.fadeIn(750);
        } else {
            $('#tes_descInfo').removeClass("error");
        }
        
        
        //start_date
         // validate the description
        var start_date = $('#start_date').val();
        if ((start_date == "") || (start_date == emptyerror)) {
            $('#start_date').addClass("error");
            $('#start_date').text('* This Field Required');
            errornotice.fadeIn(750);
        } else {
            $('#start_date').removeClass("error");
        }
        // if any inputs on the page have the class
        // 'error' the form will not submit
        if ($(":input").hasClass("error")) {
            $(window).scrollTop($('#containerHolder').offset().top);
            return false;
        } else {
            errornotice.hide();
            return true;
        }
                            
    });
                        
    // Clears any fields in the form when the user clicks on them
    $(":input").focus(function() {
        if ($(this).hasClass("error")) {
            $(this).val("");
            $(this).removeClass("error");
        }
			
    });
});
