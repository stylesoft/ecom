
<div id="search_main_wrapper">
<h2>Start new campaign</h2>
</div>

    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">
        <div id="step1">
        	<div id="search_main_wrapper">
			<h2>Step 01</h2>
			</div>
            <form name="form1" id="form1">
				<div id="two">
                    <fieldset style="margin-top: 20px;">
	                    <div id="separator">
	                        <label for="headline">Name Your Campaign: </label>
	                        <input name="campaign_name" type="text" size="65" value="<?=$camp_name?>" />
	                    </div>
	                    <div id="separator">
	                        <label for="headline">From Name: </label>
	                        <input name="from_name" type="text"  size="65" value="<?=$from_name?>" />
	                    </div>
	                    <div id="separator">
	                        <label for="headline">From Email: </label>
	                        <input name="from_email" type="email"  size="65" value="<?=$from_email?>" />
	                    </div>
	                    <div id="separator">
	                        <label for="headline">E mail Subject: </label>
	                        <input name="email_subject" type="text"  size="65" value="<?=$email_subject?>" />
	                    </div>
	                    
                	</fieldset>
                </div>
                <p>
                
                    <input id="testbutton" type="submit" value="Next" name="Submit" /> 
                </p>

            </form>
		</div>
		<div class="clear"></div> 
        </div>
        <div style="height:10px;"></div>
    </div>	

    <script type="text/javascript">
        $(document).ready(function() {
                // validate site_info on keyup and submit
                	
                	$("#form1").validate({
                    rules: {
                    	campaign_name: {
                    		required: true
                    	},
                    	from_email: {
                    		required: true,
                    		email: true
                    	}
                    },
                    submitHandler: function() {


                        $.ajax({
                            type: "POST",
                            url: "<?php print(ADMIN_BASE_URL); ?>modules/campaign/campaign.php?c=wizard&step=1&action=save",
                            data: $("#form1").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                            success: function(Res) {
                                window.location.replace("<?php print(ADMIN_BASE_URL); ?>module/campaign/wizard/step2");
                            },
                            error: function(Res) {

                            }
                        });
                        return false;
                    },
                    errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
                }); 

               
        });
</script>