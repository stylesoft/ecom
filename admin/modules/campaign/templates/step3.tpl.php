<div id="search_main_wrapper">
<h2>Step 03 : Select a template to start with</h2>
</div>

    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">
        <div id="step1">

            <form name="form3" id="form3">
                <div id="two">
                    <fieldset style="margin-top: 20px;">
                        <div id="separator">
                            <img  src="http://localhost/ecom/imgs/winura_logo.png"> Winura Default Email Template
                        </div>
                    </fieldset>
                </div>

                <p>
                     <input type="hidden" name="template" value="1">
                     <input id="testbutton" type="submit" value="Next" name="Submit" /> 
                </p>

            </form>
           
		</div>
		<div class="clear"></div> 
        </div>
        <div style="height:10px;"></div>
    </div>	

    <script type="text/javascript">
        $(document).ready(function() {
                // validate site_info on keyup and submit
                	
                	$("#form3").validate({
                    rules: {
                    	
                    },
                    submitHandler: function() {


                        $.ajax({
                            type: "POST",
                            url: "<?php print(ADMIN_BASE_URL); ?>modules/campaign/campaign.php?c=wizard&step=3&action=save",
                            data: $("#form3").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                            success: function(Res) {
                                                    window.location.replace("<?php print(ADMIN_BASE_URL); ?>module/campaign/wizard/step4");
                            },
                            error: function(Res) {

                            }
                        });
                        return false;
                    },
                    errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
                }); 

               
        });
</script>