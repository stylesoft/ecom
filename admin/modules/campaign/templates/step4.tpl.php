<div id="search_main_wrapper">
<h2>Step 03 : Customize your template</h2>
<h5>Click on different parts of template body to edit</h5>
</div>

    <div id="table_main_wrapper">

        <div id="dashboard" style="background-color:#FFF;">
        <div id="step1">

            <form name="form4" id="form4">
                <div class="etpl">
                     <?php require('template.tpl.php'); ?>
                </div>
                <p>
                     <input id="testbutton" type="submit" value="Next" name="Submit" /> 
                </p>

            </form>
           
		</div>
		<div class="clear"></div> 
        </div>
        <div style="height:10px;"></div>
    </div>	

    <script type="text/javascript">
        $(document).ready(function() {
                // validate site_info on keyup and submit
                	
                	$("#form4").validate({
                    rules: {
                    	
                    },
                    submitHandler: function() {


                        $.ajax({
                            type: "POST",
                            url: "<?php print(ADMIN_BASE_URL); ?>modules/campaign/campaign.php",
                            data: $("#form4").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                            success: function(Res) {
                                                    window.location.replace("<?php print(ADMIN_BASE_URL); ?>module/campaign/wizard/step5");
                            },
                            error: function(Res) {

                            }
                        });
                        return false;
                    },
                    errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
                }); 

               
        });
</script>