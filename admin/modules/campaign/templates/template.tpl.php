<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<?php print(ADMIN_BASE_URL); ?>modules/campaign/templates/ckeditor/ckeditor.js"></script>
<script>

		// This code is generally not necessary, but it is here to demonstrate
		// how to customize specific editor instances on the fly. This fits well
		// this demo because we have editable elements (like headers) that
		// require less features.
		// The "instanceCreated" event is fired for every editor instance created.

		CKEDITOR.on( 'instanceCreated', function( event ) {
			var editor = event.editor,
				element = editor.element;
				

			editor.on( 'blur', function(event) { 
				var htmlData = event.editor.getData();
				var dataString = 'html='+encodeURIComponent(htmlData)+'&editor='+editor.name;
				$.ajax({
                    type: "POST",
                    url: "<?php print(ADMIN_BASE_URL); ?>modules/campaign/campaign.php?c=wizard&step=4&action=update",
                    data: dataString, // serializes the form's elements. http://api.jquery.com/serialize/
                    success: function(Res) {
                    },
                    error: function(Res) {

                	}
				});

			});	

			// Customize editors for headers and tag list.
			// These editors don't need features like smileys, templates, iframes etc.
			if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
				// Customize the editor configurations on "configLoaded" event,
				// which is fired after the configuration file loading and
				// execution. This makes it possible to change the
				// configurations before the editor initialization takes place.
				editor.on( 'configLoaded', function() {
					// Remove unnecessary plugins to make the editor simpler.
					editor.config.removePlugins = 'colorbutton,find,flash,font,' +
						'forms,iframe,image,newpage,removeformat,' +
						'smiley,specialchar,stylescombo,templates';
					// Rearrange the layout of the toolbar.
					editor.config.toolbarGroups = [
						{ name: 'editing',		groups: [ 'basicstyles', 'links' ] },
						{ name: 'undo' },
						{ name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
						{ name: 'about' }
					];
				});
			}
		});
</script>
<title><?=$_SESSION['wizard'][$sid]['camp_name']?></title>
<?=$html_styles?>
</head>
<body>
	<!-- HEADER HTML -->
	<?=$header_html?>
	<!-- BODY HTML 
	| This is the editable content area for the user
	-->
	<div id="body_html1" contenteditable="true"><?=$body_html_1?></div>
	<div id="body_html2" contenteditable="true"><?=$body_html_2?></div>
        <div id="body_html3" contenteditable="true"><?=$body_html_3?></div>
        <div id="body_html4" contenteditable="true"><?=$body_html_4?></div>
        <div id="body_html5" contenteditable="true"><?=$body_html_5?></div>
        <div id="body_html6" contenteditable="true"><?=$body_html_6?></div>
	<!-- EOF BODY HTML -->
	<!-- FOOTER HTML -->
	<?=$footer_html?>
</body>
</html>
<script type="text/javascript">

</script>