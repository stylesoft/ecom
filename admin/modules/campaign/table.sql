--
-- Table structure for table `master_email_templates`
--
CREATE TABLE IF NOT EXISTS `master_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tpl_name` varchar(150) DEFAULT NULL,
  `tpl_description` varchar(250) DEFAULT NULL,
  `category_id` int(5) DEFAULT NULL,
  `html_styles` text NOT NULL,
  `header_html` text,
  `body_html_1` text,
  `body_html_2` text,
  `body_html_3` text,
  `body_html_4` text,
  `body_html_5` text,
  `body_html_6` text,
  `footer_html` text,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Table structure for table `set_campaign`
--

CREATE TABLE IF NOT EXISTS `set_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_name` varchar(150) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `from_email` varchar(150) DEFAULT NULL,
  `email_subject` text,
  `template` int(5) DEFAULT NULL,
  `user_list` text,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Table structure for table `set_email_templates`
--

CREATE TABLE IF NOT EXISTS `set_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tpl_name` varchar(150) DEFAULT NULL,
  `tpl_description` varchar(250) DEFAULT NULL,
  `category_id` int(5) DEFAULT NULL,
  `html_styles` text NOT NULL,
  `header_html` text,
  `body_html_1` text,
  `body_html_2` text,
  `body_html_3` text,
  `body_html_4` text,
  `body_html_5` text,
  `body_html_6` text,
  `footer_html` text,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Table structure for table `set_template_category`
--

CREATE TABLE IF NOT EXISTS `set_template_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) DEFAULT NULL,
  `category_desc` varchar(250) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;