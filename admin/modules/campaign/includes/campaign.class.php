<?php
/**
 * @package     Email Campaign Module
 * @copyright   2013 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1.0.0.0
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.c@pluspro.com
 */

require "includes/class.phpmailer.php";

class Campaign extends Core_Database{

    //### Email Template Properties
    private $template_id = 1; //default is 1 since we have only one template atm
    public $content_filed;
    public $content_filed_value;

    public $id = '';
    public $camp_nam;
    public $from_name;
    public $from_email;
    public $email_subject;
    public $template;
    public $user_list;
    private $created_on;// = date('Y:m:d');
    private $created_by;
    private $last_modified_on;//date('Y:m:d');
    private $last_modified_by;

    public $mail_list = array();



	//constructor
    function __construct() {
        try {
            parent::connect();
            $this->isModuleExists();

        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }
    /*check for module existance
    */
    public function isModuleExists(){
    	echo 'This Module is not exists';
    }
    /*create module related tables
    */
    public function installModule(){

    }
    /*pull email template from database
    */
    public function getEmailTemplate(){
        $objEmailTemplate= new stdClass();
        try {
                $colums = '*';
                $where = 'id = 1';
                $this->select('set_email_templates',$colums, $where);
                $templateInfo = $this->getResult();

                $objEmailTemplate->id = $templateInfo['id'];
                $objEmailTemplate->tpl_name = $templateInfo['tpl_name'];
                $objEmailTemplate->category_id = $templateInfo['category_id'];
                $objEmailTemplate->html_styles = $templateInfo['html_styles'];
                $objEmailTemplate->header_html = $templateInfo['header_html'];
                $objEmailTemplate->body_html_1  = $templateInfo['body_html_1'];
                $objEmailTemplate->body_html_2  = $templateInfo['body_html_2'];
                $objEmailTemplate->body_html_3  = $templateInfo['body_html_3'];
                $objEmailTemplate->body_html_4  = $templateInfo['body_html_4'];
                $objEmailTemplate->body_html_5  = $templateInfo['body_html_5'];
                $objEmailTemplate->body_html_6  = $templateInfo['body_html_6'];
                $objEmailTemplate->footer_html  = $templateInfo['footer_html'];
                $objEmailTemplate->created_on  = $templateInfo['created_on'];
                $objEmailTemplate->last_modified_on  = $templateInfo['last_modified_on'];
                $objEmailTemplate->last_modified_by   = $templateInfo['last_modified_by'];

            return $objEmailTemplate;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Update Email Template
    */
    public function updateMailTemplate(){
        $isUpdated = false;
        try {
            $arrayData = array(''.$this->content_filed.'' => mysql_real_escape_string($this->content_filed_value));
            $arrWhere = array("id = '" . $this->template_id . "'");
            $isUpdated = $this->update('set_email_templates', $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Pull Customer List
    */
    public function getCustomerList(){
        $objCustomer = new Customer();
        try {
            return $objCustomer->getAll();      
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Pull Subscriber List
    */
    public function getSubscriberList(){
        $objSubscriber = new Subcribe();
        try {
            return $objSubscriber->getAll();      
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Insert Campagin Data
    */
    public function insertCampaignData(){
        $insertedID = '';
        $this->created_on = date('Y:m:d');
        $this->created_by = 1;
        $this->last_modified_on = date('Y:m:d');
        $this->last_modified_by = 1;

        //var_dump($this);exit();
        try {
            $inserted = $this->insert('set_campaign',
                array(
                $this->id,
                $this->camp_name,
                $this->from_name,
                $this->from_email,
                $this->email_subject,
                $this->template,
                $this->user_list,
                $this->created_on,
                $this->created_by,
                $this->last_modified_on,
                $this->last_modified_by
                )
            );
            if($inserted){
                $insertedID = $this->getLastInsertedId();
            }
            return $insertedID;
        } catch (Exception $e) {
             throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Launch Campaign
    */
    public function launchCampaign(){
        foreach ($this->mail_list[0] as $key => $user) {
            $this->sendCampaignMail($user['email']);
        }
        return true;
    }

    /*Send Campaign Mail
    */
    public function sendCampaignMail($email){


        $mailTemplate = '';
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->AddReplyTo(
            $this->from_email,
            $this->from_name
        );
        $mail->AddAddress($email);
        $mail->SetFrom(
            $this->from_email,
            $this->from_name
        );
        $mail->Subject = $this->email_subject;

        $template = $this->getEmailTemplate();

        $mailTemplate .= $template->html_styles;
        $mailTemplate .= $template->header_html;
        $mailTemplate .= $template->body_html_1;
        $mailTemplate .= $template->body_html_2;
        $mailTemplate .= $template->body_html_3;
        $mailTemplate .= $template->body_html_4;
        $mailTemplate .= $template->body_html_5;
        $mailTemplate .= $template->body_html_6;
        $mailTemplate .= $template->footer_html;

        $mail->MsgHTML($mailTemplate);
        try {
            $mail->Send();
        } catch (Exception $e) {
             throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
       
    }

    /*Get all master mail templates
    */
    public function getAllMasterTemplates(){
        $data_array = array();
        try {
                $colums = '*';
                $this->select('master_email_templates',$colums);
                $templateInfo = $this->getResult();
                var_dump($templateInfo);
                foreach ($templateInfo as $key => $value) {
                    $objMasterTemplate= new stdClass();
                    $objMasterTemplate->id = $templateInfo['id'];
                    $objMasterTemplate->tpl_name = $templateInfo['tpl_name'];
                    $objMasterTemplate->category_id = $templateInfo['category_id'];
                    $objMasterTemplate->html_styles = $templateInfo['html_styles'];
                    $objMasterTemplate->header_html = $templateInfo['header_html'];
                    $objMasterTemplate->body_html_1  = $templateInfo['body_html_1'];
                    $objMasterTemplate->body_html_2  = $templateInfo['body_html_2'];
                    $objMasterTemplate->body_html_3  = $templateInfo['body_html_3'];
                    $objMasterTemplate->body_html_4  = $templateInfo['body_html_4'];
                    $objMasterTemplate->body_html_5  = $templateInfo['body_html_5'];
                    $objMasterTemplate->body_html_6  = $templateInfo['body_html_6'];
                    $objMasterTemplate->footer_html  = $templateInfo['footer_html'];
                    $objMasterTemplate->created_on  = $templateInfo['created_on'];
                    $objMasterTemplate->last_modified_on  = $templateInfo['last_modified_on'];
                    $objMasterTemplate->last_modified_by   = $templateInfo['last_modified_by'];
                    array_push($data_array, $objMasterTemplate);

                }
                
            return $data_array;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }



        /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrOrders = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $orderResult = $this->getResult();
                foreach ($orderResult As $orderRow) {
                    $orderId = $orderRow['id'];
                    $orderInfo = $this->getOrder($orderId);
                    array_push($arrOrders, $orderInfo);
                }
            }

            return $arrOrders;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }





    /*     * '
     * @name         :   getOrder
     * @param        :   Integer (Order ID)
     * Description   :   The function is to get a Order details
     * @return       :   Order Object
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getMasterTemplate($id) {
        $objOrder = new stdClass();
        try {
                $colums = '*';
                $where = 'id = ' . $id;
                $this->select('master_email_templates', $colums, $where);
                $orderInfo = $this->getResult();

                $objMasterTemplate->id = $templateInfo['id'];
                $objMasterTemplate->tpl_name = $templateInfo['tpl_name'];
                $objMasterTemplate->category_id = $templateInfo['category_id'];
                $objMasterTemplate->html_styles = $templateInfo['html_styles'];
                $objMasterTemplate->header_html = $templateInfo['header_html'];
                $objMasterTemplate->body_html_1  = $templateInfo['body_html_1'];
                $objMasterTemplate->body_html_2  = $templateInfo['body_html_2'];
                $objMasterTemplate->body_html_3  = $templateInfo['body_html_3'];
                $objMasterTemplate->body_html_4  = $templateInfo['body_html_4'];
                $objMasterTemplate->body_html_5  = $templateInfo['body_html_5'];
                $objMasterTemplate->body_html_6  = $templateInfo['body_html_6'];
                $objMasterTemplate->footer_html  = $templateInfo['footer_html'];
                $objMasterTemplate->created_on  = $templateInfo['created_on'];
                $objMasterTemplate->last_modified_on  = $templateInfo['last_modified_on'];
                $objMasterTemplate->last_modified_by   = $templateInfo['last_modified_by'];
                
                //get order realational data
                $objOrder->cutomerDetails = $insCustomer->getCustomer($objOrder->customerId);
                $objOrder->orderItemDetails = $insOrderItems->getAll($objOrder->id);

            return $objOrder;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}