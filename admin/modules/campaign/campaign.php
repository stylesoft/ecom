<?php
/** '
 * This file loads the default dashbaord template 
 * @author Gayan Chathuranga <chathurangagc@gmail.com>
 */
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');
require_once('includes/campaign.class.php');

$controller =  $_GET['c'];
$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";

$objCampagin = new campaign();
$action = '';

switch ($controller) {
	
	/* List campaign history
	*/
	case 'list':
		$CONTENT = MODULE_TEMPLATES_PATH."camp_list.tpl.php";
		break;
	
	case 'new':
		//header("Location: ".SITE_BASE_URL."admin/module/campaign/wizard/step1");
		$CONTENT = MODULE_TEMPLATES_PATH."camp_new.tpl.php";
		break;
	
	/*Campaign Wizard
	*/
	case 'wizard':
		$step = $_GET['step'];
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		$sid = session_id(); //create unique session campaign id		
		//create wizard session
			switch ($step) {
				//Step 01 : 
				case '1': 					
						/* Save Cmpagin Info
						*/
						if($action) {
							switch ($action) {
								/* Get Campagin Base Info
								*/
								case 'save':
								$_SESSION['wizard'][$sid]['camp_name'] = isset($_POST['campaign_name']) ? $_POST['campaign_name'] : '' ;
								$_SESSION['wizard'][$sid]['from_name'] = isset($_POST['from_name']) ? $_POST['from_name'] : '';
								$_SESSION['wizard'][$sid]['from_email'] = isset($_POST['from_email']) ? $_POST['from_email'] : '';
								$_SESSION['wizard'][$sid]['email_subject'] = isset($_POST['email_subject']) ? $_POST['email_subject'] : ''; 								
								break;
							}
						}else{

							$camp_name = isset($_SESSION['wizard'][$sid]['camp_name']) ? $_SESSION['wizard'][$sid]['camp_name'] : '' ;
				    		$from_name = isset($_SESSION['wizard'][$sid]['from_name'])  ? $_SESSION['wizard'][$sid]['from_name'] : '' ;
				    		$from_email = isset($_SESSION['wizard'][$sid]['from_email'])  ? $_SESSION['wizard'][$sid]['from_email'] : '' ;
				    		$email_subject = isset($_SESSION['wizard'][$sid]['email_subject']) ? $_SESSION['wizard'][$sid]['email_subject'] : ''  ;

							$CONTENT = MODULE_TEMPLATES_PATH."camp_new.tpl.php";
						}

				break;
				
				//Step 02 : Import Mailling List
				case '2':					
						
						/*Controller actions
						*/	
						if($action) {
							switch ($action) {
								/* Get Campagin Mail List
								*/
								case 'getlist':	

									$list = $_POST['recepient'];
									if($list == 'customer'){
										$objCustomer = new Customer();
										$objCustomer->tb_name = 'tbl_customer';										
										//keep step data on campaign instance
										$_SESSION['wizard'][$sid]['user_list'] = 'customer';
										$_SESSION['wizard'][$sid]['mail_list'] = $objCustomer->getUserMailList();

									}elseif($list == 'subscriber'){
										$objSubsciber = new Subcribe();
										//keep step data on campaign instance
										$_SESSION['wizard'][$sid]['user_list'] = 'subscriber';
										$_SESSION['wizard'][$sid]['mail_list'] = $objSubsciber->getUserMailList();
									}
								break;
							}
						}else{
							$CONTENT = MODULE_TEMPLATES_PATH."step2.tpl.php";
						}
				break;

				case '3':

						/*Controller actions
						*/
						if($action) {
							switch ($action) {
								/* Set Campaign Template
								*/
								case 'save':
								$_SESSION['wizard'][$sid]['template'] = $_POST['template'];	
								//TODO: save step 03
								break;
							}
						}else{
							$CONTENT = MODULE_TEMPLATES_PATH."step3.tpl.php";
						}
					
					break;
				case '4':

						/*Controller actions
						*/
						if($action) {
							switch ($action) {
								/* Edit the email Template
								*/
								case 'update':
									$editor = $_POST['editor'];
									$field_value = $_POST['html'];

									if( $editor == 'body_html1'){

										$objCampagin->content_filed = 'body_html_1';
										$objCampagin->content_filed_value = $field_value;

									}elseif( $editor == 'body_html2'){

										$objCampagin->content_filed = 'body_html_2';
										$objCampagin->content_filed_value = $field_value;
									}
									$objCampagin->updateMailTemplate(); //update the default email tempate set to 1	
								break;
							}
						}else{
								$email_template = $objCampagin->getEmailTemplate();
								$html_styles = $email_template->html_styles;
								$header_html = $email_template->header_html;
								$body_html_1 = $email_template->body_html_1;
								$body_html_2 = $email_template->body_html_2;
								$footer_html = $email_template->footer_html;
								$CONTENT = MODULE_TEMPLATES_PATH."step4.tpl.php";
						}

					break;
				case '5':
				    //var_dump($_SESSION['wizard'][$sid]);
				    $objCampagin->camp_name = $_SESSION['wizard'][$sid]['camp_name'];
				    $objCampagin->from_name = $_SESSION['wizard'][$sid]['from_name'];
				    $objCampagin->from_email = $_SESSION['wizard'][$sid]['from_email'];
				    $objCampagin->email_subject = $_SESSION['wizard'][$sid]['email_subject'];
				    $objCampagin->user_list = $_SESSION['wizard'][$sid]['user_list'];
				    array_push($objCampagin->mail_list,$_SESSION['wizard'][$sid]['mail_list']);
				    $objCampagin->template = $_SESSION['wizard'][$sid]['template'];

				    if($action){
				    	switch ($action) {
				    		case 'send':
				    			$objCampagin->insertCampaignData();
				    			$objCampagin->launchCampaign();
				    			break;
				    		case 'schedule':
				    			//$objCampagin->scheduleCampaign();
				    			$CONTENT = MODULE_TEMPLATES_PATH."schedule.tpl.php";
				    			break;
				    		
				    		default:
				    			# code...
				    			break;
				    	}
				    }else{
				    	$CONTENT = MODULE_TEMPLATES_PATH."step5.tpl.php";
				    }					
					break;
				default:
					# code...
					break;
			}
		break;
	
	/*send campaign to receipent list
	*/
	case 'templates':
		$master_temp_data = $objCampagin->getAllMasterTemplates();
		var_dump($master_temp_data);
		$CONTENT = MODULE_TEMPLATES_PATH."templates.tpl.php";
		break;

	/*schedule the campaign to receipent list
	*/
	case 'schedule_campaign':
		# code...
		break;
			
	default:
		# code...
		break;
}
require_once $LAYOUT;
?>