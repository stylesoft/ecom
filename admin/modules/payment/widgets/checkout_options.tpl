<form name="frmPaymentMethods" id="frmPaymentMethods" action="" method="post">
<?php if(count($payment_options)){?>
	<?php foreach ($payment_options as $key => $value):?>
  		<label class=" inline">
		 	<input name="payment_method" value="<?=$value->flag?>" id="<?=$value->flag?>" <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] == $value->flag ){?> checked="checked" <?php } ?>  type="radio">
		  	<img src="<?=$value->logo?>"  alt="<?=$value->displayName?>"  width="64px" height="64px"/>
		  	<?=$value->displayName?>
	  	</label>
	<?php	endforeach;?> 
<?php }else{?>
			<div class="alert alert-warning">No Active Payment Method Avaialbe!!</div>
<?php }?>

    <div class="pull-right">
              <div class="privacy">I have read and agree to the <a id="button" href="#">Privacy Policy</a>
              <input type="checkbox" name="chkTermsConditions" value="1">
              <br>
              <input value="Continue" id="button-payment-method" name="button-payment-method" class="btn btn-success pull-right" type="submit">  
             <div class="span1 pull-right" ><a href="<?php print(SITE_BASE_URL); ?>checkout/4/checkout.html?page_type=checkout&pageId=4" class="btn btn-success">Back</a></div>
              </div>
            </div>
</form>
<!--
//FORNT END VIEW INTEGRATION CODE
<div id="wiget_checkout"></div>
<script type="text/javascript">
        $.ajax({
              type: "POST",
              url: "<?php print(ADMIN_BASE_URL); ?>modules/payment/payment.php?c=checkout",
              dataType: 'json',
              data: 'view=true',
              success: function(R) {
                   $('#wiget_checkout').append(R);
              },
              error: function(Res) {
                   $('#wiget_checkout').append('<div class="alert alert-warning">Error Rendering Payment Widget!!</div>');
              }
          });
</script>
-->
<script>
        $(document).ready(function() {
                $("#frmPaymentMethods").validate({
                    rules: { 
                      chkTermsConditions: {
                            required : true,
                            //email : true
                        },
                        payment_method: {
                            required : true,
                            //number : true
                        }

                    },
                    messages: {
                      cmbTitle: "Please select a title",
                      txtBillingCountry : "Please select a country",
                      //txtBillingCountyState : "Please select a region/state",
                      chkTermsConditions : "You must agree on Privacy Policy"
                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL);?>ajex/checkout/payment-methods.php",
                            data: $("#frmPaymentMethods").serialize()+'&ajax=1', 
                            success: function(data) {
                                if(data == 'success'){
                           window.location.href='<?php print(SITE_BASE_URL); ?>checkout/6/checkout.html?page_type=checkout&pageId=6';    
                                }else if(data == 'failed'){
                                    $('#error_notice').fadeIn(750);
                                    $('#error_notice').fadeOut(750);
                                }
                            }
                            
                        });
                        return false;
                    }
                }); 
        });
</script>