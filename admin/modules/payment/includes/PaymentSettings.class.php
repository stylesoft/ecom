<?php 
/**
 * @package     Payment Module
 * @copyright   2013 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1.0.0.0
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.c@pluspro.com
 */
class PaymentSettings extends Core_Database{


    public $id = '';
    public $userName;
    public $password;
    public $signature;
    public $currencyCode;
    public $mode;
    public $returnUrl;
    public $cancelUrl;
    private $created_on;// = date('Y:m:d');
    private $created_by;
    private $last_modified_on;//date('Y:m:d');
    private $last_modified_by;


	//constructor
    function __construct() {
        try {
            parent::connect();
            $this->isModuleExists();

        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*check for module existance
    */
    public function isModuleExists(){
    	//TODO: implement the functionality
    }
    /*create module related tables
    */
    public function installModule(){
    	//TODO: implement the functionality
    }
    
    /*Get Paypal Merchant Configuration Info
    */
    public function getPayPalMerchantSettings(){
    	$objPayPalMerchantInfo= new stdClass();
        try {
                $colums = '*';
                $where = 'id = 1'; //one default account
                $this->select('set_paypal_config',$colums, $where);
                $merchantInfo = $this->getResult();

                $objPayPalMerchantInfo->id = $merchantInfo['id'];
                $objPayPalMerchantInfo->userName = $merchantInfo['user_name'];
                $objPayPalMerchantInfo->password = $merchantInfo['password'];
                $objPayPalMerchantInfo->signature = $merchantInfo['signature'];
                $objPayPalMerchantInfo->currencyCode = $merchantInfo['currency_code'];
                $objPayPalMerchantInfo->mode  = $merchantInfo['mode'];
                $objPayPalMerchantInfo->returnUrl  = $merchantInfo['return_url'];
                $objPayPalMerchantInfo->cancelUrl  = $merchantInfo['cancel_url'];
                $objPayPalMerchantInfo->created_on  = $merchantInfo['created_on'];
                $objPayPalMerchantInfo->created_by  = $merchantInfo['created_by'];
                $objPayPalMerchantInfo->last_modified_on  = $merchantInfo['last_modified_on'];
                $objPayPalMerchantInfo->last_modified_by  = $merchantInfo['last_modified_by'];

            return $objPayPalMerchantInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Update PayPal Merchant Configurations
    */
    public function updatePayPalMerchantSettings(){
        //TODO : update Merchant Settings
    }

    /*Get payment options
    */
    public function getPaymentOptions(){
        $data_array = array();
        try {
                $colums = '*';
                $where = ''; //one default account
                $this->select('set_payment_options',$colums, $where);
                $paymentInfo = $this->getResult();
                foreach ($paymentInfo as $key => $data) {
                    $objPaymentOptions= new stdClass();
                    $objPaymentOptions->id = $data['id'];
                    $objPaymentOptions->displayName = $data['display_name'];
                    $objPaymentOptions->flag = $data['flag'];
                    $objPaymentOptions->logo = $data['logo'];
                    $objPaymentOptions->url = $data['url'];
                    $objPaymentOptions->status  = $data['status'];
                    $objPaymentOptions->created_on  = $data['created_on'];
                    $objPaymentOptions->created_by  = $data['created_by'];
                    $objPaymentOptions->last_modified_on  = $data['last_modified_on'];
                    $objPaymentOptions->last_modified_by   = $data['last_modified_by'];
                    array_push($data_array, $objPaymentOptions);
                }
            return $data_array;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Get active payment options
    */
    public function getActivePaymentOptions(){
        $data_array = array();
        try {
                $colums = '*';
                $where = "status = 'Enabled'";
                $this->select('set_payment_options',$colums, $where);
                $paymentInfo = $this->getResult();
                if($this->getNumRows() > 1){
                    foreach ($paymentInfo as $key => $data) {
                        $objPaymentOptions= new stdClass();
                        $objPaymentOptions->id = $data['id'];
                        $objPaymentOptions->displayName = $data['display_name'];
                        $objPaymentOptions->flag = $data['flag'];
                        $objPaymentOptions->logo = $data['logo'];
                        $objPaymentOptions->url = $data['url'];
                        $objPaymentOptions->status  = $data['status'];
                        $objPaymentOptions->created_on  = $data['created_on'];
                        $objPaymentOptions->created_by  = $data['created_by'];
                        $objPaymentOptions->last_modified_on  = $data['last_modified_on'];
                        $objPaymentOptions->last_modified_by   = $data['last_modified_by'];
                        array_push($data_array, $objPaymentOptions);
                    }    
                }elseif($this->getNumRows() == 1){
                        $objPaymentOptions= new stdClass();
                        $objPaymentOptions->id = $paymentInfo['id'];
                        $objPaymentOptions->displayName = $paymentInfo['display_name'];
                        $objPaymentOptions->flag = $paymentInfo['flag'];
                        $objPaymentOptions->logo = $paymentInfo['logo'];
                        $objPaymentOptions->url = $paymentInfo['url'];
                        $objPaymentOptions->status  = $paymentInfo['status'];
                        $objPaymentOptions->created_on  = $paymentInfo['created_on'];
                        $objPaymentOptions->created_by  = $paymentInfo['created_by'];
                        $objPaymentOptions->last_modified_on  = $paymentInfo['last_modified_on'];
                        $objPaymentOptions->last_modified_by   = $paymentInfo['last_modified_by'];
                        array_push($data_array, $objPaymentOptions);
                }
            return $data_array;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*Update payment option
    */
    public function updatePaymentOptions($payOption,$value){
        
        $isUpdated = false;
        try {
            $arrayData = array('status' => $value);
            $arrWhere = array("flag = '" . $payOption . "'");
            $isUpdated = $this->update('set_payment_options', $arrayData, $arrWhere);
            if($isUpdated){
                $res = array(
                    'option'  => $payOption,
                    'val'     => $value
                );
                echo json_encode($res);exit();
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*get widget checkout
    */
    public function getFrontWidgetCheckOut(){
        /*http://php.net/manual/en/function.ob-get-clean.php
        */
        ob_start();
        $payment_options = $this->getActivePaymentOptions();
        $view = include('widgets/checkout_options.tpl');
        $view = ob_get_clean();
        echo json_encode($view);exit();
    }
}
?>