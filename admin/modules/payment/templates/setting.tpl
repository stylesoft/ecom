<style type="text/css">
img {
     display: inline;
     margin: 0 auto;
   }
#two fieldset label {
    margin: 31px 14px 6px !importatnt;
}

.label-success {
    background-color: #5CB85C;
}
.label {
    border-radius: 0.25em 0.25em 0.25em 0.25em;
    color: #FFFFFF;
    display: inline;
    font-size: 75%;
    font-weight: bold;
    line-height: 1;
    padding: 0.2em 0.6em 0.3em;
    text-align: center;
    vertical-align: baseline;
    white-space: nowrap;
}
.label-danger {
    background-color: #D9534F;
}
.btn-success {
    background-color: #5CB85C;
    border-color: #4CAE4C;
    color: #FFFFFF;
}
.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px 4px 4px 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: bolder;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 0px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
     margin-left: 600px;
}
</style>
<div id="table_main_wrapper">
    <a href="<?php print(ADMIN_BASE_URL); ?>modules/payment/payment.php?c=expchkout"> Express Checkout </a>
	<div id="search_main_wrapper"  style="margin-bottom:55px;">
            <h2>Payment Options</h2>     
</div>
    <div id="dashboard" style="background-color: #FFF;">

            <form name="formPayActivate" id="formPayActivate" action="" method="post">
              <p style="color:red;">Note : <i>Unless you are fully aware on these configuration parameters, please DO NOT change.</i></p>
                     <div id="two">
                        <fieldset style="margin-top: 20px;">                  
	                        <?php foreach ($payment_options as $key => $value):?>            
			                    <div id="separator">
			                    	<img src="<?=$value->logo?>" height="64px" width="64px">
			                        <label for="pagename"><?=$value->displayName?></label>
			                        <input type="checkbox" class="radio" value="1" name="paymthd[]" id="<?=$value->flag?>" <?php if($value->status == 'Enabled'){?>  checked <?php }?> />
			                        <i><a href="<?=$value->url?>" target="new"><?=$value->url?></a></i> &nbsp;&nbsp;&nbsp;
			                        
			                        <span class="label label-success <?=$value->flag?>suc" style="display:none;">Enabled</span>
			                        <span class="label label-danger <?=$value->flag?>err" style="display:none;">Disabled</span>
			                       
			                        
			                    </div>            
			                    <br/>
			                <?php endforeach; ?>
		                    <br/>           
                </fieldset>
                     </div>
            </form>


<!-- PAY PAL MERCHANT INFO -->
<div id="paypalMrchInfo" style="display:none;">
	<div id="search_main_wrapper"  style="margin-bottom:5px;">
	            <h2>PayPal Merchant Information</h2>     
	</div>


            <form name="formPayMerchantSettings" id="formPayMerchantSettings" action="" method="post">

                     <div id="two">
                         <fieldset style="margin-top: 10px;">
                   
                    
                    <div id="separator">
                        <label for="headline">API User Name: </label>
                        <input name="txtPayApiUsername" type="text" id="txtPayApiUsername" value="<?php echo $payment_settings->userName; ?>" size="65"  />
                    </div>                   
                    <div id="separator">
                        <label for="headline">API Password: </label>
                        <input name="txtPayApiPassword" type="text" id="txtPayApiPassword" value="<?php echo $payment_settings->password; ?>" size="65" />
                    </div>                  
                     <div id="separator">
                        <label for="headline">API Signature: </label>
                        <input name="txtPayApiSignature" type="text" id="txtPayApiSignature" value="<?php echo $payment_settings->signature; ?>" size="65" />
                    </div>
                     <div id="separator">
                        <label for="headline">Currency Code: </label>
                        <input name="txtPayCurrencyCode" type="text" id="txtPayCurrencyCode" value="<?php echo $payment_settings->currencyCode; ?>" size="65" />
                    </div>
                    <div id="separator">
                        <label for="headline">Return URL: </label>
                        <input name="txtPayReturnUrl" type="text" id="txtPayReturnUrl" value="<?php echo $payment_settings->returnUrl; ?>" size="65" />
                    </div>
                    <div id="separator">
                        <label for="headline">Cancel URL: </label>
                        <input name="txtPayCancelUrl" type="text" id="txtPayCancelUrl" value="<?php echo $payment_settings->cancelUrl; ?>" size="65" />
                        <div class="clearfix"></div>
                        <i>Cancel URL if user clicks cancel</i>
                    </div>                                          
                    <div id="separator">
                        <label for="pagename">Mode: </label>

                        <select name="optPayMode" id="optPayMode" style="width: 200px;">
                            <option value="sandbox" <?php if ($payment_settings->mode == 'sandbox') { ?> selected="selected" <?php } ?> selected>SandBox</option>
                            <option value="live" <?php if ($payment_settings->mode == 'live') { ?> selected="selected" <?php } ?>>Live</option>
                        </select>

                        <i>sandbox or live</i>
                    </div>
                                   <p>
                                   	<button type="submit" class="btn btn-success">Save Settings</button>
                    </p>
                </fieldset>
                     </div>



 </div>
 <!-- EOF MERCHANT INFO -->
        </div>


    </div>
<script type="text/javascript">

$(document).ready(function() {
    $.each($("input[name='paymthd[]']:checked"), function() {
	  if($(this).attr('id') == 'paypal'){
	  	$('#paypalMrchInfo').css('display','block');

	  	payPalSettingValidate();
	  }
	  $('.'+$(this).attr('id')+'suc').css('display','inline');
	});
});

$(document).on('click',"input[name='paymthd[]']",function(){
		var paymethod = $(this).attr('id');
		var value = 'Disabled';

   		if($(this).is(':checked')){
   			paymethod = $(this).attr('id');
   			value = 'Enabled';//$(this).val();
   		}
        $.ajax({
                type: "POST",
                url: "<?php print(ADMIN_BASE_URL); ?>modules/payment/payment.php?c=settings&action=pay_activate",
                dataType: "json",
                data: 'paymethod='+paymethod+'&value='+value,
                success: function(R) { 

                		payPalSettingValidate();
                		if(R.val == 'Disabled')	{ 
                			$('.'+R.option+'suc').css('display','none');	
                			$('.'+R.option+'err').css('display','inline'); 
                			$('#'+R.option+'MrchInfo').css('display','none'); }
                		else {
                			$('.'+R.option+'suc').css('display','inline');
                			$('.'+R.option+'err').css('display','none'); 		 
                			$('#'+R.option+'MrchInfo').css('display','block');

                		}
                			
                    	
                },
                error: function(Res) {

                }
         });
});

function payPalSettingValidate(){
	console.log('fireing');
	$("#formPayMerchantSettings").validate({
                rules: {
                	txtPayApiUsername:{
                        required : true
                    },
                    txtPayApiPassword:{
                        required : true
                    },
                    txtPayApiSignature:{
                        required : true
                    },
                    txtPayCurrencyCode:{
                        required : true
                    },
                    txtPayReturnUrl:{
                        required : true
                    },
                    txtPayCancelUrl:{
                        required : true
                    }
                },
                submitHandler: function() {


                    $.ajax({
                        type: "POST",
                        url: "<?php print(ADMIN_BASE_URL); ?>modules/payment/payment.php?c=settings&action=update",
                        data: $("#formPayMerchantSettings").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                        success: function(Res) {
                             alert('Data Saved');
                        },
                        error: function(Res) {

                        }
                    });
                    return false;
                },
                errorPlacement: function(error, element) { 
                    //element.prop("placeholder", error.text()); //overide the error 
                    //error.insertAfter(element);
                }
             });
}
</script>