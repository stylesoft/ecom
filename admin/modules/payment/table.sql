--
-- Table structure for table `set_paypal_config`
--

CREATE TABLE IF NOT EXISTS `set_paypal_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `signature` varchar(250) DEFAULT NULL,
  `currency_code` char(10) DEFAULT NULL,
  `mode` char(10) DEFAULT 'sandbox',
  `return_url` text,
  `cancel_url` text,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `password` (`password`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `set_paypal_config`
--

INSERT INTO `set_paypal_config` (`id`, `user_name`, `password`, `signature`, `currency_code`, `mode`, `return_url`, `cancel_url`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`) VALUES
(1, 'dev_api1.pluspro.com', '1389882452', 'AdgdwtaEkktiz4t4FGaq8bko52e6Ai4SBZqu9OnSY5llOBDw3ZML.eAI', 'USD', 'sandbox', 'http://www.demos.pluspro.com/~plusprod/plusproecommerce.com/payprocess.php', 'http://www.demos.pluspro.com/~plusprod/plusproecommerce.com/payerror.php', '2014-01-17 09:46:54', 1, '2014-01-17 09:47:01', 1);
--
-- Table structure for table `set_payment_options`
--

CREATE TABLE IF NOT EXISTS `set_payment_options` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(256) NOT NULL,
  `flag` char(10) NOT NULL,
  `logo` text,
  `url` text,
  `status` enum('Enabled','Disabled') NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `set_payment_options`
--
INSERT INTO `set_payment_options` (`id`, `display_name`, `flag`, `logo`, `url`, `status`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`) VALUES
(1, 'Pay Pal', 'paypal', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoYC8VkL96zqnIu2ziymo0c9crMX6uQpZUlqHXdpaFob9oclng', 'http://www.paypal.com', 'Enabled', '2014-01-17 15:18:07', 1, '2014-01-17 15:18:07', 1),
(2, 'Secure Trading', 'securetrad', 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTV9yNa1MadLxMFtHuoQMLBfbJiLQcIU7e83_eJiyqrKeNE_kCzaA', 'http://www.securetrading.com', 'Enabled', '2014-01-17 15:18:07', 1, '2014-01-17 15:18:07', 1);
