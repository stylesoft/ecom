<?php
/** '
 * Module : Payment
 * File: Payment.php
 * @author Gayan Chathuranga <gayan.c@pluspro.com>
 */
require_once '../../../bootstrap.php';
require_once('includes/PaymentSettings.class.php');
require_once('includes/PayPal.class.php');

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";
$controller =  $_GET['c'];
if($controller != 'checkout') require_once('../../includes/auth.php');
$objPaymentSettings = new PaymentSettings();

switch ($controller) {
	case 'settings':
		$payment_options  = $objPaymentSettings->getPaymentOptions();
		$payment_settings = $objPaymentSettings->getPayPalMerchantSettings();
		$CONTENT = MODULE_TEMPLATES_PATH."setting.tpl";
			$action = isset($_GET['action']) ? $_GET['action'] : '';
			switch ($action) {
				case 'update':
					print_r($_POST);exit();
					break;
				case 'pay_activate':
					isset($_POST['paymethod']) ? $payOption = $_POST['paymethod'] : $payOption = '';
					isset($_POST['paymethod']) ? $value = $_POST['value'] : $value = 'Disabled';		
					$objPaymentSettings->updatePaymentOptions($payOption,$value);
					break;
				
				default:
					# code...
					break;
			}
		break;
	case 'checkout':	    
		$objPaymentSettings->getFrontWidgetCheckOut();
		break;
	case 'expchkout':	    
		$CONTENT = MODULE_TEMPLATES_PATH."express_checkout.tpl";
		break;
	default:
		# code...
		break;
}
require_once $LAYOUT;
?>