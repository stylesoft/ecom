<script type="text/javascript">
	
    $(document).ready(function(){
		
        //Display Loading Image
        function Display_Load()
        {
            $("#manageloading").fadeIn(900,0);
            $("#manageloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        //Hide Loading Image
        function Hide_Load()
        {
            $("#manageloading").fadeOut('slow');
        };
	
        //Default Starting Page Results
   
        $("#pagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
	
        Display_Load();
	
        $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/product/includes/specialOffer_pagination_data.php?q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=product", Hide_Load());

        //Pagination Click
        $("#pagination li").click(function(){
			
            Display_Load();
		
            //CSS Styles
            $("#pagination li")
            .css({'border' : 'solid #dddddd 1px'})
            .css({'color' : '#0063DC'});
		
            $(this)
            .css({'color' : '#FF0084'})
            .css({'border' : 'none'});

            //Loading Data
            var pageNum = this.id;
		
            $("#managecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/product/includes/specialOffer_pagination_data.php?page=" + pageNum+"&m=product", Hide_Load());
        });
        
        
        
        
        
        // non featured......................
        
        //Display Loading Image
        function nonFeaturedDisplay_Load()
        {
            $("#nonFeaturedmanageloading").fadeIn(900,0);
            $("#nonFeaturedmanageloading").html("<img src='<?php print(ADMIN_BASE_URL); ?>templates/contents/images/bigLoader.gif' />");
        }
        //Hide Loading Image
        function nonFeaturedHide_Load()
        {
            $("#nonFeaturedmanageloading").fadeOut('slow');
        };
	
        //Default Starting Page Results
   
        $("#nonFeaturedpagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
	
        nonFeaturedDisplay_Load();
	
        $("#nonFeaturedmanagecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/product/includes/nonSpecialOffer_pagination_data.php?q=<?php print($_SEARCH_QUERY); ?>&orderby=<?php print($ORDER_BY); ?>&order=<?php print($ORDER); ?>&rows=<?php print($_REC_PER_PAGE); ?>&m=product", nonFeaturedHide_Load());

        //Pagination Click
        $("#nonFeaturedpagination li").click(function(){
			
            nonFeaturedDisplay_Load();
		
            //CSS Styles
            $("#pagination li")
            .css({'border' : 'solid #dddddd 1px'})
            .css({'color' : '#0063DC'});
		
            $(this)
            .css({'color' : '#FF0084'})
            .css({'border' : 'none'});

            //Loading Data
            var pageNum = this.id;
		
            $("#nonFeaturedmanagecontent").load("<?php print(ADMIN_BASE_URL); ?>modules/product/includes/nonSpecialOffer_pagination_data.php?page=" + pageNum+"&m=product", nonFeaturedHide_Load());
        });
        
        

    });
</script>



<div id="search_main_wrapper">
          <h2>The Special Offer Products</h2>     
</div>



<div id="table_main_wrapper">
    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard"></div>
    </div>

    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                          <td align="center">
                           <div style="margin-top:45px;"></div>
                        </td>
                    </tr>

                    

                

                    <tr>
                        <td align="center">




                            <div id="manageloading" ></div>
                            <div id="managecontent" ></div>




                        </td>
                    </tr>

                    

                </tbody></table>
        </div>
    </div>
    <div style="height:10px;"></div>
</div>



<!-- Non Featured Products -->





<div id="search_main_wrapper">
          <h2>The Non Special Offer Products</h2>     
</div>



<div id="table_main_wrapper">
    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard"></div>
    </div>

    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                          <td align="center">
                           <div style="margin-top:45px;"></div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <form action="" method="get" name="form1" id="search">
                                <div align="center"><label for="searchtext">Search:</label>
                                    <input type="text" name="q" id="q" />
                                    <input type="hidden" name="action" id="action" value="featured" />
                                    <input type="submit" id="searchbutt" value="" name="search" />
                                </div>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:5px">	
                                <table width="750px">
                                    <tr>
                                        <td width="30">Pages:</td>
                                        <td>
                                            <ul id="nonFeaturedpagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $nonSpecialOfferProductPageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td>
                                        <td width="90"></td>
                                        <td width="30">
                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">




                            <div id="nonFeaturedmanageloading" ></div>
                            <div id="nonFeaturedmanagecontent" ></div>




                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <div style="margin-left:40px">	<table width="800px">
                                    <tr><td width="30">Pages:</td><td>
                                            <ul id="nonFeaturedpagination">
                                                <?php
                                                //Show page links
                                                for ($i = 1; $i <= $nonSpecialOfferProductPageNumbers; $i++) {
                                                    echo '<li id="' . $i . '">' . $i . '</li>';
                                                }
                                                ?>
                                            </ul>	
                                        </td></tr></table></div>

                        </td>
                    </tr>

                </tbody></table>
        </div>
    </div>
    <div style="height:10px;"></div>
</div>