<script type="text/javascript">
	

function deleterow(id){
if (confirm('Are you sure want to delete?')) {
$.post('<?php print(ADMIN_BASE_URL); ?>modules/product/includes/delete-product-sizes.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder_"+id).fadeOut("slow");
return false;
//$(".message").delay(2000).fadeOut(1000);
});
}
}

</script>

<div id="search_main_wrapper">
          <h2>The Product Size Details for "<?php print($productInfo->productName);?>"</h2>     
</div>



<div id="table_main_wrapper">
    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard"></div>
    </div>

    <div class="table_wrapper2" id="side_panel_box_5_content">
        <div id="dashboard">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                          <td align="center">
                           <div style="margin-top:45px;">
                               <a href="<?php print(ADMIN_BASE_URL); ?>product/product-sizes.html?action=add&pid=<?php print($productId);?>" id="testbutton">Add New Product Size</a>
                           </div>
                        </td>
                    </tr>

                    

                    <tr>
                        <td align="center">

                            &nbsp;
                   
                        </td>
                    </tr>

                    <tr>
                        <td align="center">




                           <div id="list">
<div id="table_main_div" >

                    	<div class="row_heding">
                        	
                                <div class="colum" style="width:70px;">
                                    <strong>ID&nbsp;</strong>
                                </div>
                                <div class="colum" style="width:240px;">
                                    <strong>Product Size</strong>
                                </div>
                            <div class="colum" style="width:140px;">
                                    <strong>Stock in Hand</strong>
                                </div>
                                
                                <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
                            </div>
                            <div class="clear"></div>
                    	
                            <ul>
                            
                                
                                
                                <?php if($productSizes){?>
                                <?php foreach($productSizes As $psIndex=>$productSize){?>
                                <li id="arrayorder_<?php print($productSize->id);?>">
                           
                                    
                                   <?php if($psIndex%2 != 0){?>
                            <div class="row1">
                             <?php } else {?>
                                 <div class="row2">
                              <?php } ?>
                                
                        	<div class="colum" style="width:70px;"><?php print($psIndex+1);?></div>
                            <div class="colum" style="width:240px;"><?php print($productSize->size->name);?></div>
                            <div class="colum" style="width:140px;"><?php print($productSize->stockInHand);?></div>
                          
                            <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                
                                
                                <a href="product-sizes.html?action=edit&pid=<?php print($productId);?>&id=<?php print($productSize->id);?>">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                
                                <a href="javascript:deleterow('<?php print($productSize->id);?>');">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                </a>
                            </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                            <?php } ?>
                            <?php } ?>
                            
                   
                                
                                
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>




                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                          &nbsp;

                        </td>
                    </tr>

                </tbody></table>
        </div>
    </div>
    <div style="height:10px;"></div>
</div>