<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');
?>

<script type="text/javascript">

$(document).ready(function() {
    
    // load product types...
     $('#cmbProductSubCategory').change(function(){
        
       $('#productTypeloader').show(); // sub category load
        
        $.post("<?php print(ADMIN_BASE_URL);?>modules/product/includes/ajexSecSubCategory.php", {
			parent_id: $('#cmbProductSubCategory').val(),
	 }, function(response){
			$('#productType').html(unescape(response));
                        $('#productTypeloader').hide(); // sub category load
	});
	return false;
        
    });
    
    
    
});

</script>
<?php 
$categoryId   = $_POST['parent_id'];
$objCategory = new Category();
$subCategories = $objCategory->getAllSubCategory($categoryId);
?>
<select name="cmbProductSubCategory" id="cmbProductSubCategory" style="width: 200px;">
<option value=""></option>
<?php foreach($subCategories As $scIndex=>$subCategory){?>
<option value="<?php print($subCategory->id);?>"><?php print($subCategory->category_name);?></option>
<?php } ?>
</select>