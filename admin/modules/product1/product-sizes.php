<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$productSizeObjId = '';
$productSizeId = '';
$productSizeRelId = '';
$productSizeStock = '';
$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$currencies = "";
$productSizes = "";



$objProductSizes = new ObjectSizeMap();

$productId = $_GET['pid'];

$objProduct = new Product();
$objProduct->tb_name = 'tbl_product';
$productInfo = $objProduct->getProduct($productId);

if (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action == 'edit') {
        $productSizeObjId = $_GET['id'];
    }
} else {
    $productSizes = $objProductSizes->getAllByType($productId);
}


if ($_POST) {
    $productSizeObjId = $_POST['id'];
    $productSizeId = $_POST['cmbProductSize'];
    $productSizeRelId = $_POST['txtProductId'];
    $productSizeStock = $_POST['txtStockInHand'];
    $action = $_POST['txtAction'];

    $objProductSizes = new ObjectSizeMap();
    $objProductSizes->tb_name = 'tbl_object_size_map';
    $objProductSizes->id = $productSizeObjId;
    $objProductSizes->size = $productSizeId;
    $objProductSizes->object = $productSizeRelId;
    $objProductSizes->stockInHand = $productSizeStock;
    if ($action == 'add') {
        $objProductSizes->addObjectSizeMap();
    } else {
        $objProductSizes->editObjectSizeMap();
    }
    
    
    print("<div id='boxes'>");
                print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
                //The message comes here..
                print("The product size has been updated!<br /><br /><br />Want to add another size for this product ?<br /><br /><a href='product-sizes.html?action=add&pid=$productSizeRelId'  id='testbutton_sm' style='margin-right:20px'>Yes</a><a href='product-sizes.html?pid=$productSizeRelId'  id='resetbutton_sm'>No</a>");
                print("</div>");
                print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
                print("</div>");
    
    
    
}



if (isset($_GET['action'])) {

    // get all product sizes
    $objProductSize = new ObjectSize();
    $product_foot_wear = $objProductSize->getAllByType('PRODUCT_FOOTWEAR');
    $objProductSize = new ObjectSize();
    $product_foot_belt = $objProductSize->getAllByType('PRODUCT_BELT');
    $objProductSize = new ObjectSize();
    $product_foot_default = $objProductSize->getAllByType('PRODUCT_DEFAULT');

    $objProductSizes = new ObjectSizeMap();
    $objProductSizes->tb_name = 'tbl_object_size_map';
    $productSizeInfo  = $objProductSizes->getObjectSizeMap($productSizeObjId);
    $productSizeObjId = $productSizeInfo->id;
    if($productSizeInfo->size){
        $productSizeId = $productSizeInfo->size->id;
    } else {
        $productSizeId = "";
    }
    
    $productSizeRelId = $productSizeInfo->object;
    $productSizeStock = $productSizeInfo->stockInHand;

    $CONTENT = MODULE_TEMPLATES_PATH . "size-form.tpl.php";
} else {
    
     
    
    
    $CONTENT = MODULE_TEMPLATES_PATH . "size-index.tpl.php";
}



$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
