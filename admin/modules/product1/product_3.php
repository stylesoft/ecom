<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id                 = ''; 	
$productName = ''; 	
$productSmallDescription = ''; 	
$unitPrice  = '';	
$discountPrice  = '';	
$priceCurrency = ''; 	
$stockInHand = ''; 	
$reorderLevel = ''; 	
$category  = ''; 	
$addedOn  = '';	
$addedBy = ''; 	
$status = ''; 	
$featured  = '';	 
$productDescription = ''; 	
$keywords = ''; 	
$displayOrder = '';
$isSpecialOffer = '';
$weight_unit = '';
$product_weight = '';
$productCode   = '';
$productBrand  = '';

$firstParentCategoryID = "";
$parentCategoryID      = "";
$firstParentSubCategories  = "";
$secondParentSubCategories = "";



// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$currencies = "";



// get all currencies....
$objCurrency = new Currency();
$currencies = $objCurrency->getAll();


$objProduct = new Product();
$objProduct->tb_name = 'tbl_product';


$objCategory = new Category();
$objCategory->tb_name = 'tbl_category';
$objCategory->searchStr    = '';
$objCategory->limit        = '';
$objCategory->listingOrder = '';
$categoryResult            = $objCategory->getAllParentCategory('Enabled');


$objBrand = new Brand();
$objBrand->tb_name = 'tbl_brand';
$objBrand->status = 'Enabled';
$brandsInfo  = $objBrand->search();


if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit') {

        if (isset($_GET['id'])) {

            $productId = $_GET['id'];

            $productInfo = $objProduct->getProduct($productId);

            $id = $productInfo->id;
            $productName = $productInfo->productName; 	
            $productSmallDescription = $productInfo->productSmallDescription; 	
            $unitPrice  = $productInfo->unitPrice;	
            $discountPrice  = $productInfo->discountPrice;	
            $priceCurrency = $productInfo->priceCurrency; 	
            $stockInHand = $productInfo->stockInHand; 	
            $reorderLevel = $productInfo->reorderLevel; 	
            $category  = $productInfo->category; 	
            $addedOn  = $productInfo->addedOn;	
            $addedBy = $productInfo->addedBy; 	
            $status = $productInfo->status; 	
            $featured  = $productInfo->isFeatured;	 
            $productDescription = $productInfo->productDescription; 	
            $keywords = $productInfo->keywords; 	
            $displayOrder = $productInfo->displayOrder;
            $isSpecialOffer = $productInfo->isSpecialOffer;
            $weight_unit = $productInfo->weightUnit;
            $product_weight = $productInfo->productWeight;
            $productCode   = $productInfo->productCode;
            if($productInfo->productBrand){
                $productBrand   = $productInfo->productBrand->id;
            } else {
                $productBrand   = null;
            }
            $productSize   = $productInfo->productSize;
            

            $productCategoryID      = $category->id;
            $parentCategoryID      = $category->parent_category;
            if($parentCategoryID == 0){
                $parentCategoryID = $productCategoryID;
            }
            
            //$firstParentCategoryID = $objCategory->getParentCategory($parentCategoryID);
            $firstParentSubCategories = $objCategory->getAllSubCategory($parentCategoryID);
            $secondParentSubCategories = $objCategory->getAllSubCategory($parentCategoryID,'Enabled');
            
            
            
        }
    } elseif($action == 'addToFeatured'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isFeatured = 'yes';	 
        $isRecordUpdated = $objProduct->updateFeatured();
        header('Location: product.html?action=featured');
    }elseif($action == 'removeFeatured'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isFeatured = 'no';	 
        $isRecordUpdated = $objProduct->updateFeatured();
        header('Location: product.html?action=featured');
    }elseif($action == 'addToSpecialOffer'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isSpecialOffer = 'Yes';	 
        $isRecordUpdated = $objProduct->updateSpecialOffer();
        header('Location: product.html?action=specialOffers');
    }elseif($action == 'removeSpecialOffer'){
        $productId = $_GET['id'];
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        $currentProductId = $productId;
        $objProduct->id = $currentProductId;
        $objProduct->isSpecialOffer = 'No';	 
        $isRecordUpdated = $objProduct->updateSpecialOffer();
        header('Location: product.html?action=specialOffers');
    }
}

if ($_POST) {
    //print_r($_POST); exit;
    $tstamp = time();
        $tstamp = gmdate("Y-m-d H:i:s", $tstamp);

    $action = $_POST['txtAction'];

    $id = ($action == 'add') ? "" : $_POST['id'];
    $productName = mysql_real_escape_string($_POST['txtProductName']); 	
    $productSmallDescription = mysql_real_escape_string($_POST['description']); 	
    $unitPrice  = $_POST['txtUnitPrice'];	
    $discountPrice  = $_POST['txtUnitPriceD'];	
    $priceCurrency = $_POST['cmbPriceCurrency']; 	
    $stockInHand = $_POST['txtStockInHand']; 	
    $reorderLevel = $_POST['txtReorderLevel']; 	
    if($_POST['cmbProductSubCategory'] && $_POST['cmbProductSubCategory'] != ""){
        $category  = $_POST['cmbProductSubCategory']; 
    } else {
        $category  = $_POST['cmbProductCategory']; 
    }
    
    $addedOn  = $tstamp;	
    $addedBy = '1'; 	
    $status = $_POST['status']; 	
    $featured  = $_POST['cmbIsFeatured'];	 
    $productDescription = mysql_real_escape_string($_POST['mainbody']); 	
    $keywords = $_POST['keywords']; 	
    $displayOrder = $_POST['displayOrder'];
    $isSpecialOffer = $_POST['cmbIsSpecialOffer'];
    $weight_unit = $_POST['cmbProductWeightU'];
    $product_weight = $_POST['txtProductWeight'];
    $productCode   = ($_POST['txtProductCode'] == '') ? "" : $_POST['txtProductCode'];//$_POST['txtProductCode'];
    
    $productBrand   = ($_POST['cmbProductBrand'] == '') ? "" : $_POST['cmbProductBrand'];//$_POST['txtProductCode'];
    $productSize   = ($_POST['cmbProductSize'] == '') ? "" : $_POST['cmbProductSize'];//$_POST['txtProductCode'];

    $objProduct->id = $id;
    $objProduct->productName = $productName; 	
    $objProduct->productSmallDescription = $productSmallDescription; 	
    $objProduct->unitPrice = $unitPrice;	
    $objProduct->discountPrice = $discountPrice;	
    $objProduct->priceCurrency = $priceCurrency; 	
    $objProduct->stockInHand = $stockInHand; 	
    $objProduct->reorderLevel = $reorderLevel; 	
    $objProduct->category = $category; 	
    $objProduct->addedOn = $addedOn;	
    $objProduct->addedBy = $addedBy; 	
    $objProduct->status = $status; 	
    $objProduct->isFeatured = $featured;	 
    $objProduct->productDescription = $productDescription; 	
    $objProduct->keywords = $keywords; 	
    $objProduct->displayOrder = $displayOrder;
    $objProduct->isSpecialOffer = $isSpecialOffer;
    $objProduct->weightUnit = $weight_unit;
    $objProduct->productWeight = $product_weight;
    $objProduct->productCode = $productCode;
    
    $objProduct->productBrand = $productBrand;
    $objProduct->productSize = $productSize;
    

    $currentProductId = '';
    
    if ($action == 'add') {
        $lastInsertedId = $objProduct->addProduct();
        $currentProductId = $lastInsertedId;
    } else if ($action == 'edit') {
        $currentProductId = $id;
        $isRecordUpdated = $objProduct->editProduct();
    }

    
    
    
    
    $newPageUrl = "../images.php?m=product&c=Product&id=".$currentProductId;
    $mainPageUrl = "product.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Product <span class='red'>$productName</span> has been added!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Product <span class='red'>$productName</span> has been added!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Product <span class='red'>$productName</span> has been updated!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Menu <span class='red'>$productName</span> has been updated!<br /><br /><br />Want to upload images now?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }
        
        if(isset($_GET['brand'])){
                $productBrand = $_GET['brand'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "display_order";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the product details
    $objSearchParam = new stdClass();
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->productBrand = $productBrand;
    $totalNumberOfMenus = $objProduct->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'add' || $action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
} elseif($action == 'featured'){
    
    // count featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isFeatured = 'Yes';
    $totalNumberOfProducts = $objProduct->countRec();
    $featuredProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
    
    // count non featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isFeatured = 'No';
    $totalNumberOfProducts = $objProduct->countRec();
    $nonfeaturedProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
   
    
     $CONTENT = MODULE_TEMPLATES_PATH . "featured.tpl.php";
     
     
     
  } elseif($action == 'specialOffers'){
    
    // count featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isSpecialOffer = 'Yes';
    $totalNumberOfProducts = $objProduct->countRec();
    $specialOfferProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);

    
    // count non featured product pages...
    $objProduct = new Product();
    $objProduct->tb_name = 'tbl_product';
    $objProduct->searchStr = $_SEARCH_QUERY;
    $objProduct->isSpecialOffer = 'No';
    $totalNumberOfProducts = $objProduct->countRec();
    $nonSpecialOfferProductPageNumbers = ceil($totalNumberOfProducts / $_REC_PER_PAGE);
    
    
    
     $CONTENT = MODULE_TEMPLATES_PATH . "specialOffers.tpl.php";   
     
     
     
} else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}


$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
