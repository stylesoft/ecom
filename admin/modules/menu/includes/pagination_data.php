<?php
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$orderBy = "";
$order = "";
$pageNumber = "";
$searchQ = "";
$recLimit = "";
$orderBy = "";
$order = "";
$orderStr = "";
$status  = 'Enable';
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if (isset($_GET)) {

    // get the search query....
    if (isset($_GET['m'])) {
        $module = $_GET['m'];
    }

    // the page numbers............
    if (isset($_GET['page'])) {
        $pageNumber = $_GET['page'];
    } else {
        $pageNumber = 1;
    }

    // get the search query....
    if (isset($_GET['q'])) {
        $searchQ = $_GET['q'];
    }


    // get the order list...
    if (isset($_GET['orderby'])) {
        $orderBy = $_GET['orderby'];
    }

    if (isset($_GET['order'])) {
        $order = $_GET['order'];
    }

    if ($orderBy != '' && $order != '') {
        $orderStr = $orderBy . " " . $order;
    } else {

        $orderStr = 'display_order' . " " . 'Asc';
    }

    if (isset($_GET['rows'])) {
        $_REC_PER_PAGE = $_GET['rows'];
    }
}




$recStart = ($pageNumber - 1) * $_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit = " LIMIT " . $recStart . "," . $recLimitTo;
//--------------------------------------------

$objMenu = new Menu();
$objMenu->tb_name = 'tbl_menu';


$objMenuField = new MenuField();
$objMenuField->tb_name = 'tbl_menu_data';


$objMenu->searchStr = $searchQ;
$objMenu->limit = $recLimit;
$objMenu->listingOrder = $orderStr;
$menuResult = $objMenu->getAllParentCategory($status);

//print('<pre>');print_r($menuResult);print('</pre>');
?>
<style type="text/css">
    .row3{
        background: none repeat scroll 0 0 #E5F1FF;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
        
    }
    .row3:hover{
        opacity: 2.0;
    }
    .row4{
        background: none repeat scroll 0 0 #E5F1FF;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity:  0.7;
        transition: opacity 0.8s;
    }
        .row4:hover{
        opacity: 2.0;
    }
    
    
       .row5{
        background: none repeat scroll 0 0 #F7F4DC;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
        
    }
    .row5:hover{
        opacity: 2.0;
    }
    .row6{
        background: none repeat scroll 0 0 #F7F4DC;
        border-bottom: 1px solid #DDDDDD;
        border-right: medium none;
        clear: both;
        height: 40px;
        line-height: 29px;
        opacity: 0.7;
        transition: opacity 0.8s;
    }
        .row6:hover{
        opacity: 2.0;
    } 
    
</style>
<script type="text/javascript">
    $(document).ready(function(){ 	
        function slideout(){
            setTimeout(function(){
                $("#response").slideUp("slow", function () {
                });
    
            }, 2000);}
	
        $("#response").hide();
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
                    var order = $(this).sortable("serialize") + '&update=update'; 
                    $.post("<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/updateListingOrder.php", order, function(theResponse){
                        $("#response").html(theResponse);
                        $("#response").slideDown('slow');
                        slideout();
                    }); 															 
                }								  
            });
        });

    });	

</script>

<script type="text/javascript" >
    function deleterow(id){
        if (confirm('This will delte with all its sub categories. Are you sure delete this?')) {
            $.post('../modules/menu/includes/deletemenu.php', {id: +id, ajax: 'true' },
            function(){
                $("#arrayorder_"+id).fadeOut("slow");
                $(".message").fadeIn("slow");
                $(".message").delay(2000).fadeOut(1000);
                return false;
            });
        }
    }

</script>

<script type="text/javascript">
 
    $(document).ready(function(){
 
        $(".slidingDiv").hide();
        $(".show_hide").show();
 
 
        $("input[name=showHide]").click(function(){
            $(".cls"+this.id).slideToggle();
        });
        
 
    });
 
</script>


<div id="list">
    <div id="table_main_div" >
        <div id="response"> </div>
        <div class="row_heding">

            <div class="colum" style="width:70px;">
                <strong>ID&nbsp;</strong>
               
            </div>
            <div class="colum" style="width:100px;">
                <strong>Menu Name</strong>
                
            </div>

            
            <div class="colum" style="width:50px;">
                
                
            </div>
            <div class="colum_right" align="right" style="margin-top:2px; width:185px;"></div>
        </div>
        <div class="clear"></div>

        <ul>
            <?php
            if (count($menuResult) > 0) {
                foreach ($menuResult As $rowIndex => $menuData) {


$maincategoryId = $menuData->id;
$ArrmenuFields = $objMenu->getAllSubCategory($maincategoryId);

//print_r($ArrmenuFields);

//exit();

                    ?> 

                  <li id="arrayorder_<?php print($menuData->id); ?>">
                        <?php if ($rowIndex % 2 != 0) { ?>
                            <div class="row1">
                            <?php } else { ?>
                                <div class="row2">
                                <?php } ?>
                                <div class="colum" style="width:70px;"><?php print($menuData->id); ?></div>
                                <div class="colum" style="width:280px;"><?php print($menuData->menu_name); ?></div>

                                <div class="colum_right" align="right" style="margin-top:2px;  width:185px;">
                                <?php  if (count($ArrmenuFields)) { ?>
                                
                                    <input type="checkbox" name="showHide" id="<?php print($menuData->id); ?>"> Dropdown Menu &nbsp;

                                    <?php }?>
                                    <a href="<?= $module ?>.html?action=edit&id=<?php print($menuData->id); ?>">
                                       &nbsp; <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                    <a href="javascript:deleterow(<?php echo $menuData->id; ?>);">
                                        <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                    </a>
                                </div>
                            </div>
                            <div class="cls<?php print($menuData->id); ?>" style="
                                 display: none; 
                                 background-color: #cccc;
                                 padding: 0px 0px 10px 50px;
                                 margin-top:5px;
                                 margin-bottom:5px;
                                 border-bottom:3px solid #3399FF;">
                                <ul>
                                        <?php
                                        
                                        
                                        
                                        //print_r($ArrmenuFields);
                                        
                                        if (count($ArrmenuFields)) {
                                            foreach ($ArrmenuFields As $subIndex => $ArrmenuData) {

$menuFieldinfo = $objMenuField-> getAllEdit($ArrmenuData->id);



                                        ?> 
                                    <li id="arrayorder_<?php print($ArrmenuData->id); ?>">
                                        <?php if ($subIndex % 2 != 0) { ?> <div class="row3" id="row_<?php print($ArrmenuData->id); ?>"> <?php } else { ?> <div class="row4" id="row_<?php print($ArrmenuData->id); ?>"> <?php } ?>
                                               
                                                <div class="colum" style="width:350px;"><?php print($ArrmenuData->menu_name); ?></div>
                                                    <div class="colum_right" align="right" style="margin-top:2px;  width:185px;"> 
                                                    
                                                       &nbsp;&nbsp;
                                                         <?php  if (count($menuFieldinfo)) {?>
                                                        <a href="<?php print(ADMIN_BASE_URL); ?>menu/menu.html?action=editField&id=<?php print($ArrmenuData->id); ?>&prouduct_id=<?php print($menuData->id); ?>">
                                                            <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                                        </a>
                                                        <?php }?>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                         <input type="hidden" name="showHide" id="<?php print($ArrmenuData->id); ?>">  <a href="<?php print(ADMIN_BASE_URL); ?>menu/menu.html?action=addmenuField&id=<?php print($ArrmenuData->id); ?>&parent_id=<?php print($menuData->id); ?>">
                                                            <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH); ?>images/add.png" alt="" width="16" height="16" border="0" title="Edit" />
                                                        </a>
                                                       
                                                     

                                                       
                                                    </div>

                                               
                                                </div>
                                            
                                            
                                                    <!-- level 2 subcategories-->
                     
                                                            
                                                <!-- eof level 2 subcategories-->
                                               <div class="clear"></div>
    
                                      </li>
                                     <?php } }?>
                                
                               
                                 </ul>                            
                            </div>

                            <div class="clear"></div>
                    </li>

                <?php }
            } else { ?>
                <li>
                    <div id="listings" style="background:#eee;">No Matching Records Found </div>
                </li>
<?php } ?>

        </ul>



    </div>

</div>
