jQuery(document).ready(function() {
    // Place ID's of all required fields here.
    var required = ["content_title"];
    // If using an ID other than #email or #error then replace it
    // here
    //email = $("#txtEmail");
    var errornotice = jQuery("#error");
    // The text to show up within a field when it is incorrect
    var emptyerror = "This field is required.";

             
    jQuery("#two").submit(function() {
        // Validate required fields
   
        for (i = 0; i < required.length; i++) {
             
            var input = jQuery('#' + required[i]);
            if ((input.val() == "") || (input.val() == emptyerror)) {
                input.addClass("error");
                input.val(emptyerror);                        
                errornotice.fadeIn(750);
            //return false; 

            } else {
                input.removeClass("error");
            //return true;
            }
                
        }
        
        for (i = 0; i < required.length; i++) {
             
            var input2 = jQuery('#' + required[i]);
            if ((input2.val() == "") || (input2.val() == emptyerror)) {
                return false; 

            }
                
        }
                 
       
    });
             
             
             
    // Clears any fields in the form when the user clicks on them
    jQuery(":input").focus(function() {
        if (jQuery(this).hasClass("error")) {
            jQuery(this).val("");
            jQuery(this).removeClass("error");
        }
                 
    });
             
             
             
});
