// JavaScript Document



jQuery(document).ready(function($){
		 
    $("#carnamelist").sortable({
        connectWith: '#cardeleteArea',
        update: function(event, ui){
            //Run this code whenever an item is dragged and dropped out of this list
            var order = $(this).sortable('serialize') + '&action=updateRecordsListings';
            $.ajax({
                url: '../modules/banner/includes/updateListingOrder.php',
                type: 'POST',
                data: order,
                // callback handler that will be called on success
                success: function(response, textStatus, jqXHR){
                    // log a message to the console
                    //alert("Hooray, it worked!" + response);
                },
                // callback handler that will be called on error
                error: function(jqXHR, textStatus, errorThrown){
                    // log the error to the console
                    /*
                    alert(
                        "The following error occured: "+
                        textStatus, errorThrown
                    );
                    */
                },
                // callback handler that will be called on completion
                // which means, either on success or error
                complete: function(){
                    // enable the inputs
                   // alert("Done");
                }

            });
        }
    });

    $("#cardeleteArea").droppable({
        accept: '#carnamelist > li',
        hoverClass: 'dropAreaHover',
        drop: function(event, ui) {
            deleteImage(ui.draggable,ui.helper);
        },
        activeClass: 'dropAreaHover'
    });

    function deleteImage($draggable,$helper){
        var answer = confirm('Permantly delete this item ?');
        if (answer == true)
        {
            params = 'PID=' + $draggable.attr('id');
            $.ajax({
                url: '../modules/banner/includes/deleteImage.php',
                type: 'POST',
                data: params
            });
            $helper.effect('transfer', {
                to: '#cardeleteArea', 
                className: 'ui-effects-transfer'
            },500);
            $draggable.remove();
        }

 
        else {
	 
        }

    }

});