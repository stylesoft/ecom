<?php

require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$recordID = '';
$recordText  = '';
$recordListingID  = '';
$bannerType  = ''; //types : 'SliderBanners', 'HomeBanners', 'Downloads'
$banner_link  = '';
$banner_title  = '';
$banner_description  = '';

// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";
$_BANNER_TYPE = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';

$PAGE_TITLE = "";


$objHomeBanner = new HomeBanner();
$objHomeBanner->tb_name = 'home_banner_images';


if (isset($_GET['action'])) {
    $action = $_GET['action'];


    if ($action == 'edit') {

        if (isset($_GET['id'])) {

            $recordID = $_GET['id'];

            $homeBannerInfo = $objHomeBanner->getHomeBanner($recordID);

            $recordID = $homeBannerInfo->recordID;
            $recordText = $homeBannerInfo->recordText;
            $recordListingID = $homeBannerInfo->recordListingID;
            $bannerType = $homeBannerInfo->bannerType;
            $banner_link = $homeBannerInfo->banner_link;
            $banner_title = $homeBannerInfo->banner_title;
            $banner_description  = $homeBannerInfo->banner_description;
        }
    }
    
    if($action == 'add'){
        
        if (isset($_GET['bannerType'])) {
            $_BANNER_TYPE = $_GET['bannerType'];
        }
        
        $orderStr       = 'recordListingID'." ".'Asc';  
 
        $objHomeBanner->bannerType   = $_BANNER_TYPE;
        $objHomeBanner->searchStr    = "";
        $objHomeBanner->limit        = "";
        $objHomeBanner->listingOrder = $orderStr;
        $homeBannerArray = $objHomeBanner->search();
       
    }
}

if ($_POST) {

    $action = $_POST['txtAction'];

    $recordID = $_POST['recordID'];
    $recordText = $_POST['recordText'];
    $recordListingID = $_POST['recordListingID'];
    $bannerType = $_POST['bannerType'];
    $banner_link = $_POST['banner_link'];
    $banner_title = $_POST['banner_title'];
    $banner_description  = $_POST['bannerDescription'];

    //validation
    if($banner_title == ""){
            array_push($objHomeBanner->error, 'The Banner title is required');
            $invalid = true;
        }

    $objHomeBanner->recordID = $recordID;
    $objHomeBanner->recordText = $recordText;
    $objHomeBanner->recordListingID = $recordListingID;
    $objHomeBanner->bannerType = $bannerType;
    $objHomeBanner->banner_link = $banner_link;
    $objHomeBanner->banner_title = $banner_title;
    $objHomeBanner->banner_description = $banner_description;

    if (!$invalid) {
        $isRecordUpdated = $objHomeBanner->editHomeBanner();
    }

    $newPageUrl = "banner.html?action=add&bannerType=".$bannerType;
    $mainPageUrl = "banner.html?bannerType=".$bannerType;

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Banner <span class='red'>$banner_title</span> has been added!<br /><br /><br />Want to upload images ?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Banner <span class='red'>$banner_title</span> has been added!<br /><br /><br />Want to upload images ?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Banner <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Want to upload images ?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Banner <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Want to upload images ?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {

    if ($_GET) {

        if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }

        if (isset($_GET['orderby'])) {
            $ORDER_BY = $_GET['orderby'];
        } else {
            $ORDER_BY = "recordListingID";
        }

        if (isset($_GET['order'])) {
            $ORDER = $_GET['order'];
        } else {
            $ORDER = 'Asc';
        }
        
        
        if (isset($_GET['bannerType'])) {
            $_BANNER_TYPE = $_GET['bannerType'];
            
            if($_BANNER_TYPE == 'HomeBanners'){
                $PAGE_TITLE = " Home Banners";
            }else if($_BANNER_TYPE == 'SliderBanners'){
                $PAGE_TITLE = " Slider Banners";
            }else if($_BANNER_TYPE == 'SidebarBanner'){
                $PAGE_TITLE = " Sidebar Banners";
            }else if($_BANNER_TYPE == 'Downloads'){
                $PAGE_TITLE = " Downloads";
            }else if($_BANNER_TYPE == 'SpecialOffer'){
                $PAGE_TITLE = "Special Offer Banners";
            }else if($_BANNER_TYPE == 'Brand'){
                $PAGE_TITLE = "Our Brands Logos";
            }else if($_BANNER_TYPE == 'Members'){
                $PAGE_TITLE = "Memberships Logos";
            }
            
            
        } 
        


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }
    }

    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objHomeBanner->bannerType = $_BANNER_TYPE;
    $objHomeBanner->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objHomeBanner->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

 $mainPageUrl = "banner/banner.html?bannerType=".$_BANNER_TYPE;

if ($action == 'edit') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}elseif($action == 'add'){
    $CONTENT = MODULE_TEMPLATES_PATH . "upload.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
