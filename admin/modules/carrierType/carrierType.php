<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');
//require_once('../../../application/classes/Subcribe.php');

$id = '';			
$carrierType = '';			
$delay 	= '';			
$status = '';			
$isFreeShipping = '';	


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';

$arrcarrierType = array();

$objCarrierType = new CarrierType();
//$objCarrierType->tb_name = 'tbl_carrier_types';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                //$arrSubcribe = $objSubcribe->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {             
            $carrierTypeInfo = $objCarrierType->getCarrierType($id);

            $id                   = $carrierTypeInfo->id;
            $carrierType          = $carrierTypeInfo->carrierType;
            $delay                = $carrierTypeInfo->delay;
            $status               = $carrierTypeInfo->status;
            $isFreeShipping       = $carrierTypeInfo->isFreeShipping;
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}

if ($_POST) {
//print_r($_POST);
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $carrierType    = $_POST['carrierType'];
    $delay          = $_POST['delay'];
    $status         = $_POST['status'];
    $isFreeShipping = $_POST['isFreeShipping'];
    
   

    //validation
    if($carrierType == ""){
            array_push($objCarrierType->error, 'The Carrier Type is required');
            $invalid = true;
        }


    $objCarrierType->id   = $id ;
    $objCarrierType->carrierType = $carrierType;
    $objCarrierType->delay  = $delay;
    $objCarrierType->status = $status;
    $objCarrierType->isFreeShipping = $isFreeShipping;
    
    if ($action == 'add' && !$invalid) {
    	
          $lastInsertedId = $objCarrierType->addCarrierType($objCarrierType);
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objCarrierType->editCarrierType($objCarrierType);
    }

    $newPageUrl = "carrierType.html?action=add";
    $mainPageUrl = "carrierType.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Shipping Method<span class='red'>$email</span> has been added!<br /><br /><br />Want to enter more Shipping?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Shipping Method <span class='red'>$email</span> has been updated!<br /><br /><br />Want to enter more Shipping?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {
	
 if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }
       
        if(isset($_GET['orderby'])){
        	$orderBy = $_GET['orderby'];
        }
        
        if(isset($_GET['order'])){
        	$order = $_GET['order'];
        }
        
        if($orderBy != '' && $order != ''){
        	$orderStr       = $orderBy." ".$order;
        } else {
        
        	$orderStr       = 'id'." ".'Asc';
        }


        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }


    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objCarrierType->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objCarrierType->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}




if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
