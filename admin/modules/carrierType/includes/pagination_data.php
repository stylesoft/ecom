<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "";
$order          = "";
$orderStr       = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            // get the search query....
            if(isset($_GET['q'])){
                $searchQ   = $_GET['q'];
            }
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }
            
            if(isset($_GET['order'])){
                $order = $_GET['order'];
            }
            
            if($orderBy != '' && $order != ''){
            	$orderStr       = $orderBy." ".$order;
            } else {
            
            	$orderStr       = 'id'." ".'Asc';
            }

          
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}



//--------------------------------------------

$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;

$objSubcribe1 = new CarrierType();
//$objSubcribe1->tb_name = 'tbl_carrier_types';

$objSubcribe1->searchStr = $searchQ;
$objSubcribe1->limit = $recLimit;
$objSubcribe1->listingOrder = $orderStr;
$subcribeResult1 = $objSubcribe1->search();
//print_r($subcribeResult1);

//--------------------------------------------

?>
<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			$.post("<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/updateListingOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

function deleterow(id){
if (confirm('Are you sure want to delete?')) {
	 $.post('<?php print(ADMIN_BASE_URL); ?>modules/<?= $module ?>/includes/deletesubcribe.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder"+id).fadeOut("slow");

//$(".message").delay(2000).fadeOut(1000);
});
}
}

</script>

<div id="tablecontents"> 
<table id="users">
    <thead> 
		<tr height="12px">
		  <td  width="70">
		  	ID
		  	<a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=id&order=asc">&darr;</a>
		  </td>
        <td  width="300">
        Shipping Methods
        <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=carrier_type&order=desc">&uarr;</a> 
            <a href="<?php print(ADMIN_BASE_URL);?><?=$module?>/<?=$module?>.html?q=<?php print($searchQ);?>&orderby=carrier_type&order=asc">&darr;</a>
        </td>
     
        <td width="125">status</td>
       
        </tr>
        </thead> 

        <tbody> 
	
        </tbody>
        
        
         <?php 
      if(count($subcribeResult1) >0){
      foreach($subcribeResult1 As $rowIndex=>$subcribeData) {?> 
        	<tr <?php if($rowIndex%2 == 0){?> class="odd" <?php } ?> id="arrayorder<?php print($subcribeData->id);?>">
        	<?php //if ($subcribeData->type == "Enabled") {?>
	    	<td height="29" width="70"><?php print($subcribeData->id);?></td>
            <td height="29" width="300"><?php print($subcribeData->carrierType);?></td>
            <td height="29" width="125"><?php print($subcribeData->status);?></td>
	    <td class="action">
                 <a href="<?php print($module);?>.html?action=edit&id=<?php print($subcribeData->id);?>" class="edit">Edit</a>
                 <a href="#" onclick="deleterow(<?php echo $subcribeData->id; ?>)" class="delete">Delete</a>
            </td>
	   
                
         <?php }//} ?>
           <?php } else { ?>      
       
		
	</table>
	 <div id="listings" style="background:#eee;">No Matching Records Found </div>
		<?php }?>
	</div>