<?php 
require_once '../../bootstrap.php';	
$action = mysql_real_escape_string($_POST['action']); 
$updateRecordsArray 	= $_POST['recordsArray'];


$objPageData  = new stdClass();
$subPageObject = new SubPage();

if ($action == "updateRecordsListings"){
	
	$listingCounter = 1;
	foreach ($updateRecordsArray as $recordIDValue) {
		$objPageData->id = $recordIDValue;
                $objPageData->listingId = $listingCounter;
                $subPageObject->updateOrder($objPageData);
		$listingCounter = $listingCounter + 1;	
	}
	echo 'If you refresh the page, you will see that records will stay just as you modified.';
}
?>