<?php	

	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_MEMBER_ID']) || (trim($_SESSION['SESS_MEMBER_ID']) == '')) {
		header("location: ".ADMIN_BASE_URL."login.php?ref=denied&&request_url=".$_SERVER['REQUEST_URI']);
		exit();
	}
?>
