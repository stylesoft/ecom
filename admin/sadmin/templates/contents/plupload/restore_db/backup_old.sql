-- MySQL dump 10.11
--
-- Host: localhost    Database: cardemo
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car_color`
--

DROP TABLE IF EXISTS `car_color`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_color` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `COLOR` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_color`
--

LOCK TABLES `car_color` WRITE;
/*!40000 ALTER TABLE `car_color` DISABLE KEYS */;
INSERT INTO `car_color` VALUES (28,'White'),(27,'Turquoise'),(26,'Silver'),(25,'Red'),(24,'Purple'),(23,'Pink'),(22,'Orange'),(21,'Grey'),(20,'Green'),(19,'Gold'),(18,'Brown'),(17,'Blue'),(16,'Black'),(15,'Beige'),(29,'Yellow'),(30,'Custom');
/*!40000 ALTER TABLE `car_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_color_int`
--

DROP TABLE IF EXISTS `car_color_int`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_color_int` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `COLOR` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_color_int`
--

LOCK TABLES `car_color_int` WRITE;
/*!40000 ALTER TABLE `car_color_int` DISABLE KEYS */;
INSERT INTO `car_color_int` VALUES (1,'Beige'),(2,'Black'),(3,'Blue'),(4,'Brown'),(5,'Gold'),(6,'Green'),(7,'Grey'),(8,'Orange'),(9,'Purple'),(10,'Red'),(11,'Silver'),(12,'White'),(13,'Yellow'),(14,'Custom');
/*!40000 ALTER TABLE `car_color_int` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_drive_train`
--

DROP TABLE IF EXISTS `car_drive_train`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_drive_train` (
  `ID` tinyint(2) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_drive_train`
--

LOCK TABLES `car_drive_train` WRITE;
/*!40000 ALTER TABLE `car_drive_train` DISABLE KEYS */;
INSERT INTO `car_drive_train` VALUES (1,'FWD'),(2,'RWD'),(3,'AWD'),(4,'4x4'),(5,'Unknown');
/*!40000 ALTER TABLE `car_drive_train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_engine_type`
--

DROP TABLE IF EXISTS `car_engine_type`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_engine_type` (
  `ID` tinyint(2) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_engine_type`
--

LOCK TABLES `car_engine_type` WRITE;
/*!40000 ALTER TABLE `car_engine_type` DISABLE KEYS */;
INSERT INTO `car_engine_type` VALUES (1,'5 Cylinder'),(2,'6 Cylinder'),(3,'8 Cylinder'),(4,'10 Cylinder'),(5,'12 Cylinder'),(6,'16 Cylinder'),(7,'Unknown');
/*!40000 ALTER TABLE `car_engine_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_fuel_types`
--

DROP TABLE IF EXISTS `car_fuel_types`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_fuel_types` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_fuel_types`
--

LOCK TABLES `car_fuel_types` WRITE;
/*!40000 ALTER TABLE `car_fuel_types` DISABLE KEYS */;
INSERT INTO `car_fuel_types` VALUES (1,'Petrol'),(2,'Diesel'),(3,'Ethanol'),(4,'Hybrid'),(5,'Electric'),(6,'Other');
/*!40000 ALTER TABLE `car_fuel_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_gearbox`
--

DROP TABLE IF EXISTS `car_gearbox`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_gearbox` (
  `ID` tinyint(2) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_gearbox`
--

LOCK TABLES `car_gearbox` WRITE;
/*!40000 ALTER TABLE `car_gearbox` DISABLE KEYS */;
INSERT INTO `car_gearbox` VALUES (1,'Manual'),(2,'Automatic'),(3,'Semi-automatic'),(4,'F1'),(5,'E-gear'),(6,'Dual-clutch'),(7,'CVT'),(8,'Triptronic'),(9,'Unknown');
/*!40000 ALTER TABLE `car_gearbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_hook`
--

DROP TABLE IF EXISTS `car_hook`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_hook` (
  `ID` tinyint(2) NOT NULL auto_increment,
  `HOOK` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_hook`
--

LOCK TABLES `car_hook` WRITE;
/*!40000 ALTER TABLE `car_hook` DISABLE KEYS */;
INSERT INTO `car_hook` VALUES (1,'LHD'),(2,'RHD');
/*!40000 ALTER TABLE `car_hook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_james_List`
--

DROP TABLE IF EXISTS `car_james_List`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_james_List` (
  `ID` int(2) NOT NULL auto_increment,
  `JAMESID` int(11) default NULL,
  `REFERENCE` text,
  `DESCRIPTION` text,
  `PHONE` text,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_james_List`
--

LOCK TABLES `car_james_List` WRITE;
/*!40000 ALTER TABLE `car_james_List` DISABLE KEYS */;
INSERT INTO `car_james_List` VALUES (1,4646,'ZEROID','Zero ID Bespoke Cars','+44 7920 107339');
/*!40000 ALTER TABLE `car_james_List` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_listing`
--

DROP TABLE IF EXISTS `car_listing`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_listing` (
  `ID` int(11) NOT NULL auto_increment,
  `HEADLINE` text,
  `DESCRIPTION` text,
  `MAKE` int(4) default NULL,
  `MODEL` tinytext,
  `YEAR` int(4) default NULL,
  `COLOR` int(4) default NULL,
  `INTCOLOR` int(4) default NULL,
  `MILAGE` int(8) default NULL,
  `MUNIT` tinytext,
  `PRICE` tinytext,
  `CURRENCY` tinytext,
  `VAT` int(3) default NULL,
  `CARTYPE` int(4) default NULL,
  `FUELTYPE` int(4) default NULL,
  `HOOK` int(2) default NULL,
  `DRIVETRAIN` int(2) default NULL,
  `ENGINE` int(2) default NULL,
  `GEARBOX` int(2) default NULL,
  `VIN` text,
  `LIVE` int(1) default NULL,
  `FEATURED` int(2) default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_listing`
--

LOCK TABLES `car_listing` WRITE;
/*!40000 ALTER TABLE `car_listing` DISABLE KEYS */;
INSERT INTO `car_listing` VALUES (4,'Alfa Romeo','<p>Zero Identity offer a professional, first class, personal service ensuring you a confidential smooth path through to acquiring the vehicle you require or selling your outgoing car.We all work hard for our money and there are many people who are ready to take it away, let&rsquo;s keep hold of as much as we can!&nbsp;The company was originally set up with just footballers in mind, \'owned by footballers to benefit footballers\' but through word of mouth we have helped many non football industry individuals and companies get great prices and now we have exclusive clients from various sporting and entertainment backgrounds along with clients who are only interested in a fair price, you dont need to be buying an expensive car to benefit from our services, we just want to get every client the most that their money can buy!</p>\r\n<p>DO NOT LET YOUR PRICE BE EFFECTED&nbsp;BY WHO YOU ARE OR WHAT YOU HAVE ACHIEVED!</p>\r\n<p>We will not use the identity of any of our clients to promote ourselves in anyway, no pictures of or text relating to the identity of our clients be it top city lawyer, international football star or entertainment world diva will be used as a cheap marketing tool. Through our own competiveness and our customer service we want to be judged and spoken of.</p>',6,'other',2011,0,0,120000,'KM','20000','USD',0,0,1,0,0,0,0,'',9,0),(65,'test','<p>test</p>',0,'other',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(26,'Oakly design special - Porche','<p>Limited edition CarreraConsectetur adipiscing elit. Phasellus adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec congue augue neque vitae augue. Suspendisse vitae elit a purus commodo lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis, interdum vel, felis. Integer laoreet, justo quis placerat lobortis, metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem. Integer non leo et enim tincidunt lobortis. Pellentesque fermentum fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus. Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec congue augue neque vitae augue. Suspendisse vitae elit a purus commodo lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis, interdum vel, felis. Integer laoreet, justo quis placerat lobortis, metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem. Integer non leo et enim tincidunt lobortis. Pellentesque fermentum fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus. Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.</p>',260,'other',2010,28,2,10000,'Miles','110000','EUR',4,2,1,1,4,6,3,'23456789',2,1),(18,'Aston Martin DB9','<p>dagsdgsfg</p>',25,'DB7',0,0,0,0,NULL,'60000','EUR',0,0,0,0,0,0,NULL,NULL,9,0),(19,'Aston Martin DB9.... Excellent condition. As new.','<p>Limited edition CarreraConsectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.</p>',25,'DB',2009,28,2,1000000,'Miles','90000','EUR',2,1,1,1,1,3,1,'12345667',9,0),(27,'Ferrari1','<p>Ferrari1</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(28,'Ferrari2','<p>ferrari&nbsp;</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(29,'Ferrari 612','<p>Ferrari</p>',126,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(30,'Jaguar XK','<p>Jaguar XK</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(31,'Rossion Q1','<p>Rossion Q1</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(32,'1958 Lotus 15 ','<p>1958 Lotus 15&nbsp;</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(33,'1965 Bizzarrini','<p>1965 Bizzarrini</p>',0,'other',0,0,0,0,NULL,NULL,'EUR',0,0,0,0,0,0,NULL,NULL,0,0),(34,'For Sale: 2011 Aston Martin V12 Vantage','<p>Limited edition CarreraConsectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.</p>',25,'DB',2011,28,14,120,'','POA','EUR',0,2,1,1,3,6,8,'SCFEBBCF9BGS00589',1,1),(36,'AUDI R8 for Sale','<p>2009 AUDI R8 COUPE EXOTIC CLASSICS IS PLEASED TO PRESENT THIS 2009 AUDI R8 COUPE. THIS VEHICLE IS FEATURED IN JET BLUE METALLIC OVER TUSCAN BROWN LEATHER INTERIOR. THE OPTIONS ARE: OXYGEN SILVER SIDE BLADE, ALCANTARA HEADLINER IN BLACK, FRONT LICENSE PLATE HOLDER, BANG AND OLUFSEN SOUND SYSTEM, ENHANCED FINE NAPPA LEATHER PACKAGE, AUDI DVD NAVIGATION PLUS AND PREMIUM PACKAGE.</p>',28,'A1',2009,17,4,5626,'Miles','132000','USD',0,2,1,1,4,5,1,NULL,1,1),(37,'For Sale: Maserati MC12 Corsa,','<p>Rare colour scheme!</p>\r\n<p>This is a Maserati MC12 Corsa for sale &nbsp;The asking price is available upon request</p>',205,'other',2007,26,2,20,'Miles','850000','GBP',0,2,1,1,3,5,1,'FMMS392756TY',1,1),(69,'zcv cbcnc','<p>fghfghfjndf</p>',41,'Turbo S',2001,18,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(38,'For Sale: ONYX CONCEPT GTO','<p>Zero Identity offer a professional, first class, personal service  ensuring you a confidential smooth path through to acquiring the vehicle  you require or selling your outgoing car. We all work hard for our  money and there are many people who are ready to take it away, let\'s  keep hold of as much as we can!  The company was originally set up with  just footballers in mind, \'owned by footballers to benefit footballers\'  but through word of mouth we have helped many non football industry  individuals and companies get great prices and now we have exclusive  clients from various sporting and entertainment backgrounds along with  clients who are only interested in a fair price, you dont need to be  buying an expensive car to benefit from our services, we just want to  get every client the most that their money can buy!</p>\nDO NOT LET YOUR PRICE BE EFFECTED BY WHO YOU ARE OR WHAT YOU HAVE ACHIEVED!\n<p>We will not use the identity of any of our clients to promote  ourselves in anyway, no pictures of or text relating to the identity of  our clients be it top city lawyer, international football star or  entertainment world diva will be used as a cheap marketing tool. Through  our own competiveness and our customer service we want to be judged and  spoken of.</p>\n\n',41,'Arnage',2011,16,2,1000,'Miles','150000','GBP',0,2,1,1,4,6,2,NULL,1,1),(39,'For Sale: BRABUS 700 BI-TURBO','<h1>Mercedes SLS AMG</h1>\r\n<p>World Debut at the Geneva Motor Show 2011</p>\r\n<p>BRABUS 700 Biturbo Based on the Mercedes SLS AMG</p>\r\n<p>700 hp / 514 kW, 850 Nm and 340 km/h Top Speed</p>\r\n<p>&amp;</p>\r\n<p>BRABUS 700 Biturbo: Powered by the new BRABUS twin-turbo engine with 700 hp</p>\r\n<p>(690 bhp) / 514 kW and a peak torque of 850 Nm (626 lb-ft) and clad in the thrilling</p>\r\n<p>WIDESTAR wide-body version the Mercedes SLS AMG from BRABUS ascends to a new</p>\r\n<p>level in the league of super sports cars. The gullwing, further refined by an exclusive interior,</p>\r\n<p>reaches a top speed of 340 km/h (213 mph) and celebrates its world debut at the</p>\r\n<p>Geneva Motor Show 2011.</p>\r\n<p>A high-tech turbocharger system for the high-tech engine of the SLS: The BRABUS engine&nbsp;engineers have designed a state-of-the-art twin turbo system for the fast-revving&nbsp;eight-cylinder four-valve engine of the SLS. The high-performance exhaust manifolds are optimized for exhaust-gas flow and&nbsp;were custom-developed for this sports car. They drive the two turbochargers.&nbsp;The chargers themselves are another BRABUS custom development. For faster response&nbsp;and better efficiency both chargers feature an air diverter valve. V-engines are normally fitted with two right-turning chargers, which entails drawbacks forthe response characteristics. To eliminate these drawbacks the BRABUS engineers have&nbsp;developed the B63 bi-turbo system with a left-turning charger for the left side of the engine.&nbsp;That allows the turbine and its inlet to be designed for perfect gas flow and gas dynamics.&nbsp;The result is an outstanding engine with an exemplary power curve. The BRABUS engine conversion also includes a generously dimensioned intercooling system&nbsp;with two water-to-air heat exchangers. The dual-flow intake manifold gets its combustion&nbsp;air through a package-optimized air filter housing and high-performance air filters. The company engine shop also modifies the inner workings of the engine that like all&nbsp;BRABUS engines is lubricated exclusively with fully-synthetic ARAL high-performance motor oil.&nbsp;Specially manufactured BRABUS forged pistons reduce the compression ratio to 9.0:1. The newly programmed engine management system with special mapping for ignition&nbsp;and injection orchestrates the perfect interaction of the BRABUS high-performance&nbsp;components. The system also features an integrated electronic boost pressure control.&nbsp;The result is an engine that delivers its extreme power smoothly while meeting the strict&nbsp;emission limits set by EURO V standards. The exhaust side of the V8-engine is upgraded with free-flow high-performance catalysts&nbsp;and the BRABUS high-performance exhaust system that features four slanted tailpipes&nbsp;with diameters of 84 millimeters (3.2 in.). The BRABUS exhaust is made from ultra-light&nbsp;titanium and weighs 12 kilograms (26 lbs.) less than the production exhaust. A pneumatic&nbsp;flap system, controlled from the cockpit, adds a &ldquo;coming home&rdquo; sound setting that is quieter&nbsp;than the production exhaust. The alternatively selectable sport setting gives the 6.3-liter&nbsp;eight-cylinder engine an even throatier note than the stock exhaust. Powered by this engine the BRABUS 700 Biturbo is among the most powerful super sports&nbsp;cars in the world. As the name indicates, the engine produces a rated power output of 700 hp&nbsp;(690 bhp) / 514 kW at 6,600 rpm. The peak torque of 850 Nm (626 lb-ft) is already available&nbsp;at a low 4,300 rpm. In combination with the SPEEDSHIFT DCT seven-speed sport transmission the resulting&nbsp;performance is breathtaking: The two-seater car sprints from rest to 100 km/h (62 mph)&nbsp;in just 3.7 seconds and reaches 200 km/h (124 mph) in 10.2 seconds. Top speed is 340 km/h(213 mph). To give the SLS also a sportier appearance the BRABUS WIDESTAR wide-body version was&nbsp;developed in the wind tunnel. Like in Formula 1 racing all aerodynamic-enhancement&nbsp;components are manufactured from lightweight yet high-strength carbon-fiber compounds.&nbsp;The body components can be painted in body or contrasting color or finished with a clear&nbsp;coat for a purebred racing look. The BRABUS WIDESTAR flares on the rear axle add 20 millimeters (0.8 in.) to the width of&nbsp;the gullwing. They also facilitate the installation of ultra-light forged wheels, which widen the&nbsp;track and thus further improve driving dynamics. To put even more emphasis on the wedge&nbsp;shape of the coupe the BRABUS Monoblock F &ldquo;PLATINUM EDITION&rdquo; wheels are mounted in a&nbsp;staggered combination of size 9.5Jx20 wheels in front and size 11Jx21 on the rear axle.&nbsp;Despite the fact that the wheels are larger than their production counterparts they weigh up to12 percent less. The wheels are either ceramics-polished or come with a brushed-design&nbsp;surface. They can also be painted any desired color. The high-performance tires in sizes&nbsp;275/30 ZR 20 and 295/25 ZR 21 are supplied by BRABUS technology partners Pirelli and&nbsp;YOKOHAMA. At the high speeds attained by the BRABUS 700 Biturbo aerodynamic stability is absolutely&nbsp;essential. The BRABUS front spoiler reduces lift on the front axle at high speed. Combined&nbsp;with the two BRABUS covers for the upper air inlets in the apron it lends the SLS an evenmore striking face. For optimal aerodynamic balance BRABUS upgrades the rear with a spoiler lip on the trunk lidand a diffuser. Additionally the rear apron is fitted with side air outlets. The sides of the gullwing are upgraded by the BRABUS gills for the front fenders.&nbsp;At night their integrated, blue-illuminated BRABUS lettering, activated by the keyless fob&nbsp;or by pulling on a door handle, stands out immediately. Sporty yet elegant side skirts create a perfect aerodynamic transition between front and&nbsp;rear fenders. The cladding features integrated entrance lights for safe entering and exiting&nbsp;of the vehicle in the dark. The BRABUS suspension was developed in cooperation with technology partner BILSTEIN.&nbsp;It offers advantages both visual and driving dynamics benefits. Those include a ride-height&nbsp;lowering by up to 30 millimeters (1.2 in.). The integrated &lsquo;Ride Control&rsquo; function offers the&nbsp;driver a choice between more comfortable or sportier damper settings compared to theproduction car, all at the push of a button in the cockpit.</p>\r\n<p>BRABUS sport stabilizers on front and rear axle affect even more agile turn-in ability</p>\r\n<p>and further reduced body roll.</p>\r\n<p>The BRABUS Ride Control smooth-ride suspension is also available with the optional&nbsp;BRABUS Front Lift system. BRABUS Front Lift can raise the front axle by 50 mm (2.0 in.)&nbsp;and increases the approach angle. Custom-tailored interiors have been a special BRABUS domain for more than three decades.The company upholstery shop crafts individual, masterful exclusive interiors in any&nbsp;desired leather or Alcantara color for the SLS as well. These interiors are complemented by an ergonomically shaped sport steering wheel,&nbsp;a speedometer with 400-km/h (250-mph) dial and matte or shiny carbon-fiber&nbsp;elements in any desired color. Matte anodized aluminum pedals and foot rest addfurther distinctly sporty highlights.</p>\r\n<p>&nbsp;</p>',213,'other',2011,26,2,0,'Miles','300000','GBP',0,2,1,1,4,3,4,'',2,1),(66,'test','<p>test</p>',0,'other',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(67,'test 2','<p>test 2</p>',0,'other',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(68,'test 3','<p>test 3</p>',0,'other',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(40,'For Sale: Datsun 280ZX 10th Anniversary','<h1>For Sale: Datsun ZX 10th Anniversary</h1>\n<p>Limited edition CarreraConsectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus  adipiscing, nunc quis vehicula vehicula, mi nunc tempor lorem, nec  congue augue neque vitae augue. Suspendisse vitae elit a purus commodo  lobortis. Nullam placerat magna id sem. Nulla lacinia ultrices est. Ut  auctor, sem sed interdum aliquam, diam nulla viverra quam, sed faucibus  risus dui sit amet mauris. Proin quam enim, luctus ut, faucibus quis,  interdum vel, felis. Integer laoreet, justo quis placerat lobortis,  metus mauris iaculis augue, sit amet fermentum lectus ante vel lorem.  Integer non leo et enim tincidunt lobortis. Pellentesque fermentum  fermentum justo. Nam sodales tempor tortor. Mauris sit amet tellus.  Integer at dolor non felis vestibulum luctus. Ut pretium massa a metus  condimentum faucibus. Quisque convallis, risus quis dapibus tempor, sem  mi aliquam risus, eu ullamcorper metus sapien sit amet nunc. Fusce enim.</p>',96,'other',1980,19,1,60000,'Miles','15000','GBP',0,2,1,1,1,2,1,NULL,2,0),(63,'qwer','<p>qwe</p>',28,'A1',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,NULL,0,0),(64,'qwer','<p>qwe</p>',0,'other',0,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,NULL,0,0),(50,'form test ','<p>form test</p>',50,'M1',2011,20,8,10000,'Miles','10000','GBP',2,3,5,2,0,6,4,'12345678',0,0),(62,'For Sale: Land Rover Range Rover','<h1><span style=\"font-size: 10px; font-weight: normal;\">This 2011 Land Rover Range Rover 4dr 4WD 4dr HSE LUX 4x4 SUV features a 5.0L SMPI 32-VALVE V8 8 cyl Gasoline engine. It is equipped with a 6 Speed Automatic transmission. The vehicle is Galway Green with a Ivory Leather interior. It is offered with a full factory warranty. - AM-FM, Leather Interior Surface, Four Wheel Drive&nbsp;</span></h1>\n<p>&nbsp;</p>',267,'other',2011,28,2,10000,'Miles','50000','GBP',1,5,1,2,1,6,4,'123456',0,0),(44,'For Sale: Roadster 4.2 sÃƒÂ©rie 2','<p>This Roadster Series 2 is in excellent condition. He never knew of corrosion or shock. The alignments are perfect. The Eiffel Towers perfectly straight.&nbsp;<br />Mechanics also in perfect condition, strong engine, gearbox smoothly.&nbsp;<br />The restoration of 1998, this painting very well. Upholstery and hood in very good condition. Vehicle tracking and maintained.&nbsp;</p>',172,'other',1969,20,2,120000,'Miles','50000','GBP',0,2,1,1,0,2,1,'etpe123456',0,0),(70,'bad car','<p>even badder</p>',8,'other',1997,0,0,0,'Miles','POA','GBP',0,0,0,0,0,0,0,'',9,0),(71,'23452345','<p>345</p>',1,'other',2011,0,0,1234,'Miles','12345','GBP',0,6,0,0,0,0,0,'34',9,0);
/*!40000 ALTER TABLE `car_listing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_listing_history`
--

DROP TABLE IF EXISTS `car_listing_history`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_listing_history` (
  `ID` int(11) NOT NULL auto_increment,
  `LISTING_ID` int(10) default NULL,
  `STATUS` tinyint(2) default NULL,
  `USER` tinytext,
  `TSTAMP` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_listing_history`
--

LOCK TABLES `car_listing_history` WRITE;
/*!40000 ALTER TABLE `car_listing_history` DISABLE KEYS */;
INSERT INTO `car_listing_history` VALUES (1,4,1,'Peter','0000-00-00 00:00:00'),(2,4,2,'Peter','0000-00-00 00:00:00'),(3,4,1,'Peter','2011-05-13 12:21:54'),(4,4,2,'','2011-05-13 12:25:47'),(5,4,0,'Peter','2011-05-13 12:26:28'),(6,0,0,'Peter','2011-05-13 12:33:46'),(7,68,0,'Peter','2011-05-13 12:34:35'),(8,67,9,'Peter','2011-05-13 12:38:54'),(9,4,1,'Peter','2011-05-15 16:09:45'),(10,69,0,'Peter','2011-05-16 11:15:58'),(11,69,1,'Peter','2011-05-16 11:17:53'),(12,70,0,'kriptik','2011-05-26 08:42:01'),(13,71,0,'kriptik','2011-05-30 09:36:17'),(14,71,9,'kriptik','2011-05-30 09:38:08'),(15,4,9,'kriptik','2011-05-30 19:24:05'),(16,18,9,'kriptik','2011-05-30 19:24:10'),(17,69,9,'kriptik','2011-05-30 19:24:28'),(18,66,9,'kriptik','2011-05-30 19:24:31'),(19,70,9,'kriptik','2011-05-30 19:24:34'),(20,65,9,'Peter','2011-05-31 11:21:10');
/*!40000 ALTER TABLE `car_listing_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_listing_images`
--

DROP TABLE IF EXISTS `car_listing_images`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_listing_images` (
  `recordID` int(11) NOT NULL auto_increment,
  `recordText` varchar(255) default NULL,
  `recordListingID` int(11) default NULL,
  `LISTINGID` int(10) default NULL,
  PRIMARY KEY  (`recordID`)
) ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_listing_images`
--

LOCK TABLES `car_listing_images` WRITE;
/*!40000 ALTER TABLE `car_listing_images` DISABLE KEYS */;
INSERT INTO `car_listing_images` VALUES (144,'p15vcoo0h118to2r4i2u9b41fmn1.jpg',0,49),(4,'p15v39100bv861nkv15h5spa1gam1.jpg',2,19),(6,'p15v73k9692h539i7t4gdlpbv1.jpg',1,19),(7,'p15v7k4ik5bun1uamjtaqeac6b1.jpg',2,26),(8,'p15v7k4ik5rnf16vm15jp1c031hk52.jpg',3,26),(9,'p15v7k4ik5c1vehtjhs11uvlkp3.jpg',1,26),(10,'p15v7k4ik51ob5tfsfd329mhl4.jpg',4,26),(11,'p15v7k4ik51llaro51kh4fuc1scv5.jpg',5,26),(12,'p15v7k4ik5fkt11lh10ij3f1pob6.jpg',6,26),(13,'p15v7k4ik5111alsf191kmp1j9a7.jpg',7,26),(14,'p15v7k4ik5ud2bnp165t183csmt8.jpg',8,26),(15,'p15v7k4ik5lscud0193ct771jv89.jpg',9,26),(26,'p15v8l9q9e17ob5qp1v9o1ucb1aa22.png',1,27),(98,'p15vbcq9pb18uh1jq6k47r2cm4o3.jpg',6,38),(97,'p15vbcq9pbn1hftq1tqbkgt6ed2.jpg',3,38),(27,'p15v8l9q9e1a9v1ilq1dhb1qj72rb3.png',2,27),(25,'p15v8l9q9evbeosc1mpcrcrc4u1.png',3,27),(28,'p15v8l9q9eoou13dv1ipv1b9ljb44.png',4,27),(29,'p15v8l9q9entovk8b1gpoh1rn55.png',5,27),(30,'p15v8l9q9e15jojj41ev517cd1q9r6.png',6,27),(31,'p15v8lsca5q918341h2i1dv61j3t1.png',2,28),(32,'p15v8ltrlg1a331jpp1ve51qoqb9q1.png',1,28),(33,'p15v8ltrlg1cb9vfso6p40jvjc2.png',3,28),(34,'p15v8ltrlgcu61os6nsjot3l1d3.png',4,28),(35,'p15v8ltrlg17oj7i71d6qrn71km34.png',5,28),(36,'p15v8ltrlg1s7i1uke1f4i19p17m05.png',6,28),(37,'p15v8m6bg71fg1elo1a9psgu3e41.png',3,29),(38,'p15v8m6bg79e41h97slkirksq82.png',4,29),(39,'p15v8m6bg710s11g7h1v8695lr7p3.png',1,29),(40,'p15v8m6bg71lmm1ept9cu1drdcam4.png',5,29),(41,'p15v8m6bg7tv59cs9811e2f375.png',2,29),(96,'p15vbcq9pb13d7oeec14kc133e1.jpg',4,38),(47,'p15v8msdhu1am01ff1og31nm093j1.png',1,30),(48,'p15v8msdhu86ieor104n1jq51b1v2.png',2,30),(49,'p15v8msdhu12m31up483j136m1hjs3.png',3,30),(50,'p15v8msdhu25n1so618m8nc7e4r4.png',4,30),(51,'p15v8msdhub5v1qi513vifmidam5.png',5,30),(52,'p15v8n1gbu1k524upegq426bbt1.png',3,31),(53,'p15v8n1gbubjoeko651dvrr7i2.png',2,31),(54,'p15v8n1gbudob7o1651hao11h3.png',4,31),(55,'p15v8n1gbu1drb1vd810e7igv7vd4.png',1,31),(56,'p15v8n1gbu14g41hgf1vk41926k295.png',5,31),(57,'p15v8n1gbumkr5q67mjodc1o7e6.png',6,31),(58,'p15v8n8mbi1a9k118g10rb1sirseu1.jpg',1,32),(59,'p15v8n8mbi7651n881i7a58iddp2.jpg',2,32),(60,'p15v8n8mbi964sliftu1lg913ge3.jpg',3,32),(61,'p15v8n8mbib741ud6rmg144c1m2n4.jpg',4,32),(62,'p15v8nbrsg1d2o11r51fh8mphoiu1.jpg',2,33),(63,'p15v8nbrsgqvv1sojc0u1del3vb2.jpg',1,33),(64,'p15v8nbrsg7fm1cn01vpevpe16eg3.jpg',4,33),(65,'p15v8nbrsg1rjm5cj3ug123s1gu64.jpg',5,33),(66,'p15v8nbrsg12cr17rcs7cqlp19fs5.jpg',3,33),(67,'p15vasc2grs1l1qjc1dg01el5l611.jpg',1,34),(68,'p15vasc2gs1f3janq1amn1aml1kh42.jpg',2,34),(69,'p15vasc2gsocb1a4dea36vp1abf3.jpg',3,34),(70,'p15vasc2gs1vid13cgbvci0geg14.jpg',4,34),(71,'p15vasc2gs4robaqh761evaqhv5.jpg',5,34),(72,'p15vasc2gs1vi054o10e55bbggn6.jpg',6,34),(73,'p15vasc2gs5s42jc4sg2vh10727.jpg',7,34),(75,'p15vbac43g1ihq1olvsmii3je2p1.jpg',5,36),(76,'p15vbac43g1pe8rpq1g38eee14jh2.jpg',1,36),(77,'p15vbac43g1g7khoo5dh17dk1uof3.jpg',7,36),(78,'p15vbac43gkk5dhd1do1jj17854.jpg',8,36),(79,'p15vbac43g1oq311q16i51okp17vq5.jpg',3,36),(80,'p15vbacf63je5183l1ius1dtug2m6.jpg',4,36),(81,'p15vbacf63and3s0stmrmcqr7.jpg',6,36),(82,'p15vbacf631jmk5mokq3su716sn8.jpg',12,36),(83,'p15vbacf63hu8pj89e811o5a9.jpg',11,36),(84,'p15vbacf63luq1nnhcj76g21pq0a.jpg',10,36),(85,'p15vbacf63u2j7773l03ves6tb.jpg',9,36),(86,'p15vbad79ld0adu51j971h6s10ivc.jpg',2,36),(87,'p15vbb8fd110daium1gva1dh91hdu1.jpg',2,37),(88,'p15vbb8fd1odf105r1h1m1cpcf142.jpg',3,37),(89,'p15vbb8fdajun1k8r1cc45c2ljl3.jpg',1,37),(90,'p15vbcii4s1tl1osnl0f8il5281.jpg',5,0),(91,'p15vbcii4s1p75je61hqk1fdskd2.jpg',4,0),(92,'p15vbcii4s1hms1ogk1um91v31cda3.jpg',3,0),(93,'p15vbcii4s138p2q51fn48i81upm4.jpg',2,0),(94,'p15vbcii4s37419t2u90178p1gsk5.jpg',1,0),(95,'p15vbcii4to5qio71r3f922113i6.jpg',6,0),(99,'p15vbcq9pb18un1pch19adbjujnd4.jpg',5,38),(100,'p15vbcq9pbmeo11gu57s19o31j3p5.jpg',1,38),(101,'p15vbcq9pb1n1v1ico1aslchu1n1o6.jpg',2,38),(102,'p15vbdatlc1scr1nnnv02uko17ll1.jpg',12,39),(103,'p15vbdatlcbb41hvjn5f1p4h51a2.jpg',1,39),(104,'p15vbdatlcpha14kp1umanvl1e7l3.jpg',6,39),(105,'p15vbdatlcso711hl7502al1d024.jpg',3,39),(106,'p15vbdatlc12jt4ppg413u1akc5.jpg',2,39),(107,'p15vbdatlcf2fh3782418jplht6.jpg',5,39),(108,'p15vbdatlc2lck9iss6101b14pm7.jpg',4,39),(109,'p15vbdatlc16erbi7gf91gok1u6a8.jpg',11,39),(110,'p15vbdatlc1aae1nal11u67bpt29.jpg',10,39),(111,'p15vbdatlc1bgi5un6b11hsn7da.jpg',9,39),(112,'p15vbdatlcs5p102c17m111fh1mb6b.jpg',8,39),(113,'p15vbdatlcpmc1t5o6bl1io9p8cc.jpg',7,39),(114,'p15vbe1lgdcji1n587ashpk5ad1.jpg',4,40),(115,'p15vbe1lged601bm3315be3uul2.jpg',1,40),(116,'p15vbe1lge1d1cv2f1kg931p12et3.jpg',6,40),(117,'p15vbe1lge1ok91m2t7ior161j5j4.jpg',18,40),(118,'p15vbe1lgesmln6l1d4d14hj3645.jpg',3,40),(119,'p15vbe1lge1jmsbuo15291v7d1sa26.jpg',19,40),(120,'p15vbe1lgecp8s176517m77up7.jpg',20,40),(121,'p15vbe1lgeuch3mpqsl1u3stvk8.jpg',21,40),(122,'p15vbe1lge1vpv1t7918vlagev1r9.jpg',22,40),(123,'p15vbe1lge1j2k1p7g1qgrhl030pa.jpg',23,40),(124,'p15vbe1lgebgo1voj1u9j1hn293b.jpg',24,40),(125,'p15vbe1lgesmtsec1bbmlfv19upc.jpg',17,40),(126,'p15vbe1lge1a7c1tv3iue10qji1nd.jpg',16,40),(127,'p15vbe1lge1m53h00nhk16gpub5e.jpg',15,40),(128,'p15vbe1lge1saq1oj318p8us6pg0f.jpg',14,40),(129,'p15vbe1lge6tesv76e518per6gg.jpg',7,40),(130,'p15vbe1lge1vasu45nf21pde1mdrh.jpg',8,40),(131,'p15vbe1lge13tba2vk0316005cbi.jpg',9,40),(132,'p15vbe1lgemu11jlo3d31f3n1l78j.jpg',10,40),(133,'p15vbe1lge1jf1ub31rdt19r77qrk.jpg',11,40),(134,'p15vbe1lgeqm11uq1qi91hgl1mnql.jpg',5,40),(135,'p15vbe1lges5g1ctn1pd014vvdc8m.jpg',12,40),(136,'p15vbe1lge1i244jvtog1nvu14lvn.jpg',2,40),(137,'p15vbe1lge1borl6ro4qro8g65o.jpg',13,40),(138,'p15vbe1lgebuoh11b7e13mjdvp.jpg',25,40),(139,'p15vbfl880ocp1fmj1f011a3nsmt1.jpg',1,44),(140,'p15vbfl8811itc6m0qt41e41mgs2.jpg',2,44),(141,'p15vbfl8871i941ipmaktf873v93.jpg',3,44),(142,'p15vbfl8871e7t1i971map19no1ls4.jpg',4,44),(143,'p15vbfl8871oac1f31kmvq139c45.jpg',5,44),(145,'p15vcoo0h1iq61aqt1gsr1ips6is2.jpg',0,49),(146,'p15vcq19su1fqlo2mjpk10ol1g2b1.jpg',2,50),(147,'p15vcq19sum5k3idnsv1c28ro2.jpg',3,50),(148,'p15vcq19sujt91fi515t5rg2152s3.jpg',1,50),(149,'p15vcsekaa1enl1soqo02q0jql71.gif',2,52),(150,'p15vcsfd7e2hd1muj54ksnncbk1.png',1,52),(151,'p15vcspc3a1ardti2vpc18l11fr61.gif',1,53),(152,'p15vcvja5t1c5vjtkqgr1mljsp41.gif',1,60),(153,'p15vcvja5t6p51an11lb9degkt02.png',1,60),(154,'p15vd3rndjsi4hu410221osevcn1.jpg',2,62),(155,'p15vd3rndj1sqb1k8310v21bcq7kf2.jpg',1,62),(156,'p15vd3rndkmkk22qg021tfg14vj3.jpg',3,62),(157,'p15vd3rndkuegie61pncc6i1a364.jpg',5,62),(158,'p15vd3rndk13b0mqu1aae1147eq95.jpg',7,62),(159,'p15vd3rndk1atj1ji41ip7hu314ck6.jpg',4,62),(160,'p15vd3rndk1bkorjn18ac9js1ouo7.jpg',13,62),(161,'p15vd3rndk16dm1r1c119a1grj1ddh8.jpg',12,62),(162,'p15vd3rndk523pfqvno1j9tlgd9.jpg',11,62),(163,'p15vd3rndk3os8o7lj5kpf1fo3a.jpg',10,62),(164,'p15vd3rndkdp71ulupri1ass1rdvb.jpg',9,62),(165,'p15vd3rndk1p4d92r16i01q9aad6c.jpg',8,62),(166,'p15vd3rndk1u4t1s2b1mr99g7155d.jpg',6,62),(167,'p15vd3rndk1j901d961c5k1c3c433e.jpg',14,62),(168,'p15vfogf5gof43dnctd1ql81hv61.jpg',2,63),(169,'p15vfogf5g1kk3tffbqs1m2p1jga2.jpg',1,63),(170,'p15vfogf5g1tqhi0esmo4sc013.jpg',3,63),(175,'p15vsf8jcq156n1hkiv241sr63ol1.jpg',1,69),(173,'p15vfplrug5lc17qd1bskcqf1o9j2.jpg',2,64),(174,'p15vfplrug1nmaq75t92o9t1ka3.jpg',1,64),(176,'p15vsf8ot2tr21i6jjar1pq81apo2.JPG',1,69),(178,'p1610b2nj31moc19sg1hg9e501aan2.jpg',1,71),(179,'p1610b2nj3114v1h9611r6m0caac3.jpg',2,71);
/*!40000 ALTER TABLE `car_listing_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_makes`
--

DROP TABLE IF EXISTS `car_makes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_makes` (
  `ID` int(10) NOT NULL auto_increment,
  `MAKE` text,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=362 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_makes`
--

LOCK TABLES `car_makes` WRITE;
/*!40000 ALTER TABLE `car_makes` DISABLE KEYS */;
INSERT INTO `car_makes` VALUES (1,'300sl panamericana'),(2,'Abarth'),(3,'Abarth Simca'),(4,'AC'),(5,'Adler'),(6,'Alfa Romeo'),(7,'Allard'),(8,'Alpina'),(9,'Alvis'),(10,'Am general'),(11,'AMC'),(12,'American IronHorse'),(13,'American LaFrance'),(14,'Amilcar'),(15,'Amphicar'),(16,'Aprilia'),(17,'Arash'),(18,'Archer'),(19,'Ariel'),(20,'Arlen Ness'),(21,'Armstrong siddeley'),(22,'Arnolt'),(23,'ASA'),(24,'Ascari Cars'),(25,'Aston Martin'),(26,'Attila'),(27,'Auburn'),(28,'Audi'),(29,'Austin'),(30,'Austin Healey'),(31,'Auto Union'),(32,'Autobianchi'),(33,'Autocar'),(34,'Autounion'),(35,'Avanti'),(36,'BAC'),(37,'Baci'),(38,'Baja'),(39,'Bantam'),(40,'Bayliner'),(41,'Bentley'),(42,'Bergholt'),(43,'Berkeley'),(44,'Berkely'),(45,'Big Dog'),(46,'Big Inch Bikes'),(47,'Bimota'),(48,'Bisc&#8730;&#8747;ter'),(49,'Bizzarrini'),(50,'BMW'),(51,'Borgward'),(52,'Boss Hoss'),(53,'Bourget'),(54,'Brabham'),(55,'Bricklin'),(56,'Bristol'),(57,'Brush'),(58,'Buell'),(59,'Bufori'),(60,'Bugatti'),(61,'Buick'),(62,'Cadillac'),(63,'Cagiva'),(64,'Campagna'),(65,'Can-Am'),(66,'Carver'),(67,'Caterham'),(68,'Century'),(69,'Chalmers'),(70,'Chambers'),(71,'Checker'),(72,'Chevrolet'),(73,'Chevron'),(74,'Chevron Cars Ltd'),(75,'Chris Craft'),(76,'Chrysler'),(77,'Cigarette'),(78,'Citroen'),(79,'Classic'),(80,'Clenet'),(81,'Cobra'),(82,'Connaught'),(83,'Continental'),(84,'Cooper'),(85,'Cord'),(86,'Coronet'),(87,'Corsair'),(88,'Corvette'),(89,'Crosley'),(90,'Crownline'),(91,'Custom'),(92,'Custom Built'),(93,'DAF'),(94,'Daimler'),(95,'Dartz Kombat'),(96,'Datsun'),(97,'DB'),(98,'de Havilland'),(99,'Delage'),(100,'Delahaye'),(101,'DeLorean'),(102,'DeSoto'),(103,'Destiny'),(104,'DeTomaso'),(105,'Detroiter'),(106,'Devaux'),(107,'Diana'),(108,'Divco'),(109,'Dodge'),(110,'Donkervoort'),(111,'Dort'),(112,'Dual-Ghia'),(113,'Ducati'),(114,'Duesenberg'),(115,'E.M.F'),(116,'Eagle'),(117,'Edsel'),(118,'EMW'),(119,'Essex'),(120,'Excalibur'),(121,'Excelsior'),(122,'Facel'),(123,'Factory Five'),(124,'Falcon'),(125,'Fend'),(126,'Ferrari'),(127,'Fiat'),(128,'Fisker'),(129,'Flanders'),(130,'Foose'),(131,'Ford'),(132,'Formula'),(133,'Fornasari'),(134,'Fountain'),(135,'Franklin'),(136,'Gatsby'),(137,'Gaz'),(138,'General Motors'),(139,'Geo'),(140,'Ghia'),(141,'Gillet'),(142,'Ginetta'),(143,'Glaspar'),(144,'GMC'),(145,'Graham'),(146,'Gumpert'),(147,'Harley-Davidson'),(148,'Healey'),(149,'Hillegus'),(150,'Hillman'),(151,'Hispano-Suiza'),(152,'Honda'),(153,'Horch'),(154,'Hotchkiss'),(155,'HRG'),(156,'HTT'),(157,'Hudson'),(158,'Humber'),(159,'Hummer'),(160,'Hupmobile'),(161,'Hurtan'),(162,'Ifa'),(163,'Indian Motorcycle'),(164,'Infiniti'),(165,'Innocenti'),(166,'Intermeccanica'),(167,'International'),(168,'Invicta'),(169,'Iso'),(170,'Isotta-Fraschini'),(171,'Jackson'),(172,'Jaguar'),(173,'Javelin'),(174,'Jeep'),(175,'Jeepster'),(176,'Jensen Healey'),(177,'Jewel'),(178,'Johnson Motor Company'),(179,'Kaiser'),(180,'Kawasaki'),(181,'Kenworth'),(182,'King Yachts'),(183,'Kissel'),(184,'Koenigsegg'),(185,'KTM'),(186,'Lagonda'),(187,'Lamborghini'),(188,'Lanchester'),(189,'Lancia'),(190,'Land'),(191,'Land Rover'),(192,'Larson'),(193,'Lea Francis'),(194,'Leblanc'),(195,'Lexus'),(196,'Ligier'),(197,'Lincoln'),(198,'Little'),(199,'Lola'),(200,'Lotus'),(201,'Lucra'),(202,'Maico'),(203,'Marmon'),(204,'Marussia'),(205,'Maserati'),(206,'Matchless'),(207,'Matra'),(208,'Maxwell'),(209,'Maybach'),(210,'Mazda'),(211,'McLaren'),(212,'Melkus'),(213,'Mercedes-Benz'),(214,'Mercury'),(215,'Metz'),(216,'MG'),(217,'Mil'),(218,'Milburn'),(219,'Mini'),(220,'Mitsouka'),(221,'Mitsubishi'),(222,'Monteverdi'),(223,'Morgan'),(224,'Morris'),(225,'Mosler'),(226,'Moto Guzzi'),(227,'N2A Motors'),(228,'Nash Motors'),(229,'Neustadt'),(230,'Nissan'),(231,'Noble'),(232,'Norton'),(233,'Not specified'),(234,'NSU'),(235,'O.S.C.A.'),(236,'Oakland'),(237,'Officine Meccaniche'),(238,'Oldsmobile'),(239,'Opel'),(240,'Orange County Choppers'),(241,'Other'),(242,'Overland'),(243,'Packard'),(244,'Pagani'),(245,'Palmetto'),(246,'Panamericana'),(247,'Panhard'),(248,'Panoz'),(249,'Panther'),(250,'Pedrazzini'),(251,'Peerless'),(252,'Perana'),(253,'Peugeot'),(254,'PGO'),(255,'Phoenix Marine'),(256,'Piaggio'),(257,'Pierce-Arrow'),(258,'Plymouth'),(259,'Pontiac'),(260,'Porsche'),(261,'Prombron'),(262,'PT'),(263,'Puch'),(264,'Qvale'),(265,'R-C-H'),(266,'Rambler'),(267,'Range Rover'),(268,'Ranger'),(269,'Rapier'),(270,'Reliant'),(271,'Renault'),(272,'Reo'),(273,'Reo Flying Cloud'),(274,'Replica'),(275,'Riley'),(276,'Rio'),(277,'Riva'),(278,'Robinson'),(279,'Roller'),(280,'Rolls-Royce'),(281,'Rossion'),(282,'Rossion Automotive'),(283,'Rover'),(284,'Royal'),(285,'RUF'),(286,'Rush'),(287,'Saab'),(288,'Saleen'),(289,'Salmson'),(290,'Savage Rivale'),(291,'Saxon'),(292,'Scanner'),(293,'Scarab'),(294,'Scout'),(295,'Sea Ray'),(296,'Shadow Hawk Vehicles'),(297,'Shayton'),(298,'Shelby'),(299,'Simca'),(300,'Smart'),(301,'Spartan'),(302,'Spyker'),(303,'SsangYong'),(304,'SSC'),(305,'Stanley'),(306,'Star'),(307,'Studebaker'),(308,'Stutz'),(309,'Subaru'),(310,'Sunbeam'),(311,'Superformance'),(312,'Suzuki'),(313,'Talbot'),(314,'Talbot Lago'),(315,'Tatra'),(316,'Tesla'),(317,'Thomas Flyer'),(318,'Tiffany'),(319,'Tirrito'),(320,'Toyota'),(321,'Trabant'),(322,'Tramontana'),(323,'Trident'),(324,'Triumph'),(325,'Trojan'),(326,'Trumbull'),(327,'Tucker'),(328,'TVR'),(329,'Ultima'),(330,'Ultra'),(331,'UM'),(332,'Unknown'),(333,'US Military'),(334,'Valkry Empyrean'),(335,'Vanden Plas'),(336,'Vauxhall'),(337,'Vector'),(338,'Velorex'),(339,'Venom SS'),(340,'Vespa'),(341,'Volga'),(342,'Volvo'),(343,'VW'),(344,'Walker'),(345,'Wanderer'),(346,'Wartburg'),(347,'Watson'),(348,'Wellcraft'),(349,'Westfield'),(350,'Wiesmann'),(351,'Willys'),(352,'Winnebago'),(353,'Wolseley'),(354,'Wolverine'),(355,'X'),(356,'Yamaha'),(357,'Yes'),(358,'Yes!'),(359,'ZIL'),(360,'Zimmer'),(361,'ZIS');
/*!40000 ALTER TABLE `car_makes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_models`
--

DROP TABLE IF EXISTS `car_models`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_models` (
  `ID` int(10) NOT NULL auto_increment,
  `MODEL` text,
  `MAKE_ID` int(10) default NULL,
  `SPARE` text,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_models`
--

LOCK TABLES `car_models` WRITE;
/*!40000 ALTER TABLE `car_models` DISABLE KEYS */;
INSERT INTO `car_models` VALUES (69,'A1',28,NULL),(68,'DB11',25,NULL),(67,'DB10',25,NULL),(66,'DB9',25,NULL),(65,'DB7',25,NULL),(64,'DB6',25,NULL),(63,'DB5',25,NULL),(62,'DB4',25,NULL),(61,'DB2',25,NULL),(60,'DB',25,NULL),(70,'A2',28,NULL),(71,'A3',28,NULL),(72,'A4',28,NULL),(73,'A5',28,NULL),(74,'A6',28,NULL),(75,'A7',28,NULL),(76,'A8',28,NULL),(77,'TT',28,NULL),(78,'V',28,NULL),(79,'S2',28,NULL),(80,'S3',28,NULL),(81,'S4',28,NULL),(82,'S5',28,NULL),(83,'S6',28,NULL),(84,'S7',28,NULL),(85,'S8',28,NULL),(86,'Q5',28,NULL),(87,'Q7',28,NULL),(88,'R8',28,NULL),(89,'RS2',28,NULL),(90,'RS6',28,NULL),(91,'RS8',28,NULL),(92,'ALL',28,NULL),(93,'Allroad',28,NULL),(94,'Cabriolet',28,NULL),(95,'Coupe',28,NULL),(96,'V8',28,NULL),(97,'Veyron',187,NULL),(98,'Veyron Grand Sport',187,NULL),(99,'Veyron Roadster',187,NULL),(100,'Veyron Super Sport',187,NULL),(101,'Arnage',41,NULL),(102,'Arnage Final Series',41,NULL),(103,'Azure',41,NULL),(104,'Azure T',41,NULL),(105,'Brooklands',41,NULL),(106,'Continental',41,NULL),(107,'Continental Flying Spur',41,NULL),(108,'Continental Flying Spur Speed',41,NULL),(109,'Continental GT',41,NULL),(110,'Continental GT Speed',41,NULL),(111,'Continental GTC',41,NULL),(112,'Continental Super Sports Eight',41,NULL),(113,'GTZ',41,NULL),(114,'Eight',41,NULL),(115,'Mark VI',41,NULL),(116,'Mulsanne',41,NULL),(117,'R Type',41,NULL),(118,'S1',41,NULL),(119,'S2',41,NULL),(120,'S3',41,NULL),(121,'T1',41,NULL),(122,'T2',41,NULL),(123,'Turbo R',41,NULL),(124,'Turbo RT',41,NULL),(125,'Turbo S',41,NULL),(126,'M1',50,NULL),(127,'M3',50,NULL),(128,'M5',50,NULL),(129,'M6',50,NULL),(130,'X3',50,NULL),(131,'X5',50,NULL),(132,'X6',50,NULL),(133,'Z1',50,NULL),(134,'Z3',50,NULL),(135,'Z3',50,NULL);
/*!40000 ALTER TABLE `car_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_types`
--

DROP TABLE IF EXISTS `car_types`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_types` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_types`
--

LOCK TABLES `car_types` WRITE;
/*!40000 ALTER TABLE `car_types` DISABLE KEYS */;
INSERT INTO `car_types` VALUES (1,'Convertible'),(2,'Coupe'),(3,'Limousine'),(4,'Sedan'),(5,'Suv'),(6,'Station/Estate wagon'),(7,'Green/Electric'),(9,'Other');
/*!40000 ALTER TABLE `car_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_vat`
--

DROP TABLE IF EXISTS `car_vat`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `car_vat` (
  `ID` int(2) NOT NULL auto_increment,
  `TYPE` tinytext,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `car_vat`
--

LOCK TABLES `car_vat` WRITE;
/*!40000 ALTER TABLE `car_vat` DISABLE KEYS */;
INSERT INTO `car_vat` VALUES (1,'VAT Included'),(2,'VAT Excluded'),(3,'VAT Exempt'),(4,'VAT Paid'),(5,'VAT Unpaid'),(6,'No Duty Paid'),(7,'US Duty Paid'),(8,'EU Duty Paid');
/*!40000 ALTER TABLE `car_vat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `countries` (
  `ID` int(4) NOT NULL auto_increment,
  `COUNTRY` text,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan'),(2,'Aland Islands'),(3,'Albania'),(4,'Algeria'),(5,'American Samoa'),(6,'Andorra'),(7,'Angola'),(8,'Anguilla'),(9,'Antarctica'),(10,'Antigua and Barbuda'),(11,'Argentina'),(12,'Armenia'),(13,'Aruba'),(14,'Australia'),(15,'Austria'),(16,'Azerbaijan'),(17,'Bahrain'),(18,'Bangladesh'),(19,'Barbados'),(20,'Belarus'),(21,'Belgium'),(22,'Belize'),(23,'Benin'),(24,'Bermuda'),(25,'Bhutan'),(26,'Bolivia'),(27,'Bosnia and Herzegovina'),(28,'Botswana'),(29,'Bouvet Island'),(30,'Brazil'),(31,'British Indian Ocean Territory'),(32,'British Virgin Islands'),(33,'Brunei'),(34,'Bulgaria'),(35,'Burkina Faso'),(36,'Burundi'),(37,'Cambodia'),(38,'Cameroon'),(39,'Canada'),(40,'Cape Verde'),(41,'Cayman Islands'),(42,'Central African Republic'),(43,'Chad'),(44,'Chile'),(45,'China'),(46,'Christmas Island'),(47,'Cocos (Keeling) Islands'),(48,'Colombia'),(49,'Comoros'),(50,'Congo'),(51,'Cook Islands'),(52,'Costa Rica'),(53,'Croatia'),(54,'Cuba'),(55,'Cyprus'),(56,'Czech Republic'),(57,'Democratic Republic of Congo'),(58,'Denmark'),(59,'Disputed Territory'),(60,'Djibouti'),(61,'Dominica'),(62,'Dominican Republic'),(63,'East Timor'),(64,'Ecuador'),(65,'Egypt'),(66,'El Salvador'),(67,'Equatorial Guinea'),(68,'Eritrea'),(69,'Estonia'),(70,'Ethiopia'),(71,'Falkland Islands'),(72,'Faroe Islands'),(73,'Federated States of Micronesia'),(74,'Fiji'),(75,'Finland'),(76,'France'),(77,'French Guiana'),(78,'French Polynesia'),(79,'French Southern Territories'),(80,'Gabon'),(81,'Georgia'),(82,'Germany'),(83,'Ghana'),(84,'Gibraltar'),(85,'Greece'),(86,'Greenland'),(87,'Grenada'),(88,'Guadeloupe'),(89,'Guam'),(90,'Guatemala'),(91,'Guinea'),(92,'Guinea-Bissau'),(93,'Guyana'),(94,'Haiti'),(95,'Heard Island and McDonald Islands'),(96,'Honduras'),(97,'Hong Kong'),(98,'Hungary'),(99,'Iceland'),(100,'India'),(101,'Indonesia'),(102,'Iran'),(103,'Iraq'),(104,'Iraq-Saudi Arabia Neutral Zone'),(105,'Ireland'),(106,'Israel'),(107,'Italy'),(108,'Ivory Coast'),(109,'Jamaica'),(110,'Japan'),(111,'Jordan'),(112,'Kazakhstan'),(113,'Kenya'),(114,'Kiribati'),(115,'Kuwait'),(116,'Kyrgyzstan'),(117,'Laos'),(118,'Latvia'),(119,'Lebanon'),(120,'Lesotho'),(121,'Liberia'),(122,'Libya'),(123,'Liechtenstein'),(124,'Lithuania'),(125,'Luxembourg'),(126,'Macau'),(127,'Macedonia'),(128,'Madagascar'),(129,'Malawi'),(130,'Malaysia'),(131,'Maldives'),(132,'Mali'),(133,'Malta'),(134,'Marshall Islands'),(135,'Martinique'),(136,'Mauritania'),(137,'Mauritius'),(138,'Mayotte'),(139,'Mexico'),(140,'Moldova'),(141,'Monaco'),(142,'Mongolia'),(143,'Montenegro'),(144,'Montserrat'),(145,'Morocco'),(146,'Mozambique'),(147,'Myanmar'),(148,'Namibia'),(149,'Nauru'),(150,'Nepal'),(151,'Netherlands'),(152,'Netherlands Antilles'),(153,'New Caledonia'),(154,'New Zealand'),(155,'Nicaragua'),(156,'Niger'),(157,'Nigeria'),(158,'Niue'),(159,'Norfolk Island'),(160,'North Korea'),(161,'Northern Mariana Islands'),(162,'Norway'),(163,'Oman'),(164,'Pakistan'),(165,'Palau'),(166,'Palestinian Occupied Territories'),(167,'Panama'),(168,'Papua New Guinea'),(169,'Paraguay'),(170,'Peru'),(171,'Philippines'),(172,'Pitcairn Islands'),(173,'Poland'),(174,'Portugal'),(175,'Puerto Rico'),(176,'Qatar'),(177,'Reunion'),(178,'Romania'),(179,'Russia'),(180,'Rwanda'),(181,'Saint Kitts and Nevis'),(182,'Saint Pierre and Miquelon'),(183,'Saint Vincent and the Grenadines'),(184,'Saint-Barthelemy'),(185,'Saint-Martin'),(186,'Samoa'),(187,'San Marino'),(188,'Sao Tome and Principe'),(189,'Saudi Arabia'),(190,'Senegal'),(191,'Serbia'),(192,'Seychelles'),(193,'Sierra Leone'),(194,'Singapore'),(195,'Slovakia'),(196,'Slovenia'),(197,'Solomon Islands'),(198,'Somalia'),(199,'South Africa'),(200,'South Georgia and the South Sandwich Islands'),(201,'South Korea'),(202,'Spain'),(203,'Spratly Islands'),(204,'Sri Lanka'),(205,'St Helena Ascension and Tristan da Cunha'),(206,'St. Lucia'),(207,'Sudan'),(208,'Suriname'),(209,'Svalbard and Jan Mayen'),(210,'Swaziland'),(211,'Sweden'),(212,'Switzerland'),(213,'Syria'),(214,'Taiwan'),(215,'Tajikistan'),(216,'Tanzania'),(217,'Thailand'),(218,'The Bahamas'),(219,'The Gambia'),(220,'Togo'),(221,'Tokelau'),(222,'Tonga'),(223,'Trinidad and Tobago'),(224,'Tunisia'),(225,'Turkey'),(226,'Turkmenistan'),(227,'Turks and Caicos Islands'),(228,'Tuvalu'),(229,'US Virgin Islands'),(230,'Uganda'),(231,'Ukraine'),(232,'United Arab Emirates'),(233,'United Kingdom'),(234,'United Nations Neutral Zone'),(235,'United States'),(236,'United States Minor Outlying Islands'),(237,'Uruguay'),(238,'Uzbekistan'),(239,'Vanuatu'),(240,'Vatican City'),(241,'Venezuela'),(242,'Vietnam'),(243,'Wallis and Futuna'),(244,'Western Sahara'),(245,'Yemen'),(246,'Zambia'),(247,'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `index_page`
--

DROP TABLE IF EXISTS `index_page`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `index_page` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `TITLE` tinytext character set latin1,
  `KEYWORDS` tinytext character set latin1,
  `DESCRIPTION` tinytext character set latin1,
  `BODY` text character set latin1,
  `NAME` text character set latin1,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `index_page`
--

LOCK TABLES `index_page` WRITE;
/*!40000 ALTER TABLE `index_page` DISABLE KEYS */;
INSERT INTO `index_page` VALUES (1,'Knetiks CMS Demo','knetiks cms demo, content management ','Knetiks Content Management System Demo','<h1>Welcome to Klist demo from Knetiks Internet solutions.</h1>\r\n<p>&nbsp;</p>\r\n<p>Browse through our demo and get a feel for the simplicity of our system, feel free to add, delete or make changes to pages &amp; listings, this demo will refresh its database every hour to its default settings.  Enjoy!</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&bull; JAMES LIST ENABLED.</p>\r\n<p>&bull; AUTOMATED SITE MAP GENERATION &nbsp;</p>\r\n<p>&bull; SEO OPTIMISED</p>\r\n<p>&bull; SOCIAL NETWORKING &amp; SHARE LISTING FUNCTIONS</p>\r\n<p>&bull; SMART IMAGE DISPLAY.</p>\r\n<p>&bull; SMART &amp; SIMPLE&nbsp;MEDIA MANAGEMENT &nbsp;</p>\r\n<p>&bull; MODULAR PRODUCT MANAGEMENT</p>\r\n<p>&bull; HOME PAGE FEATURED LISTINGS SLIDESHOW.</p>\r\n<p>&bull; CUSTOM DESIGN &nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','name');
/*!40000 ALTER TABLE `index_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pages` (
  `ID` tinyint(3) NOT NULL auto_increment,
  `TITLE` text,
  `KEYWORDS` text,
  `DESCRIPTION` text,
  `BODY` longblob,
  `ListingID` tinyint(4) default NULL,
  `NAME` mediumtext,
  `LIVE` text,
  `ISCONTACT` tinytext,
  `ALLOWDELETE` tinyint(2) default '1',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'View Current Listings','','    ','<p>.</p>',2,'VIEW LISTINGS','Published','No',0),(2,'Contact Us','','','<p>To Contact us by telephone please call: 05767657865775</p>',3,'CONTACT','Published','Yes',1),(3,'About Us','about us','This is the about us page','<h1>About Us</h1>\r\n<p>&nbsp;</p>\r\n<p>This a example of a static html page.</p>\r\n<p>Images have been disabled for demo version.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>',1,'ABOUT US','Published','No',1);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `settings` (
  `ID` tinyint(2) NOT NULL auto_increment,
  `URL` tinytext character set latin1,
  `SITE_NAME` tinytext character set latin1,
  `SITE_EMAIL` tinytext character set latin1,
  `TEMPLATE` text character set latin1,
  `SIDEBOX` tinyint(4) default NULL,
  `BASEURL` text,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'http://demos.knetiks.co.uk/klist/admin/','Knetiks Klist Demo','demo@knetiks.co.uk','zero_id',0,'http://demos.knetiks.co.uk/klist/');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `member_id` smallint(11) NOT NULL auto_increment,
  `firstname` text character set latin1,
  `lastname` text character set latin1,
  `login` text character set latin1,
  `passwd` tinytext character set latin1,
  `isadmin` tinyint(4) default '0',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Peter','Humphrey','peterbhumphrey@gmail.com','3b66d7e4bab6ad3b84e1a23cd8f0c434',1),(2,'kriptik','one','kriptik','94d2910e5d0f45410501b3ce2864b8e5',0),(3,'Demo','Demo','demo@knetiks.co.uk','fe01ce2a7fbac8fafaed7c982a04e229',0),(4,'Tim','Maynard','kreata@kriptik.co.uk','7bcb9fa32475ca822fe3d54aad4c5746',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cardemo'
--

