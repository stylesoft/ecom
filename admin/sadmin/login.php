<?php
require_once '../../bootstrap.php';

//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
    $str = @trim($str);
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}

if ($_POST) {
    //Sanitize the POST values
    $login = clean($_POST['login']);
    $password = clean($_POST['password']);

    //Input Validations
    if ($login == '') {
        $errmsg_arr[] = 'Login ID missing';
        $errflag = true;
    }
    if ($password == '') {
        $errmsg_arr[] = 'Password missing';
        $errflag = true;
    }

    //If there are input validations, redirect back to the login form
    if ($errflag) {
        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
        session_write_close();
        header("location: login.php");
        exit();
    } else {
        
        
        
        $objUser  = new User();
        $userInfo = $objUser->loginUser($login, $password,'2');
        if($userInfo){
                        session_regenerate_id();
			$_SESSION['SUPER_ADMIN_SESS_MEMBER_ID'] = $userInfo->id;
			$_SESSION['SUPER_ADMIN_SESS_FIRST_NAME'] = $userInfo->firstName;
			$_SESSION['SUPER_ADMIN_SESS_LAST_NAME'] = $userInfo->lastName;
			$_SESSION['SUPER_ADMIN_SESS_IS_ADMIN'] = $userInfo->isAdmin;
			session_write_close();
			header("location: index.php");
        } else {
                        //Login failed
			header("location: login.php?ref=failed");
			exit();
        }
        
    }
}

$LAYOUT = SUPER_ADMIN_LAYOUT_PATH."tpl/login_layout.tpl";

require_once $LAYOUT;
?>
