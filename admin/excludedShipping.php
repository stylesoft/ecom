<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');
require_once('modules/payment/includes/PaymentSettings.class.php');

$id = '';
$country_id = '';
//$payment_id = '';
$shipping_id = '';
//$status = '';


// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrpaymentMethod= array();

$objexcluded = new ExcludedShipping();
$objexcluded->tb_name = 'tbl_shipping_excluded';

$objexcluded1 = new ExcludedShipping();
$objexcluded1->tb_name = 'tbl_shipping_excluded';






$objPayment = new PaymentSettings();
$paymentNames = $objPayment->getActivePaymentOptions();




//$objCountry = new Country();
//$countryList = $objCountry->getAll();


$objshipping = new CarrierType();
$shippingList = $objshipping->getAllByStatus('Enabled');


/*

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['paymentid'])) { $paymentidId = $_GET['paymentid'];}
                //$arrSubcribe = $objSubcribe->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {
    	    $objexcluded->payment_id = $paymentidId;
            $excludedInfo = $objexcluded->search();
            
            foreach ($excludedInfo as $Dindex=>$excludeDelete) {
      
                         
            $excludeDelete->id = $id ;
            $objexcluded->deleteExcludedPaymentAndShipping();
            
            
            //$country_id = $excludedInfo->country_id;
            //$payment_id = $excludedInfo->payment_id;
            //$objexcluded1->payment_id = $payment_id;
            //$excludedCountryInfo = $objexcluded1->search();
            
            
            //$shipping_id = $excludedInfo->shipping_id;
            //$status = $excludedInfo->status;
            
            }
        
    }
    
    if($action == 'add'){
        
       // $homeBannerArray = $objHomeBanner->getAllByType();
    }
}
*/
if ($_POST) {
//print_r($_POST); exit;
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    $country_id = $_POST['country_id'];
    //$payment_id = $_POST['payment_id'];
    $shipping_id = $_POST['shipping_id'];
    //$status = $_POST['status'];
    
    
    //print_r($country_id); exit;
   

    //validation
    if($shipping_id == ""){
            array_push($objexcluded->error, 'The status is required');
            $invalid = true;
        }
       
        $objexcluded1->shipping_id = $shipping_id;
        $objexcluded1->deleteExcludedShipping();
        
       foreach ($country_id as $index=>$value) {
       	
       
    $objexcluded->id = $id;
    $objexcluded->country_id = $value;
        
    $objexcluded->shipping_id = $shipping_id;
    //$objexcluded->deleteExcludedPaymentAndShipping();
   //exit;
    
   
   
    if ($action == 'add' && !$invalid) {
    	
          $lastInsertedId = $objexcluded->addExcludedShipping();
    }
    elseif ($action == 'edit' && !$invalid) {
       $isRecordUpdated = $objexcluded->addExcludedShipping();
        //exit;
    }
       }
      
    
    $newPageUrl = "excludedShipping.html?action=add";
    $mainPageUrl = "excludedShipping.html";

    if ($lastInsertedId) {
    	
    	
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Excluded From <span class='red'>E-commerce</span><br /><br /><br />Go to Exclude Shipping Methods<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
          
        
    } else if ($isRecordUpdated) {
        
       
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Excluded From <span class='red'>E-commerce</span> has been updated!<br /><br /><br />Want to Exclude more?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
       
    }
} 




$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH . "tpl/excludedshipping/form.tpl";



require_once $LAYOUT;
?>
