<?php
/*
   $uid = getmyuid();
   $a = posix_getpwuid($uid);
   $home = $a['name'];
   $curr_dir = basename(dirname(__FILE__)); 
   $ssl_url = "https://secure-www.pluspro.com/~" . $home . "/" . $curr_dir . "/";
   if($_SERVER["HTTPS"] != "on") {
   header("HTTP/1.1 301 Moved Permanently");
   header("Location:  $ssl_url ");
   exit();
}
*/

require_once '../bootstrap.php';
//print_r($_SERVER);
//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
    $str = @trim($str);
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}

if ($_POST) {
    //Sanitize the POST values
    $login = clean($_POST['login']);
    $password = clean($_POST['password']);

    //Input Validations
    if ($login == '') {
        $errmsg_arr[] = 'Login ID missing';
        $errflag = true;
    }
    if ($password == '') {
        $errmsg_arr[] = 'Password missing';
        $errflag = true;
    }

    //If there are input validations, redirect back to the login form
    if ($errflag) {
        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
        session_write_close();
        header("location: login.php");
        exit();
    } else {
        
        
        
        $objUser  = new User();
        $userInfo = $objUser->loginUser($login, $password);
        if($userInfo){
                        session_regenerate_id();
			$_SESSION['SESS_MEMBER_ID'] = $userInfo->id;
			$_SESSION['SESS_FIRST_NAME'] = $userInfo->firstName;
			$_SESSION['SESS_LAST_NAME'] = $userInfo->lastName;
			$_SESSION['SESS_IS_ADMIN'] = $userInfo->isAdmin;
			session_write_close();
			header("location: index.php");
        } else {
                        //Login failed
			header("location: login.php?ref=failed");
			exit();
        }
        
    }
}

$LAYOUT = ADMIN_LAYOUT_PATH . "tpl/login_layout.tpl";

require_once $LAYOUT;
?>
