<?php
	set_include_path(get_include_path().PATH_SEPARATOR.LIBRARY_PATH);
	function autoload($class) {
		$class = str_replace('_', '/', $class);
		include $class.".php";
	}
	spl_autoload_register('autoload');
	
?>