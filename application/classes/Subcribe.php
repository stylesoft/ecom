<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Subcribe extends Core_Database {

    //news propoerties
    public $id;
    public $email;
    public $type;
    public $error = array();
    public $data_array = array();

    //constructor
    public function Subcribe() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCategory
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addSubcribe() {
        $recordId = null;
        try {
                $id = $this->id;
                $email = $this->email;
                $type = $this->type;
                $inserted = $this->insert($this->tb_name, array($id,$email,$type));
                if ($inserted) {
                	
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editSubcribe() {
        $isUpdated = false;
        try {
                $id = $this->id;
                $email = $this->email;
                $type = $this->type;
                $arrayData = array(
                    'id' => $id,
                    'email' => $email,
                    'type'  => $type
                )
                ;
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteSubcribe() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getSubcribe($subId) {
        $objSubcribe = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $subId;
                $this->select('tbl_subscribers', $colums, $where);
                $subcribeInfo = $this->getResult();
                $objSubcribe->id = $subcribeInfo['id'];
                $objSubcribe->email = $subcribeInfo['email'];
                $objSubcribe->type = $subcribeInfo['type'];
            }
            return $objSubcribe;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
      * Added By      :   Rasvi
      * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrSubcribe = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'id';
                $this->select($this->tbl_subscribers, $colums, $where);
                $subribeResult = $this->getResult();
                foreach ($subribeResult As $subRow) {
                    $subId = $subRow['id'];
                    $subInfo = $this->getSubcribe($subId);
                    array_push($arrSubcribe, $subInfo);
                }
            }

            return $arrSubcribe;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

 
    
    

    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Added By      :   Rasvi
     * Added On      :   05-04-2013
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                                                             
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
	
	
	
   /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "email LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllBySubcribeObject($param = '') {
        $arrSubcribe = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "'";
            $orderBy = "email ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $subRes = $this->getResult();

            foreach ($subRes As $nIndex => $subRow) {
                $subId = $subRow['id'];
                $subInfo = $this->getSubcribe($subId);
                array_push($arrSubcribe, $subInfo);
            }

            return $arrSubcribe;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
 

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Rasvi
     * Added On      :   05-04-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrSubcribe = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "email LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);
            
            if ($this->listingOrder) {
            	$SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
            	$SQL.= $this->limit;
            }
     
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $subRes = $this->getResult();
            foreach ($subRes As $subRow) {
                $id = $subRow['id'];
                $subcribeInfo = $this->getSubcribe($id);
                array_push($arrSubcribe,  $subcribeInfo);
            }
            return $arrSubcribe;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }


    /**'
     * @name         :   getUserMailList
     * @param        :   ''
     * Description   :   The function is to get cusotmer email list
     * @return       :   Customer Email Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   30-12-2013
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getUserMailList(){
       $mail_list = array();
        try {
                $colums = 'email';
                $this->select('tbl_subscribers', $colums);
                $mail_list = $this->getResult();
            return $mail_list;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
   


}
?>
