<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class Address extends Core_Database {

    //news propoerties
    public $id;
    public $address;
    public $region;
    public $state;
    public $city;
    public $postcode;
    public $country;
    public $membersId;
    public $addressType;
    public $addressObjectType;
    public $houseNumber;
    public $lastName;
    public $title;
    public $firstName;
    public $phone;
    public $error = array();
    public $data_array = array();

    //constructor

    public function Address() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addAddress() {
        $recordId = null;
        try {
            $id = $this->id;
            $address = $this->address;
            $region = $this->region;
            $state = $this->state;
            $city = $this->city;
            $postcode = $this->postcode;
            $country = $this->country;
            $membersId = $this->membersId;
            $addressType = $this->addressType;
            $addressObjectType = $this->addressObjectType;
            $houseNumber = $this->houseNumber;
            $lastName = $this->lastName;
            $title = $this->title;
            $firstName = $this->firstName;
            $phone = $this->phone;

     
            $inserted = $this->insert('tbl_address', array($id, $address, $region, $state, $city, $postcode, $country, $membersId, $addressType, $addressObjectType, $houseNumber, $lastName, $title,$firstName,$phone));
            if ($inserted) {
                $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function updateAddress() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $address = $this->address;
            $region = $this->region;
            $state = $this->state;
            $city = $this->city;
            $postcode = $this->postcode;
            $country = $this->country;
            $membersId = $this->membersId;
            $addressType = $this->addressType;
            $addressObjectType = $this->addressObjectType;
            $houseNumber = $this->houseNumber;
            $lastName = $this->lastName;
            $title = $this->title;
            $firstName = $this->firstName;
            $phone = $this->phone;

            $arrayData = array('address' => $address, 'region' => $region, 'state' => $state, 'city' => $city, 'postcode' => $postcode, 'country' => $country, 'tbl_members_id' => $membersId, 'address_type' => $addressType, 'address_object_type' => $addressObjectType, 'house_number' => $houseNumber, 'last_name' => $lastName, 'title' => $title, 'first_name' => $firstName,'phone'=>$phone);
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");

            $isUpdated = $this->update('tbl_address', $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            
        }

        throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    }

    public function deleteAddress() {
        $isDeleted = false;
        try {
            $id = $this->id;
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete('tbl_address', $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAddress($addressId) {
        $objAddress = new stdClass();
        
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $addressId;
                $this->select('tbl_address', $colums, $where);
                $addressInfo = $this->getResult();
               // print(count($addressInfo));
//if(count($customerInfo) == 1){
                $objAddress->id = $addressInfo['id'];
                $objAddress->address = $addressInfo['address'];
                $objAddress->region = $addressInfo['region'];
                $objAddress->state = $addressInfo['state'];
                $objAddress->city = $addressInfo['city'];
                $objAddress->postcode = $addressInfo['postcode'];
                $objAddress->country = $addressInfo['country'];
                $objAddress->membersId = $addressInfo['tbl_members_id'];
                $objAddress->addressType = $addressInfo['address_type'];
                $objAddress->addressObjectType = $addressInfo['address_object_type'];
                $objAddress->houseNumber = $addressInfo['house_number'];
                $objAddress->lastName = $addressInfo['last_name'];
                $objAddress->title = $addressInfo['title'];
                $objAddress->firstName = $addressInfo['first_name'];
                $objAddress->phone = $addressInfo['phone'];
            }
            return $objAddress;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAllByMember($memberId, $addressType) {
        $arrAddress = array();
        try {
            $SQL = "SELECT * FROM tbl_address WHERE tbl_members_id = '" . $memberId . "' AND address_type = '" . $addressType . "'";
            
            
            $dbResult = $this->executeSelectQuery($SQL);
            $addressRes = $this->getResult();
            foreach ($addressRes As $addressRow) {
                $addressId = $addressRow['id'];
                $addressInfo = $this->getAddress($addressId);
                array_push($arrAddress, $addressInfo);
            }
            return $arrAddress;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>