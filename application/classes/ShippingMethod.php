<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ShippingMethod extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $cost;
    public $isDefault;
    public $isEnabled;
    public $error = array();
    public $data_array = array();

    //constructor
    public function ShippingMethod() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addShippingMethod
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addShippingMethod() {
        $recordId = null;
        try {

            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editShippingMethod
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editShippingMethod() {
        $isUpdated = false;
        try {

            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteShippingMethod
     * @param        :   CurrencyObject
     * Description   :   The function is to delete Currency details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteShippingMethod() {
        $isDeleted = false;
        try {

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getShippingMethod
     * @param        :   Integer (Currency ID)
     * Description   :   The function is to get a Shipping Method details
     * @return       :   Currency Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getShippingMethod($shippingMethodId) {
        $objShippingMethod = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $shippingMethodId;
                $this->select('tbl_shipping_method', $colums, $where);
                $shippingMethodInfo = $this->getResult();
                if ($shippingMethodInfo) {
                    $objShippingMethod->id = $shippingMethodInfo['id'];
                    $objShippingMethod->name = $shippingMethodInfo['name'];
                    $objShippingMethod->cost = $shippingMethodInfo['cost'];
                    $objShippingMethod->isDefault = $shippingMethodInfo['is_default'];
                    $objShippingMethod->isEnabled = $shippingMethodInfo['is_enabled'];
                } else {
                    $objShippingMethod->id = null;
                    $objShippingMethod->name = null;
                    $objShippingMethod->cost = null;
                    $objShippingMethod->isDefault = null;
                    $objShippingMethod->isEnabled = null;
                }
            }
            return $objShippingMethod;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAll() {
        $arrShippingMethod = array();
        try {

            $SQL = "SELECT * FROM tbl_shipping_method";
            $dbResult = $this->executeSelectQuery($SQL);
            $shippingMethodResult = $this->getResult();
            //print_r($orderResult); exit;
            foreach ($shippingMethodResult As $dataRow) {
                $shippingMethodId = $dataRow['id'];
                $shippingMethodInfo = $this->getShippingMethod($shippingMethodId);
                array_push($arrShippingMethod, $shippingMethodInfo);
            }
            return $arrShippingMethod;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAllShippingMethod() {
        $arrShippingMethod = array();
        try {

            $SQL = "SELECT * FROM tbl_shipping_method WHERE is_enabled = 'Yes'";
            $dbResult = $this->executeSelectQuery($SQL);
            $shippingMethodResult = $this->getResult();
            //print_r($orderResult); exit;
            foreach ($shippingMethodResult As $dataRow) {
                $shippingMethodId = $dataRow['id'];
                $shippingMethodInfo = $this->getShippingMethod($shippingMethodId);
                array_push($arrShippingMethod, $shippingMethodInfo);
            }
            return $arrShippingMethod;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getDefaultShippingMethod() {
        $objShippingMethod = new stdClass();
        try {

            $SQL = "SELECT * FROM tbl_shipping_method WHERE is_default = 'Yes'";
            $dbResult = $this->executeSelectQuery($SQL);
            $shippingMethodResult = $this->getResult();
            if ($shippingMethodResult) {
                foreach ($shippingMethodResult As $dataRow) {
                    $shippingMethodId = $dataRow['id'];
                    $objShippingMethod = $this->getShippingMethod($shippingMethodId);
                }
            } else {
                return null;
            }
            return $objShippingMethod;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>