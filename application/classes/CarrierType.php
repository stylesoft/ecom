<?php
/**
 * @name        CarrierTypes
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class CarrierType extends Core_Database{

	/**'
	 * @name         :   addCarrierType
	 * @param        :   CarrierTypesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addCarrierType($objCarrierType){
		$recordId = null;
		try{
			if($this->connect()){
				$id 			= $objCarrierType->id;
				$carrierType 		= $objCarrierType->carrierType;
				$delay 			= $objCarrierType->delay;
				$status 		= $objCarrierType->status;
				$isFreeShipping 	= $objCarrierType->isFreeShipping;
				
				$inserted = $this->insert('tbl_carrier_types',array($id,$carrierType,$delay,$status,$isFreeShipping)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>addCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editCarrierTypes
	 * @param        :   CarrierTypesObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editCarrierType($objCarrierType){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 			= $objCarrierType->id;
				$carrierType 		= $objCarrierType->carrierType;
				$delay 			= $objCarrierType->delay;
				$status 		= $objCarrierType->status;
				$isFreeShipping 	= $objCarrierType->isFreeShipping;
                                
				$arrayData          = array('carrier_type'=>$carrierType,'delay'=>$delay,'status'=>$status,'is_free_shipping'=>$isFreeShipping);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
				$isUpdated = $this->update('tbl_carrier_types',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>addCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteCarrierTypes
	 * @param        :   CarrierTypesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteCarrierTypes(){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id = $this->id;
				//$id 				= $objCarrierType->id;
				$arrWhere  = array("id = '" . $id . "'");
				$isDeleted = $this->delete('tbl_carrier_types',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>addCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getCarrierTypes
	 * @param        :   Integer (CarrierTypes ID)
	 * Description   :   The function is to get a page details
	 * @return       :   CarrierTypes Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getCarrierType($id){
		$objCarrierType = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'id = '.$id;
				$this->select('tbl_carrier_types',$colums,$where);
				$carrierInfo = $this->getResult();
                                if(count($carrierInfo)>0){
				$objCarrierType->id = $carrierInfo['id'];
                                $objCarrierType->carrierType = $carrierInfo['carrier_type'];
                                $objCarrierType->delay = $carrierInfo['delay'];
                                $objCarrierType->status = $carrierInfo['status'];
                                $objCarrierType->isFreeShipping = $carrierInfo['is_free_shipping'];
                                }
			}
			return $objCarrierType;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>getCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of CarrierTypes Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrCarrierTypes       = array();
		try{
			if($this->connect()){
				$colums = 'id';
				$where  = "";
                                $orderBy = " id Asc";
				$this->select('tbl_carrier_types',$colums,$where,$orderBy);
				$carrierRes = $this->getResult();
				foreach($carrierRes As $carrierRow){
					$carrierId = $carrierRow['id'];
					$carrierInfo = $this->getCarrierType($carrierId);
					array_push($arrCarrierTypes,$carrierInfo);
				}
					
			}
			return $arrCarrierTypes;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>getCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        
	 
	
	/**'
	 * @name         :   getAllByStatus
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of CarrierTypes Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByStatus($status){
		$arrCarrierTypes       = array();
		try{
			if($this->connect()){
				$colums = 'id';
				$where  = "status = '".$status."'";
                                $orderBy = " id Asc";
				$this->select('tbl_carrier_types',$colums,$where,$orderBy);
				$carrierRes = $this->getResult();
					
				foreach($carrierRes As $pIndex=>$carrierRow){
					$carrierId = $carrierRow['id'];
					$carrierInfo = $this->getCarrierType($carrierId);
					array_push($arrCarrierTypes,$carrierInfo);
				}
					
			}
			return $arrCarrierTypes;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierTypes</em>, <strong>Function -</strong> <em>getCarrierTypes()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	
	/** '
	 * @name         :   count
	 * @param        :   CarrierType
	 * @desc         :   The function is to search  CarrierType details by name
	 * @return       :   Array (Array Of CarrierType Object)
	 * Added By      :   Rasvi
	 * Added On      :   22-01-2014
	 * Modified By   :   -
	 * Modified On   :   -
	 */
	
	public function countRec() {
		$totalNumberOfRec = 0;
		$arrWhere = array();
		try {
			$SQL = "SELECT * FROM tbl_carrier_types ";
			if ($this->searchStr != '') {
				array_push($arrWhere, "carrier_type LIKE '" . "%" . $this->searchStr . "%" . "'");
			}
	
			if (count($arrWhere) > 0)
				$SQL.= "WHERE " . implode(' AND ', $arrWhere);
	
	
			$dbResult = $this->executeSelectQuery($SQL);
			$catRes = $this->getResult();
			$totalNumberOfRec = count($catRes);
			return $totalNumberOfRec;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}
	
	/** '
	 * @name         :   search
	 * @param        :   CarrierType
	 * @desc         :   The function is to search  CarrierType details by name
	 * @return       :   Array (Array Of CarrierType Object)
	 * Added By      :   Rasvi
	 * Added On      :   22-01-2014
	 * Modified By   :   -
	 * Modified On   :   -
	 */
	
	public function search() {
		$arrCarrierTypes  = array();
		$arrWhere = array();
		try {
			$SQL = "SELECT * FROM tbl_carrier_types ";
			if ($this->searchStr != '') {
				array_push($arrWhere, "carrier_type LIKE '" . "%" . $this->searchStr . "%" . "'");
			}
	
			if (count($arrWhere) > 0)
				$SQL.= "WHERE " . implode(' AND ', $arrWhere);
	
			if ($this->listingOrder) {
				$SQL.= ' ORDER BY ' . $this->listingOrder;
			}
	
			if ($this->limit) {
				$SQL.= $this->limit;
			}
			 
			//echo $SQL;
			$dbResult = $this->executeSelectQuery($SQL);
			$subRes = $this->getResult();
			foreach ($subRes As $subRow) {
				$id = $subRow['id'];
				$subcribeInfo = $this->getCarrierType($id);
				array_push($arrCarrierTypes,  $subcribeInfo);
			}
			return $arrCarrierTypes;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}
        
	 
}
?>