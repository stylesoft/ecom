<?php

/*
 * Classname :  HomeBanner
 * 
 */

class HomeBanner extends Core_Database{
    
  public $recordID;
  public $recordText;
  public $recordListingID;
  public $bannerType; //types : 'SliderBanners', 'HomeBanners', 'Downloads'
  public $banner_link;
  public $banner_title;
  public $banner_description;
  
      
  public $error = array();
  public $data_array = array();
   //constructor
    public function HomeBanner() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /** '
     * @name         :   addHomeBanner
     * @param        :   HomeBanner Object
     * @desc   :   The function is to Home Banner details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addHomeBanner() {
        $recordId = null;
        try {
                $recordID = $this->recordID;
                $recordText = $this->recordText;
                $recordListingID = $this->recordListingID;
                $bannerType = $this->bannerType;
                $banner_link = $this->banner_link;
                $banner_title = $this->banner_title;
                $banner_description = $this->banner_description;

                $inserted = $this->insert($this->tb_name, array($recordID, $recordText, $recordListingID, $bannerType, $banner_link,$banner_title,$banner_description));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editHomeBanner
     * @param        :   Home Banner Object
     * @desc         :   The function is to edit a Home Banner details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editHomeBanner() {
        $isUpdated = false;
        try {

                $recordID = $this->recordID;
                $recordText = $this->recordText;
                $recordListingID = $this->recordListingID;
                $bannerType = $this->bannerType;
                $banner_link = $this->banner_link;
                $banner_title = $this->banner_title;
                $banner_description = $this->banner_description;

                $arrayData = array(
                   // 'recordID' => $recordID,
                    'recordText' => $recordText,
                    'recordListingID' => $recordListingID,
                    'bannerType' => $bannerType,
                    'banner_link' => $banner_link,
                    'banner_title' => $banner_title,
                    'banner_description' => $banner_description
                );
           
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("recordID = '" . $recordID . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
     /** '
     * @name         :   updateBannerListingOrder
     * @param        :   Home Banner Object
     * @desc         :   update home banner listing order so to change how they appears in the front end
     * @return       :   boolean
     * Request File  :   updateDB.php
     * Added By      :   Gayan Chathuranga
     * Added On      :   05-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function updateBannerListingOrder() {
        $isUpdated = false;
        try {
                $recordID = $this->recordID;
                $recordListingID = $this->recordListingID;
                $arrayData = array(
                    'recordListingID' => $recordListingID
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("recordID = '" . $recordID . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    

    /** '
     * @name         :   deleteHomeBanner
     * @param        :   Home Banner Object
     * @desc   :   The function is to delete Home Banner details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteHomeBanner() {
        $isDeleted = false;
        try {

                $id = $this->recordID;
                $arrWhere = array("recordID = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
                
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * @desc   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getHomeBanner($recordID) {
        $objHmBanner = new stdClass();
        try {

                $colums = '*';
                $where = 'recordID = ' . $recordID;
                $this->select($this->tb_name, $colums, $where);
                $homeBannerInfo = $this->getResult();

                $objHmBanner->recordID = $homeBannerInfo['recordID'];
                $objHmBanner->recordText = $homeBannerInfo['recordText'];
                $objHmBanner->recordListingID = $homeBannerInfo['recordListingID'];
                $objHmBanner->bannerType = $homeBannerInfo['bannerType'];
                $objHmBanner->banner_link = $homeBannerInfo['banner_link'];
                $objHmBanner->banner_title = $homeBannerInfo['banner_title'];
                $objHmBanner->banner_description = $homeBannerInfo['banner_description'];

            return $objHmBanner;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all Home Banner details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrhmBanner = array();
        try {

                $colums = 'id';
                $where = '';
                $orderBy = "recordID ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $hmbannerRslt = $this->getResult();
                foreach ($hmbannerRslt As $bannerRow) {
                    $recordID = $bannerRow['recordID'];
                    $hmBannerInfo = $this->getHomeBanner($recordID);
                    array_push($arrhmBanner, $hmBannerInfo);
                }


            return $arrhmBanner;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllByType
     * @param        :
     * @desc   :   The function is to get all home banners by banner types=> 'SliderBanners', 'HomeBanners', 'Downloads'
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByType() {
        $arrhmBanner = array();
        try {

                $colums = '*';
                $where = "bannerType = '" . $this->bannerType . "'";
                $orderBy = "recordID ASC";
                $this->selectRow($this->tb_name, $colums, $where, $orderBy);
                $hmBnnerRes = $this->getResult();
                
                
               // (sizeof($Res) == 6) ? $hmBnnerRes[0] = $Res : $hmBnnerRes = $Res;
                foreach ($hmBnnerRes As $Index => $bnrRow) {
                   
                    $recordID = $bnrRow['recordID'];
                    $bnrInfo = $this->getHomeBanner($recordID);
                    array_push($arrhmBanner, $bnrInfo);
                }

            return $arrhmBanner;
        } catch (Exception $e) { 
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
     /** '
     * @name         :   countRec
     * @param        :   HomeBanner
     * @desc         :   The function is to count the nnumer of active home banner contents
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "banner_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }
            
            if ($this->bannerType != '') {
                array_push($arrWhere, "bannerType = '".$this->bannerType. "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            //print($SQL);
            $dbResult = $this->executeSelectQuery($SQL);
            $newsRes = $this->getResult();
            $totalNumberOfRec = count($newsRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrBannerImg = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "banner_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }
            
            if ($this->bannerType != '') {
                array_push($arrWhere, "bannerType = '".$this->bannerType. "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $bannerRes = $this->getResult();
            foreach ($bannerRes As $bannerRow) {
                $recordID = $bannerRow['recordID'];
                $bannerInfo = $this->getHomeBanner($recordID);
                array_push($arrBannerImg, $bannerInfo);
            }
            return $arrBannerImg;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    public function imageGallerySearch() {
        $arrBannerImg = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "banner_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $bannerRes = $this->getResult();
            foreach ($bannerRes As $bannerRow) {
                $recordID = $bannerRow['recordID'];
                $bannerInfo = $this->getHomeBanner($recordID);
                array_push($arrBannerImg, $bannerInfo);
            }
            return $arrBannerImg;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>