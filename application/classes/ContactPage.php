<?php
/**
 * @name        ContactPage
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class ContactPage extends Core_Database{


        public $subContactPages;

	/**'
	 * @name         :   addContactPage
	 * @param        :   ContactPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addContactPage($objContactPage){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objContactPage->id;
				$title 				= $objContactPage->title;
				$keywords 			= $objContactPage->keywords;
				$description                    = $objContactPage->description;
				$body 				= $objContactPage->body;
				$listingId 			= $objContactPage->listingId;
				$name 				= $objContactPage->name;
				$live 				= $objContactPage->live;
				$isContact 			= $objContactPage->isContact;
				$allowToDelete 		= $objContactPage->allowToDelete;
				$inserted = $this->insert('tbl_contactpage',array($id,$title,$keywords,$description,$body,$listingId,$name,$live,$isContact,$allowToDelete)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>addContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editContactPage
	 * @param        :   ContactPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editContactPage($objContactPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objContactPage->id;
				$title 				= $objContactPage->title;
				$keywords 			= $objContactPage->keywords;
				$description 		= $objContactPage->description;
				$body 				= $objContactPage->body;
				$listingId 			= $objContactPage->listingId;
				$name 				= $objContactPage->name;
				$live 				= $objContactPage->live;
				$isContact 			= $objContactPage->isContact;
				$allowToDelete 		= $objContactPage->allowToDelete;
				$arrayData          = array('TITLE'=>$title,
										'KEYWORDS'=>$keywords,
										'DESCRIPTION'=>$description,
										'BODY'=>$body,
										'ListingID'=>$listingId,
										'NAME'=>$name,
										'LIVE'=>$live,
										'ISCONTACT'=>$isContact,
										'ALLOWDELETE'=>$allowToDelete);
				//$this->update('tbl_contactpage',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('tbl_contactpage',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>addContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteContactPage
	 * @param        :   ContactPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteContactPage($objContactPage){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objContactPage->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('tbl_contactpage',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>addContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getContactPage
	 * @param        :   Integer (ContactPage ID)
	 * Description   :   The function is to get a page details
	 * @return       :   ContactPage Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getContactPage($pageId){
                $objSubPage = new SubPage();
		$objContactPage = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$pageId;
				$this->select('tbl_contactpage',$colums,$where);
				$pageInfo = $this->getResult();
                                
                                if(count($pageInfo)>0){
				$objContactPage->id = $pageInfo['ID'];
				$objContactPage->title = $pageInfo['TITLE'];
				$objContactPage->keywords = $pageInfo['KEYWORDS'];
				$objContactPage->description = $pageInfo['DESCRIPTION'];
				$objContactPage->body = $pageInfo['BODY'];
				$objContactPage->listingId = $pageInfo['ListingID'];
				$objContactPage->name = $pageInfo['NAME'];
				$objContactPage->live = $pageInfo['LIVE'];
				$objContactPage->isContact = $pageInfo['ISCONTACT'];
				$objContactPage->allowToDelete = $pageInfo['ALLOWDELETE'];
                                //$objContactPage->subContactPages = $objSubPage->getAllByMainContactPageId($pageId);
                                }
			}
			return $objContactPage;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>getContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of ContactPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrContactPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = null;
                                $orderBy = " ListingID Asc";
				$this->select('tbl_contactpage',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
				foreach($pageRes As $pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getContactPage($pageId);
					array_push($arrContactPages,$pageInfo);
				}
					
			}
			return $arrContactPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>getContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   updateOrder
	 * @param        :   ContactPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateOrder($objContactPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objContactPage->id;
				$listingId 			= $objContactPage->listingId;
				$arrayData          = array('ListingID'=>$listingId);
				//$this->update('tbl_contactpage',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('tbl_contactpage',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>addContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	 
	
	/**'
	 * @name         :   getAllByStatus
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of ContactPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByStatus($status){
		$arrContactPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = "LIVE = '".$status."'";
                $orderBy = " ListingID Asc";
				$this->select('tbl_contactpage',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getContactPage($pageId);
					array_push($arrContactPages,$pageInfo);
				}
					
			}
			return $arrContactPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ContactPage</em>, <strong>Function -</strong> <em>getContactPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        
	
public function formateUrlString($strUrl) {
    $strUrl = str_replace('"', '', $strUrl);
    $strUrl = str_replace(' ', '-', $strUrl);
    $strUrl = str_replace('?', '-', $strUrl);
    $strUrl = str_replace("'", '-', $strUrl);
     $strUrl = str_replace("&", 'and', $strUrl);
    $strUrl = strtolower($strUrl);
    return $strUrl;
}
	 
}
?>