<?php
/**
 * @name        SecSubPage
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class SecSubPage extends Core_Database{


	/**'
	 * @name         :   addSecSubPage
	 * @param        :   SecSubPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addSecSubPage($objSecSubPage){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objSecSubPage->id;
				$title 				= $objSecSubPage->title;
				$keywords 			= $objSecSubPage->keywords;
				$description                    = $objSecSubPage->description;
				$body 				= $objSecSubPage->body;
				$listingId 			= $objSecSubPage->listingId;
				$name 				= $objSecSubPage->name;
				$live 				= $objSecSubPage->live;
				$isContact 			= $objSecSubPage->isContact;
				$allowToDelete                  = $objSecSubPage->allowToDelete;
                                $mainPageId                     = $objSecSubPage->mainPageId;
				$inserted = $this->insert('sec_sub_pages',array($id,$title,$keywords,$description,$body,$listingId,$name,$live,$isContact,$allowToDelete,$mainPageId)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>addSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editSecSubPage
	 * @param        :   SecSubPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editSecSubPage($objSecSubPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objSecSubPage->id;
				$title 				= $objSecSubPage->title;
				$keywords 			= $objSecSubPage->keywords;
				$description 		= $objSecSubPage->description;
				$body 				= $objSecSubPage->body;
				$listingId 			= $objSecSubPage->listingId;
				$name 				= $objSecSubPage->name;
				$live 				= $objSecSubPage->live;
				$isContact 			= $objSecSubPage->isContact;
				$allowToDelete 		= $objSecSubPage->allowToDelete;
				$arrayData          = array('TITLE'=>$title,
										'KEYWORDS'=>$keywords,
										'DESCRIPTION'=>$description,
										'BODY'=>$body,
										'ListingID'=>$listingId,
										'NAME'=>$name,
										'LIVE'=>$live,
										'ISCONTACT'=>$isContact,
										'ALLOWDELETE'=>$allowToDelete);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('sec_sub_pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>addSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteSecSubPage
	 * @param        :   SecSubPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteSecSubPage($objSecSubPage){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objSecSubPage->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('sec_sub_pages',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>addSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getSecSubPage
	 * @param        :   Integer (SecSubPage ID)
	 * Description   :   The function is to get a page details
	 * @return       :   SecSubPage Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getSecSubPage($pageId){
		$objSecSubPage = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$pageId;
				$this->select('sec_sub_pages',$colums,$where);
				$pageInfo = $this->getResult();
					
				$objSecSubPage->id = $pageInfo['ID'];
				$objSecSubPage->title = $pageInfo['TITLE'];
				$objSecSubPage->keywords = $pageInfo['KEYWORDS'];
				$objSecSubPage->description = $pageInfo['DESCRIPTION'];
				$objSecSubPage->body = $pageInfo['BODY'];
				$objSecSubPage->listingId = $pageInfo['ListingID'];
				$objSecSubPage->name = $pageInfo['NAME'];
				$objSecSubPage->live = $pageInfo['LIVE'];
				$objSecSubPage->isContact = $pageInfo['ISCONTACT'];
				$objSecSubPage->allowToDelete = $pageInfo['ALLOWDELETE'];
                                $objSecSubPage->mainPageId = $pageInfo['MAIN_PAGE_ID'];
			}
			return $objSecSubPage;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>getSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of SecSubPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrSecSubPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = null;
                                $orderBy = " ListingID Asc";
				$this->select('sec_sub_pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getSecSubPage($pageId);
					array_push($arrSecSubPages,$pageInfo);
				}
					
			}
			return $arrSecSubPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>getSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   updateOrder
	 * @param        :   SecSubPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateOrder($objSecSubPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objSecSubPage->id;
				$listingId 			= $objSecSubPage->listingId;
				$arrayData          = array('ListingID'=>$listingId);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('sec_sub_pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>addSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of SecSubPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByMainPageId($mainPageId){
		$arrSecSubPages       = array();
		try{
			if($this->connect()){
				/*
                                $colums = 'ID';
				$where  = 'MAIN_PAGE_ID = '.$mainPageId;
                                $orderBy = " ListingID Asc";
				$this->select('sec_sub_pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
                                */
                            
                                $SQL = "SELECT * FROM sec_sub_pages WHERE MAIN_PAGE_ID = '".$mainPageId."' AND LIVE = 'Published' ORDER BY ListingID Asc";
                                 //print($SQL); exit;
                                 $dbResult = $this->executeSelectQuery($SQL);
                                 $pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getSecSubPage($pageId);
					array_push($arrSecSubPages,$pageInfo);
				}
					
			}
			return $arrSecSubPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SecSubPage</em>, <strong>Function -</strong> <em>getSecSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	 
	 
}
?>