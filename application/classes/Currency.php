<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Currency extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $code;
    public $codeStr;
    public $isDefault;
    public $status;
    public $error = array();
    public $data_array = array();

    //constructor
    public function Currency() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCurrency
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addCurrency() {
        $recordId = null;
        try {
        	$id = $this->id;
        	$name = $this->name;
        	$code = $this->code;
        	$codeStr = $this->codeStr;
        	$isDefault = $this->isDefault;
        	$status = $this->status;
        	//$status = $this->status;
        	//$displayorder = $this->displayorder;
        	
        	$inserted = $this->insert('tbl_currency', array($id, $name, $code,$codeStr,$isDefault,$status));
        	if ($inserted) {
        		$recordId = $this->getLastInsertedId();
        	}  
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editCurrency() {
        $isUpdated = false;
        try {
        	$id = $this->id;
        	$name = $this->name;
        	$code = $this->code;
        	$codeStr = $this->codeStr;
        	$isDefault = $this->isDefault;
        	$status = $this->status;
        	
        	$arrayData = array(
        			'id' => $id,
        			'name' => $name,
        			'currency_code' => $code,
        			'currency_code_str' => $codeStr,
        			'is_default' => $isDefault,
        			'status' => $status	
        	);
        	//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
        	$arrWhere = array("id = '" . $id . "'");
        	$isUpdated = $this->update('tbl_currency', $arrayData, $arrWhere);
        	return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    


    /*     * '
     * @name         :   deleteCurrency
     * @param        :   CurrencyObject
     * Description   :   The function is to delete Currency details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteCurrency() {
        $isDeleted = false;
        try {
        	if ($this->connect()) {
        		$id = $this->id;
        		$arrWhere = array("id = '" . $id . "'");
        		$isDeleted = $this->delete('tbl_currency', $arrWhere);
        	}
        	return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCurrency
     * @param        :   Integer (Currency ID)
     * Description   :   The function is to get a Currency details
     * @return       :   Currency Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getCurrency($currencyId) {
        $objCurrency = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$currencyId;
                $this->select('tbl_currency', $colums, $where);
                $categoryInfo = $this->getResult();

                if($categoryInfo){
                    $objCurrency->id = $categoryInfo['id'];
                    $objCurrency->name = $categoryInfo['name'];
                    $objCurrency->code = $categoryInfo['currency_code'];
                    $objCurrency->codeStr = $categoryInfo['currency_code_str'];
                    $objCurrency->isDefault = $categoryInfo['is_default'];
                    $objCurrency->status = $categoryInfo['status'];
                } else {
                	$objCurrency->id = null;
                    $objCurrency->name = null;
                    $objCurrency->code = null;
                    $objCurrency->codeStr = null;
                    $objCurrency->isDefault = null;
                    $objCurrency->status = null;
                }
            }
            return $objCurrency;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrCurrency = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_currency', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getCurrency($currencyId);
                    array_push($arrCurrency, $currencyInfo);
                }
            }

            return $arrCurrency;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /*     * '
     * @name         :   getDefaultCurrency
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getDefaultCurrency() {
        $currencyInfo = null;
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = " is_default = 'Yes' ";
                $orderBy = "id ASC";
                $this->select('tbl_currency', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getCurrency($currencyId);
                }
            }

            return $currencyInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    
    
    
    /** '
     * @name         :   countRec
     * @param        :   CurrencyObject
     * @desc         :   The function is to count the nnumer of active Currencies
     * @return       :   Integer (Total number Of Currencies)
     * Added By      :   Mohamed Rasvi
     * Added On      :   23-12-2013
     * Modified By   :   -
     * Modified On   :   -
     */
    
    public function countRec() {
    	$totalNumberOfRec = 0;
    	$arrWhere = array();
    	try {
    		$SQL = "SELECT * FROM $this->tb_name ";
    		if ($this->searchStr != '') {
    			array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
    		}
    
    		if (count($arrWhere) > 0)
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    		$dbResult = $this->executeSelectQuery($SQL);
    		$catRes = $this->getResult();
    		$totalNumberOfRec = count($catRes);
    		return $totalNumberOfRec;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    
    public function search() {
    	$arrCurrency = array();
    	$arrWhere = array();
    	try {
    		$SQL = "SELECT * FROM $this->tb_name ";
    		if ($this->searchStr != '') {
    			array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
    		}
             
    		if($this->status != ''){
    			array_push($arrWhere, "status = '" .$this->status. "'");
    		}
    		
    		if (count($arrWhere) > 0)
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    		if ($this->listingOrder) {
    			$SQL.= ' ORDER BY ' . $this->listingOrder;
    		}
    
    		if ($this->limit) {
    			$SQL.= $this->limit;
    		}
    		//echo $SQL;
    		$dbResult = $this->executeSelectQuery($SQL);
    		$CurrencyRes = $this->getResult();
    		foreach ($CurrencyRes As $currencyRow) {
    			$id = $currencyRow['id'];
    			$currencyInfo = $this->getCurrency($id);
    			array_push($arrCurrency, $currencyInfo);
    		}
    		return $arrCurrency;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }

}
?>