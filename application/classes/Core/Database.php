<?php

/**
 * @name        Database		
 * @copyright  2012 Monara IT UK Ltd
 * @license    http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 * 
 * @Description This Database class let you create a new database connection, and
 * 				execute sql queries for the data manipulation such as insert, edit,
 * 				delete, select and etc on the database.
 *
 * @Note        This class is only for the mysql database. In feature version, this class will be able to support
 * 				for the other databases such as MSSSQL Server, Oracle,Mysqli and etc.
 * 				And this class developed and tested on Mysql 5.0.13, We recomended to use MySql 5.0.13 or above.
 */
class Core_Database extends Exception { // database class starts
    // declare variables

    private $_DB;
    private $_DB_SERVER; // the database server
    private $_DB_USER; // the database username
    private $_DB_PASSWORD; // the database password
    private $_DB_NAME; // the databse name
    private $con = false; // a variable for the database connection
    private $result = array(); // the result variable
    private $lastInsertedId = null;
    private $numResults = null;
    public $tb_name;

    /*
     * Connects to the database, only one connection
     * allowed
     */

    public function connect() {
        $this->_DB_SERVER = DB_SERVER;
        $this->_DB_USER = DB_USER;
        $this->_DB_PASSWORD = DB_PASSWORD;
        $this->_DB_NAME = DB_NAME;

        try {
            if (!$this->con) {
                $myconn = mysql_connect($this->_DB_SERVER, $this->_DB_USER, $this->_DB_PASSWORD);
                if ($myconn) {
                    mysql_set_charset('utf8',$myconn); 
                    $seldb = mysql_select_db($this->_DB_NAME, $myconn) or die(mysql_error());
                    if ($seldb) {
                        $this->con = true;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>connect()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Changes the new database, sets all current results
     * to null
     */

    public function setDatabase($name) {
        try {
            if ($this->con) {
                if (mysql_close()) {
                    $this->con = false;
                    $this->result = null;
                    $this->_DB_NAME = $name;
                    $this->connect();
                }
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>setDatabase()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Checks to see if the table exists when performing
     * queries
     */

    private function tableExists($table) {
        try {
            $tablesInDb = mysql_query('SHOW TABLES FROM ' . $this->_DB_NAME . ' LIKE "' . $table . '"');
            if ($tablesInDb) {
                if (mysql_num_rows($tablesInDb) == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>tableExists()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Selects information from the database.
     * Required: table (the name of the table)
     * Optional: rows (the columns requested, separated by commas)
     *           where (column = value as a string)
     *           order (column DIRECTION as a string)
     */

    public function select($table, $rows = '*', $where = null, $order = null, $limit = null) {
        try {
            $q = 'SELECT ' . $rows . ' FROM ' . $table;
            if ($where != null)
                $q .= ' WHERE ' . $where;
            if ($order != null)
                $q .= ' ORDER BY ' . $order;
            if ($limit != null)
               $q .= " " . $limit;

           // if($table == 'set_payment_options')    print($q); 
            $query = mysql_query($q);
            if ($query) {
                $this->numResults = mysql_num_rows($query);
                for ($i = 0; $i < $this->numResults; $i++) {
                    $r = mysql_fetch_array($query);
                    $key = array_keys($r);
                    for ($x = 0; $x < count($key); $x++) {
                        // Sanitizes keys so only alphavalues are allowed
                        if (!is_int($key[$x])) {
                            if (mysql_num_rows($query) > 1)
                                $this->result[$i][$key[$x]] = $r[$key[$x]];
                            else if (mysql_num_rows($query) < 1)
                                $this->result = null;
                            else
                                $this->result[$key[$x]] = $r[$key[$x]];
                        }
                    }
                }
                return true;
            }
            else {
                return false;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>select()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Insert values into the table
     * Required: table (the name of the table)
     *           values (the values to be inserted)
     * Optional: rows (if values don't match the number of rows)
     */

    public function insert($table, $values, $rows = null) {
        try {
            if ($this->tableExists($table)) {
                $insert = 'INSERT INTO ' . $table;
                if ($rows != null) {
                    $insert .= ' (' . $rows . ')';
                }

                for ($i = 0; $i < count($values); $i++) {
                    if (is_string($values[$i]))
                        $values[$i] = '"' . $values[$i] . '"';
                }
                $values = implode(',', $values);
                $insert .= ' VALUES (' . $values . ')';
               
            //print_r($insert); exit;
                $ins = mysql_query($insert);
               

                if ($ins) {
                    $this->lastInsertedId = mysql_insert_id();
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>insert()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Deletes table or records where condition is true
     * Required: table (the name of the table)
     * Optional: where (condition [column =  value])
     */

    public function delete($table, $where = null) {
        try {
            if ($this->tableExists($table)) {
                if ($where == null) {
                    $delete = 'DELETE ' . $table;
                } else {

                    for ($i = 0; $i < count($where); $i++) {
                        if ($i % 2 != 0) {
                            if (is_string($where[$i])) {
                                if (($i + 1) != null)
                                    $where[$i] = '"' . $where[$i] . '"';
                                else
                                    $where[$i] = '"' . $where[$i] . '"';
                            }
                        }
                    }
                    $where = implode(' AND ', $where);
                    $delete = 'DELETE FROM ' . $table . ' WHERE ' . $where;
                }
               
                $del = mysql_query($delete);

                if ($del) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>delete()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Updates the database with the values sent
     * Required: table (the name of the table to be updated
     *           rows (the rows/values in a key/value array
     *           where (the row/condition in an array (row,condition) )
     */

    public function update($table, $rows, $where) {
        try {
            if ($this->tableExists($table)) {
                // Parse the where values
                // even values (including 0) contain the where rows
                // odd values contain the clauses for the row
                for ($i = 0; $i < count($where); $i++) {
                    if ($i % 2 != 0) {
                        if (is_string($where[$i])) {
                            if (($i + 1) != null)
                                $where[$i] = '"' . $where[$i] . '"';
                            else
                                $where[$i] = '"' . $where[$i] . '"';
                        }
                    }
                }
                $where = implode(' AND ', $where);

                $update = 'UPDATE ' . $table . ' SET ';
                $keys = array_keys($rows);
                for ($i = 0; $i < count($rows); $i++) {
                    if (is_string($rows[$keys[$i]])) {
                        $update .= $keys[$i] . '="' . $rows[$keys[$i]] . '"';
                    } else {
                        $update .= $keys[$i] . '=' . $rows[$keys[$i]];
                    }

                    // Parse to add commas
                    if ($i != count($rows) - 1) {
                        $update .= ',';
                    }
                }
                $update .= ' WHERE ' . $where;
               //print_r($update);exit;
                $query = mysql_query($update);
                
                if ($query) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>update()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Selects information from the database.
     * Required: table (the name of the table)
     * Optional: rows (the columns requested, separated by commas)
     *           where (column = value as a string)
     *           order (column DIRECTION as a string)
     */

    public function executeSelectQuery($q) {
        try {
            //print($q); 
		$this->result = null;
            $query = mysql_query($q);
            if ($query) {
                $this->numResults = mysql_num_rows($query);
                for ($i = 0; $i < $this->numResults; $i++) {
                    $r = mysql_fetch_array($query);
                    $key = array_keys($r);

                    for ($x = 0; $x < count($key); $x++) {
                        // Sanitizes keys so only alphavalues are allowed
                        if (!is_int($key[$x])) {
                            if (mysql_num_rows($query) > 0) {
                                $this->result[$i][$key[$x]] = $r[$key[$x]];
                            }
                        }
                    }
                }

                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>select()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Returns the result set
     */

    public function getResult() {
        return $this->result;
    }

    /* Return number of Rows
    */
    public function getNumRows(){
        return $this->numResults;
    }
    /*
     * Returns the last inserted ID
     */

    public function getLastInsertedId() {
        return $this->lastInsertedId;
    }

    public function disconnect() {
        try {
            if ($this->con) {
                if (mysql_close()) {
                    $this->con = false;
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>disconnect()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*
     * Selects information from the database.
     * Required: table (the name of the table)
     * Optional: rows (the columns requested, separated by commas)
     *           where (column = value as a string)
     *           order (column DIRECTION as a string)
     * specially applies working with one dimentional arrays
     */

    public function selectRow($table, $rows = '*', $where = null, $order = null, $limit = null) {
        try {
            $q = 'SELECT ' . $rows . ' FROM ' . $table;
            if ($where != null)
                $q .= ' WHERE ' . $where;
            if ($order != null)
                $q .= ' ORDER BY ' . $order;
            if ($limit != null)
                $q .= " " . $limit;

            //print($q).'<br/>'; 
            $query = mysql_query($q);
            if ($query) {
                $this->numResults = mysql_num_rows($query);

                for ($i = 0; $i < $this->numResults; $i++) {
                    $r = mysql_fetch_array($query);
                    $key = array_keys($r);
                    for ($x = 0; $x < count($key); $x++) {
                        // Sanitizes keys so only alphavalues are allowed
                        if (!is_int($key[$x])) {
                            if (mysql_num_rows($query) >= 1)
                                $this->result[$i][$key[$x]] = $r[$key[$x]];
                            else if (mysql_num_rows($query) < 1)
                                $this->result = null;
                        }
                    }
                }
                return true;
            }
            else {
                return false;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Core_Database</em>, <strong>Function -</strong> <em>select()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getTodayDate() {
        $myDate = "";
        try {
            $myDate = date("Y-m-d");
        } catch (Exception $ex) {
            print("Error - " . $ex->getMessage());
        }
        return $myDate;
    }

    public function getTodayDateTime() {
        $myDate = "";
        try {
            $myDate = date("Y-m-d H:i:s");
        } catch (Exception $ex) {
            print("Error - " . $ex->getMessage());
        }
        return $myDate;
    }

}

//database class ends
?>
