<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class Testimonial extends Core_Database {

    //testimonial propoerties
    public $id = '';
    public $project_id = '';
    public $tes_title = '';
    public $tes_desc = '';
    public $features = '';
    public $quote_by = '';
    public $designation = '';
    public $featured = '';
    public $listingId = '';
    public $image; //mamin testimonial image
    public $searchStr = '';
    public $error = array();
    public $data_array = array();

    //constructor

    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addTestimonial() {
        $recordId = null;
        try {
            $id = $this->id;
            $project_id = $this->project_id;


            $tes_title = $this->tes_title;
            $tes_desc = $this->tes_desc;
            $features = $this->features;
            $quote_by = $this->quote_by;
            $designation = $this->designation;
            $featured = $this->featured;
            $listingId = $this->listingId;

            $inserted = $this->insert($this->tb_name, array($id, $project_id, $tes_title, $tes_desc, $features, $quote_by, $designation, $featured, $listingId));
            if ($inserted) {
                $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editTestimonial
     * @param        :   NewsObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function editTestimonial() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $project_id = $this->project_id;

            $tes_title = $this->tes_title;
            $tes_desc = $this->tes_desc;
            $features = $this->features;
            $quote_by = $this->quote_by;
            $designation = $this->designation;
            $featured = $this->featured;
            $listingId = $this->listingId;

            $arrayData = array(
                'project_id' => $project_id,
                'tes_title' => $tes_title,
                'tes_desc' => $tes_desc,
                'features' => $features,
                'quote_by' => $quote_by,
                'designation' => $designation,
                'featured' => $featured,
                'listingId' => $listingId
            );
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   updateOrder
     * @param        :   NewsObject
     * @desc   :   The function is to edit a News Listing order
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */

    public function updateOrder() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $listingId = $this->listingId;
            $arrayData = array('listingId' => $listingId);
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");

            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            
        }

        throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    }

    /** '
     * @name         :   removeFeatured
     * @param        :   
     * @desc         :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   15-10-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function removeFeatured($id) {
        $isUpdated = false;
        try {

            $featured = 0;
            $arrayData = array(
                'featured' => $featured
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   removeFeatured
     * @param        :   
     * @desc         :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   15-10-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function addFeatured($id) {
        $isUpdated = false;
        try {

            $featured = 1;
            $arrayData = array(
                'featured' => $featured
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   deleteTestimonial
     * @param        :   NewsObject
     * @desc   :   The function is to delete news details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deleteTestimonial() {
        $isDeleted = false;
        try {
            $id = $this->id;
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getTestimonial
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a News details
     * @return       :   News Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     * TODO : // get testimonial for the given project id
     */
    public function getProjectTestimonial($id) {
        $objTestimonial = new stdClass();

        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'project_id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $tstmInfo = $this->getResult();
                if (count($tstmInfo)) {
                    $objTestimonial->id = $tstmInfo['id'];
                    $objTestimonial->project_id = $tstmInfo['project_id'];

                    $objTestimonial->tes_title = $tstmInfo['tes_title'];
                    $objTestimonial->tes_desc = $tstmInfo['tes_desc'];
                    $objTestimonial->features = $tstmInfo['features'];
                    $objTestimonial->quote_by = $tstmInfo['quote_by'];
                    $objTestimonial->designation = $tstmInfo['designation'];
                    $objTestimonial->featured = $tstmInfo['featured'];
                    $objTestimonial->listingId = $tstmInfo['listingId'];
                    $objTestimonial->image = $objImage->getMainImage('Testimonial', $id);
                }
            }
            return $objTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getTestimonial
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a News details
     * @return       :   News Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getTestimonial($id) {
        $objTestimonial = new stdClass();
        $objImage = new Image();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $tstmInfo = $this->getResult();
                if (count($tstmInfo)) {
                    $objTestimonial->id = $tstmInfo['id'];
                    $objTestimonial->project_id = $tstmInfo['project_id'];

                    $objTestimonial->tes_title = $tstmInfo['tes_title'];
                    $objTestimonial->tes_desc = $tstmInfo['tes_desc'];
                    $objTestimonial->features = $tstmInfo['features'];
                    $objTestimonial->quote_by = $tstmInfo['quote_by'];
                    $objTestimonial->designation = $tstmInfo['designation'];
                    $objTestimonial->featured = $tstmInfo['featured'];
                    $objTestimonial->listingId = $tstmInfo['listingId'];
                    $objTestimonial->image = $objImage->getMainImage('Testimonial', $id);
                }
            }
            return $objTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all news details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getAll() {
        $arrTestimonial = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "listingId Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $tstmResult = $this->getResult();
                foreach ($tstmResult As $tstmRow) {
                    $id = $tstmRow['id'];
                    $tstmInfo = $this->getTestimonial($id);
                    array_push($arrTestimonial, $tstmInfo);
                }
            }

            return $arrTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllFeatured
     * @param        :
     * @desc         :   The function is to get all News details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getAllFeatured() {
        $arrTestimonial = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "featured  = '1'";
                $orderBy = "";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $tstmRes = $this->getResult();
                if (count($tstmRes)) {
                    foreach ($tstmRes As $nIndex => $tstmRow) {
                        $id = $tstmRow['id'];
                        $tstmInfo = $this->getTestimonial($id);
                        array_push($arrTestimonial, $tstmInfo);
                    }
                }
            }
            return $arrTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAllNonFeatured() {
        $arrTestimonial = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "featured  = '0'";
                $orderBy = "";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $tstmRes = $this->getResult();
                if (count($tstmRes)) {
                    foreach ($tstmRes As $nIndex => $tstmRow) {
                        $id = $tstmRow['id'];
                        $tstmInfo = $this->getTestimonial($id);
                        array_push($arrTestimonial, $tstmInfo);
                    }
                }
            }
            return $arrTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :Restaurant Menu
     * @desc         :   The function is to count the News details
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "tes_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $newsRes = $this->getResult();
            $totalNumberOfRec = count($newsRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Testimonial
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function search() {
        $arrTestimonial = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "tes_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $tstmRes = $this->getResult();
            foreach ($tstmRes As $nIndex => $tstmRow) {
                $id = $tstmRow['id'];
                $tstmInfo = $this->getTestimonial($id);
                array_push($arrTestimonial, $tstmInfo);
            }

            return $arrTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all news details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   2012-10-15
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getAllByProjectId($projectId) {
        $arrTestimonial = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT id FROM tbl_testimonials WHERE project_id = '" . $projectId . "'";

                $dbResult = $this->executeSelectQuery($SQL);
                $tstmResult = $this->getResult();
                foreach ($tstmResult As $tstmRow) {
                    $id = $tstmRow['id'];
                    $tstmInfo = $this->getTestimonial($id);
                    array_push($arrTestimonial, $tstmInfo);
                }
            }

            return $arrTestimonial;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>

