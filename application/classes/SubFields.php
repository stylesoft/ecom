<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SubFields extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $subFieldValues;
    public $parentFieldId;
    
    
    

    //constructor
    public function SubFields() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addSubFields
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addSubFields() {
        $recordId = null;
        try {
               
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editSubFields() {
        $isUpdated = false;
        try {
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteSubFields
     * @param        :   SubFieldsObject
     * Description   :   The function is to delete SubFields details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteSubFields() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getSubFields
     * @param        :   Integer (SubFields ID)
     * Description   :   The function is to get a SubFields details
     * @return       :   SubFields Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getSubFields($fieldId) {
        $objSubFields = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$fieldId;
                $this->select('tbl_sub_fields', $colums, $where);
                $fieldsInfo = $this->getResult();
                if($fieldsInfo){
                    
                    $objFieldValue = new FieldValue();
                    $objFieldValue->tb_name = 'tbl_field_value';
                    $fieldId  = $fieldsInfo['parent_field_id'];
                    $subFieldId = $fieldsInfo['id'];
                    $objSubFields->id = $fieldsInfo['id'];
                    $objSubFields->name = $fieldsInfo['name'];
                    $objSubFields->parentFieldId = $fieldId;
                    $objSubFields->subFieldValues = $objFieldValue->getFieldValueByFieldIdAndSubFieldId($fieldId,$subFieldId); 
                } else {
                    return null;
                }
            }
            return $objSubFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrSubFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_sub_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getSubFields($fieldId);
                    array_push($arrSubFields,$fieldInfo);
                }
            }

            return $arrSubFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
        /*     * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByParentId($parentId) {
        $arrSubFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "parent_field_id = '" . $parentId . "'";
                $orderBy = "id ASC";
                $this->select('tbl_sub_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                //print_r($fieldsResult); exit;
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getSubFields($fieldId);
                    array_push($arrSubFields,$fieldInfo);
                }
            }

            return $arrSubFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>