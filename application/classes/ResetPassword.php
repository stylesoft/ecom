<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ResetPassword extends Core_Database {

    //news propoerties
    public $id;
    public $email;
    public $code;
    
    //constructor
    public function ResetPassword() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCountry
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addResetPassword() {
        $recordId = null;
    try {
        	$id = $this->id;
        	$email = $this->email;
        	$code = $this->code;
        	
        	
        	$inserted = $this->insert('tbl_resetpassword', array($id, $email, $code));
        	if ($inserted) {
        		$recordId = $this->getLastInsertedId();
        	}  
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editResetPassword() {
        $isUpdated = false;
    try {
        	$id = $this->id;
        	$email = $this->email;
        	$code = $this->code;
        	
        	
        	$arrayData = array(
        			'id' => $id,
        			'email' => $email,
        			'code' => $code
        			
        	);
        	//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
        	$arrWhere = array("id = '" . $id . "'");
        	$isUpdated = $this->update('tbl_resetpassword', $arrayData, $arrWhere);
        	return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCountry
     * @param        :   CountryObject
     * Description   :   The function is to delete Country details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteCountry() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCountry
     * @param        :   Integer (Country ID)
     * Description   :   The function is to get a Country details
     * @return       :   Country Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getResetPassword($ResetPasswordEmail) {
        $objCountry = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'email = ' .$ResetPasswordEmail;
                $this->select('tbl_resetpassword', $colums, $where);
                $categoryInfo = $this->getResult();

                if($categoryInfo){
                    $objCountry->id = $categoryInfo['id'];
                    $objCountry->email = $categoryInfo['email'];
                    $objCountry->code = $categoryInfo['code'];
                    
                } else {
                	$objCountry->id = null;
                    $objCountry->email = null;
                    $objCountry->code = null;
                   
                }
            }
            return $objCountry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    

    
    /*     * '
     * @name         :   getCountry
    * @param        :   Integer (Country ID)
    * Description   :   The function is to get a Country details
    * @return       :   Country Object
    * Added By      :   Gayan Chathuranga
    * Added On      :   29-08-2012
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function getResetPasswordId($id) {
    	$objCountry = new stdClass();
    	try {
    		if ($this->connect()) {
    			$colums = '*';
    			$where = 'id = ' .$id;
    			$this->select('tbl_resetpassword', $colums, $where);
    			$categoryInfo = $this->getResult();
    
    			if($categoryInfo){
    				$objCountry->id = $categoryInfo['id'];
    				$objCountry->email = $categoryInfo['email'];
    				$objCountry->code = $categoryInfo['code'];
    
    			} else {
    				$objCountry->id = null;
    				$objCountry->email = null;
    				$objCountry->code = null;
    				 
    			}
    		}
    		return $objCountry;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }

}
?>
