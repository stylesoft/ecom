<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ObjectSizeMap extends Core_Database {

    //news propoerties
    public $id;
    public $size;
    public $object;
    public $stockInHand;

    //constructor
    public function ObjectSizeMap() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addObjectSizeMap
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addObjectSizeMap() {
        $recordId = null;
        try {
      
                $id = $this->id;
                $size = $this->size;
                $object = $this->object;
                $stockInHand = $this->stockInHand;
                
                $inserted = $this->insert($this->tb_name, array($id,$size,$object,$stockInHand));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editObjectSizeMap() {
        $isUpdated = false;
        try {

             $id = $this->id;
                $size = $this->size;
                $object = $this->object;
                $stockInHand = $this->stockInHand;
                
                $arrayData = array(
                    'id' => $id,
                    'size_id' => $size,
                    'size_rel_id' => $object,
                    'size_stock' => $stockInHand
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteObjectSizeMap
     * @param        :   ObjectSizeMapObject
     * Description   :   The function is to delete ObjectSizeMap details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteObjectSizeMap() {
        $isDeleted = false;
        try {

            $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
                
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getObjectSizeMap
     * @param        :   Integer (ObjectSizeMap ID)
     * Description   :   The function is to get a ObjectSizeMap details
     * @return       :   ObjectSizeMap Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getObjectSizeMap($sizeId) {
        $objObjectSizeMap = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $sizeId;
                $this->select('tbl_object_size_map', $colums, $where);
                $fieldsInfo = $this->getResult();
                $objSizes = new ObjectSize();
                if ($fieldsInfo) {    
                    $objObjectSizeMap->id = $sizeId;
                     $objObjectSizeMap->size = $objSizes->getObjectSize($fieldsInfo['size_id']);
                    $objObjectSizeMap->object = $fieldsInfo['size_rel_id'];
                    $objObjectSizeMap->stockInHand = $fieldsInfo['size_stock'];
                }
            }
            return $objObjectSizeMap;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrObjectSizeMap = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_object_size_map', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getObjectSizeMap($fieldId);
                    array_push($arrObjectSizeMap, $fieldInfo);
                }
            }

            return $arrObjectSizeMap;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByType($objectRel) {
        $arrObjectSizeMap = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT id FROM tbl_object_size_map WHERE size_rel_id = '" . $objectRel . "'";
                $dbResult = $this->executeSelectQuery($SQL);
                $productSizeRes = $this->getResult();
                foreach ($productSizeRes As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getObjectSizeMap($fieldId);
                    array_push($arrObjectSizeMap, $fieldInfo);
                }
            }

            return $arrObjectSizeMap;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>