<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * @modified on : 2012-11-01 <gayan.chathuranga@monara.com>
 * @desc        : two database columns added for product weight unit and product weight based on that base product class modifications
 */
class Product extends Core_Database {

    //product propoerties
    public $id;
    public $productName;
    public $productSmallDescription;
    public $unitPrice;
    public $discountPrice;
    public $priceCurrency;
    public $weightUnit;
    public $productWeight;
    public $stockInHand;
    public $reorderLevel;
    public $category;
    public $addedOn;
    public $addedBy;
    public $status;
    public $isFeatured;
    public $productDescription;
    public $keywords;
    public $displayOrder;
    public $productCode;
    public $listParam;
    public $searchStr;
    public $listingOrder;
    public $limit;
    public $categoryGroup;
    public $productImages;
    public $productDefaultImage;
    public $error = array();
    public $data_array = array();
    public $isSpecialOffer;
    public $productBrand;
    public $productSize;
    

    //constructor

    public function Product() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /** '
     * @name         :   editProduct
     * @param        :   ProductObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :  2012-11-01
     */
    public function addProduct() {
        $recordId = null;
        try {
            $id = $this->id;
            $productName = $this->productName;
            $productSmallDescription = $this->productSmallDescription;
            $unitPrice = $this->unitPrice;
            $discountPrice = $this->discountPrice;
            $priceCurrency = $this->priceCurrency;
            $weightUnit = $this->weightUnit;
            $productWeight = $this->productWeight;
            $stockInHand = $this->stockInHand;
            $reorderLevel = $this->reorderLevel;
            $category = $this->category;
            $addedOn = $this->addedOn;
            $addedBy = $this->addedBy;
            $status = $this->status;
            $isFeatured = $this->isFeatured;
            $productDescription = $this->productDescription;
            $keywords = $this->keywords;
            $displayOrder = $this->displayOrder;
            $productCode = $this->productCode;
            $isSpecialOffer = $this->isSpecialOffer;
            $productBrand = $this->productBrand;
            $productSize = $this->productSize;

            $inserted = $this->insert('tbl_product', array($id, $productName, $productSmallDescription, $unitPrice, $discountPrice, $priceCurrency, $weightUnit, $productWeight, $stockInHand, $reorderLevel, $category, $addedOn, $addedBy, $status, $isFeatured, $productDescription, $keywords, $displayOrder, $productCode, $isSpecialOffer, $productBrand,$productSize));
            if ($inserted) {
                $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editProduct
     * @param        :   ProductObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :  Gayan Chathuranga
     * Modified On   :  2012-11-01
     */
    public function editProduct() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $productName = $this->productName;
            $productSmallDescription = $this->productSmallDescription;
            $unitPrice = $this->unitPrice;
            $discountPrice = $this->discountPrice;
            $priceCurrency = $this->priceCurrency;
            $weightUnit = $this->weightUnit;
            $productWeight = $this->productWeight;
            $stockInHand = $this->stockInHand;
            $reorderLevel = $this->reorderLevel;
            $category = $this->category;
            $addedOn = $this->addedOn;
            $addedBy = $this->addedBy;
            $status = $this->status;
            $isFeatured = $this->isFeatured;
            $productDescription = $this->productDescription;
            $keywords = $this->keywords;
            $displayOrder = $this->displayOrder;
            $productCode = $this->productCode;
            $isSpecialOffer = $this->isSpecialOffer;
            $productBrand = $this->productBrand;
            $productSize = $this->productSize;

            $arrayData = array(
                'id' => $id,
                'product_name' => $productName,
                'product_small_description' => $productSmallDescription,
                'unit_price' => $unitPrice,
                'discount_price' => $discountPrice,
                'price_currency' => $priceCurrency,
                'weight_unit' => $weightUnit,
                'product_weight' => $productWeight,
                'stock_in_hand' => $stockInHand,
                'reorder_level' => $reorderLevel,
                'tbl_category_id' => $category,
                'added_on' => $addedOn,
                'added_by' => $addedBy,
                'status' => $status,
                'is_featured' => $isFeatured,
                'product_description' => $productDescription,
                'keywords' => $keywords,
                'display_order' => $displayOrder,
                'product_code' => $productCode,
                'is_special_offers' => $isSpecialOffer,
                'product_brand' => $productBrand,
                'product_size' => $productSize
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update('tbl_product', $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   updateOrder
     * @param        :   ProductObject
     * @desc   :   The function is to edit a Product Listing order
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   04-09-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function updateOrder() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $displayOrder = $this->displayOrder;
            $arrayData = array('display_order' => $displayOrder);
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");

            $isUpdated = $this->update('tbl_product', $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            
        }

        throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    }

    /** '
     * @name         :   deleteProduct
     * @param        :   ProductObject
     * @desc   :   The function is to delete product details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deleteProduct() {
        $isDeleted = false;
        try {
            $id = $this->id;
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete('tbl_product', $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   deleteProduct
     * @param        :   ProductObject
     * @desc   :   The function is to delete product details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deleteProductByCategoryId() {
        $isDeleted = false;
        try {
            $id = $this->category;
            $arrWhere = array("tbl_category_id = '" . $id . "'");
            $isDeleted = $this->delete('tbl_product', $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    

    /** '
     * @name         :   getProduct
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a Product details
     * @return       :   Product Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   2012-11-01
     */
    public function getProduct($productId) {
        $objProduct = new stdClass();
        $objProductImage = new Image();
        $objCurrency = new Currency();
        $objCategory = new Category();
        $objBrand = new Brand();
        $objSize = new ObjectSizeMap();

        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $productId;
                $this->select('tbl_product', $colums, $where);
                $productInfo = $this->getResult();

                $objProduct->id = $productInfo['id'];
                $objProduct->productName = $productInfo['product_name'];
                $objProduct->productSmallDescription = $productInfo['product_small_description'];
                $objProduct->unitPrice = $productInfo['unit_price'];
                $objProduct->discountPrice = $productInfo['discount_price'];

                $currencyInfo = $objCurrency->getCurrency($productInfo['price_currency']);

                if ($currencyInfo) {
                    $objProduct->priceCurrency = $currencyInfo;
                }
                $objProduct->weightUnit = $productInfo['weight_unit'];
                $objProduct->productWeight = $productInfo['product_weight'];
                $objProduct->stockInHand = $productInfo['stock_in_hand'];
                $objProduct->reorderLevel = $productInfo['reorder_level'];
                $objProduct->category = $objCategory->getCategory($productInfo['tbl_category_id']);
                $objProduct->addedOn = $productInfo['added_on'];
                $objProduct->addedBy = $productInfo['added_by'];
                $objProduct->status = $productInfo['status'];
                $objProduct->isFeatured = $productInfo['is_featured'];
                $objProduct->productDescription = $productInfo['product_description'];
                $objProduct->keywords = $productInfo['keywords'];
                $objProduct->displayOrder = $productInfo['display_order'];
                $objProduct->isSpecialOffer = $productInfo['is_special_offers'];

                $productId = $productInfo['id'];
                $defaultImage = "";
                $productImages = $objProductImage->getAllByImageObject('Product', $productId);
                if ($productImages) {
                    $defaultImage = $productImages[0];
                    $objProduct->productDefaultImage = $defaultImage;
                    $objProduct->productImages = $productImages;
                }

                $objProduct->productCode = $productInfo['product_code'];
                $objProduct->productBrand = $objBrand->getBrand($productInfo['product_brand']);
                $objProduct->productSize = $objSize->getAllByType($productInfo['id']);
            }
            return $objProduct;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :Restaurant Menu
     * @desc         :   The function is to count the Product details
     * @return       :   Integer (Total number Of Product)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */
    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM tbl_product ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "product_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }


            if ($this->status != '') {
                array_push($arrWhere, "status =  '" . "%" . $this->status . "%" . "'");
            }


            if ($this->isFeatured != '') {
                array_push($arrWhere, "is_featured =  '" . $this->isFeatured . "'");
            }

            if ($this->isSpecialOffer != '') {
                array_push($arrWhere, "is_special_offers =  '" . $this->isSpecialOffer . "'");
            }

            if ($this->category != '') {
                array_push($arrWhere, "tbl_category_id =  '" . $this->category . "'");
            }


            if ($this->categoryGroup) {
                $comma_separated_categories = implode(",", $this->categoryGroup);
                array_push($arrWhere, "tbl_category_id IN ($comma_separated_categories)");
            }
            
            if ($this->productBrand != '') {
                    array_push($arrWhere, "product_brand =  '" . $this->productBrand . "'");
                }



            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            
            $dbResult = $this->executeSelectQuery($SQL);
            $productRes = $this->getResult();
            $totalNumberOfRec = count($productRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */
    public function search() {
        $arrProduct = array();
        $arrWhere = array();
        try {


            if ($this->listParam == 'top-sellers') {

                $SQL = "SELECT `productId` As id,count(`productId`) as tot FROM `tbl_order_items` GROUP BY productId  ORDER BY tot Desc";
            }

            if ($this->listBrand == 'top-brands') {
            
            	$SQL = "SELECT `product_brand` As id,count(`product_brand`) as tot FROM `tbl_product` GROUP BY product_brand  ORDER BY tot Desc";
            }
             
             else {

                $SQL = "SELECT * FROM tbl_product ";

                if ($this->searchStr != '') {
                    array_push($arrWhere, "product_name LIKE '" . "%" . $this->searchStr . "%" . "'");
                }

                if ($this->isFeatured != '') {
                    array_push($arrWhere, "is_featured =  '" . $this->isFeatured . "'");
                }

                if ($this->isSpecialOffer != '') {
                    array_push($arrWhere, "is_special_offers =  '" . $this->isSpecialOffer . "'");
                }

                if ($this->status != '') {
                    array_push($arrWhere, "status =  '" . $this->status . "'");
                }


                if ($this->category != '') {
                    array_push($arrWhere, "tbl_category_id =  '" . $this->category . "'");
                }

                if ($this->categoryGroup) {
                    $comma_separated_categories = implode(",", $this->categoryGroup);
                    array_push($arrWhere, "tbl_category_id IN ($comma_separated_categories)");
                }
                
                if ($this->productBrand != '') {
                    array_push($arrWhere, "product_brand =  '" . $this->productBrand . "'");
                }

                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);


                if ($this->listingOrder) {
                    $SQL.= ' ORDER BY ' . $this->listingOrder;
                }
            }


            //print($SQL);exit;
            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;exit;
            $dbResult = $this->executeSelectQuery($SQL);
            $productRes = $this->getResult();
            foreach ($productRes As $productRow) {
                $productId = $productRow['id'];
                $productInfo = $this->getProduct($productId);
                array_push($arrProduct, $productInfo);
            }
            return $arrProduct;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   updateFeatured
     * @param        :   ProductObject
     * @desc   :   The function is to edit a Product Listing order
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   04-09-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function updateFeatured() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $isFeatured = $this->isFeatured;
            $arrayData = array('is_featured' => $isFeatured);
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");

            $isUpdated = $this->update('tbl_product', $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            
        }

        throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    }

    /*     * '
     * @name         :   updateSpecialOffer
     * @param        :   ProductObject
     * @desc   :   The function is to edit a Product Listing order
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   04-09-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function updateSpecialOffer() {
        $isUpdated = false;
        try {

            $id = $this->id;
            $isSpecialOffer = $this->isSpecialOffer;
            $arrayData = array('is_special_offers' => $isSpecialOffer);
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");

            $isUpdated = $this->update('tbl_product', $arrayData, $arrWhere);

            return $isUpdated;
        } catch (Exception $e) {
            
        }

        throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    }

    public function countProductByCategory($categoryId) {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM tbl_product ";
            array_push($arrWhere, "status =  'Published'");
            array_push($arrWhere, "tbl_category_id =  '" . $categoryId . "'");

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);

            $dbResult = $this->executeSelectQuery($SQL);
            $productRes = $this->getResult();
            $totalNumberOfRec = count($productRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>