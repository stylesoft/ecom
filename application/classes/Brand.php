<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Brand extends Core_Database {

    //news propoerties
    public $id;
    public $brand_name;
    public $brand_description;
    public $status;
    public $displayorder;
    public $brand_image;
    public $Cattype ;
    public $error = array();
    public $data_array = array();

    //constructor
   public function Brand() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addBrand
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */


    
    public function addBrand() {
        $recordId = null;
        try {
                $id = $this->id;
                $brand_name = $this->brand_name;
                $brand_description = $this->brand_description;
                $status = $this->status;
                $displayorder = $this->displayorder;
                $brand_image = $this->brand_image;
                $Cattype = $this->Cattype;
                $inserted = $this->insert($this->tb_name, array($id,$brand_name,$brand_description,$status,$displayorder,$brand_image,$Cattype));
                
                if ($inserted) {
                	
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }  
    
    
    

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editBrand() {
        $isUpdated = false;
        try {
                $id = $this->id;
                $brand_name = $this->brand_name;
                $brand_description = $this->brand_description;
                $status = $this->status;
                $displayorder = $this->displayorder;
                $brand_image = $this->brand_image;
                $Cattype = $this->Cattype;

                $arrayData = array(
                    'id' => $id,
                    'brand_name' => $brand_name,
                    'brand_description' => $brand_description,
                    'status' => $status,
                    'display_order' => $displayorder,
                    'brand_image' => $brand_image,
                        'category' => $Cattype
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteBrand() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getBrand($catId) {
        $objBrand = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $catId;
                $this->select('tbl_brand', $colums, $where);
                $brandInfo = $this->getResult();
                $objBrand->id = $brandInfo['id'];
                $objBrand->brand_name = $brandInfo['brand_name'];
                $objBrand->brand_description = $brandInfo['brand_description'];
                $objBrand->status = $brandInfo['status'];
                $objBrand->displayorder = $brandInfo['display_order'];
                $objBrand->brand_image = $brandInfo['brand_image'];
                $objBrand->Cattype = $brandInfo['category'];
            }
            return $objBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'status = 1';
                $orderBy = "brand_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $brandResult = $this->getResult();
                foreach ($brandResult As $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getBrand($catId);
                    array_push($arrBrand, $catInfo);
                }
            }

            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "status = '" . $status . "'";
                $orderBy = "brand_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();
                
                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getBrand($catId);
                    array_push($arrBrand, $catInfo);
                }
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentByStatus($status,$cat_id) {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "id != '".$cat_id."' AND status = '" . $status . "'";
                $orderBy = "brand_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getBrand($catId);
                    array_push($arrBrand, $catInfo);
                }
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayorder 			= $this->displayorder;                               
				$arrayData          = array('display_order'=>$displayorder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByBrandObject($param = '') {
        $arrBrand = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "' AND status = '" . $status . "'";
            $orderBy = "brand_name ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $catRes = $this->getResult();

            foreach ($catRes As $nIndex => $catRow) {
                $catId = $catRow['id'];
                $catInfo = $this->getBrand($catId);
                array_push($arrBrand, $catInfo);
            }

            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   12-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "brand_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrBrand = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "brand_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

	    if ($this->status != '') {
                    array_push($arrWhere, "status = '" . $this->status . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }
            else{
                $SQL.= ' ORDER BY display_order ASC ';
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $brandInfo = $this->getBrand($id);
                array_push($arrBrand, $brandInfo);
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    
    
 
    
 

}
?>
