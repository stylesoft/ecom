<?php

/**
 * @name        SocialMediaSetting
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */
class SocialMediaSetting extends Core_Database {
    /*     * '
     * @name         :   editSocialMediaSetting
     * @param        :   SocialMediaSettingObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function editSocialMediaSetting($objSocialMediaSetting) {
        $isUpdated = false;
        try {
            if ($this->connect()) {
                $id             = $objSocialMediaSetting->id;
                $facebook       = $objSocialMediaSetting->facebook;
                $twitter        = $objSocialMediaSetting->twitter;
                $linkedIn       = $objSocialMediaSetting->linkedIn;
                $googlePlus     = $objSocialMediaSetting->googlePlus;
                $other          = $objSocialMediaSetting->other;
                $pinterest      = $objSocialMediaSetting->pinterest;
                
                
                

                $arrayData = array('facebook' => $facebook,
                    'twitter' => $twitter,
                    'linkedIn' => $linkedIn,
                    'googlePlus' => $googlePlus,
                    'other' => $other,
                    'pinterest' => $pinterest
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update('tbl_social_media', $arrayData, $arrWhere);
            }
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SocialMediaSetting</em>, <strong>Function -</strong> <em>addSocialMediaSetting()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getSocialMediaSetting
     * @param        :   Integer (SocialMediaSetting ID)
     * Description   :   The function is to get a page details
     * @return       :   SocialMediaSetting Object
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function getSocialMediaSetting($SocialMediaSettingId) {
        $objSocialMediaSetting = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $SocialMediaSettingId;
                $this->select('tbl_social_media', $colums, $where);
                $socialMediaInfo = $this->getResult();

                $objSocialMediaSetting->id                      = $socialMediaInfo['id'];
                $objSocialMediaSetting->facebook                = $socialMediaInfo['facebook'];
                $objSocialMediaSetting->twitter                 = $socialMediaInfo['twitter'];
                $objSocialMediaSetting->linkedIn                = $socialMediaInfo['linkedIn'];
                $objSocialMediaSetting->googlePlus              = $socialMediaInfo['googlePlus'];
                $objSocialMediaSetting->other                   = $socialMediaInfo['other'];
                $objSocialMediaSetting->pinterest               = $socialMediaInfo['pinterest'];
                
            }
            return $objSocialMediaSetting;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SocialMediaSetting</em>, <strong>Function -</strong> <em>getSocialMediaSetting()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>