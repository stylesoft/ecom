<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Category extends Core_Database {

    //news propoerties
    public $id;
    public $category_name;
    public $category_description;
    public $sub_category_description;
    public $parent_category;
    public $subCategory;
    public $status;
    public $displayorder;
    public $error = array();
    public $data_array = array();

    //constructor
    public function Category() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCategory
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addCategory() {
        $recordId = null;
        try {
                $id = $this->id;
                $category_name = $this->category_name;
                $category_description = $this->category_description;
                $sub_category_description = $this->sub_category_description;
                $parent_category = $this->parent_category;
                $display_style   =  $this->display_style ;
                $status = $this->status;
                $displayorder = $this->displayorder;

                $inserted = $this->insert($this->tb_name, array($id, $category_name, $category_description,$sub_category_description, $parent_category,$display_style, $status,$displayorder));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editCategory() {
        $isUpdated = false;
        try {
                $id = $this->id;
                $category_name = $this->category_name;
                $category_description = $this->category_description;
                $sub_category_description = $this->sub_category_description;
                $parent_category = $this->parent_category;
                 $display_style   =  $this->display_style ;
                $status = $this->status;
                $displayorder = $this->displayorder;

                $arrayData = array(
                    'id' => $id,
                    'category_name' => $category_name,
                    'category_description' => $category_description,
                		'sub_category_description' => $sub_category_description,
                    'parent_category' => $parent_category,
                    'display_style'  => $display_style, 
                    'status' => $status,
                    'display_order' => $displayorder
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteCategory() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getCategory($catId) {
        $objCategory = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $catId;
                $this->select('tbl_category', $colums, $where);
                $categoryInfo = $this->getResult();
                $objCategory->id = $categoryInfo['id'];
                $objCategory->category_name = $categoryInfo['category_name'];
                $objCategory->category_description = $categoryInfo['category_description'];
                $objCategory->sub_category_description = $categoryInfo['sub_category_description'];
                $objCategory->parent_category = $categoryInfo['parent_category'];
                 $objCategory->display_style = $categoryInfo['display_style'];
                $objCategory->status = $categoryInfo['status'];
                $objCategory->displayorder = $categoryInfo['display_order'];
                $objCategory->subCategory = $this->getAllSubCategory($catId, 'Enabled');
            }
            return $objCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrCatgry = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'status = 1';
                $orderBy = "category_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $categoryResult = $this->getResult();
                foreach ($categoryResult As $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getCategory($catId);
                    array_push($arrCatgry, $catInfo);
                }
            }

            return $arrCatgry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrCategory = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "status = '" . $status . "'";
                $orderBy = "category_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();
                
                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getCategory($catId);
                    array_push($arrCategory, $catInfo);
                }
            }
            return $arrCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentByStatus($status,$cat_id) {
        $arrCategory = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "id != '".$cat_id."' AND status = '" . $status . "'";
                $orderBy = "category_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getCategory($catId);
                    array_push($arrCategory, $catInfo);
                }
            }
            return $arrCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayorder 			= $this->displayorder;                               
				$arrayData          = array('display_order'=>$displayorder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByCategoryObject($param = '') {
        $arrCategory = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "' AND status = '" . $status . "'";
            $orderBy = "category_name ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $catRes = $this->getResult();

            foreach ($catRes As $nIndex => $catRow) {
                $catId = $catRow['id'];
                $catInfo = $this->getCategory($catId);
                array_push($arrCategory, $catInfo);
            }

            return $arrCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   12-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "category_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrCategory = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "category_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $categoryInfo = $this->getCategory($id);
                array_push($arrCategory, $categoryInfo);
            }
            return $arrCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    
    
    // get all subcategory....
    public function getAllSubCategory($maincategoryId = null, $categoryStatus = null){
        $arrSubCategory = array();
        $arrWhere = array();
         try { 
             
             if ($this->connect()) {
                $colums = 'id';
                
                $SQL = "SELECT * FROM tbl_category ";
                
                if($maincategoryId != null){
                    array_push($arrWhere, "parent_category = '" .$maincategoryId. "'");
                }
                
                
                if($categoryStatus != null){
                    array_push($arrWhere, "status = '" .$categoryStatus. "'");
                }
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
                 $dbResult = $this->executeSelectQuery($SQL);
                 $subCatRes = $this->getResult();
          

                foreach ($subCatRes As $nIndex => $subCatRow) {
                    $subCatId       = $subCatRow['id'];
                    $subCatInfo     = $this->getCategory($subCatId);
                    array_push($arrSubCategory, $subCatInfo);
                }
            }
             
            return $arrSubCategory;
             
         } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
        
        
    }
    
    
    
    
     /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentCategory($status = null) {
        $arrCategory = array();
        $arrWhere = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT * FROM tbl_category ";
                array_push($arrWhere, "parent_category = 0");
                
                if ($this->searchStr != '') {
                	array_push($arrWhere, "category_name LIKE '" . "%" . $this->searchStr . "%" . "'");
                }
                
                if($status != null){
                    array_push($arrWhere, "status = '" .$status. "'");
                }
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
          
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId       = $catRow['id'];
                    $catInfo     = $this->getCategory($catId);
                    array_push($arrCategory, $catInfo);
                }

                
            }
            return $arrCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    /*     * '
     * @name         :   getParentCategory
     * @param        :
     * Description   :   The function is to get parent category id
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getParentCategory($childCategory) {
        $arrCategory = array();
        $parentCatId = 0;
        try {
            if ($this->connect()) {
                $SQL = "SELECT parent_category FROM tbl_category WHERE id = 0";
                
          
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $parentCatId       = $catRow['parent_category'];
                }

                
            }
            return $parentCatId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>