<?php
/**
 * 
 * This class is used for language translation purposes
 *Core_Database including this class extends database core functionalities 
 * Change methods in thiss class if you want to add new translation functionalities
 * @author GAYAN CHATHUARANGA   <gayan.chathuranga@monara.com>
 * @package com.oop.monara.implementation;
 *
 */

class Translation extends Core_Database{
    
    public $id; //field id in tbl_lang_fields
    public $fieldText;
   

   
    const DEFAULT_LANG = ''; //default language is set to English
    
    /*
     * Constructor: connect to the database 
     */
    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
             throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    
     /** 
     * @name         :   addFieldText
     * @param        :   Translation Object
     * @desc   :   The function is to add new Field Text to translation
     * @return       :   boolean
     * Table         :   tbl_lang_fields 
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addFieldText() {
        $recordId = null;
        try {
                $recordID = $this->id;
                $name = $this->fieldText;

                $inserted = $this->insert($this->tb_name, array($recordID, $name));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** 
     * @name         :   addSiteTranslation
     * @param        :   Translation Object
     * @desc   :   The function edit translated static text
     * @return       :   boolean
     * Table         :   tbl_phrases 
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function editFieldText(){
        $isUpdated = false;
        try {
                $recordID = $this->id;
                $name = $this->fieldText;

                $arrayData = array(
                    'name' => $name);
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $recordID . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
     /** '
     * @name         :   deleteFieldText
     * @param        :   --
     * @desc   :   The function is to delte field text
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   10-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteFieldText() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * @desc         :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getPhrase($id) {
        $objPhr = new stdClass();
        $objPhrTrans = new PhraseTranslation();
        try {

                $colums = '*';
                $where = 'id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $phraseInfo = $this->getResult();

                $objPhr->id = $phraseInfo['id'];
                $objPhr->name = $phraseInfo['name'];
                $objPhrTrans->tb_name = 'tbl_phrases';
                $objPhr->Trans = $objPhrTrans->getAllPhraseTranslation($phraseInfo['id'],'tbl_lang_fields');

            return $objPhr;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
   
    
     /** '
     * @name         :   countRec
     * @param        :   HomeBanner
     * @desc         :   The function is to count the nnumer of active home banner contents
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   06-09-2012
     */

     public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);

           
            $dbResult = $this->executeSelectQuery($SQL);
            $phrRes = $this->getResult();
            $totalNumberOfRec = count($phrRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   06-09-2012
     */

    public function search() {
        $arrPhr = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $phrRes = $this->getResult();
            foreach ($phrRes As $phrRow) {
                $id = $phrRow['id'];
                $phrInfo = $this->getPhrase($id);
                array_push($arrPhr, $phrInfo);
            }
            return $arrPhr;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>
