<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Order extends Core_Database {

    //news propoerties
    public $id;
    public $customerId;
    public $orderDate;
    public $orderStatus;
    public $paymentStatus;
    public $subTotal;
    public $shippingAmount;
    public $grandTotal;
    public $paymentMethod;
    
    
    public $error = array();
    public $data_array = array();

    //constructor
    public function Order() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addOrder
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addOrder() {
        $recordId = null;
        try {
                $id	= $this->id;
    			$customerId = $this->customerId;
    			$orderDate	= $this->orderDate;
    			$orderStatus	= $this->orderStatus;
    			$paymentStatus	= $this->paymentStatus;
    			$subTotal	= $this->subTotal;
    			$shippingAmount = $this->shippingAmount;
    			$grandTotal = $this->grandTotal;
                        $paymentMethod  = $this->paymentMethod;

                $inserted = $this->insert('tbl_order', array($id, $customerId,$orderDate,$orderStatus,$paymentStatus,$subTotal,$shippingAmount,$grandTotal,$paymentMethod));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editOrder
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editOrder() {
        $isUpdated = false;
        try {
                 $id	= $this->id;
    			$customerId = $this->customerId;
    			$orderDate	= $this->orderDate;
    			$orderStatus	= $this->orderStatus;
    			$paymentStatus	= $this->paymentStatus;
    			$subTotal	= $this->subTotal;
    			$shippingAmount = $this->shippingAmount;
    			$grandTotal = $this->grandTotal;
                        $paymentMethod  = $this->paymentMethod;

                $arrayData = array(
                    'customerId' => $category_name,
                    'order_date' => $category_description,
                    'order_status' => $parent_category,
                    'payment_status' => $status,
                    'sub_total' => $displayorder,
                	'shipping_amount' => $displayorder,
                	'grand_total' => $grandTotal,
                    'payment_method' => $paymentMethod
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update('tbl_order', $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    public function updateOrderStatus($id){
        try {

            $orderStatus = $this->orderStatus;
            $arrayData = array(
                'order_status' => $orderStatus,
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update('tbl_order', $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $exc) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    public function updatePaymentStatus($id){
        try {

            $paymentStatus = $this->paymentStatus;
            $arrayData = array(
                'payment_status' => $paymentStatus,
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update('tbl_order', $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $exc) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    

    /*     * '
     * @name         :   deleteOrder
     * @param        :   OrderObject
     * Description   :   The function is to delete Order details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteOrder() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getOrder
     * @param        :   Integer (Order ID)
     * Description   :   The function is to get a Order details
     * @return       :   Order Object
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getOrder($id) {
        $objOrder = new stdClass();
            $insCustomer = new Customer(); $insCustomer->tb_name = 'tbl_customer';
            $insOrderItems = new OrderItem();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $id;
                $this->select('tbl_order', $colums, $where);
                $orderInfo = $this->getResult();
                $objOrder->id = $orderInfo['id'];
                $objOrder->customerId = $orderInfo['customerId'];
                $objOrder->orderDate = $orderInfo['order_date'];
                $objOrder->orderStatus = $orderInfo['order_status'];
                $objOrder->paymentStatus = $orderInfo['payment_status'];
                $objOrder->subTotal = $orderInfo['sub_total'];
                $objOrder->shippingAmount = $orderInfo['shipping_amount'];
                $objOrder->grandTotal = $orderInfo['grand_total'];
                $objOrder->paymentMethod = $orderInfo['payment_method'];
                
                //get order realational data
                $objOrder->cutomerDetails = $insCustomer->getCustomer($objOrder->customerId);
                $objOrder->orderItemDetails = $insOrderItems->getAll($objOrder->id);
            }
            return $objOrder;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrOrders = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $orderResult = $this->getResult();
                foreach ($orderResult As $orderRow) {
                    $orderId = $orderRow['id'];
                    $orderInfo = $this->getOrder($orderId);
                    array_push($arrOrders, $orderInfo);
                }
            }

            return $arrOrders;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
    $arrOrders = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "order_status = '" . $status . "'";
                $orderBy = "id ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $orderResult = $this->getResult();
                foreach ($orderResult As $orderRow) {
                    $orderId = $orderRow['id'];
                    $orderInfo = $this->getOrder($orderId);
                    array_push($arrOrders, $orderInfo);
                }
            }

            return $arrOrders;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    
     /** '
     * @name         :   countRec
     * @param        :   OrderObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   09-11-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM tbl_order ";
            if ($this->customerId != '') {
                array_push($arrWhere, "customerId = '" . $this->customerId . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $orderRes = $this->getResult();
            $totalNumberOfRec = count($orderRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrOrders = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM tbl_order ";
            if ($this->id != '') {
                array_push($arrWhere, "id = '" . $this->id . "'");
            }
        
             if ($this->orderDate != '') {
                $replaceseach = $this->orderDate;
                $searchdate = date("Y-m-d",strtotime($replaceseach));
                array_push($arrWhere, "order_date LIKE '" ."%" . $searchdate ."%" . "'");
            }

            if($this->paymentMethod !=''){
                array_push($arrWhere, "payment_method = '".$this->paymentMethod."'");
            }
            
            if($this->paymentStatus !=''){
                array_push($arrWhere, "payment_status = '".$this->paymentStatus."'");
            }
            
            if($this->oderStatus !=''){
            	array_push($arrWhere, "order_status = '".$this->oderStatus."'");
            }
            
            if($this->customerId !=''){
                array_push($arrWhere, "customerId = '".$this->customerId."'");
            }
            
            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);
            
            if ($this->listingOrder) {
            	$SQL.= ' ORDER BY ' . $this->listingOrder;
            }
            
            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
        	$orderResult = $this->getResult();
                //print_r($orderResult); exit;
                foreach ($orderResult As $orderRow) {
                    $orderId = $orderRow['id'];
                    $orderInfo = $this->getOrder($orderId);
                    array_push($arrOrders, $orderInfo);
                }
            return $arrOrders;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }

}
?>