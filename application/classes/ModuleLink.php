<?php

/**
 * @name        ModuleLink
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */
class ModuleLink extends Core_Database {

    public $id;
    public $moduleLinkName;
    public $isEnabled;
    public $moduleLinkLabel;
    public $moduleId;
    public $displayOrder;

    /*     * '
     * @name         :   isModuleLinkEnable
     * @param        :
     * Description   :   The function is to get all ebabled ModuleLink
     * @return       :   Array (Array Of ModuleLink Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function isModuleLinkEnable($moduleLinkName) {
        $isEnabled = false;

        try {
            if ($this->connect()) {
                $SQL = "SELECT id FROM tbl_module_links WHERE module_link_name = '" . $moduleLinkName . "' AND Is_Enabled = 'Yes'";
                $dbResult = $this->executeSelectQuery($SQL);
                $moduleRes = $this->getResult();

                if ($moduleRes) {
                    $isEnabled = true;
                } else {
                    $isEnabled = false;
                }
            }
            return $isEnabled;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ModuleLink</em>, <strong>Function -</strong> <em>getModuleLink()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    
    /**'
	 * @name         :   getModules
	 * @param        :   Integer (Modules ID)
	 * Description   :   The function is to get a page details
	 * @return       :   Modules Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getModuleLink($menuLinkId){

		$objModuleLink = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'id = '.$menuLinkId;
				$this->select('tbl_module_links',$colums,$where);
				$objModuleLinkInfo = $this->getResult();
				if(count($objModuleLinkInfo)>0){
					$objModuleLink->id                  = $objModuleLinkInfo['id'];
                                        $objModuleLink->moduleLinkName      = $objModuleLinkInfo['module_link_name'];
                                        $objModuleLink->isEnabled           = $objModuleLinkInfo['Is_Enabled'];
                                        $objModuleLink->moduleLinkLabel     = $objModuleLinkInfo['module_link_label'];
                                        $objModuleLink->moduleId            = $objModuleLinkInfo['module_id'];
                                        $objModuleLink->displayOrder        = $objModuleLinkInfo['display_order'];
				}
			}
			return $objModuleLink;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>getModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}
        
        
        
        
        /*     * '
     * @name         :   getAllByModuleId
     * @param        :
     * Description   :   The function is to get all ebabled ModuleLink
     * @return       :   Array (Array Of ModuleLink Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function getAllByModuleId($moduleId) {
        $arrModuleLinks       = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT id FROM tbl_module_links WHERE module_id = '" . $moduleId . "'";
                $dbResult = $this->executeSelectQuery($SQL);
                $moduleLink = $this->getResult();
               
                $moduleLinkRes = $this->getResult();
		foreach($moduleLinkRes As $moduleLinkRow){
                    $moduleLinkId = $moduleLinkRow['id'];
		    $moduleLinkInfo = $this->getModuleLink($moduleLinkId);
		    array_push($arrModuleLinks,$moduleLinkInfo);
		}
                
            }
            return $arrModuleLinks;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>ModuleLink</em>, <strong>Function -</strong> <em>getModuleLink()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    
     /**'
	 * @name         :   updateStatus
	 * @param        :   Module Object
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateStatus($objModuleLink){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objModuleLink->id;
				$isEnabled 			= $objModuleLink->isEnabled;
				$arrayData          = array('Is_Enabled'=>$isEnabled);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
				$isUpdated = $this->update('tbl_module_links',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   disableAll
	 * @param        :   Module Object
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function disableAll(){
		$isUpdated = false;
		try{
			if($this->connect()){
				$isEnabled 	     = 'No';
				$arrayData          = array('Is_Enabled'=>$isEnabled);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id != ''");
				$isUpdated = $this->update('tbl_module_links',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

}
?>
