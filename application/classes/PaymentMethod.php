<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 4.0
 * @version    	4
 * @author     	Mohamed Rasvi
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class PaymentMethod extends Core_Database {

    //Pyamnetmethod propoerties
    public $id;
    public $name;
    public $code;
    public $status;
    
    
    
    public $error = array();
    public $data_array = array();
    
    


    //constructor

    public function PaymentMethod() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addPaymentMethod() {
        $recordId = null;
        try {
                $id = $this->id;
                $name = $this->name;
                $code = $this->code;
                $status = $this->status;
                

                $inserted = $this->insert($this->tb_name, array($id, $name, $code, $status));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editPaymentMethod
     * @param        :   PaymentMethodObject
     * @desc         :   The function is to edit a PaymentMethod details
     * @return       :   boolean
     * Added By      :   Mohamed Rasvi
     * Added On      :   9-01-2014
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editPaymentMethod() {
        $isUpdated = false;
        try {

               $id = $this->id;
                $name = $this->name;
                $code = $this->code;
                $status = $this->status;

                $arrayData = array(
                    'id' => $id,
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    /** '
     * @name         :   deletePaymentMethod
     * @param        :   ServicesObject
     * @desc         :   The function is to delete PaymentMethod details
     * @return       :   boolean
     * Added By      :   Mohamed Rasvi
     * Added On      :   9-01-2014
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deletePaymentMethod() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getPaymentMethod
     * @param        :   Integer (PaymentMethod ID)
     * @desc         :   The function is to get a PaymentMethod details
     * @return       :   PaymentMethod Object
     * Added By      :   Mohamed Rasvi
     * Added On      :   9-01-2014
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getPaymentMethod($paymentMethodId) {
        $objPaymentMethod = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $paymentMethodId;
                $this->select($this->tb_name, $colums, $where);
                $paymentMethodInfo = $this->getResult();

                $objPaymentMethod->id = $paymentMethodInfo['id'];
                $objPaymentMethod->name = $paymentMethodInfo['name'];
                $objPaymentMethod->code = $paymentMethodInfo['code'];
                $objPaymentMethod->status = $paymentMethodInfo['status'];
                
            }
            return $objPaymentMethod;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc         :   The function is to get all PaymentMethods details
     * @return       :   Array (Array Of PaymentMethod Object)
     * Added By      :   Mohamed Rasvi
     * Added On      :   09-01-2014
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrPaymentMethods = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'status = Enabled';
                $orderBy = "id Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $paymentMethodsResult = $this->getResult();
                foreach ($paymentMethodsResult As $paymentMethodsRow) {
                    $paymentMethodsId = $paymentMethodsRow['id'];
                    $paymentMethodsInfo = $this->getPaymentMethod($paymentMethodsId);
                    array_push($arrPaymentMethods, $paymentMethodsInfo);
                }
            }

            return $arrPaymentMethods;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllByStatus
     * @param        :
     * @desc         :   The function is to get all PaymentMethods details by status
     * @return       :   Array (Array Of PaymentMethods Object)
     * Added By      :   Mohamed Rasvi
     * Added On      :   09-01-2014
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrPaymentMethods = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "status = '" . $status . "'";
                $orderBy = " id Asc";
               $this->select($this->tb_name, $colums, $where, $orderBy);
                $paymentMethodsResult = $this->getResult();
                foreach ($paymentMethodsResult As $paymentMethodsRow) {
                    $paymentMethodsId = $paymentMethodsRow['id'];
                    $paymentMethodsInfo = $this->getPaymentMethod($paymentMethodsId);
                    array_push($arrPaymentMethods, $paymentMethodsInfo);
                }
            }
            return $arrPaymentMethods;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :   PaymentMethods
     * @desc         :   The function is to count the PaymentMethods details
     * @return       :   Integer (Total number Of PaymentMethods)
     * Added By      :   Mohamed Rasvi
     * Added On      :   09-01-2014
     
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $paymentMethodsRes = $this->getResult();
            $totalNumberOfRec = count($paymentMethodsRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  PaymentMethods  details
     * @return       :   Array (Array Of PaymentMethods Object)
     * Added By      :   Mohamed Rasvi
     * Added On      :   09-01-2014
    
     */

    public function search() {
        $arrPaymentMethods = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $paymentMethodsRes = $this->getResult();
            foreach ($paymentMethodsRes As $paymentMethodsRow) {
                $paymentMethodsId = $paymentMethodsRow['id'];
                $paymentMethodsInfo = $this->getPaymentMethod($paymentMethodsId);
                array_push($arrPaymentMethods, $paymentMethodsInfo);
            }
            return $arrPaymentMethods;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     
}
?>
