<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Country extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $code;
    public $countryFlags;
    public $isDefault;
    public $error = array();
    public $data_array = array();

    //constructor
    public function Country() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCountry
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addCountry() {
        $recordId = null;
        try {
               
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editCountry() {
        $isUpdated = false;
        try {
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCountry
     * @param        :   CountryObject
     * Description   :   The function is to delete Country details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteCountry() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCountry
     * @param        :   Integer (Country ID)
     * Description   :   The function is to get a Country details
     * @return       :   Country Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getCountry($currencyId) {
        $objCountry = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$currencyId;
                $this->select('tbl_country', $colums, $where);
                $categoryInfo = $this->getResult();

                if($categoryInfo){
                    $objCountry->id = $categoryInfo['id'];
                    $objCountry->name = $categoryInfo['name'];
                    $objCountry->code = $categoryInfo['country_code'];
                    $objCountry->countryFlags = $categoryInfo['country_flags'];
                    $objCountry->isDefault = $categoryInfo['is_default'];
                } else {
                	$objCountry->id = null;
                    $objCountry->name = null;
                    $objCountry->code = null;
                    $objCountry->countryFlags = null;
                    $objCountry->isDefault = null;
                }
            }
            return $objCountry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    


    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrCountry = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "name ASC";
                $this->select('tbl_country', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getCountry($currencyId);
                    array_push($arrCountry, $currencyInfo);
                }
            }

            return $arrCountry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /*     * '
     * @name         :   getDefaultCountry
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getDefaultCountry() {
        $currencyInfo = null;
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = " is_default = 'Yes' ";
                $orderBy = "name ASC";
                $this->select('tbl_country', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getCountry($currencyId);
                }
            }

            return $currencyInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>