<?php
/**
 * @name        Page
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class Page extends Core_Database{


        public $subPages;

	/**'
	 * @name         :   addPage
	 * @param        :   PageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addPage($objPage){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objPage->id;
				$title 				= $objPage->title;
				$keywords 			= $objPage->keywords;
				$description 		= $objPage->description;
				$body 				= $objPage->body;
				$listingId 			= $objPage->listingId;
				$name 				= $objPage->name;
				$live 				= $objPage->live;
				$isContact 			= $objPage->isContact;
				$allowToDelete 		= $objPage->allowToDelete;
				$inserted = $this->insert('pages',array($id,$title,$keywords,$description,$body,$listingId,$name,$live,$isContact,$allowToDelete)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editPage
	 * @param        :   PageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editPage($objPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objPage->id;
				$title 				= $objPage->title;
				$keywords 			= $objPage->keywords;
				$description 		= $objPage->description;
				$body 				= $objPage->body;
				$listingId 			= $objPage->listingId;
				$name 				= $objPage->name;
				$live 				= $objPage->live;
				$isContact 			= $objPage->isContact;
				$allowToDelete 		= $objPage->allowToDelete;
				$arrayData          = array('TITLE'=>$title,
										'KEYWORDS'=>$keywords,
										'DESCRIPTION'=>$description,
										'BODY'=>$body,
										'ListingID'=>$listingId,
										'NAME'=>$name,
										'LIVE'=>$live,
										'ISCONTACT'=>$isContact,
										'ALLOWDELETE'=>$allowToDelete);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deletePage
	 * @param        :   PageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deletePage($objPage){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objPage->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('pages',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getPage
	 * @param        :   Integer (Page ID)
	 * Description   :   The function is to get a page details
	 * @return       :   Page Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getPage($pageId){
                $objSubPage = new SubPage();
		$objPage = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$pageId;
				$this->select('pages',$colums,$where);
				$pageInfo = $this->getResult();
                                if(count($pageInfo)>0){
				$objPage->id = $pageInfo['ID'];
				$objPage->title = $pageInfo['TITLE'];
				$objPage->keywords = $pageInfo['KEYWORDS'];
				$objPage->description = $pageInfo['DESCRIPTION'];
				$objPage->body = $pageInfo['BODY'];
				$objPage->listingId = $pageInfo['ListingID'];
				$objPage->name = $pageInfo['NAME'];
				$objPage->live = $pageInfo['LIVE'];
				$objPage->isContact = $pageInfo['ISCONTACT'];
				$objPage->allowToDelete = $pageInfo['ALLOWDELETE'];
                                $objPage->subPages = $objSubPage->getAllByMainPageId($pageId);
                                }
			}
			return $objPage;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of Page Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = null;
                                $orderBy = " ListingID Asc";
				$this->select('pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
				foreach($pageRes As $pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getPage($pageId);
					array_push($arrPages,$pageInfo);
				}
					
			}
			return $arrPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   updateOrder
	 * @param        :   PageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateOrder($objPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objPage->id;
				$listingId 			= $objPage->listingId;
				$arrayData          = array('ListingID'=>$listingId);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	 
	
	/**'
	 * @name         :   getAllByStatus
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of Page Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByStatus($status){
		$arrPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = "LIVE = '".$status."'";
                $orderBy = " ListingID Asc";
				$this->select('pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getPage($pageId);
					array_push($arrPages,$pageInfo);
				}
					
			}
			return $arrPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        
        
        /**'
	 * @name         :   getAllByStatus
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of Page Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function countByStatus($status){
		$totalNumberOfRec = 0;
                $arrWhere = array();
                try {
                    $SQL = "SELECT * FROM pages WHERE LIVE = '".$status."'";
                    $dbResult = $this->executeSelectQuery($SQL);
                    $newsRes = $this->getResult();
                    $totalNumberOfRec = count($newsRes);
                    return $totalNumberOfRec;
                } catch (Exception $e) {
                    throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
                }
	}
        
        
        
	
public function formateUrlString($strUrl) {
    $strUrl = str_replace('"', '', $strUrl);
    $strUrl = str_replace(' ', '-', $strUrl);
    $strUrl = str_replace(',', '-', $strUrl);
    $strUrl = str_replace('?', '-', $strUrl);
    $strUrl = str_replace("'", '-', $strUrl);
    $strUrl = str_replace("(", '', $strUrl);
    $strUrl = str_replace(")", '', $strUrl);
    $strUrl = str_replace("%", '-', $strUrl);
    $strUrl = str_replace(",", '', $strUrl);
        $strUrl = str_replace("&", 'and', $strUrl);
    $strUrl = strtolower($strUrl);
    return $strUrl;
}
	 
}
?>