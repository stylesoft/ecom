<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function getCurrentDateTime(){
    $tstamp = time();
    return $tstamp = gmdate("Y-m-d H:i:s", $tstamp);
}


function priceFormateNumber($price){
	$formated_price = sprintf('%0.2f', $price);
	return $formated_price;
}

function formateUrlString($strUrl) {
    $strUrl = str_replace('"', '', $strUrl);
    $strUrl = str_replace(' ', '-', $strUrl);
    $strUrl = str_replace(',', '-', $strUrl);
    $strUrl = str_replace('?', '-', $strUrl);
    $strUrl = str_replace("'", '-', $strUrl);
    $strUrl = str_replace("(", '', $strUrl);
    $strUrl = str_replace(")", '', $strUrl);
    $strUrl = str_replace("%", '-', $strUrl);
    $strUrl = str_replace(",", '', $strUrl);
        $strUrl = str_replace("&", 'and', $strUrl);
    $strUrl = strtolower($strUrl);
    return $strUrl;
}

function getCartContents(){
	
$arrCartContents = array();
    
    if (isset($_SESSION["gids"]))
    {
        $i = 1;
        $objProduct = new Product();
        $objProduct->tb_name = 'tbl_product';
        foreach($_SESSION["gids"] As $cindex=>$cartProduct){
        	$sizeSelectedProduct = $_SESSION["size"];
        	$colorSelectedProduct = $_SESSION["color"];
            $objCartContent = new stdClass();
            $productInfo  = $objProduct->getProduct($cartProduct);
            $objCartContent->id = $i;
            $objCartContent->productId = $cartProduct;
            $objCartContent->productName = $productInfo->productName;
            $productUnitePrice = $productInfo->unitPrice;
            $productDiscountPrice = $productInfo->discountPrice;
            $productQty = $_SESSION["counts"][$cindex];
            if ($sizeSelectedProduct[$cartProduct] !='') {   
            $objCartContent->productSize = $sizeSelectedProduct[$cartProduct];
        }
        
        if ($sizeSelectedProduct[$cartProduct] =='') {
        	$sizeDefult = $productInfo->productSize;
        	$objCartContent->productSize =$sizeDefult[1]->size->name;
        }
            $objCartContent->color = $colorSelectedProduct[$cartProduct];
            
            if($productInfo->isSpecialOffer == 'Yes'){
                $productUnitePrice = $productDiscountPrice;
            }
            
            $cartIteamTotal    = '';
            if($productDiscountPrice>0){
            	$cartIteamTotal    = $productDiscountPrice;
            } else {
            	$cartIteamTotal    = $productUnitePrice;
            }
            
            $productSubTotal  = $cartIteamTotal * $productQty;
            
            
            $objCartContent->productUnitePrice = $productUnitePrice;
            $objCartContent->productDiscountPrice = $productDiscountPrice;
            $objCartContent->productQty = $_SESSION["counts"][$cindex];
            $objCartContent->productSubTotal = $productSubTotal;
            $objCartContent->productCartPrice = $cartIteamTotal;
            
            $objCartContent->currencyCode = $productInfo->priceCurrency;
            $objCartContent->currencyCodeStr = $productInfo->priceCurrency;
            $objCartContent->productInfo = $productInfo;
            $productImage   = $productInfo->productDefaultImage;
            $objCartContent->productImage = $productImage;
            array_push($arrCartContents, $objCartContent);
            
            
            
        }
    }
    
    return $arrCartContents;
	
}


function clearCartContents(){
	unset($_SESSION["gids"]);
}


// get the Shipping Method.....
function getShippingMethod(){
    $shippingMethod = '';
    if (isset($_SESSION["shippingMethod"]))
    {
        $exitsShippingMethod = unserialize($_SESSION["shippingMethod"]);
        $shippingMethod      = $exitsShippingMethod;
    } /* else {
        // get the default shipping amount....
        $objCarrierRange = new CarrierRange();
        $defShippingMethodObj = $objCarrierRange->getCarrierRange(1);
        
        $shippingMethod   = $defShippingMethodObj;
        if(!$shippingMethod){
            $shippingMethod = null;
        }
    }
    */
    return $shippingMethod;
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function posttofacebookfeed($objStream) {
    
	$siteId = 1;
    	$objSetting         	= new Setting();
    	$siteSettingsInfo       = $objSetting->getSetting($siteId);
    
	$app_id                 = $siteSettingsInfo->facebook_app_id;
	$secret_key             = $siteSettingsInfo->facebook_secret_key;


	$url = "https://graph.facebook.com/oauth/access_token";
	$client_id = $app_id;
	$client_secret = $secret_key;
	$postString = "client_id=$client_id&client_secret=$client_secret&type=client_cred";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postString);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
	$token = curl_exec($curl);
	curl_close($curl);

	$description = $objStream->description;
	$actionText  = "Read News";
        $actionLink  = $objStream->url;
	$action_links = json_encode(array( array('text' => $actionText, 'href' => $actionLink)));
	$attachment   = '';
	$message = rawurlencode($description);
	$url = "https://api.facebook.com/method/stream.publish";
	$postString = "message=$message&action_links=$action_links&attachment=$attachment&uid=100003514369504&$token";
	$curl = curl_init();

	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postString);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
	$response = curl_exec($curl);
	//print_r($response);
	curl_close($curl);
	return $response;
}


// added by iyngaran on 3 - oct - 2012
// the function is to post the tweet to twitter
function post_tweet($tweet_text) {
    $siteId = 1;
    $objSetting         	= new Setting();
    $siteSettingsInfo       	= $objSetting->getSetting($siteId);
    
    $consumer_key               = $siteSettingsInfo->consumer_key;
    $consumer_secret            = $siteSettingsInfo->consumer_secret;
    $user_token                 = $siteSettingsInfo->user_token;
    $user_secret                = $siteSettingsInfo->user_secret;


  $connection = new TmhOAuth(array(
    'consumer_key' => $consumer_key,
    'consumer_secret' => $consumer_secret,
    'user_token' => $user_token,
    'user_secret' => $user_secret,
  )); 
  
  // Make the API call
  $connection->request('POST', 
    $connection->url('1/statuses/update'), 
    array('status' => $tweet_text));
  return $connection->response['code'];
}



function ConverCurrency($from_currency, $to_currency) {
	//$string = $amount . strtolower($from_currency) . "=?" . strtolower($to_currency);
	$google_url = 'http://download.finance.yahoo.com/d/quotes.csv?s='.$from_currency.$to_currency.'=X&f=sl1d1t1ba&e=.csv';
		$handle =fopen($google_url, 'r');

	if($handle)
	{
		$result = fgets($handle, 4096);
		fclose($handle);
	}

	$currencyData = explode(',',$result);
	return $currencyData[1];
}


function convertPriceValue($from_currency,$toCurrency){
    $from_currency     = $from_currency;
    $objCurrency = new stdClass();
    
    if(isset($_SESSION['TO_CURRENCY'])){
        if($_SESSION['TO_CURRENCY'] == $toCurrency){
            $_to_currency_price   = $_SESSION['TO_CURRENCY_PRICE'];
        } else {
                $_SESSION['TO_CURRENCY'] = $toCurrency;
                $_to_currency_price   = ConverCurrency($from_currency, $toCurrency);
                $_SESSION['TO_CURRENCY_PRICE'] = $_to_currency_price;
        }
    } else {
        $_SESSION['TO_CURRENCY'] = $toCurrency;
        $_to_currency_price   = ConverCurrency($from_currency, $toCurrency);
        $_SESSION['TO_CURRENCY_PRICE'] = $_to_currency_price;
    }
    $convertedRate = $_to_currency_price;
    return $convertedRate;
}


function convertPriceValueObj($amount,$toCurrency){
    $from_currency     = 'GBP';
    $objCurrency = new stdClass();
    
    if(isset($_SESSION['TO_CURRENCY'])){
        if($_SESSION['TO_CURRENCY'] == $toCurrency){
            $_to_currency_price   = $_SESSION['TO_CURRENCY_PRICE'];
        } else {
                $_SESSION['TO_CURRENCY'] = $toCurrency;
                $_to_currency_price   = ConverCurrency($from_currency, $toCurrency);
                $_SESSION['TO_CURRENCY_PRICE'] = $_to_currency_price;
        }
    } else {
        $_SESSION['TO_CURRENCY'] = $toCurrency;
        $_to_currency_price   = ConverCurrency($from_currency, $toCurrency);
        $_SESSION['TO_CURRENCY_PRICE'] = $_to_currency_price;
    }
    
    $objCurrency->toCurrencyPrice = $_to_currency_price;
    $objCurrency->toCurrencySymbol = $toCurrency;
   $objCurrency->toCurrencyAmount = $_to_currency_price * $amount;
    return $objCurrency;
}

function resizeImageBeforeSaving($fileName, $width_to_resize, $height_to_resize){
	$image = new SimpleImage();
	$tmpFile = DOC_ROOT.'/tmp/'.$fileName;
	$image->load($tmpFile);
	$image->resize($width_to_resize, $height_to_resize);
	$newFile = DOC_ROOT.'/imgs/'.$fileName;
	$image->save($newFile);
} 


function convertProductNameToPageName($productName){
	return strtolower(str_replace(" ", "_", preg_replace("/[^\w\s]+/", "", $productName)));
}


?>
