<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class OrderItem extends Core_Database {

	//news propoerties
	public $id;
	public $orderId;
	public $productId;
	public $productQty;
	public $productPrice;



	public $error = array();
	public $data_array = array();

	//constructor
	public function OrderItem() {
		try {
			parent::connect();
		} catch (Exception $exc) {
			throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
		}
	}

	/*     * '
	 * @name         :   addOrderItem
	 * @param        :   categoryObject
	 * Description   :   The function is to category details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   29-08-2012
	 * Modified By   :   -
	 * Modified On   :   -
	 */

	public function addOrderItem() {
		$recordId = null;
		try {
			$id = $this->id;
			$orderId = $this->orderId;
			$productId	=	$this->productId;
			$productQty	=	$this->productQty;
			$productPrice = $this->productPrice;

			$inserted = $this->insert('tbl_order_items', array($id, $orderId,$productId,$productQty,$productPrice));
			if ($inserted) {
				$recordId = $this->getLastInsertedId();
			}
			return $recordId;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}

	/*     * '
	 * @name         :   editOrderItem
	 * @param        :   NewsObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   28-08-2012
	 * Modified By   :   -
	 * Modified On   :   -
	 */

	public function editOrderItem() {
		$isUpdated = false;
		try {
			$id = $this->id;
			$orderId = $this->orderId;
			$productId	=	$this->productId;
			$productQty	=	$this->productQty;
			$productPrice = $this->productPrice;

			$arrayData = array(
                    'order_id' => $orderId,
                    'productId' => $productId,
                    'productQty' => $productQty,
                    'product_price' => $productPrice
			);
			//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
			$arrWhere = array("id = '" . $id . "'");
			$isUpdated = $this->update('tbl_order_items', $arrayData, $arrWhere);
			return $isUpdated;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}

	/*     * '
	 * @name         :   deleteOrderItem
	 * @param        :   OrderItemObject
	 * Description   :   The function is to delete OrderItem details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   29-08-2012
	 * Modified By   :   -
	 * Modified On   :   -
	 */

	public function deleteOrderItem() {
		$isDeleted = false;
		try {
			if ($this->connect()) {
				$id = $this->id;
				$arrWhere = array("id = '" . $id . "'");
				$isDeleted = $this->delete('tbl_order_items', $arrWhere);
			}
			return $isDeleted;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}

	/*     * '
	 * @name         :   getOrderItem
	 * @param        :   Integer (OrderItem ID)
	 * Description   :   The function is to get a OrderItem details
	 * @return       :   OrderItem Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   29-08-2012
	 * Modified By   :   -
	 * Modified On   :   -
	 */

	public function getOrderItem($orderItemId) {
		$objOrderItem = new stdClass();
                    $insProduct = new Product();$insProduct->tb_name = 'tbl_product';
		try {
			if ($this->connect()) {
				$colums = '*';
				$where = 'id = ' . $orderItemId;
				$this->select('tbl_order_items', $colums, $where);
				$orderItemInfo = $this->getResult();

				$objOrderItem->id = $orderItemInfo['id'];
				$objOrderItem->orderId = $orderItemInfo['order_id'];
				$objOrderItem->productId = $orderItemInfo['productId'];
				$objOrderItem->productQty = $orderItemInfo['productQty'];
				$objOrderItem->productPrice = $orderItemInfo['product_price'];
                                
                                //get order details
                                $objOrderItem->productDtls = $insProduct->getProduct($objOrderItem->productId);
                                
			}
                       // print_r($objOrderItem);
			return $objOrderItem;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}

	/*     * '
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all category details
	 * @return       :   Array (Array Of Page Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   28-08-2012
	 * Modified By   :   -
	 * Modified On   :   -
	 */

	public function getAll($orderId) {
		$arrOrderItems = array();
		try {
			if ($this->connect()) {
				$colums = 'id';
				$where = "order_id = '" . $orderId . "'";
				$orderBy = "id ASC";
				$this->select('tbl_order_items', $colums, $where, $orderBy);
				$orderItemResult = $this->getResult();
				foreach ($orderItemResult As $orderItemRow) {
					$orderItemId = $orderItemRow['id'];
					$orderItemInfo = $this->getOrderItem($orderItemId);
					array_push($arrOrderItems, $orderItemInfo);
				}
			}

			return $arrOrderItems;
		} catch (Exception $e) {
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
		}
	}



}
?>
