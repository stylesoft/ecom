<?php

/**
 * @name        Image
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */
class Image extends Core_Database {

    // declare variables................
    public $recordId;
    public $recordText;
    public $recordListingId;
    public $imageObject;
    public $imageObjectKeyId;
    public $tb_name = 'tbl_images';
    public $image_title;
    public $image_description;
    public $image_link;
    public $searchStr = '';
    public $specialTag;

    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /*     * '
     * @name         :   addImage
     * @param        :   ImageObject
     * Description   :   The function is to add a new page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function addImage() {
        $recordId = null;
        try {
                $recordId = $this->recordId;
                $recordText = $this->recordText;
                $recordListingId = $this->recordListingId;
                $imageObject = $this->imageObject;
                $image_title = $this->image_title;
                $image_description = $this->image_description;
                $image_link = $this->image_link;
                $imageObjectKey = $this->imageObjectKeyId;
                $specialTag     = $this->specialTag;

                $inserted = $this->insert("tbl_images", array($recordId, $recordText, $recordListingId, $imageObject, $image_title, $image_description, $image_link, $imageObjectKey,$specialTag));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editImage
     * @param        :   ImageObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function editImage($id) {
        $isUpdated = false;
        try {

            if ($this->connect()) {
                $image_title = $this->image_title;
                $image_description = $this->image_description;
                $image_link = $this->image_link;
                $specialTag     = $this->specialTag;

                $arrayData = array(
                    'image_title' => $image_title,
                    'image_description' => $image_description,
                    'image_link' => $image_link,
                    'special_tag' => $specialTag
                );

                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("recordId 	 = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
                return $isUpdated;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteImage
     * @param        :   ImageObject
     * Description   :   The function is to add a new page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */
    public function deleteImage($recordId) {
        $isDeleted = false;
        try {

            $arrWhere = array("recordId = '" . $recordId . "'");
            $isDeleted = $this->delete('tbl_images', $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @desc : delete images for given Image Gallery object
     * @param :  image_object , image_object_keyId
     * @author : Gayan Chathuranga <chathurangagc@gmail.com>
     */
    public function deleteImageByObject($imgObj, $objKeyId) {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $arrWhere = array("image_object_keyId = '" . $objKeyId . "' AND image_object = '" . $imgObj . "'");
                $isDeleted = $this->delete('tbl_images', $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getImage
     * @param        :   Integer (Image ID)
     * Description   :   The function is to get a page details
     * @return       :   Image Object
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function getImage($recordId) {
        $objImage = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'recordId = ' . $recordId;
                $this->select('tbl_images', $colums, $where);
                $imageInfo = $this->getResult();

                $objImage->recordId = $imageInfo['recordId'];
                $objImage->recordText = $imageInfo['recordText'];
                $objImage->recordListingId = $imageInfo['recordListingId'];
                $objImage->imageObject = $imageInfo['image_object'];
                $objImage->image_title = $imageInfo['image_title'];
                $objImage->image_description = $imageInfo['image_description'];
                $objImage->image_link = $imageInfo['image_link'];

                $objImage->imageObjectKeyId = $imageInfo['image_object_keyId'];
                $objImage->specialTag = $imageInfo['special_tag'];
            }
            return $objImage;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>getImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /*     * '
     * @name         :   getImage
     * @param        :   Integer (Image ID)
     * Description   :   The function is to get a page details
     * @return       :   Image Object
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function getMainImage($imgObject = null, $objKeyId = null) {
       $arrImag = array();
        $arrWhere = array();
        try {

            $SQL = "SELECT * FROM tbl_images ";
            if ($imgObject != null) {
                array_push($arrWhere, "image_object = '" . $imgObject . "'");
            }

            if ($objKeyId != null) {
                array_push($arrWhere, "image_object_keyId = '" . $objKeyId . "'");
            }

            array_push($arrWhere, "recordListingId = 1");
            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            //  echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $imgRes = $this->getResult();
            foreach ($imgRes As $imgRow) {
                $recordId = $imgRow['recordId'];
                $imageInfo = $this->getImage($recordId);
                array_push($arrImag, $imageInfo);
            }
            return $arrImag;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>getAllByImageObject()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   updateOrder
     * @param        :   RestaurantMenuObject
     * Description   :   The function is to edit a Restaurant Menu details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function updateOrder() {
        $isUpdated = false;
        try {

            if ($this->connect()) {
                $recordId = $this->recordId;
                $recordListingId = $this->recordListingId;
                $arrayData = array('recordListingId' => $recordListingId);
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("recordId = '" . $recordId . "'");
                $isUpdated = $this->update('tbl_images', $arrayData, $arrWhere);

                return $isUpdated;
            }
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByImageObject
     * @param        :
     * Description   :   The function is to get all images for object
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function getAllByImageObject($imgObject = null, $objKeyId = null) {
        $arrImages = array();
        $arrWhere = array();
        try {

            $SQL = "SELECT * FROM tbl_images ";
            if ($imgObject != null) {
                array_push($arrWhere, "image_object = '" . $imgObject . "'");
            }

            if ($objKeyId != null) {
                array_push($arrWhere, "image_object_keyId = '" . $objKeyId . "'");
            }


            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);



            $SQL.= ' ORDER BY recordListingID Asc';

            //  echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $imgRes = $this->getResult();
            foreach ($imgRes As $imgRow) {
                $recordId = $imgRow['recordId'];
                $imageInfo = $this->getImage($recordId);
                array_push($arrImages, $imageInfo);
            }
            return $arrImages;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>getAllByImageObject()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   12-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "	image_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if ($this->imageObject != null) {
                array_push($arrWhere, "image_object = '" . $this->imageObject . "'");
            }

            if ($this->imageObjectKeyId != null) {
                array_push($arrWhere, "image_object_keyId = '" . $this->imageObjectKeyId . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);

            $SQL.= ' ORDER BY recordListingID Asc';
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function search() {
        $arrImg = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "image_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if ($this->imageObject != '') {
                array_push($arrWhere, "image_object = '" . $this->imageObject . "'");
            }

            if ($this->imageObjectKeyId != '') {
                array_push($arrWhere, "image_object_keyId = '" . $this->imageObjectKeyId . "'");
            }
            
            if ($this->specialTag != '') {
                array_push($arrWhere, "special_tag = '" . $this->specialTag . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $imgRes = $this->getResult();
            foreach ($imgRes As $imgRow) {
                $id = $imgRow['recordId'];
                $imgInfo = $this->getImage($id);
                array_push($arrImg, $imgInfo);
            }
            return $arrImg;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>