<?php

/** 
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * 
 */
class SiteContent extends Core_Database {

    //siteContent propoerties
    public $id;
    public $name;
    public $title;
    public $displayOrder;
    public $contents;
    public $contentType;
    public $pageLink;
    public $isLive;
    public $image;
    
    public $searchStr;
    public $limit;
    public $listingOrder;
    
 

    public $error = array();
    public $data_array = array();

    //constructor

    public function SiteContent() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addSiteContent() {
        $recordId = null;
        try {
                $id = $this->id;
                $name = $this->name;
                $title = $this->title;
                $displayOrder = $this->displayOrder;
                $contents = $this->contents;
                $contentType = $this->contentType;
                $pageLink = $this->pageLink;
                $isLive = $this->isLive;
                
                $inserted = $this->insert($this->tb_name, array($id,$name,$title,$displayOrder,$contents,$contentType,$pageLink,$isLive));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editSiteContent
     * @param        :   SiteContentObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editSiteContent() {
        $isUpdated = false;
        try {

                $id = $this->id;
                $name = $this->name;
                $title = $this->title;
                $displayOrder = $this->displayOrder;
                $contents = $this->contents;
                $contentType = $this->contentType;
                $pageLink = $this->pageLink;
                $isLive = $this->isLive;

                $arrayData = array(
                    'name' => $this->name,
                    'title' => $this->title,
                    'display_order' => $this->displayOrder,
                    'contents' => $this->contents,
                    'content_type' => $this->contentType,
                    'page_link' => $this->pageLink,
                    'is_live' => $this->isLive
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /**'
	 * @name         :   updateOrder
	 * @param        :   SiteContentObject
	 * @desc   :   The function is to edit a SiteContent Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayOrder 			= $this->displayOrder;                               
				$arrayData          = array('display_order'=>$displayOrder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}

        
        

    /** '
     * @name         :   deleteSiteContent
     * @param        :   SiteContentObject
     * @desc   :   The function is to delete siteContent details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteSiteContent() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getSiteContent
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a SiteContent details
     * @return       :   SiteContent Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getSiteContent($siteContentId) {
        $objSiteContent = new stdClass();
        $objImage       = new Image();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = "id = '$siteContentId'";
                $this->select($this->tb_name, $colums, $where);
                $siteContentInfo = $this->getResult();
                if(count($siteContentInfo)>0){
                $objSiteContent->id = $siteContentInfo['id'];
                $objSiteContent->name = $siteContentInfo['name'];
                $objSiteContent->title = $siteContentInfo['title'];
                $objSiteContent->displayOrder = $siteContentInfo['display_order'];
                $objSiteContent->contents = $siteContentInfo['contents'];
                $objSiteContent->contentType = $siteContentInfo['content_type'];
                $objSiteContent->pageLink = $siteContentInfo['page_link'];
                $objSiteContent->isLive = $siteContentInfo['is_live'];
                $objSiteContent->image = $objImage->getMainImage('Content', $siteContentId);
                }
            }
            return $objSiteContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all siteContent details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll($content_type) {
        $arrSiteContent = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "content_type= '$content_type' AND is_live = 'yes'";
                $orderBy = " display_order ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $siteContentResult = $this->getResult();
                foreach ($siteContentResult As $siteContentRow) {
                    $siteContentId = $siteContentRow['id'];
                    $siteContentInfo = $this->getSiteContent($siteContentId);
                    array_push($arrSiteContent, $siteContentInfo);
                }
            }

            return $arrSiteContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllByStatus
     * @param        :
     * @desc   :   The function is to get all SiteContent details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrSiteContent = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "siteContent_status = '" . $status . "'";
                $orderBy = " added_date Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $siteContentRes = $this->getResult();

                foreach ($siteContentRes As $nIndex => $siteContentRow) {
                    $siteContentId = $siteContentRow['id'];
                    $siteContentInfo = $this->getSiteContent($siteContentId);
                    array_push($arrSiteContent, $siteContentInfo);
                }
            }
            return $arrSiteContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :Restaurant Menu
     * @desc         :   The function is to count the SiteContent details
     * @return       :   Integer (Total number Of SiteContent)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "siteContent_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $siteContentRes = $this->getResult();
            $totalNumberOfRec = count($siteContentRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrSiteContent = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }
            
            
            if ($this->contentType != '') {
                array_push($arrWhere, "content_type = '" .$this->contentType. "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $siteContentRes = $this->getResult();
            foreach ($siteContentRes As $siteContentRow) {
                $siteContentId = $siteContentRow['id'];
                $siteContentInfo = $this->getSiteContent($siteContentId);
                array_push($arrSiteContent, $siteContentInfo);
            }
            return $arrSiteContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     
}
?>

