<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Stock extends Core_Database {

    //news propoerties
    public $id;
    public $menu_id;
    public $product_id;
    public $stock;
  public $parent_menu_id;
    
  
    
    
    
    
    
    
    public $error = array();
    public $data_array = array();

    //constructor
    public function Stock() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCategory
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Zumry deen
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addStock() {
        $recordId = null;
        try {
        	

                $id = $this->id;
                $menu_id= $this->menu_id;
                $product_id = $this->product_id;
                $stock= $this->stock;
                $parent_menu_id = $this->parent_menu_id;
                //$product_id = $this->product_id;
          

                $inserted = $this->insert($this->tb_name, array($id,$menu_id,$parent_menu_id,$product_id,$stock));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    
    /*     * '
     * @name         :  Get superparent from stock
    * @param        :   categoryObject
    * Description   :   The function is to category details
    * @return       :   boolean
    * Added By      :   Zumry deen
    * Added On      :   29-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function superparent($productId) {
         $arrMenu = array();
        $arrWhere = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT DISTINCT parent_menu_id FROM tbl_stock";
                array_push($arrWhere, "product_id = '" .$ProId. "'");                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

              
                
                foreach ($catRes As $nIndex => $catRow) {
                    $catId       = $catRow['parent_menu_id'];
                    $catInfo     = $this->getParentMenu($catId);
                    array_push($arrMenu, $catInfo);
                }

                
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    
    
    
    
    
    /*     * '
     * @name         :  Get Sub Mneu details for Edit
    * @param        :   categoryObject
    * Description   :   The function is to category details
    * @return       :   boolean
    * Added By      :   Zumry deen
    * Added On      :   29-08-2013
    * Modified By   :   -
    * Modified On   :   -
    *
    */
     
    public function get_parentMenu($productId) {

    	
    	$arrMenu = array();
    	try {
    		if ($this->connect()) {
    			//$colums = 'DISTINCT parent_menu_id';
    			 $sql = 'select DISTINCT parent_menu_id from tbl_stock where product_id ='.$productId;
    			//$where = 'product_id = ' . $productId;
    			// $orderBy = "menu_name ASC";
    			$this->executeSelectQuery($sql);
    			$catRes = $this->getResult();
    	
    			
    			
    			foreach ($catRes As $Index => $catRow) {
    				
    				
    				
    				 $catId = $catRow['parent_menu_id'];
    				//$catInfo = $this->getMenu($catId);
    				array_push($arrMenu, $catId);
    			}
    		}
    		
    		
    		return $arrMenu;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}

    	}
    	 
    	 
    	
    	/*     * '
    	 * @name         :  Get Sub Mneu details for Edit 
    	* @param        :   categoryObject
    	* Description   :   The function is to category details
    	* @return       :   boolean
    	* Added By      :   Zumry deen
    	* Added On      :   29-08-2013
    	* Modified By   :   -
    	* Modified On   :   -
    	*
    	*/
    	
    	public function get_stock($productId) {
    		$arrMenu = array();
    		try {
    			if ($this->connect()) {
    				$colums = 'id';
    				$where = 'product_id = ' . $productId;
    				//$where = "product_id  = '" . $productId . "'";
    				//$orderBy = "menu_name ASC";
    				$this->select('tbl_stock', $colums, $where, $orderBy);
    				$catRes = $this->getResult();
    	
    				foreach ($catRes As $nIndex => $catRow) {
    					$catId = $catRow['id'];
    					$catInfo = $this->getMenu($catId);
    					array_push($arrMenu, $catInfo);
    	
    					//print_r($arrMenu);
    				}
    			}
    			return $arrMenu;
    		} catch (Exception $e) {
    			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    		}}
    	
    	
    	
    
    
    /*     * '
     * @name        :  EditMenuFiled
    * @param        :   NewsObject
    * Description   :   The function is to edit a page details
    * @return       :   boolean
    * Added By      :
    * Added On      :   28-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function editMenuFiled() {
    	$isUpdated = false;
    	try {	
    		$id = $this->id;
    		$menu_id= $this->menu_id;
    		$product_id = $this->product_id;
    		$field_value = $this->field_value;
    		$stock = $this->stock;
    		$product_id = $this->product_id;
    		
    		$arrayData = array(
    				'id' => $id,
    				//'menu_id' => $menu_id,
    				'product_id' => $product_id,
    				'field_value' => $field_value,
    				//'stock' => $stock,
    				//'product_id' => $product_id
    		);
    		//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
    		$arrWhere = array("id = '" . $id . "'");
    		$isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
    		return $isUpdated;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   
     * Added On      :   28-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editMenu() {
        $isUpdated = false;
        try {
                $id = $this->id;
                $menu_name = $this->menu_name;
                $status = $this->status;
                $display_order = $this->display_order;
    

                $arrayData = array(
                    'id' => $id,
                    'menu_name' => $menu_name,
                		'display_order' => $display_order,
                    'status' => $status
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteMenu() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    /*     * '
     * @name         :   getFieldMenu
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getMenu($catId) {
        $objMenu = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $catId;
                $this->select('tbl_stock', $colums, $where);
                $menunfo = $this->getResult();
              
                
                $objMenu->id = $menunfo['id'];
                $objMenu->menu_id = $menunfo['menu_id'];
                $objMenu->product_id = $menunfo['product_id'];
                $objMenu->stock = $menunfo['stock'];
                $objMenu->parent_menu_id = $menunfo['parent_menu_id'];
                //$objMenu->stock = $menunfo['stock'];
                //$objMenu->product_id = $menunfo['product_id'];
             
     
            }
            return $objMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   28-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getParentMenu($productId) {
        $arrSubCategory = array();
        $arrWhere = array();
         try { 
             
             if ($this->connect()) {
                $colums = 'id';
                
                $SQL = "SELECT DISTINCT parent_menu_id FROM tbl_stock ";
                
                if($productId != null){
                    array_push($arrWhere, "product_id = '$productId'");
                }
                
                
            
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
                 $dbResult = $this->executeSelectQuery($SQL);
                 $subCatRes = $this->getResult();
                 array_push($arrSubCategory, $subCatRes);
                // print_r($subCatRes);

             
            }
             
            return $arrSubCategory;
             
         } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
        
        
    }
    
    
    /*     * '
     * @name         :   Get menu form menu data
    * @param        :
    * Description   :   The function is to get all category details
    * @return       :   Array (Array Of Page Object)
    * Added By      :
    * Added On      :   28-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    
    public function getMenuFileds($catId) {
    	$objMenu = new stdClass();
    	try {
    		if ($this->connect()) {
    			$colums = '*';
    			$where = 'menu_id = ' . $catId;
    			$this->select('tbl_menu_data', $colums, $where);
    			$menunfo = $this->getResult();
    
    
    			$objMenu->id = $menunfo['id'];
    			//$objMenu->menu_id = $menunfo['menu_id'];
    			$objMenu->field_name = $menunfo['field_name'];
    			//$objMenu->field_value = $menunfo['field_value'];
    			//$objMenu->stock = $menunfo['stock'];
    			//$objMenu->product_id = $menunfo['product_id'];
    			 
    			 
    		}
    		return $objMenu;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    /*     * '
     * @name         :   getmenu exisit optiona
    * @param        :
    * Description   :   The function is to get all category details
    * @return       :   Array (Array Of Page Object)
    * Added By      :
    * Added On      :   28-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    
    public function getEditMenuoption($productId,$menuID) {
    	$arrMenu = array();
    	try {
    		if ($this->connect()) {
    			$colums = 'id';
    			$where = "parent_menu_id = '$menuID' AND product_id  = '" . $productId . "'";
    			//$orderBy = "menu_name ASC";
    			$this->select($this->tb_name, $colums, $where, $orderBy);
    			$catRes = $this->getResult();
    
    			
    			
    			foreach ($catRes As $nIndex => $catRow) {
    				$catId = $catRow['id'];
    				$catInfo = $this->getMenu($catId);
    				array_push($arrMenu, $catInfo);
    			}
    		}
    		return $arrMenu;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
    
    
    
    
    public function getAll() {
        $arrCatgry = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
               // $where = 'status = 1';
                $orderBy = "tbl_menu";
                $this->select('tbl_menu_data', $colums);
                $categoryResult = $this->getResult();
                
                //print_r($categoryResult);
                
                foreach ($categoryResult As $catRow) {
                    $catId = $catRow['id'];
                    $menudatainfo = $this->getMenu($catId);
                    
                   // print_r($menudatainfo);
                    
                    array_push($arrCatgry, $menudatainfo );
                }
            }

            return $arrCatgry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentMenu($menuID) {
        $arrMenu = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
             
                $where = "parent_menu_id = '$menuID'";
               // $orderBy = "menu_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();
                
                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getMenu($catId);
                    array_push($arrMenu, $catInfo);
                }
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    /*     * '
     * @name         :   getAllByStatus
    * @param        :
    * Description   :   The function is to get all category details by status
    * @return       :   Array (Array Of Page Object)
    * Added By      :
    * Added On      :   29-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function getAllByProduct($productId) {
    	$arrMenu = array();
    	try {
    		if ($this->connect()) {
    			$colums = 'id';
    			$where = 'product_id = ' . $productId;
    			//$where = "product_id  = '" . $productId . "'";
    			//$orderBy = "menu_name ASC";
    			$this->select('tbl_stock', $colums, $where, $orderBy);
    			$catRes = $this->getResult();
    
    			foreach ($catRes As $nIndex => $catRow) {
    				$catId = $catRow['id'];
    				$catInfo = $this->getMenu($catId);
    				array_push($arrMenu, $catInfo);
    				
    				//print_r($arrMenu);
    			}
    		}
    		return $arrMenu;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}}
    
    
    
    
    /*     * '
     * @name         :   Update Edit Field
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllEdit($menu_id) {
        $arrMenu = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
                array_push($arrWhere, "menu_id = '" . $menu_id . "'");
       if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $menunfo = $this->getMenu($id);
                array_push($arrMenu, $menunfo);
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    /**'
     * @name         :   Stock Update 
    * @param        :   ProductObject
    * @desc   :   The function is to edit a Product Listing order
    * @return       :   boolean
    * Added By      :   Zumry Deen
    * Added On      :   20-01-2014
    * Modified By   :
    * Modified On   :   20-01-2014
    */
    
    public function Updatestock($id) {
    	
    	
    	$isUpdated = false;
    	try {
    		//$id = $this->id;
    		$stock= $this->stock;
    		$arrayData = array(
    
    				'stock' => $stock
    
    
    		);
    		//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
    		$arrWhere = array("id = '" . $id . "'");
    		$isUpdated = $this->update('tbl_stock', $arrayData, $arrWhere);
    		return $isUpdated;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2013
	 * Modified By   :   
	 * Modified On   :   04-09-2013
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayorder 			= $this->displayorder;                               
				$arrayData          = array('display_order'=>$displayorder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   19-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByCategoryObject($param = '') {
        $arrMenu = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "' AND status = '" . $status . "'";
            $orderBy = "menu_name ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $catRes = $this->getResult();

            foreach ($catRes As $nIndex => $catRow) {
                $catId = $catRow['id'];
                $catInfo = $this->getFieldMenu($catId);
                array_push($arrMenu, $catInfo);
            }

            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   
     * Added On      :   12-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   menu_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrMenu = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $menunfo = $this->getFieldMenu($id);
                array_push($arrMenu, $menunfo);
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    
    
    // get all subcategory....
    public function getAllSubCategorypage($menuID){
        $arrSubCategory = array();
        $arrWhere = array();
         try { 
             
             if ($this->connect()) {
                $colums = 'id';
                
                $SQL = "SELECT * FROM tbl_menu_data ";
                
                if($maincategoryId != null){
                    array_push($arrWhere, "menu_id = '$menuID'");
                }
                
                
                if($categoryStatus != null){
                    array_push($arrWhere, "status = '" .$categoryStatus. "'");
                }
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
                 $dbResult = $this->executeSelectQuery($SQL);
                 $subCatRes = $this->getResult();
          

                foreach ($subCatRes As $nIndex => $subCatRow) {
                    $subCatId       = $subCatRow['id'];
                    $subCatInfo     = $this->getFieldMenu($subCatId);
                    array_push($arrSubCategory, $subCatInfo);
                }
            }
             
            return $arrSubCategory;
             
         } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
        
        
    }
    
    
    
    
     /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentCategory($status = null) {
        $arrMenu = array();
        $arrWhere = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT * FROM tbl_category ";
                array_push($arrWhere, "parent_category = 0");
                
                if ($this->searchStr != '') {
                	array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
                }
                
                if($status != null){
                    array_push($arrWhere, "status = '" .$status. "'");
                }
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
          
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId       = $catRow['id'];
                    $catInfo     = $this->getFieldMenu($catId);
                    array_push($arrMenu, $catInfo);
                }

                
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    /*     * '
     * @name         :   getParentCategory
     * @param        :
     * Description   :   The function is to get parent category id
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getParentCategory($childCategory) {
        $arrMenu = array();
        $parentCatId = 0;
        try {
            if ($this->connect()) {
                $SQL = "SELECT parent_category FROM tbl_category WHERE id = 0";
                
          
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $parentCatId       = $catRow['parent_category'];
                }

                
            }
            return $parentCatId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>