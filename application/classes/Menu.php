<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Menu extends Core_Database {

    //news propoerties
    public $id;
    public $menu_name;
    public $parent_menu;
    public $status;
    
    
    // menu field id
    
    
    public $error = array();
    public $data_array = array();

    //constructor
    public function Menu() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCategory
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Zumry deen
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addMenu() {
        $recordId = null;
        try {
        	
                $id = $this->id;
                $menu_name = $this->menu_name;
                $parent_menu= $this->parent_menu;
                $status = $this->status;
          

                $inserted = $this->insert($this->tb_name, array($id,$menu_name,$parent_menu,$status));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    
    /*     * '
     * @name         :   addCategory
    * @param        :   categoryObject
    * Description   :   The function is to category details
    * @return       :   boolean
    * Added By      :   Zumry deen
    * Added On      :   29-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function addMenuField() {
    	$recordId = null;
    	try {
    		 
    		$id = $this->id;
    		$menu_name = $this->menu_name;
    		$parent_menu= $this->parent_menu;
    		$status = $this->status;
    
    
    		$inserted = $this->insert('tbl_menu_data', array($id,$menu_name,$parent_menu,$status));
    		if ($inserted) {
    			$recordId = $this->getLastInsertedId();
    		}
    		return $recordId;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   
     * Added On      :   28-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editMenu() {
        $isUpdated = false;
        try {
                $id = $this->id;
                $menu_name = $this->menu_name;
                $status = $this->status;
                $parent_menu = $this->parent_menu;
    

                $arrayData = array(
                    'id' => $id,
                    'menu_name' => $menu_name,
                		'parent_menu' => $parent_menu,
                    'status' => $status
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteMenu() {
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    /*     * '
     * @name         :   getMenu
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getMenu($catId) {
        $objMenu = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $catId;
                $this->select('tbl_menu', $colums, $where);
                $menunfo = $this->getResult();
                $objMenu->id = $menunfo['id'];
                $objMenu->menu_name = $menunfo['menu_name'];
                
                $objMenu->status = $menunfo['status'];
                $objMenu->parent_menu = $menunfo['parent_menu'];
     
            }
            return $objMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    
    
    
    
    
    
    
    
    
    
    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   28-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrCatgry = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
               // $where = 'status = 1';
                $orderBy = "menu_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $categoryResult = $this->getResult();
                foreach ($categoryResult As $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getMenu($catId);
                    array_push($arrCatgry, $catInfo);
                }
            }

            return $arrCatgry;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrMenu = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
             
                $where = "status = '" . $status . "'";
                $orderBy = "menu_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();
                
                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getMenu($catId);
                    array_push($arrMenu, $catInfo);
                }
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentByStatus($status,$cat_id) {
        $arrMenu = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "id != '".$cat_id."' AND status = '" . $status . "'";
                $orderBy = "menu_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId = $catRow['id'];
                    $catInfo = $this->getMenu($catId);
                    array_push($arrMenu, $catInfo);
                }
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    
    
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2013
	 * Modified By   :   
	 * Modified On   :   04-09-2013
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayorder 			= $this->displayorder;                               
				$arrayData          = array('parent_menu'=>$displayorder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   19-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByCategoryObject($param = '') {
        $arrMenu = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "' AND status = '" . $status . "'";
            $orderBy = "menu_name ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $catRes = $this->getResult();

            foreach ($catRes As $nIndex => $catRow) {
                $catId = $catRow['id'];
                $catInfo = $this->getMenu($catId);
                array_push($arrMenu, $catInfo);
            }

            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   
     * Added On      :   12-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   menu_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrMenu = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $menunfo = $this->getMenu($id);
                array_push($arrMenu, $menunfo);
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }
    
    
    
    
    // get all subcategory....
    public function getAllSubCategory($maincategoryId = null){
        $arrSubCategory = array();
        $arrWhere = array();
         try { 
             
             if ($this->connect()) {
                $colums = 'id';
                
                $SQL = "SELECT * FROM tbl_menu ";
                
                if($maincategoryId != null){
                    array_push($arrWhere, "parent_menu = '" .$maincategoryId. "'");
                }
                
                
 
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
                 $dbResult = $this->executeSelectQuery($SQL);
                 $subCatRes = $this->getResult();
          

                foreach ($subCatRes As $nIndex => $subCatRow) {
                    $subCatId       = $subCatRow['id'];
                    $subCatInfo     = $this->getMenu($subCatId);
                    array_push($arrSubCategory, $subCatInfo);
                }
            }
             
            return $arrSubCategory;
             
         } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
        
        
    }
    
    
    

    /*     * '
     * @name         :   getSeconLevelParentByStatus
    * @param        :
    * Description   :   The function is to get all category details by status
    * @return       :   Array (Array Of Page Object)
    * Added By      :
    * Added On      :   29-08-2013
    * Modified By   :   -
    * Modified On   :   -
    */
    
    public function getSecondLevelParent($status = null) {
    	$arrMenu = array();
    	$arrWhere = array();
    	try {
    		if ($this->connect()) {
    			$SQL = "SELECT * FROM tbl_menu ";
    			array_push($arrWhere, "parent_menu != 0");
    
    			if ($this->searchStr != '') {
    				array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
    			}
    
    			if($status != null){
    				array_push($arrWhere, "status = '" .$status. "'");
    			}
    
    			if (count($arrWhere) > 0)
    				$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    
    			$dbResult = $this->executeSelectQuery($SQL);
    			$catRes = $this->getResult();
    
    			foreach ($catRes As $nIndex => $catRow) {
    				$catId       = $catRow['id'];
    				$catInfo     = $this->getMenu($catId);
    				array_push($arrMenu, $catInfo);
    			}
    
    
    		}
    		return $arrMenu;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
     /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   
     * Added On      :   29-08-2013
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentCategory($status = null) {
        $arrMenu = array();
        $arrWhere = array();
        try {
            if ($this->connect()) {
                $SQL = "SELECT * FROM tbl_menu ";
                array_push($arrWhere, "parent_menu = 0");
                
                if ($this->searchStr != '') {
                	array_push($arrWhere, "menu_name LIKE '" . "%" . $this->searchStr . "%" . "'");
                }
                
                if($status != null){
                    array_push($arrWhere, "status = '" .$status. "'");
                }
                
                if (count($arrWhere) > 0)
                    $SQL.= "WHERE " . implode(' AND ', $arrWhere);
                
                
          
                $dbResult = $this->executeSelectQuery($SQL);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $catId       = $catRow['id'];
                    $catInfo     = $this->getMenu($catId);
                    array_push($arrMenu, $catInfo);
                }

                
            }
            return $arrMenu;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    

}
?>