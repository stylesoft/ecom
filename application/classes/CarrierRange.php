<?php
/**
 * @name        CarrierRanges
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class CarrierRange extends Core_Database{

	/**'
	 * @name         :   addCarrierRange
	 * @param        :   CarrierRangesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addCarrierRange($objCarrierRange){
		$recordId = null;
		try{
			if($this->connect()){
				$id 			= $objCarrierRange->id;
				$carrierTypeId 		= $objCarrierRange->carrierTypeId;
				$rangeFrom 		= $objCarrierRange->rangeFrom;
				$rangeTo 		= $objCarrierRange->rangeTo;
				$rangePrice             = $objCarrierRange->rangePrice;
				
				$inserted = $this->insert('tbl_carrier_range',array($id,$carrierTypeId,$rangeFrom,$rangeTo,$rangePrice)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>addCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editCarrierRanges
	 * @param        :   CarrierRangesObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editCarrierRange($objCarrierRange){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 			= $objCarrierRange->id;
				$carrierTypeId 		= $objCarrierRange->carrierTypeId;
				$rangeFrom 		= $objCarrierRange->rangeFrom;
				$rangeTo 		= $objCarrierRange->rangeTo;
				$rangePrice             = $objCarrierRange->rangePrice;
                                
				$arrayData          = array('tbl_carrier_type_id'=>$carrierTypeId,'range_from'=>$rangeFrom,'range_to'=>$rangeTo,'range_price'=>$rangePrice);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
				$isUpdated = $this->update('tbl_carrier_range',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>addCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteCarrierRanges
	 * @param        :   CarrierRangesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteCarrierRange($objCarrierRange){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 	   = $objCarrierRange->id;
				$arrWhere  = array("id = '" . $id . "'");
				$isDeleted = $this->delete('tbl_carrier_range',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>addCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getCarrierRanges
	 * @param        :   Integer (CarrierRanges ID)
	 * Description   :   The function is to get a page details
	 * @return       :   CarrierRanges Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getCarrierRange($id){
		$objCarrierRange = new stdClass();
                $objCarrierType  = new CarrierType();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'id = '.$id;
				$this->select('tbl_carrier_range',$colums,$where);
				$carrierRangeInfo = $this->getResult();
                                if(count($carrierRangeInfo)>0){
                                    $objCarrierRange->id                = $carrierRangeInfo['id'];
                                    $objCarrierRange->carrierTypeId     = $carrierRangeInfo['tbl_carrier_type_id'];
                                    $objCarrierRange->rangeFrom         = $carrierRangeInfo['range_from'];
                                    $objCarrierRange->rangeTo           = $carrierRangeInfo['range_to'];
                                    $objCarrierRange->rangePrice        = $carrierRangeInfo['range_price'];
                                    $objCarrierRange->carrierType       = $objCarrierType->getCarrierType($carrierRangeInfo['tbl_carrier_type_id']);
                                }
			}
			return $objCarrierRange;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>getCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of CarrierRanges Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrCarrierRanges       = array();
		try{
			if($this->connect()){
				$colums = 'id';
				$where  = null;
                                $orderBy = " id Asc";
				$this->select('tbl_carrier_range',$colums,$where,$orderBy);
				$carrierRangeRes = $this->getResult();
				foreach($carrierRangeRes As $carrierRow){
					$carrierRangeId = $carrierRow['id'];
					$carrierRangeInfo = $this->getCarrierRange($carrierRangeId);
					array_push($arrCarrierRanges,$carrierRangeInfo);
				}
					
			}
			return $arrCarrierRanges;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>getCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of CarrierRanges Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByRange($rangeValue){
		$arrCarrierRanges       = array();
		try{
			if($this->connect()){
                            
                            $SQL = "SELECT id FROM tbl_carrier_range WHERE range_from >= '".$rangeValue."' AND (range_to is not NULL AND range_to != '')";
                            $dbResult = $this->executeSelectQuery($SQL);
                            $dataRes = $this->getResult();
                            foreach ($dataRes As $dataRow) {
                                $carrierRangeId = $dataRow['id'];
					$carrierRangeInfo = $this->getCarrierRange($carrierRangeId);
					array_push($arrCarrierRanges,$carrierRangeInfo);
                            }
                            
                            $SQL_2 = "SELECT id FROM tbl_carrier_range WHERE (range_to is NULL or range_to = '') LIMIT 0,1";
                            
                            $dbResult2 = $this->executeSelectQuery($SQL_2);
                            $dataRes2 = $this->getResult();
                         
                            foreach ($dataRes2 As $dataRow2) {
                                $carrierRangeId = $dataRow2['id'];
				$carrierRangeInfo = $this->getCarrierRange($carrierRangeId);
				array_push($arrCarrierRanges,$carrierRangeInfo);
                            }
	
			}
                        
			return $arrCarrierRanges;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>getCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	
	
	/**'
	 * @name         :   getAll
	* @param        :
	* Description   :   The function is to get all page details
	* @return       :   Array (Array Of CarrierRanges Object)
	* Added By      :   Iyngaran Iyathurai
	* Modified By   :   Zumry deen
	* Modified On   :   05-08-2013
	*/
	 
public function getDefult($rangeValue){
		$arrCarrierRanges       = array();
		try{
			if($this->connect()){
	
				if($rangeValue>=5000){
					
					$SQL = "SELECT id FROM tbl_carrier_range WHERE id = 4";
					
				} else {
					$SQL = "SELECT id FROM tbl_carrier_range WHERE range_to >= $rangeValue LIMIT 1";
				}
					
					
				$SQL = "SELECT id FROM tbl_carrier_range WHERE range_to >= $rangeValue LIMIT 1";
				$dbResult = $this->executeSelectQuery($SQL);
				$dataRes = $this->getResult();
				foreach ($dataRes As $dataRow) {
					$carrierRangeId = $dataRow['id'];
					$carrierRangeInfo = $this->getCarrierRange($carrierRangeId);
					array_push($arrCarrierRanges,$carrierRangeInfo);
				}
	
				
				if($rangeValue>=5000){
						
					$SQL_2  = "SELECT id FROM tbl_carrier_range WHERE id = 4";
						
				} else {
					$SQL_2 = "SELECT id FROM tbl_carrier_range WHERE range_to >= $rangeValue LIMIT 1";
	
				}
				
				
				$dbResult2 = $this->executeSelectQuery($SQL_2);
				$dataRes2 = $this->getResult();
					
				foreach ($dataRes2 As $dataRow2) {
					$carrierRangeId = $dataRow2['id'];
					$carrierRangeInfo = $this->getCarrierRange($carrierRangeId);
					array_push($arrCarrierRanges,$carrierRangeInfo);
				}
	
			}
	
			return $arrCarrierRanges;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>CarrierRanges</em>, <strong>Function -</strong> <em>getCarrierRanges()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	
	 
}
?>
