<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Region extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $country_id;
    
    public $error = array();
    public $data_array = array();

    //constructor
    public function Region() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addRegion
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addRegion() {
        $recordId = null;
        try {
        	$id = '';
        	$region_name = ucwords(strtolower($this->name));
        	$country_id = $this->country_id;

        	$inserted = $this->insert($this->tb_name, array($id, $country_id,  $region_name));
        	if ($inserted) {
        		$recordId = $this->getLastInsertedId();
        	}
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editRegion() {
        $isUpdated = false;
        try {
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteRegion
     * @param        :   RegionObject
     * Description   :   The function is to delete Region details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteRegion() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getRegion
     * @param        :   Integer (Region ID)
     * Description   :   The function is to get a Region details
     * @return       :   Region Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getRegion($regionId) {
        $objRegion = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$regionId;
                $this->select('tbl_region', $colums, $where);
                $regionInfo = $this->getResult();

                if($regionInfo){
                    $objRegion->id = $regionInfo['id'];
                    $objRegion->name = $regionInfo['name'];
                    $objRegion->country_id = $regionInfo['country_id'];
                } else {
                	$objRegion->id = null;
                    $objRegion->name = null;
                    $objRegion->country_id = null;
                }
            }
            return $objRegion;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrRegion = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "name ASC";
                $this->select('tbl_region', $colums, $where, $orderBy);
                $regionResult = $this->getResult();
                foreach ($regionResult As $regionResultRow) {
                    $regionId = $regionResultRow['id'];
                    $regionInfo = $this->getRegion($regionId);
                    array_push($arrRegion, $regionInfo);
                }
            }

            return $arrRegion;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /*     * '
     * @name         :   getDefaultRegion
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
	/*
    public function getDefaultRegion() {
        $currencyInfo = null;
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = " is_default = 'Yes' ";
                $orderBy = "id ASC";
                $this->select('tbl_country', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getRegion($currencyId);
                }
            }

            return $currencyInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
	*/
}
?>