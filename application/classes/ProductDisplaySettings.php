<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ProductDisplaySettings extends Core_Database {

    //news propoerties
    public $id;
    public $brand_display_style;
    public $category_display_style;
   
    
    
    

    //constructor
    public function ProductDisplaySettings() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addSubFields
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

   /* public function addSubFields() {
        $recordId = null;
        try {
               
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }*/

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editProductDisplaySettings() {
        $isUpdated = false;
        try {
            $id = $this->id;
            $brand_display_style = $this->brand_display_style;
            $category_display_style = $this->category_display_style;
            
            
            
                $arrayData = array(
                   
                    'brand_display' => $brand_display_style,
                    'category_display' => $category_display_style
                    
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '1'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteSubFields
     * @param        :   SubFieldsObject
     * Description   :   The function is to delete SubFields details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    /*public function deleteSubFields() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }*/

    /*     * '
     * @name         :   getSubFields
     * @param        :   Integer (SubFields ID)
     * Description   :   The function is to get a SubFields details
     * @return       :   SubFields Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getProductDisplaySettings() {
        $objDisDisplay = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = 1';
                $this->select($this->tb_name, $colums, $where);
                $displayInfo = $this->getResult();
               
                    
                    
                   
                    $objDisDisplay->brand_display_style = $displayInfo['brand_display'];
                    $objDisDisplay->category_display_style = $displayInfo['category_display'];
                    
                 
            }
            return $objDisDisplay;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    /*public function getAll() {
        $arrSubFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_sub_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getSubFields($fieldId);
                    array_push($arrSubFields,$fieldInfo);
                }
            }

            return $arrSubFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }*/
    
    
    
        /*     * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    /*public function getAllByParentId($parentId) {
        $arrSubFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "parent_field_id = '" . $parentId . "'";
                $orderBy = "id ASC";
                $this->select('tbl_sub_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                //print_r($fieldsResult); exit;
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getSubFields($fieldId);
                    array_push($arrSubFields,$fieldInfo);
                }
            }

            return $arrSubFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }*/

}
?>