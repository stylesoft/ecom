<?php

require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'Checkout';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************


if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }
    //accType=guest
    if (isset($_GET['accType'])) {
    	$accType = $_GET['accType'];
    }
    
    if (isset($_GET['page_type'])) {
    $type = $_GET['page_type'];
    }
    $checkoutId = $_GET['pageId'];
    
    $selectedPageInfo = $pageObject->getPage(15);
    $pageId = $selectedPageInfo->id;
        $pageTitle = 'Checkout';
        $pageKeywords = 'Checkout';
        $pageDescription = 'Checkout';
        $pageBodyC = $selectedPageInfo->body;
        $pageListingID = $selectedPageInfo->listingId;
        $pageName = $selectedPageInfo->name;
        $pageIsLive = $selectedPageInfo->live;
        $isContactBoxEnable = $selectedPageInfo->isContact;

$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
$currentCustomerId = $currentCustomerInfo->id;
$objCustomer = new Customer();
$objCustomer->tb_name = ' tbl_customer';
$customerInfo = $objCustomer->getCustomer($currentCustomerId);


$billingAddress = $customerInfo->addressBillingDtls[0];
$shippingAddressInfo = $customerInfo->addressShippingDtls[0];

$cartContents = getCartContents();

$totalWeight = 0;

foreach ($cartContents As $cartContent) {


    $productWeight = $cartContent->productWeight;
    $productQty = $cartContent->productQty;
    $row_weight = $productWeight * $productQty;
    $totalWeight = $totalWeight + $row_weight;
}

$objCarrierRange = new CarrierRange();
$shippingCarrierRanges = $objCarrierRange->getAllByRange($totalWeight);

$existingShippingMethod = null;
$exitsShippingMethod = unserialize($_SESSION["shippingMethod"]);
if ($exitsShippingMethod) {
    $existingShippingMethod = $exitsShippingMethod->id;
}

$cartContents   = getCartContents();
 $cartShippingMethod = getShippingMethod();        

$CONTENT = '';
/*
if ($currentCustomerInfo != '') {
 $CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl2.php";
}
*/
if ($checkoutId == 2) {
$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl2.php";
}
elseif ($checkoutId == 3) {
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl3.php";
}

elseif ($checkoutId == 4) {
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl4.php";
}

elseif ($checkoutId == 5) {

    //get Payment Options
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl5.php";
}

elseif ($checkoutId == 6) {
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl6.php";
}

elseif ($accType == 'guest') {
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index_guest.tpl.php";
}

else{
	$CONTENT = FRONT_LAYOUT_VIEW_PATH . "checkout/index.tpl.php";
}
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>