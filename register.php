<?php
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = '';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************

  if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }
    
    if (isset($_POST['customer'])) {
    	$accountType = $_POST['customer'];
    }
    
    $selectedPageInfo = $pageObject->getPage('15');
    $pageId = $selectedPageInfo->id;
        $pageTitle = 'Register';
        $pageKeywords = $selectedPageInfo->keywords;
        $pageDescription = $selectedPageInfo->description;
        $pageBody1 = $selectedPageInfo->body;
        $pageListingID = $selectedPageInfo->listingId;
        $pageName = $selectedPageInfo->name;
        $pageIsLive = $selectedPageInfo->live;
        $isContactBoxEnable = $selectedPageInfo->isContact;


$CONTENT = '';
$CONTENT = FRONT_LAYOUT_VIEW_PATH . "customer/register.tpl.php";
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>