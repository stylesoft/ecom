<?php
require_once 'bootstrap.php';
require "includes/extLibs/class.phpmailer.php";

//print_r($_POST);exit;

// personal details ...........
$id = '';
$title = $_POST['cmbTitle'];
$firstName = $_POST['txtFirstName'];
$lastName = $_POST['txtLastName'];
$email = $_POST['txtEmail'];
$password = $_POST['txtPassword'];
$status = 'Enabled';
$addedDate = getCurrentDateTime();
$lastLoginDate = '';
$lastLoginFrom = '';

$objCustomer = new Customer();
$objCustomer->tb_name = 'tbl_customer';
$objCustomer->id = $id;
$objCustomer->title = $title;
$objCustomer->firstName = $firstName;
$objCustomer->lastName = $lastName;
$objCustomer->email = $email;
$objCustomer->password = md5($password);
$objCustomer->status = $status;
$objCustomer->addedDate = $addedDate;
$objCustomer->lastLoginDate = $lastLoginDate;
$objCustomer->lastLoginFrom = $lastLoginFrom;

$customerId = $objCustomer->addCustomer();


if ($customerId) {




// billing details

$billingAddressId = ($_POST['txtBillingAddressId'] != '' ? $_POST['txtBillingAddressId'] : ""); //$_POST['txtBillingAddressId'];
$billingAddressTitle = ($_POST['cmbBillingTitle'] != '' ? $_POST['cmbBillingTitle'] : ""); //$_POST['cmbBillingTitle'];
$billingAddressFirstName = ($_POST['txtBillingFirstName'] != '' ? $_POST['txtBillingFirstName'] : ""); //$_POST['txtBillingFirstName'];
$billingAddressLastName = ($_POST['txtBillingLastName'] != '' ? $_POST['txtBillingLastName'] : ""); //$_POST['txtBillingLastName'];
$billingAddressCompanyName = ($_POST['txtBillingCompanyName'] != '' ? $_POST['txtBillingCompanyName'] : ""); //$_POST['txtBillingCompanyName'];
$billingAddressPhoneNumber = ($_POST['txtBillingPhone'] != '' ? $_POST['txtBillingPhone'] : ""); //$_POST['txtBillingPhone'];
$billingAddressAddress1 = ($_POST['txtBillingAddress_1'] != '' ? $_POST['txtBillingAddress_1'] : ""); //$_POST['txtBillingAddress_1'];
$billingAddressAddress2 = ($_POST['txtBillingAddress_2'] != '' ? $_POST['txtBillingAddress_2'] : ""); //$_POST['txtBillingAddress_2'];
$billingAddressTown = ($_POST['txtBillingTownCity'] != '' ? $_POST['txtBillingTownCity'] : ""); //$_POST['txtBillingTownCity'];

if(trim($_POST['txtAddCountryRegion']) != ''){
	$billingAddressCountryState = trim($_POST['txtAddCountryRegion']);
	
	//Add the new region to database
	$objRegion = new Region();
	$objRegion->tb_name = 'tbl_region';
	$objRegion->name = $billingAddressCountryState;
	$objRegion->country_id = $_POST['txtAddCountryRegionCountryId'];
	$objRegion->addRegion();
	
} else{
	$billingAddressCountryState = ($_POST['txtBillingCountyState'] != '' ? $_POST['txtBillingCountyState'] : ""); //$_POST['txtBillingCountyState'];
}

$billingAddressPostal = ($_POST['txtBillingPostalZIP'] != '' ? $_POST['txtBillingPostalZIP'] : ""); //$_POST['txtBillingPostalZIP'];
$billingAddressCountry = ($_POST['txtBillingCountry'] != '' ? $_POST['txtBillingCountry'] : ""); //$_POST['txtBillingCountry'];


// subcribe details
if ($_POST['Enabled'] != '') {
$id = '';
$email  = $email;
$type  = $_POST['Enabled'];


$objSubcribe = new Subcribe();
$objSubcribe->tb_name = 'tbl_subscribers';
$objSubcribe->id = $id;
$objSubcribe->email = $email;
$objSubcribe->type = $type;
$lastInsertedId = $objSubcribe->addSubcribe();

}



// save the Your Billing Address
$objCustomerAddress = new Address();
$objCustomerAddress->id = $billingAddressId;
$objCustomerAddress->title = $title;
$objCustomerAddress->firstName = $billingAddressFirstName;
$objCustomerAddress->lastName = $billingAddressLastName;
$objCustomerAddress->houseNumber = $billingAddressCompanyName;
$objCustomerAddress->phone = $billingAddressPhoneNumber;
$objCustomerAddress->address = $billingAddressAddress1;
$objCustomerAddress->region = $billingAddressAddress2;
$objCustomerAddress->city = $billingAddressTown;
$objCustomerAddress->state = $billingAddressCountryState;
$objCustomerAddress->postcode = $billingAddressPostal;
$objCustomerAddress->country = $billingAddressCountry;
$objCustomerAddress->membersId = $customerId;
$objCustomerAddress->addressObjectType = 'CUSTOMER';
$objCustomerAddress->addressType = 'BILLING';


$isBillingAddressUpdated   = false;
 
		$isBillingAddressUpdated = $objCustomerAddress->addAddress();
		$isBillingAddressUpdated   = true;

    
    $objMailTemplate = new MailTemplate();
    $templateInfo    = $objMailTemplate->getTemplate('1');
    $fromMail = $templateInfo->fromMail;
    $fromName = $templateInfo->fromName;
    $mailSubject = $templateInfo->mailSubject;
    $mailText    = $templateInfo->mailText;
    
    $mailTemplate = str_replace("[customer_name]",$firstName,$mailText);
    $mailTemplate = str_replace("[user_name]",$email,$mailTemplate);
    $mailTemplate = str_replace("[user_password]",$password,$mailTemplate);
    
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->AddReplyTo($fromMail, $fromName);
    $mail->AddAddress($email);
    $mail->SetFrom($fromMail, $fromName);
    $mail->Subject = $mailSubject;
    $mail->MsgHTML($mailTemplate);
    if(!$mail->Send()){
    	echo "email_failure";
    	exit;
    }
    //header('Location: login.php?suc=suc');
    echo "success";
    exit;	
} else {
	echo "failure";
	exit;
}

?>
