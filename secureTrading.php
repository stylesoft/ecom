<?php
require_once 'bootstrap.php';

$pageId 				= '65';

$orderId = "";

if($_GET['orderId']){
    $orderId = $_GET['orderId'];
}

$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo  = null;
		
			$selectedPageInfo 		= $pageObject->getPage($pageId);
		
		$pageId 				= $selectedPageInfo->id;
		$pageTitle 				= $selectedPageInfo->title;
		$pageKeywords 			= $selectedPageInfo->keywords;
		$pageDescription 		= $selectedPageInfo->description;
		$pageBody 				= $selectedPageInfo->body;
		$pageListingID 			= $selectedPageInfo->listingId;
		$pageName 				= $selectedPageInfo->name;
		$pageIsLive 			= $selectedPageInfo->live;
		$isContactBoxEnable 	= $selectedPageInfo->isContact;

$finalCurrency = 'GBP';
 $shippingCost  = "15.00";

$cartContents   = getCartContents();


$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
$currentCustomerId   = $currentCustomerInfo->id;
$objCustomer = new Customer();
$objCustomer->tb_name = ' tbl_customer';
$customerInfo = $objCustomer->getCustomer($currentCustomerId);	

$objAddress = new Address();
$billingAddresses = $objAddress->getAllByMember($currentCustomerId, 'BILLING');

foreach($billingAddresses As $bIndex=>$billingAddress){
$customerFirstname      = $billingAddress->firstName;
$customerLastname       = $billingAddress->lastName;
$customerAddress1       = $billingAddress->address;
$customerAddress2       = $billingAddress->region;
$customerCity           = $billingAddress->city;
$customerState          = $billingAddress->state;
$customerZip            = $billingAddress->postcode;
$customerCountry        = 'Bedfordshire';//$billingAddress->country;
}


// save the

$customerEmailAddress   = $customerInfo->email;
$LAYOUT = FRONT_LAYOUT_PATH."default_layout.tpl.php";
$CONTENT = FRONT_LAYOUT_VIEW_PATH."payment/securetrading.tpl.php";
require_once $LAYOUT;
?>