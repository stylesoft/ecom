<?php
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'productPage';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';
$brandId = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************




// **** Menu objects ***************
// stock

$objStock = new Stock();
$objStock->tb_name = 'tbl_stock';

// Menu field
$objMenuField = new MenuField();
$objMenuField->tb_name = 'tbl_menu_data';

//Menu

$objMenu = new Menu();
$objMenu->tb_name = 'tbl_menu';

//*********************************




$category = new Category();
$category->tb_name = 'tbl_category';
$categoryInfo = $category->getAll();

$brand = new Brand();
$brand->tb_name = 'tbl_brand';
$brandInfo = $brand->getAll();


$DisplaySettings = new ProductDisplaySettings();
$DisplaySettings->tb_name = 'tbl_product_display_settings';
$DisplayInfo  = $DisplaySettings->getProductDisplaySettings();


if (isset($_GET['productId'])) {
	$productId = $_GET['productId'];
        $productBrandId = $_GET['productBrandId'];
		

        
// **** Genarate automated Menu***************
       // $menuResult = $objMenu->getSecondLevelParent($status  = 'Enable');
$EditArr = $objStock->get_parentMenu($productId);     
        



//*********************************

$objProducts = new Product();
$objProducts->tb_name = "tbl_product";
$objProductsResult = $objProducts->getProduct($productId);
$stockavalibility = $objProductsResult->stockInHand - $objProductsResult->reorderLevel;
$Sizes = $objProductsResult->productSize;

$PobjectID = $objProductsResult->id;

$objAlbumImage = new Image();
$objAlbumImage->tb_name = 'tbl_images';

$objAlbumImage->searchStr = "";
$objAlbumImage->limit = "";
$objAlbumImage->listingOrder = 'recordListingId Asc';
$objAlbumImage->imageObject = 'Product';
$objAlbumImage->imageObjectKeyId = $PobjectID;
$albumImageResult = $objAlbumImage->search();



  if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }
   
 
    
    $selectedPageInfo = $pageObject->getPage($pageId);
    $pageId = $selectedPageInfo->id;
        $pageTitle = $objProductsResult->productName;
        $pageKeywords = $objProductsResult->keywords;
        $pageDescription = $objProductsResult->productSmallDescription;
       

}
else
{
	$objProducts = new Product();
	$objProducts->tb_name = "tbl_product";
	$objProductsResult = $objProducts->search();
	
	
	$pageId = $selectedPageInfo->id;
	$pageTitle = 'All Products';
	$pageKeywords = 'all products';
	$pageDescription = 'All products';
}    
if($_GET['brandId']){
    $brandId = $_GET['brandId'];
    
    $objbrand = new Brand;
    $objbrand->tb_name = "tbl_brand";
    $objbrandName = $objbrand->getBrand($brandId);
    
    
    $pageTitle = $objbrandName->brand_name;
    $pageKeywords = $objbrandName->brand_name;
    $pageDescription = $objbrandName->brand_description;
    
   // echo $objbrandName->brand_name;
        
}
   
        
$CONTENT = '';
if(($DisplayInfo->brand_display_style == 'Category with Side Bar') && (empty($_GET['productId']))){
  $CONTENT = FRONT_LAYOUT_VIEW_PATH."index/product_side_bar.tpl.php";  
}else{
$CONTENT = FRONT_LAYOUT_VIEW_PATH."index/product.tpl.php";
}
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>