ALTER TABLE `tbl_brand` ADD `category` VARCHAR( 20 ) NULL ;

INSERT INTO `plusprod_ecom`.`tbl_category` (`id`, `category_name`, `category_description`, `sub_category_description`, `parent_category`, `status`, `display_order`) VALUES ('-1', 'Default', '<p>defualt</p>', NULL, NULL, 'Enabled', '1');

INSERT INTO `plusprod_ecom`.`tbl_brand` (`id`, `brand_name`, `brand_description`, `status`, `display_order`, `brand_image`, `category`) VALUES ('-1', 'Default', '<p>default</p>', 'Enabled', '1', NULL, NULL);

ALTER TABLE `tbl_product` ADD `compare` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Enabled';