-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 20, 2014 at 12:24 PM
-- Server version: 5.1.69
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `plusproecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_data`
--

CREATE TABLE IF NOT EXISTS `tbl_menu_data` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `menu_id` int(4) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `field_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `tbl_menu_data`
--

INSERT INTO `tbl_menu_data` (`id`, `menu_id`, `field_name`, `field_value`) VALUES
(128, 62, 'modern', '15'),
(127, 62, 'reguler', '13'),
(126, 61, 'Small', '14'),
(125, 61, 'large', '16'),
(124, 60, 'red', 'red'),
(123, 60, 'black', 'black'),
(122, 59, 'white', 'white'),
(121, 59, 'Blue', 'Blue');
