-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 20, 2014 at 12:24 PM
-- Server version: 5.1.69
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `plusproecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stock`
--

CREATE TABLE IF NOT EXISTS `tbl_stock` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `menu_id` int(8) NOT NULL,
  `parent_menu_id` int(8) NOT NULL,
  `product_id` int(8) NOT NULL,
  `stock` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `tbl_stock`
--

INSERT INTO `tbl_stock` (`id`, `menu_id`, `parent_menu_id`, `product_id`, `stock`) VALUES
(100, 127, 62, 29, 4),
(99, 128, 62, 29, 5),
(98, 121, 59, 29, 6),
(97, 122, 59, 29, 5),
(96, 121, 59, 1, 7),
(95, 122, 59, 1, 6),
(94, 125, 61, 1, 15),
(93, 126, 61, 1, 20),
(90, 121, 59, 2, 4),
(84, 123, 60, 136, 11),
(83, 124, 60, 136, 10),
(76, 125, 61, 136, 2),
(75, 126, 61, 136, 5),
(74, 121, 59, 136, 2),
(73, 122, 59, 136, 5);
