-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2014 at 06:35 PM
-- Server version: 5.1.69
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_shipping_exclude`
--

CREATE TABLE IF NOT EXISTS `tbl_payment_shipping_exclude` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `country_id` int(255) NOT NULL,
  `payment_id` int(255) DEFAULT NULL,
  `shipping_id` int(255) DEFAULT NULL,
  `status` enum('Enabled','Disabled') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_payment_shipping_exclude`
--

INSERT INTO `tbl_payment_shipping_exclude` (`id`, `country_id`, `payment_id`, `shipping_id`, `status`) VALUES
(1, 4, 3, 1, 'Disabled'),
(2, 7, 3, 1, 'Disabled');
