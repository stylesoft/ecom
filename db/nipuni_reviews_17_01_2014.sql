CREATE TABLE IF NOT EXISTS `tbl_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `added_on` date DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `subject` text COLLATE utf8_bin,
  `description` text COLLATE utf8_bin,
  `product_id` int(11) DEFAULT NULL,
  `IP` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `rating` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `modified_by` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `is_approved` enum('Approved','Pending','Rejected') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;
