-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 07, 2014 at 01:22 PM
-- Server version: 5.1.69
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resetpassword`
--

CREATE TABLE IF NOT EXISTS `tbl_resetpassword` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `code` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_resetpassword`
--

INSERT INTO `tbl_resetpassword` (`id`, `email`, `code`) VALUES
(1, 'mrasvi@ymail.com', '3036e71156fb8548fd72f2163fdcad64'),
(9, 'mrasvi@ymail.com', '250b67ccd67f56d03db3d05d7b41d097'),
(8, 'mrasvi@ymail.com', 'd5984a51143b1eee894ac6ef122018d5'),
(10, 'mrasvi@ymail.com', '8944b084840089573006d636b157e3ef'),
(11, 'iyngaran@yahoo.com', '0be08ead108e0dcb2352618feb0b64b9');
