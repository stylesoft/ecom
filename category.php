<?php 
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'categoryPage';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************

$category = new Category();
$category->tb_name = 'tbl_category';
$categoryInfo = $category->getAll();

$brand = new Brand();
$brand->tb_name = 'tbl_brand';
$brandInfo = $brand->getAll();

$DisplaySettings = new ProductDisplaySettings();
$DisplaySettings->tb_name = 'tbl_product_display_settings';
$DisplayInfo  = $DisplaySettings->getProductDisplaySettings();

if ($_GET['categoryId'] != '') {
	$categoryId = $_GET['categoryId'];



//$CId = $_GET['id'];
$category = new Category();
$category->tb_name = 'tbl_category';
$resultCategory = $category->getCategory($categoryId);
//$pageType == "categoryPage";
$display_style = $resultCategory->display_style;
$pageTitle = $resultCategory->category_name;
$pageKeywords = 'Category shops';
$pageDescription = $resultCategory->category_description;
} else

{
	$pageTitle = 'All Categories';
	$pageKeywords = 'Category shops';
	$pageDescription = 'Category shops';
	
}


$CONTENT = '';
$LAYOUT = FRONT_LAYOUT_PATH."default_layout.tpl.php";
if($DisplayInfo->category_display_style  == 'Category with Side Bar'){
  $CONTENT = FRONT_LAYOUT_VIEW_PATH."index/category_side_bar.tpl.php";  
}else{
$CONTENT = FRONT_LAYOUT_VIEW_PATH."index/category.tpl.php";
}
require_once $LAYOUT;

?>
