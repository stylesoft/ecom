<?php
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'Login';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************

  if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }
    //last visited url of the client.............
    if (isset($_GET['product_url'])) {
        $product_url = $_GET['product_url'];
    }
    
    
    $selectedPageInfo = $pageObject->getPage($pageId);
    $pageId = $selectedPageInfo->id;
        $pageTitle = "Login";
        $pageKeywords = "Login";
        $pageDescription = "Login";
     


$CONTENT = '';
$CONTENT = FRONT_LAYOUT_VIEW_PATH . "customer/login.tpl.php";
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>