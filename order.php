<?php
ob_start();
require_once 'bootstrap.php';

$paymentMethod = "";

if(isset($_SESSION["selectedPaymentMethod"])){
    
    $paymentMethod = $_SESSION["selectedPaymentMethod"];
    
}

if($paymentMethod == ""){
    header('Location: checkout.html');
} else {
    
$currentDateTime = date( 'Y-m-d H:i:s');

$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
$currentCustomerId   = $currentCustomerInfo->id;
$arrCartContents = getCartContents();

$subTotal = 0;
$cartShippingMethod = getShippingMethod();
// print_r($cartShippingMethod);
//echo $_SESSION["shippingMethod"];
if($cartShippingMethod){
	//print_r($cartShippingMethod);
	$shippingCost = $cartShippingMethod->rangePrice;
} else {
	$shippingCost = 0;
}
$shippiongAmount =  $shippingCost;
$grandTotal = 0;
  foreach($arrCartContents As $cIndex=>$cartContent){
      
     $productUnitePrice = $cartContent->productUnitePrice;
     $productQty = $cartContent->productQty;
     
     $subTotal = $subTotal + ($productUnitePrice * $productQty);
        
  }
    
 $grandTotal = $subTotal + $shippiongAmount;

// save the order details.................
$objOrder                   = new Order();
$objOrder->id               = '';
$objOrder->customerId       = $currentCustomerId;
$objOrder->orderDate        = $currentDateTime;
$objOrder->orderStatus      = 'PENDING';
$objOrder->paymentStatus    = 'PENDING';
$objOrder->subTotal         = $subTotal;
$objOrder->shippingAmount   = $shippiongAmount;
$objOrder->grandTotal       = $grandTotal;
$objOrder->paymentMethod    = $paymentMethod;
$orderId                    = $objOrder->addOrder();
$_SESSION['orderId']        = $orderId;

// save the order items....
$arrCartContents = getCartContents();
  foreach($arrCartContents As $cIndex=>$cartContent){
      
      $objOrderItems                   = new OrderItem();
      $objOrderItems->id      = "";
      $objOrderItems->orderId = $orderId;
      $objOrderItems->productId = $cartContent->productId;
      $objOrderItems->productPrice = $cartContent->productUnitePrice;
      $objOrderItems->productQty = $cartContent->productQty;
      $objOrderItems->addOrderItem();
      
  }
  
  if($currentCustomerInfo != ''){
  	
   if($paymentMethod == 'paypal'){
       header('Location: paypal.html');
   } elseif($paymentMethod == 'securetrad'){
       header('Location: secureTrading.html?orderId='.$orderId);
   }elseif($paymentMethod == 'cash'){
       header('Location: cash.php');
   }elseif($paymentMethod == 'sage'){
       header('Location: sagepay.html');
   }
  }
   else { header('Location: checkout.php?log=failed'); }
  	
 
  
  }
  ob_end_flush();
?>