<?php

require_once '../bootstrap.php';

/* config start */

$siteEmailAddress = SITE_EMAIL;

/* config end */

require "../includes/extLibs/class.phpmailer.php";

$name           = $_POST['name'];
$emailAddress   = $_POST['email'];
$subject        = $_POST['qus'];
$phoneNumber    = $_POST['phoneNumber'];
$cim            = $_POST['cim'];
$message        = $_POST['message'];


$msg=
'Name:	'.$name.'<br />
Email:	'.$emailAddress.'<br />
Subject:	'.$subject.'<br />
Phone Number:	'.$phoneNumber.'<br />
IM:	'.$cim.'<br />
IP:	'.$_SERVER['REMOTE_ADDR'].'<br /><br />

Message:<br /><br />

'.nl2br($message).'

';

sleep(3);
$mail = new PHPMailer();
$mail->IsMail();

$mail->AddReplyTo($emailAddress, $name);
$mail->AddAddress($siteEmailAddress);
$mail->SetFrom($emailAddress, $name);
$mail->Subject = "A new ".strtolower($subject)." from ".$name." | contact form feedback";

$mail->MsgHTML($msg);

$mail->Send();

echo $_POST['ajax'];
exit();
if($_POST['ajax'])
{
	echo '1';
}
else
{
	$_SESSION['sent']=1;
	
	if($_SERVER['HTTP_REFERER'])
		header('Location: '.$_SERVER['HTTP_REFERER']);
	
	exit;
}

function checkLen($str,$len=2)
{
	return isset($_POST[$str]) && mb_strlen(strip_tags($_POST[$str]),"utf-8") > $len;
}

function checkEmail($str)
{
	return preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $str);
}

?>