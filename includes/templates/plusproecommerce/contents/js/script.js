$(document).ready(function(){

	$('#loading').css('visibility','visible');
        var use_ajax=false;
	$.validationEngine.settings={};

	$("#formID").validationEngine({
		inlineValidation: false,
		success :  function(){use_ajax=true},
		failure : function(){use_ajax=false;}
	 });
        
	$("#formID").submit(function(e){
		if(use_ajax == true){
                    $("#loading").show();
                    $.post('contact/submit.php',$(this).serialize()+'&ajax=1',
					function(data){
						$("#formID").hide('slow').after("<h1 style='color:#000;'>Thank you!</h1>");
						$("#loading").hide();
					}
				
				);
                    
                    
                }
	});

});