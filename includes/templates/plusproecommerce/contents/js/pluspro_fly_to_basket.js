/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){ 
    //var siteBaseUrl = "http://localhost/ecom/";
    var siteBaseUrl = "http://testing.ecom.com/";
       
	$("#notificationsLoader").show();
        $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");       
        $("#notificationsLoader").hide(300);

	$(".cart").click(function() {
              
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 		= productIDValSplitter[1];
                
        var productQty                  = $("#productQtyID_" + productIDVal).val();
                
                
		var productX 		= $("#productImageWrapID_" + productIDVal).offset().left;
		var productY 		= $("#productImageWrapID_" + productIDVal).offset().top;
		
               // var productIdLen = $("#productID_" + productIDVal).length;
       
		if($("#productID_" + productIDVal).length > 0){
			var basketX 		= $("#productID_" + productIDVal).offset().left;
			var basketY 		= $("#productID_" + productIDVal).offset().top;			
		} else {
			var basketX 		= $("#cart_contents").offset().left;
			var basketY 		= $("#cart_contents").offset().top;
		}
		
                
		var gotoX 			= basketX - productX;
		var gotoY 			= basketY - productY;
		
		var newImageWidth 	= $("#productImageWrapID_" + productIDVal).width() / 3;
		var newImageHeight	= $("#productImageWrapID_" + productIDVal).height() / 3;
                
        $("html, body").animate({ scrollTop: 35 },2400);		
		
		$("#productImageWrapID_" + productIDVal + " img")
		.clone()
		.prependTo("#productImageWrapID_" + productIDVal)
		.css({'position' : 'absolute'})
		.animate({opacity: 0.4}, 100 )
		.animate({opacity: 0.1, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200, function() {
																																																																										  			$(this).remove();

      
			$("#notificationsLoader").show();
                        var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
			$.ajax({  
				type: "POST",  
				url: cartFunctionUrl,  
				data: { productID: productIDVal,productQty: productQty, action: "addToBasket"},  
				success: function(theResponse) {
                                        
                        $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
                        if( $("#productID_" + productIDVal).length > 0){
							$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
							$("#productID_" + productIDVal).before(theResponse).remove();
							$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
							$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
						} 
                        $("#notificationsLoader").hide();
                                     
				}  
			});  
		
		});
		
	});
	

	
    
	$(".removeItem").live("click", function(event) { 
		//alert('hello');
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 			= productIDValSplitter[1];	
		 var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 0 }, 900);
		//var loadingImageUrl = siteBaseUrl + "includes/templates/cowper/contents/images/preloader_transparent.gif";
		//var loadingImageTag = "<img src='"+loadingImageUrl+"' align='center'>";
		//$("#cartItemRow_" + productIDVal).html(loadingImageTag);
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 1 }, 600);
		 $("#notificationsLoader").show();
		$.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { productID: productIDVal, action: "deleteFromBasket"},  
			success: function(theResponse) {
				$("#cartItemRow_" + productIDVal).hide("slow",  function() {$(this).remove();});
				 $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
				$("#checkout").load(siteBaseUrl + "cart_contents.php?controller=cart");
				$("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
				 $("#notificationsLoader").hide();
			}  
		});
		return false;
	});
        
        
	
	$(".removeItemm").live("click", function(event) { 
		//alert('hello');
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 			= productIDValSplitter[1];	
		 var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 0 }, 900);
		//var loadingImageUrl = siteBaseUrl + "includes/templates/cowper/contents/images/preloader_transparent.gif";
		//var loadingImageTag = "<img src='"+loadingImageUrl+"' align='center'>";
		//$("#cartItemRow_" + productIDVal).html(loadingImageTag);
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 1 }, 600);
		 $("#notificationsLoader").show();
		$.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { productID: productIDVal, action: "deleteFromBasket"},  
			success: function(theResponse) {
				$("#cartItemRow_" + productIDVal).hide("slow",  function() {$(this).remove();});
				 $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
				$("#checkout").load(siteBaseUrl + "cart_contents.php?controller=carts");
				$("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
				 $("#notificationsLoader").hide();
			}  
		});
		return false;
	});
     
        
        
	$("#cart_contents .removeItem").live("click", function(event) { 
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 			= productIDValSplitter[1];	
		 var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 0 }, 900);
		//var loadingImageUrl = siteBaseUrl + "includes/templates/cowper/contents/images/preloader_transparent.gif";
		//var loadingImageTag = "<img src='"+loadingImageUrl+"' align='center'>";
		//$("#cartItemRow_" + productIDVal).html(loadingImageTag);
		//$("#cartItemRow_" + productIDVal).animate({ opacity: 1 }, 600);
		 $("#notificationsLoader").show();
		$.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { productID: productIDVal, action: "deleteFromBasket"},  
			success: function(theResponse) {
				$("#cartItemRow_" + productIDVal).hide("slow",  function() {$(this).remove();});
				//$("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
				$("#cartContentsData").load(siteBaseUrl + "cart_contents.php?controller=cart");
				 $("#notificationsLoader").hide();
			}  
		});
		return false;
	});


$("#cmbShippingMethod").change(function() {
               var shippingMethodId = this.value;
               var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
               $("#notificationsLoader").show();

               $.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { shippingMethodID: shippingMethodId, action: "changeShipping"},  
			success: function(theResponse) {
				$("#cartContentsData").load(siteBaseUrl + "cart_contents.php?controller=cart");
				$("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=sidebar");
				 $("#notificationsLoader").hide();
			}  
		});
		return false;
               
        });
	
	
	// update cart.....
//	$(".cart_quantity_box").keyup(function(){
$(".cart_quantity_box").live('keyup', function() {

		console.log(this);
			var productIDValSplitter 	= (this.id).split("_");
			var productIDVal 			= productIDValSplitter[1];
			var productQty 				= this.value;
			
			
		  
		  $("#notificationsLoader").show();
		  $("#cartContentsNotificationsLoader").show();//cartContentsNotificationsLoader
          var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
         // alert(cartFunctionUrl);
			$.ajax({  
				type: "POST",  
				url: cartFunctionUrl,  
				data: { productID: productIDVal,productQty: productQty, action: "updateBasket"},  
				success: function(theResponse) {
			            $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
			            $("#checkout").load(siteBaseUrl + "cart_contents.php?controller=cart");
			        	$("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
			            //if( $("#productID_" + productIDVal).length > 0){
						//$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						//$("#productID_" + productIDVal).before(theResponse).remove();
						//$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						//$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
					//} 
			        $("#notificationsLoader").hide();
			        $("#cartContentsNotificationsLoader").hide();//cartContentsNotificationsLoader
				}  
			});
		  
		  
	}); 
	
	
	
	$(".cart_quantity_boxh").live('keyup', function() {
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 			= productIDValSplitter[1];
		var productQty 				= this.value;
		
		
	  
	  $("#notificationsLoader").show();
	  $("#cartContentsNotificationsLoader").show();//cartContentsNotificationsLoader
      var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
     // alert(cartFunctionUrl);
		$.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { productID: productIDVal,productQty: productQty, action: "updateBasket"},  
			success: function(theResponse) {
		            $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
		            $("#checkout").load(siteBaseUrl + "cart_contents.php?controller=cart");
		        	$("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
		            if( $("#productID_" + productIDVal).length > 0){
					$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
					$("#productID_" + productIDVal).before(theResponse).remove();
					$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
					$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
				} 
		        $("#notificationsLoader").hide();
		        $("#cartContentsNotificationsLoader").hide();//cartContentsNotificationsLoader
			}  
		});
	  
	  
		}); 
	
	

	
	$(".cart_quantity_boxs").live('keyup', function() {
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 			= productIDValSplitter[1];
		var productQty 				= this.value;
		
		
	  
	  $("#notificationsLoader").show();
	  $("#cartContentsNotificationsLoader").show();//cartContentsNotificationsLoader
      var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
     	// alert(cartFunctionUrl);
		$.ajax({  
			type: "POST",  
			url: cartFunctionUrl,  
			data: { productID: productIDVal,productQty: productQty, action: "updateBasket"},  
			success: function(theResponse) {
		            $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
		            $("#checkout").load(siteBaseUrl + "cart_contents.php?controller=carts");
		            $("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
		            if( $("#productID_" + productIDVal).length > 0){
					$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
					$("#productID_" + productIDVal).before(theResponse).remove();
					$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
					$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
				} 
		        $("#notificationsLoader").hide();
		        $("#cartContentsNotificationsLoader").hide();//cartContentsNotificationsLoader
			}  
		});
	  
	  
	}); 

	
	
	
	

	
        
        //cart_btn
        $(".popupcartbutton").click(function() {
            
            var popupProductId = $("#crt_product_id").val();
            //var productQty     = parseInt($("#cartQty").val());
            $("#notificationsLoader").show();
            var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
            
            $.ajax({  
		type: "POST",  
		url: cartFunctionUrl,  
		data: { productID: popupProductId,productQty: productQty, action: "addToBasket"},  
		success: function(theResponse) {
                          $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
                          $("#notificationsLoader").hide();
		 }  
	    }); 
            
            $(".pop_up_ajax").colorbox.close().show(); 
            $("html, body").animate({ scrollTop: 35 },2400);
            return false;
        });
        
        
        
        //------------------- add items to cart from detail view ----------------------        
        $(".btnaddcrt").click(function() {
               
               
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 		= productIDValSplitter[1];
                
                var productQty                  = $("#cartQty").val();
                var productSize                  = $("#cartSize").val();

                
		var productX 		= $("#productImageWrapID_" + productIDVal).offset().left;
		var productY 		= $("#productImageWrapID_" + productIDVal).offset().top;
		
               // var productIdLen = $("#productID_" + productIDVal).length;
               
		if($("#productID_" + productIDVal).length > 0){
			var basketX 		= $("#productID_" + productIDVal).offset().left;
			var basketY 		= $("#productID_" + productIDVal).offset().top;			
		} else {
			var basketX 		= $("#cart_contents").offset().left;
			var basketY 		= $("#cart_contents").offset().top;
		}
		

                
		var gotoX 			= basketX - productX;
		var gotoY 			= basketY - productY;
		
		var newImageWidth 	= $("#productImageWrapID_" + productIDVal).width() / 3;
		var newImageHeight	= $("#productImageWrapID_" + productIDVal).height() / 3;
                
        $("html, body").animate({ scrollTop: 35 },2400);		
		
		$("#productImageWrapID_" + productIDVal + " img")
		.clone()
		.prependTo("#productImageWrapID_" + productIDVal)
		.css({'position' : 'absolute'})
		.animate({opacity: 0.4}, 100 )
		.animate({opacity: 0.1, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200);
					$("#notificationsLoader").show();
		            var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
					$.ajax({  
						type: "POST",  
						url: cartFunctionUrl,  
						data: { productID: productIDVal,productQty: productQty,productSize:productSize,action: "addToBasket"},  
						success: function(theResponse) {
		                                        
		                       $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
		                       //$("#checkout").load(siteBaseUrl + "cart_contents.php?controller=cart");
							   $("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
		                         if( $("#productID_" + productIDVal).length > 0){
										$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
										$("#productID_" + productIDVal).before(theResponse).remove();
										$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
										$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
								} 
		                        $("#notificationsLoader").hide();
						}  
					});
		
	});
     //EOF add item to cart detail view

     //ADD Item to cart thumb View
     $(".add_item_to_cart_product").click(function() {
        	
        	$(".image1").show();
        
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 		= productIDValSplitter[1];
                
                var productQty                  = $("#cartQty").val();
                var productSize                  = $("#cartSize").val();
                var productColor                 = $("#cartColor").val();
                
                
		var productX 		= $("#productImageWrapID_" + productIDVal).offset().left;
		var productY 		= $("#productImageWrapID_" + productIDVal).offset().top;
		
               // var productIdLen = $("#productID_" + productIDVal).length;
               
		if($("#productID_" + productIDVal).length > 0){
			var basketX 		= $("#productID_" + productIDVal).offset().left;
			var basketY 		= $("#productID_" + productIDVal).offset().top;			
		} else {
			var basketX 		= $("#cart_contents").offset().left;
			var basketY 		= $("#cart_contents").offset().top;
		}
		

                
		var gotoX 			= basketX - productX;
		var gotoY 			= basketY - productY;
		
		var newImageWidth 	= $("#productImageWrapID_" + productIDVal).width() / 3;
		var newImageHeight	= $("#productImageWrapID_" + productIDVal).height() / 3;
                
                $("html, body").animate({ scrollTop: 35 },2400);		
		
		$("#productImageWrapID_" + productIDVal + " img")
		.clone()
		.prependTo("#productImageWrapID_" + productIDVal)
		.css({'position' : 'absolute'})
		.animate({opacity: 0.4}, 100 )
		.animate({opacity: 0.1, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200, function() {
			$(this).remove();
			$(".image1").hide();
			$("#notificationsLoader").show();
		});
		
		    var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
			$.ajax({  
				type: "POST",  
				url: cartFunctionUrl,  
				data: { productID: productIDVal,productQty: productQty,productSize: productSize,productColor: productColor,action: "addToBasket"},  
				
				success: function(theResponse) {
			//$("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=sidebar");
					$("#notificationsLoader").hide();
					$("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
					$("#checkout").load(siteBaseUrl + "cart_contents.php?controller=cart");
					$("#hcart").load(siteBaseUrl + "cart_contents.php?controller=chearder");
                 	
                 	if( $("#productID_" + productIDVal).length > 0){
                                        
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);						
						$("#productID_" + productIDVal).before(theResponse).remove();						
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);	
						
						  //$(this).html(theResponse);
					} 
                                    
				}  
			}); 

	});  
    //EOF add Item to cart thumb View   
        
        
        //crt_btns
     
        $(".crt_btns").click(function() {
            $currentQty  = parseInt($("#cartQty").val());
            if(this.value == '-'){
                
                if($currentQty>1){
                    $currentQty = $currentQty - 1;
                }
                
            }else if(this.value == '+'){
                $currentQty = $currentQty + 1;
            }
            
            $("#cartQty").val($currentQty);
            
        });
       
       
        $("#wishlistNotificationsLoader").show();
        $("#wishlistHeader_contents").load(siteBaseUrl + "wish_list_contents.php?controller=header");
        $("#wishlistNotificationsLoader").hide(300);
        
        	$(".add_item_to_wish_list").click(function() {
                var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 		= productIDValSplitter[1];
                
                var productQty                  = $("#productQtyID_" + productIDVal).val();
                if(productQty == ""){
                    productQty = 1;
                }
                
		var productX 		= $("#productImageWrapID_" + productIDVal).offset().left;
		var productY 		= $("#productImageWrapID_" + productIDVal).offset().top;
		
               // var productIdLen = $("#productID_" + productIDVal).length;
               
		if($("#productID_" + productIDVal).length > 0){
			var basketX 		= $("#productID_" + productIDVal).offset().left;
			var basketY 		= $("#productID_" + productIDVal).offset().top;			
		} else {
			var basketX 		= $("#wishlistHeader_contents").offset().left;
			var basketY 		= $("#wishlistHeader_contents").offset().top;
		}
		

                
		var gotoX 			= basketX - productX;
		var gotoY 			= basketY - productY;
		
		var newImageWidth 	= $("#productImageWrapID_" + productIDVal).width() / 3;
		var newImageHeight	= $("#productImageWrapID_" + productIDVal).height() / 3;
                
                $("html, body").animate({ scrollTop: 15 },2400);		
		
		$("#productImageWrapID_" + productIDVal + " img")
		.clone()
		.prependTo("#productImageWrapID_" + productIDVal)
		.css({'position' : 'absolute'})
		.animate({opacity: 0.4}, 100 )
		.animate({opacity: 0.1, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200, function() {
																																																																										  			$(this).remove();

      
			$("#wishlistNotificationsLoader").show();
                        var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
			$.ajax({  
				type: "POST",  
				url: cartFunctionUrl,  
				data: { productID: productIDVal,productQty: productQty, action: "addToWishlist"},  
				success: function(theResponse) {
                                        $("#wishlistHeader_contents").load(siteBaseUrl + "wish_list_contents.php?controller=header");
                                        if( $("#productID_" + productIDVal).length > 0){
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						$("#productID_" + productIDVal).before(theResponse).remove();
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
					} 
                                     $("#wishlistNotificationsLoader").hide();
				}  
			});  
		
		});
		
	});
        
       
       
       
       
       $(".add_wishlist_item_to_cart").click(function() {
    	   alert('Color is selected');
		var productIDValSplitter 	= (this.id).split("_");
		var productIDVal 		= productIDValSplitter[1];
                
                var productQty                  = $("#productQtyID_" + productIDVal).val();
                
		var productX 		= $("#productImageWrapID_" + productIDVal).offset().left;
		var productY 		= $("#productImageWrapID_" + productIDVal).offset().top;
		
                var row_div_id          = $("#productRowWrapID_" + productIDVal);
                $(row_div_id).fadeOut("slow");
                
               // var productIdLen = $("#productID_" + productIDVal).length;
               
		if($("#productID_" + productIDVal).length > 0){
			var basketX 		= $("#productID_" + productIDVal).offset().left;
			var basketY 		= $("#productID_" + productIDVal).offset().top;			
		} else {
			var basketX 		= $("#cart_contents").offset().left;
			var basketY 		= $("#cart_contents").offset().top;
		}
		

                    
                
		var gotoX 			= basketX - productX;
		var gotoY 			= basketY - productY;
		
		var newImageWidth 	= $("#productImageWrapID_" + productIDVal).width() / 3;
		var newImageHeight	= $("#productImageWrapID_" + productIDVal).height() / 3;
                
                $("html, body").animate({ scrollTop: 15 },2400);		
		
		$("#productImageWrapID_" + productIDVal + " img")
		.clone()
		.prependTo("#productImageWrapID_" + productIDVal)
		.css({'position' : 'absolute'})
		.animate({opacity: 0.4}, 100 )
		.animate({opacity: 0.1, marginLeft: gotoX, marginTop: gotoY, width: newImageWidth, height: newImageHeight}, 1200, function() {
																																																																										  			$(this).remove();

      
			$("#notificationsLoader").show();
                        var cartFunctionUrl = siteBaseUrl + "includes/extLibs/cart_functions.php";
			$.ajax({  
				type: "POST",  
				url: cartFunctionUrl,  
				data: { productID: productIDVal,productQty: productQty, action: "addToBasket",actionFrom:"wishlist"},  
				success: function(theResponse) {
                               
                                        $("#cart_contents").load(siteBaseUrl + "cart_contents.php?controller=header");
                                        if( $("#productID_" + productIDVal).length > 0){
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						$("#productID_" + productIDVal).before(theResponse).remove();
						$("#productID_" + productIDVal).animate({ opacity: 0 }, 500);
						$("#productID_" + productIDVal).animate({ opacity: 1 }, 500);		
					} 
                                     $("#notificationsLoader").hide();
                                     
                                     $("#wishlistNotificationsLoader").show();
                                    $("#wishlistHeader_contents").load(siteBaseUrl + "wish_list_contents.php?controller=header");
                                    $("#wishlistNotificationsLoader").hide(300);
				}  
			});  
		
		});
		
	});
       
       $('.slide_handle').click(function() {
     
     $("html, body").animate({ scrollTop: 10 },2400);	
     
 });
 
  $('input[class^="search"]').focus(function() {
        if($(this).val() == $(this).data('Search...') || !$(this).data('Search...')) {
            $(this).data('Search...', $(this).val());
            $(this).val('');
        }
    });

    $('input[class^="search"]').blur(function() {
        if ($(this).val() == '') $(this).val($(this).data('Search...'));
    });
    
    
     $('.step_prev').click(function() {
     parent.history.back();
     return false;
     });

});

function openUrl($url)
{
	window.open($url,'_self')
}
