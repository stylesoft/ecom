<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title><?php print($pageTitle);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow" />
<meta name="keywords" content="<?php print($pageKeywords); ?>" />
<meta name="description" content="<?php print($pageDescription); ?>" />
<meta name="title" content="<?php print($pageTitle);?>" />

 <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/assets.php'); ?>
</head>
<body>
<!--header starts-->
  <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/header.tpl.php'); ?>
  <!--header end-->
  
<!--Content starts-->
<div id="maincontainer">
  
	<!--Slider Starts-->
	<?php if ($pageType == "homePage") {?>
  <?php require_once(FRONT_LAYOUT_VIEW_PATH .'common/slider.tpl.php'); ?>
  <?php }?>
    <!--Slider Ends-->
    <!--inner Content starts-->
<?php include($CONTENT); ?> 
 <!--inner Content end-->
</div>
<!-- Footer Starts-->
 <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/footer_menu.tpl.php'); ?>
 
  <?php //require_once(FRONT_LAYOUT_VIEW_PATH . 'common/header_cart.tpl.php'); ?>
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/headercart.tpl.php'); ?>
<!-- /maincontainer -->

<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/google-code-prettify/prettify.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-transition.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-alert.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-dropdown.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-scrollspy.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-popover.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-button.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-collapse.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-carousel.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-affix.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/application.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/respond.min.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/cloud-zoom.1.0.2.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.nivo.slider.js"></script>
<script defer src="<?php print(FRONT_LAYOUT_URL);?>contents/js/custom.js"></script>
<script type="text/javascript">
$(window).load(function () {
  $('#slider').nivoSlider();
});
</script>

</body>
</html>