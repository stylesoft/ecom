<?php  
         $objRegion = new Region();
         $regionList = $objRegion->getAll();
         $regionListJSONObject = json_encode($regionList);
        
         //Product Settings..
         $objProductSettings  = new ProductSettings();
         $objProductSettings->tb_name = 'tbl_product_settings';
         
         $ProductSettingsInfo = $objProductSettings->getProductSettings();
         
         $VATValue= $ProductSettingsInfo->VATValue;
         $guest_amount = $ProductSettingsInfo->guest_amount;
        ?>
<script>
$(document).ready(function(){
	$("#select01").live('change', function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>
<section id="checkout">
       	 <div id="cartContentsNotificationsLoader" style="display: none;" align ="center">
              <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/ajax-loader.gif"></img>
              </div> 
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL); ?>">Home</a><span class="divider">/</span></li>
        <li class="active">Shoping Cart</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <?php 
      $result = count($cartContents);
      ?>
      <h1 class="productname">Shopping cart</h1>
      <div class="cart-info">
        <table class="table table-striped table-bordered">
          <tbody><tr>
            <th width="14%" class="image">Image</th>
            <th width="21%" class="name">Product Name</th>
            <th width="14%" class="model">Model</th>
            <th width="12%" class="quantity">Quantity</th>
            <th width="15%" class="price">Unit Price</th>
            <th width="13%" class="total">Total</th>
            <th width="11%" class="total">Edit</th>
          </tr>
          
            <?php if($cartContents){?>
                              
                                       <?php foreach($cartContents As $cIndex=>$cartContent){
                                                $totalAmount = $totalAmount +  $cartContent->productSubTotal; 
                                                 //print_r($cartContent);
                                       ?>
          <tr>
            <td class="image"><a href="#"><img width="50" height="50" src="<?php print(SITE_BASE_URL);?>includes/extLibs/imagesize.php?file=<?php print($cartContent->productImage->recordText);?>&maxw=50&maxh=50" alt="product" title="product"></a></td>
            <td class="name"><a href="#"><?php print($cartContent->productName); ?> (<?php if($cartContent->color == ''){echo "red";} else{ print($cartContent->color);} ?>)</a></td>
            <td class="model"><?php print($cartContent->productInfo->productCode);?> (<?php print($cartContent->productSize); ?>)</td>
            <td class="quantity"><input type="text" class="span1 cart_quantity_box update_btn" name="cartQty_<?php print($cartContent->productId); ?>" id="cartQty_<?php print($cartContent->productId); ?>" value="<?php print($cartContent->productQty); ?>" size="1"></td>
            <td class="price"><?php if($cartContent->productQty == 0){echo CURRENCY_CODE."0.00";} else{ print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productUnitePrice*CURRENCY_RATE))); } ?></td>
            <td class="total"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productSubTotal*CURRENCY_RATE))); ?></td>
            <td class="total"><a href="#" class="removeItem" id="cartItemID_<?php print($cartContent->productId); ?>" onclick="return false;"><img alt="" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/remove.png" data-original-title="Remove" class="tooltip-test"></a></td>
          </tr>
          <?php } }?>
        </tbody></table>
      </div>
      <div class="row">
        <div class="pull-right">
          <div class="span7 pull-right">
          <?php if($result>0){?>
            <table class="table table-striped table-bordered ">
              <tbody><tr>
                <td><span class="extra bold">Sub-Total :</span></td>
                <td class="numcls"><span class="bold numcls"><?php print(CURRENCY_CODE); ?> <?php print(priceFormateNumber($totalAmount*CURRENCY_RATE)); ?></span></td>
              </tr>
              <?php if($cartShippingMethod) { ?>
              <tr>
                <td><span class="extra bold"><?php if($cartShippingMethod){?>(<?php print($cartShippingMethod->carrierType->carrierType);?>)<?php } ?> :</span></td>
                <td class="numcls"><span class="bold"><?php print(CURRENCY_CODE); ?><?php if($totalAmount>0){?> <?php if($cartShippingMethod){?> <?php print(priceFormateNumber($cartShippingMethod->rangePrice*CURRENCY_RATE));?> <?php } } else {?> 0.00 <?php } ?></span></td>
              </tr>
                 <?php } else { ?>
                      
                           <?php 
                           foreach($cartContents As $cartContent){
                           
                           	$productWeightUnit = $cartContent->productInfo->weightUnit;
                           	$productWeight = $cartContent->productInfo->productWeight;
                           	$productQty    = $cartContent->productQty;
                           	if ($productWeightUnit == 'mg') {
                           	$row_weight    = ($productWeight * $productQty / 1000);
                           	}
                           	else {
                           	$row_weight    = $productWeight * $productQty;
                           	}
                           	$totalWeight  = $totalWeight + $row_weight;
                           	
                           }
                           
                        //$totalWeight = 600;
                        
                        $objCarrierRange = new CarrierRange();
                        $shippingCarrierRanges = $objCarrierRange->getDefult($totalWeight);
                        
                        $DefshippingCost=$shippingCarrierRanges[0]->rangePrice*CURRENCY_RATE;

                        ?>  
                 <tr>
                <td><span class="extra bold"> Shipping Amount(def) (<?php  if($cartContent->productQty == 0){} else{print($shippingCarrierRanges[0]->carrierType->carrierType);}?>) :</span></td>
                <td class="numcls"><span class="bold"><?php print(CURRENCY_CODE); ?>  <?php if($cartContent->productQty == 0){echo "0.00";} else{ print(priceFormateNumber($DefshippingCost)); }?></span></td>
              </tr>
              <?php }
              $result = count($cartContents);
                if ($result>0) {
                	$vat = 21.0;
                }
                else {
                	$vat = "0.00";
                }
                print(CURRENCY_RATE);
              ?>        
                         
              <tr>
                <td><span class="extra bold">VAT (<?php print(SITE_DEFAULT_VAT_VALUE)?>) :</span></td>
                <td class="numcls"><span class="bold"><?php print(CURRENCY_CODE); ?> <?php echo SITE_DEFAULT_VAT_VALUE*CURRENCY_RATE; ?></span></td>
              </tr>
              <tr>
                <td><span class="extra bold totalamout">Total :</span></td>
                <td class="numcls"><span class="bold totalamout"><?php print(CURRENCY_CODE); ?> <?php 
                        
                                if($cartShippingMethod){
                                    $shippingCost = $cartShippingMethod->rangePrice;
                                } else {
                                    $shippingCost = $DefshippingCost;
                                }
                                if($totalAmount>0){
                                $finalAmount = $totalAmount + $shippingCost+$vat;
                                }
                                print(priceFormateNumber($finalAmount*CURRENCY_RATE));
                                ?></span></td>
              </tr>
              
            </tbody></table><br>
            <input type="button" onclick="location.href='<?php if($guest_amount<$finalAmount or isset($_SESSION['SESS_CUSTOMER_INFO'])){ print(SITE_BASE_URL);?>checkout.html<?php } else {?><?php print(SITE_BASE_URL);?>checkout.html<?php }?>'" class="btn btn-success pull-right" value="CheckOut">
            <input type="button" onclick="location.href='<?php print(SITE_BASE_URL);?>'" class="btn pull-right mr10" value="Continue Shopping">
            <?php }else{?>
             <div id="resutlError1" style="color:red;">Shopping cart is empty </div>
            <?php }?>
          </div>
        </div>
      </div>
      <br><br><br><br><br>
  <div style="display:none;">
      <h3 class="heading3">What would you like to do next?</h3>
      <div class="cartoptionbox">
        <h4 class="heading4"> Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost. </h4>
        <input type="radio" class="radio">
        Use Coupon Code <br>
        <input type="radio" class="radio">
        Use Gift Voucher <br>
        <input type="radio" checked="checked" class="radio">
        Estimate Shipping &amp; Taxes <br>
      </div>
      <?php 
$objCountry = new Country();
$resultsall = $objCountry->getAll();

?>
      
      <div class="cartoptionbox">
        <form class="form-vertical form-inline">
          <h4 class="heading4"> Enter your destination to get a shipping estimate.</h4>
          <fieldset>
            <div class="control-group">
              <label class="control-label">Select list</label>
              <div class="controls">
                <select id="select01" class="span3 cartcountry">
                  <option>Country:</option>
                   <?php foreach ($resultsall as $index=>$country) { ?>
			  <option  country_id="<?php print ($country->id);?>" value="<?php echo $country->name;?>"><?php echo $country->name;?></option>
				  <?php } ?>
             
                </select>
                <select id="select02" class="span3 cartstate">
                 <option value=''>Region / State:</option>
                </select>
                <input type="submit" class="btn btn-inverse" value="Get Quotes">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      </div>
    </div>
  </section>
