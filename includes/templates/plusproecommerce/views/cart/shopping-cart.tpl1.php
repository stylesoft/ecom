<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/pluspro_fly_to_basket.js"></script>

<section id="checkout">
       	 <div id="cartContentsNotificationsLoader" style="display: none;" align ="center">
              <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/ajax-loader.gif"></img>
              </div> 
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL); ?>">Home</a><span class="divider">/</span></li>
        <li class="active">Shoping Cart</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
       <?php 
      $result = count($cartContents);
      ?>
      <h1 class="productname">Summer Offer Best Product (<?php echo $result;?>)this is ajax</h1>
      <div class="cart-info">
        <table class="table table-striped table-bordered">
          <tbody><tr>
            <th width="14%" class="image">Image</th>
            <th width="21%" class="name">Product Name</th>
            <th width="14%" class="model">Model</th>
            <th width="12%" class="quantity">Quantity</th>
            <th width="15%" class="price">Unit Price</th>
            <th width="13%" class="total">Total</th>
            <th width="11%" class="total">Edit</th>
          </tr>
            <?php if($cartContents){?>
                              
                                       <?php foreach($cartContents As $cIndex=>$cartContent){
                                                $totalAmount = $totalAmount +  $cartContent->productSubTotal; 
                                                 //print_r($cartContent);
                                       ?>
          <tr>
            <td class="image"><a href="#"><img width="50" height="50" src="<?php print(SITE_BASE_URL);?>includes/extLibs/imagesize.php?file=<?php print($cartContent->productImage->recordText);?>&maxw=50&maxh=50" alt="product" title="product"></a></td>
            <td class="name"><a href="#"><?php print($cartContent->productName); ?> (<?php if($cartContent->color == ''){echo "red";} else{ print($cartContent->color);} ?>)</a></td>
            <td class="model"><?php print($cartContent->productInfo->productCode);?> (<?php print($cartContent->productSize); ?>)</td>
            <td class="quantity"><input type="text" class="span1 cart_quantity_boxs update_btn" name="cartQty_<?php print($cartContent->productId); ?>" id="cartQty_<?php print($cartContent->productId); ?>" value="<?php print($cartContent->productQty); ?>" size="1"></td>
            <td class="price"><?php if($cartContent->productQty == 0){echo CURRENCY_CODE."0.00";} else{ print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productUnitePrice*CURRENCY_RATE))); } ?></td>
            <td class="total"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productSubTotal*CURRENCY_RATE))); ?></td>
            <td class="total"><a href="#" class="removeItemm" id="cartItemID_<?php print($cartContent->productId); ?>" onclick="return false;"><img alt="" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/remove.png" data-original-title="Remove" class="tooltip-test"></a></td>
          </tr>
          <?php } }?>
        </tbody></table>
      </div>
      <div class="row">
        <div class="pull-right">
          <div class="span7 pull-right">
            <table class="table table-striped table-bordered ">
              <tbody><tr>
                <td><span class="extra bold">Sub-Total :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?> <?php print(priceFormateNumber($totalAmount*CURRENCY_RATE)); ?></span></td>
              </tr>
              <?php if($cartShippingMethod) { ?>
              <tr>
                <td><span class="extra bold"><?php if($cartShippingMethod){?>(<?php print($cartShippingMethod->carrierType->carrierType);?>)<?php } ?> :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?><?php if($totalAmount>0){?> <?php if($cartShippingMethod){?> <?php print(priceFormateNumber($cartShippingMethod->rangePrice*CURRENCY_RATE));?> <?php } } else {?> 0.00 <?php } ?></span></td>
              </tr>
                 <?php } else { ?>
                      
                           <?php 
                           foreach($cartContents As $cartContent){
                           
                           	 
                           	$productWeight = $cartContent->productInfo->productWeight;
                           	$productQty    = $cartContent->productQty;
                           	$row_weight    = $productWeight * $productQty;
                           	$totalWeight  = $totalWeight + $row_weight;
                           }
                           
                        //$totalWeight = 600;
                        
                        $objCarrierRange = new CarrierRange();
                        $shippingCarrierRanges = $objCarrierRange->getDefult($totalWeight);
                        
                        $DefshippingCost=$shippingCarrierRanges[0]->rangePrice*CURRENCY_RATE;

                        ?>  
                 <tr>
                <td><span class="extra bold"> Shipping Amount(def) (<?php  if($cartContent->productQty == 0){} else{print($shippingCarrierRanges[0]->carrierType->carrierType);}?>) :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?>  <?php if($cartContent->productQty == 0){echo "0.00";} else{ print(priceFormateNumber($DefshippingCost)); }?></span></td>
              </tr>
              <?php }?>        
                         
              <tr>
                <td><span class="extra bold">VAT (<?php print(SITE_DEFAULT_VAT_VALUE)?>) :</span></td>
                <td><span class="bold">$21.0</span></td>
              </tr>
              <tr>
                <td><span class="extra bold totalamout">Total :</span></td>
                <td><span class="bold totalamout"><?php print(CURRENCY_CODE); ?> <?php 
                        
                                if($cartShippingMethod){
                                    $shippingCost = $cartShippingMethod->rangePrice;
                                } else {
                                    $shippingCost = $DefshippingCost;
                                }
                                if($totalAmount>0){
                                $finalAmount = $totalAmount + $shippingCost;
                                }
                                print(priceFormateNumber($finalAmount*CURRENCY_RATE));
                                ?></span></td>
              </tr>
            </tbody></table><br>
            <input type="submit" class="btn btn-success pull-right" value="CheckOut">
            <input type="submit" class="btn pull-right mr10" value="Continue Shopping">
          </div>
        </div>
      </div>
      <h3 class="heading3">What would you like to do next?</h3>
      <div class="cartoptionbox">
        <h4 class="heading4"> Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost. </h4>
        <input type="radio" class="radio">
        Use Coupon Code <br>
        <input type="radio" class="radio">
        Use Gift Voucher <br>
        <input type="radio" checked="checked" class="radio">
        Estimate Shipping &amp; Taxes <br>
      </div>
      <div class="cartoptionbox">
            <?php 
$objCountry = new Country();
$countryList = $objCountry->getAll();

?>
        <form class="form-vertical form-inline">
          <h4 class="heading4"> Enter your destination to get a shipping estimate.</h4>
          <fieldset>
            <div class="control-group">
              <label class="control-label">Select list</label>
              <div class="controls">
                <select class="span3 cartcountry">
                  <option>Country:</option>
               <?php foreach ($countryList as $index=>$country) { ?>
			  <option value="<?php echo $country->name;?>"><?php echo $country->name;?></option>
				  <?php } ?>
                </select>
                <select class="span3 cartstate">
                  <option>Region / State:</option>
                  <option>Angus</option>
                  <option>highlands</option>
                </select>
                <input type="submit" class="btn btn-inverse" value="Get Quotes">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </section>
