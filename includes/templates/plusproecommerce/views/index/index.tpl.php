	<?php
// the middle contents...
$homePageContentObject                = new PageContent();
$homePageContentObject->tb_name       = 'tbl_page_contents';
$homePageMiddleContentDetails         = $homePageContentObject->getAllByPageIdAndType('1','COLUMN_CONTENT','homePage');



$bottomcontent       = $homePageContentObject->getAllByPageIdAndType('1','TOP_CONTENT','homePage');
?>

       


       	<div class="container">
		<div class="row">
        	<!--Sidebar Starts-->
		 <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/sidebar.tpl.php'); ?>
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  <section id="featured">
    <h1 class="headingfull"><span>Featured Products</span></h1>
    <div class="sidewidt">
      <ul class="thumbnails">
       <?php 
            
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            $objProducts->listingOrder = 'id DESC';
            //$objProducts->category = $CId;
            $objProducts->isFeatured = 'yes';
            $objProductsResult = $objProducts->search();
           //print_r($objProductsResult);
            foreach ($objProductsResult as $Pindex => $products) {

                if ($Pindex == 6) {
	               break;
                  }
          
            $PobjectID = $products->id;
            
            $objAlbumImage = new Image();
            $objAlbumImage->tb_name = 'tbl_images';
             
            $objAlbumImage->searchStr = "";
            $objAlbumImage->limit = "";
            //$objAlbumImage->listingOrder = 'recordListingId Asc';
            $objAlbumImage->imageObject = 'Product';
            $objAlbumImage->imageObjectKeyId = $PobjectID;
            $albumImageResult = $objAlbumImage->search();
         
      ?>
         <li class="span3">	
          <a class="prdocutname" href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><?php print(ucfirst($products->productName));?></a>
          <div class="thumbnail">	
            <span class ="featured tooltip-test" data-original-title=""></span>
            <?php //foreach ($albumImageResult as $image) {?>
            <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">  
            <?php if(($products->productDefaultImage->recordText)!=""){?>     
                       
                        <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span><img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span> </a>
                         <?php }else{ ?>
                             
                             <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span><img width="240" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/No-Image.png"alt=""></span></span> </a>
                             
                               <?php  }?>    </div>
            <?php //}?>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber($products->unitPrice*CURRENCY_RATE));?></span>
                <span class="newprice"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber($products->discountPrice*CURRENCY_RATE));?></span>
              </div>	
              <a id="productInfo_<?php print($PobjectID); ?>" class="btn pull-right add_item_to_cart_product" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <?php }?>
        <!--  
        <li class="span3">	
          <a class="prdocutname" href="product.html">Best Summer Offer</a>
          <div class="thumbnail">	
            <a href="product.html"><span><span><img alt="" src="images/product-l1.jpg" width="240"></span></span> </a>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice">$2225.00</span>
                <span class="newprice">$2225.00</span>
              </div>	
              <a class="btn pull-right" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <li class="span3">	
          <a class="prdocutname" href="product.html">Best Summer Offer</a>
          <div class="thumbnail">	
            <span class="offer tooltip-test" data-original-title="">Offer</span>
            <a href="product.html"><span><span><img alt="" src="images/product-l1.jpg" width="240"></span></span> </a>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice">$2225.00</span>
                <span class="newprice">$2225.00</span>
              </div>	
              <a class="btn pull-right" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <li class="span3">	
          <a class="prdocutname" href="product.html">Best Summer Offer</a>
          <div class="thumbnail">	
            <a href="product.html"><span><span><img alt="" src="images/product-l3.jpg" width="240"></span></span> </a>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice">$2225.00</span>
                <span class="newprice">$2225.00</span>
              </div>	
              <a class="btn pull-right" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <li class="span3">	
          <a class="prdocutname" href="product.html">Best Summer Offer</a>
          <div class="thumbnail">	
            <span class="sale tooltip-test" data-original-title="">Sale</span>
            <a href="product.html"><span><span><img alt="" src="images/product-l1.jpg" width="240"></span></span> </a>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice">$2225.00</span>
                <span class="newprice">$2225.00</span>
              </div>	
              <a class="btn pull-right" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <li class="span3">	
          <a class="prdocutname" href="product.html">Best Summer Offer</a>
          <div class="thumbnail">	
            <span class="sale tooltip-test" data-original-title="">Sale</span>
            <a href="product.html"><span><span><img alt="" src="images/product-l2.jpg" width="240"></span></span> </a>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice">$2225.00</span>
                <span class="newprice">$2225.00</span>
              </div>	
              <a class="btn pull-right" href="#">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        -->
      </ul>
    </div>
  </section>
</div>
</div>
</div>
      

        
