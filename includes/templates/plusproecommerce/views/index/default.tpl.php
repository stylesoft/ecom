<?php if($pageType == 'contactUsPage') {?>


<!--<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/js/jquery.validationEngine.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/script.js"></script>
<script>	

		$(document).ready(function() {

			
			$("#formID").validationEngine();
		});
		
		
	</script>-->

	
<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#formID").validate({
                    rules: {
                    	name: {
                            required : true
                        },
                        email: {
                            required : true,
                            email : true
                        } 

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "contact/submit.php",
                            data: $("#formID").serialize(), 
                            success: function(Res) {
                            	$("#formID").hide('slow').after("<h1 style='color:#000;'>Thank you!</h1>");
                            },
                            error: function(Res) {
                                $("#formID").hide('slow').after("<h1 style='color:#000;'>Please submit again!</h1>");
                            }
                        });
                        return false;
                    },
                }); 
        });
</script>
<!-- Jquery Validation : End -->


 <div class="container">
    <!--<ul class="breadcrumb">
        <li>
          <a href="<?php print(SITE_BASE_URL); ?>">Home</a>
          <span class="divider">/</span>
        </li>
        <li class="active">Contacts</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            <?php
$objFields = new Fields();
$openingTimeRelatedFields = $objFields->getAllByType('Opening_Times');
?>
 
			<div class="span3">
				<aside>
					<h1 class="headingfull"><span>Contact Us</span></h1>
					<div class="sidewidt p10">
						<p>Phone: <?php print(SITE_CONTACT_NUM_2)?><br>
                            Mobile: <?php print(SITE_CONTACT_NUM_2);?><br>
                            Email: <?php print(SITE_CONTACT_EMAIL);?><br>
                            <br>
                            <?php print($openingTimeRelatedFields[0]->name);?>: <?php print($openingTimeRelatedFields[0]->fieldValue->fieldValue);?><br>
                            <?php print($openingTimeRelatedFields[1]->name);?>: <?php print($openingTimeRelatedFields[1]->fieldValue->fieldValue);?><br>
                            <?php print($openingTimeRelatedFields[2]->name);?>: <?php print($openingTimeRelatedFields[2]->fieldValue->fieldValue);?>
                         </p>
					</div>
				</aside>
				
				 <?php 
            
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            $objProducts->listingOrder = 'id DESC';
            //$objProducts->category = $CId;
            $objProducts->isSpecialOffer = 'Yes';
            $objProductsResult = $objProducts->search();
           //print_r($objProductsResult);
            foreach ($objProductsResult as $Pindex => $products) {

                if ($Pindex == 1) {
	               break;
                  }
      ?>
      <!--  
				<aside>
					<h1 class="headingfull"><span>Best Offer</span> </h1>
					<div class="sidewidt">
						<img alt="" src="<?php //print(SITE_BASE_URL);?>imgs/<?php //print($products->productDefaultImage->recordText);?>">
					</div>
				</aside>
				-->
				<?php }?>
			</div>
            <!--sidebar Ends-->
            <div class="span9">
            <h1 class="productname">Contact Details</h1>
<!--  -->
            <div id="stylized" class="myform">
            <!--  -->
          <form id="formID" method="post" class="form-horizontal contactform span5" novalidate="novalidate">
            <fieldset>
              <div class="control-group">
                <label class="control-label" for="name">Name <span class="required">*</span></label>
                <div class="controls">
                  <input type="text" name="name" value="" id="name" class="required validate[required] span3">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="email">Email <span class="required">*</span></label>
                <div class="controls">
                  <input type="email" name="email" value="" id="email" class="required email required validate[required,custom[email]] span3">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="company">Company</label>
                <div class="controls">
                  <input type="text" name="cim" value="" id="company" class="span3">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="contact">Contact</label>
                <div class="controls">
                  <input type="text" name="phoneNumber" value="" id="contact" >
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="message">Message</label>
                <div class="controls">
                  <textarea name="messagee" id="message" cols="40" rows="6"></textarea>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" id="submit_id" value="Submit" class="btn btn-inverse">
                <input type="reset" value="Reset" class="btn">
              </div>
            </fieldset>
          </form>
<!--  -->
          </div>
          <!--  -->
          <div class="span4 margin-none">
          	<?php print(SITE_GMAP_CODE);?>
          </div>
        </div>
</div>
</div>
    

<?php } else {?>

		<?php
// the middle contents...
$homePageContentObject                = new PageContent();
$homePageContentObject->tb_name       = 'tbl_page_contents';
$homePageMiddleContentDetails         = $homePageContentObject->getAllByPageIdAndType('9','COLUMN_CONTENT','mainPage');
//print_r($homePageMiddleContentDetails);

?>
		<section id="checkout">
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="#">Home</a><span class="divider">/</span></li>
        <li class="active"><?php print ($pageTitle)?></li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <h1 class="productname"><?php print ($pageTitle)?></h1>
      <div class="container">
      
      <div class="span7">
           <?php print(htmlspecialchars_decode($pageBody,ENT_QUOTES));?>
</div>
      </div>
    </div>
  </section>

    

<?php }
