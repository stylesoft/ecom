<!--<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/pluspro.js"></script>-->
<script type="text/javascript">
$(document).ready(function(){
    $( "#SortBy" ).change(function () {
    
     if($("option:selected", this ).text() =="Category"){
       $("#category_sort").show();
    }else{
       $("#category_sort").hide();        
    }
     if($("option:selected", this ).text() =="Brand"){
       $("#brand_sort").show();
    }else{
       $("#brand_sort").hide();        
    }
})
});
//page == 'product.html?is_featured=yes' || page == 'product.html?special_offers=Yes' || 
function goToPage(page,elemid) {
 if(page == 'product.html?category_id='+elemid || page == 'product.html?brand_id='+elemid){
     window.location = page;
 }   
}
var stokAvlbl = <?php echo $stockavalibility;?>;
$( document ).on( "input", "#cartQty", function() {
  if($('#cartQty').val() == 0 || $('#cartQty').val() > stokAvlbl){
    $('#cartQty').val('');
  }
});

</script>

<script>
        $(document).ready(function() {
                $("#review-submit-form").validate({
                    
		    //Submit Handler
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(ADMIN_BASE_URL);?>modules/reviews/reviews_request.php",
                            data: $("#review-submit-form").serialize(), 
                            success: function(Res) {
                    
                                console.log(Res);
                                if(Res == "failure"){
                                	$("#review-submit-form").hide('slow').after("<h1 style='color:#F10505;'> failure!</h1>");
                                } else if(Res == "add-success"){
                                    //console.log('yes');
                                	$("#review-submit-form").hide().after("<h1 style='color:#000;'>Successfully added!</h1>");
                                }else if(Res == "edit-success"){
                                    //console.log('yes');
                                	$("#review-submit-form").hide().after("<h1 style='color:#000;'>Successfully updated!</h1>");
                                }else if(Res == "rating-failure"){
                                   
                                	 $("#rate-error").show();
                                     
                                }else if(Res == "title-failure"){
                                   
                                	 $("#title-error").show();
                                     
                                }else if(Res == "des-failure"){
                                    
                                	 $("#des-error").show();
                                }
				//$(window).scrollTop(0);
                            	
                            },
                            error: function(Res) {
                                $("#review-submit-form").hide('slow').after("<h1 style='color:#000;'>Please submit again!</h1>");
                            }
                        });
                        return false;
                    }
                    //Submit Handler
                }); 
        });
</script>


<?php if ($_GET['productId']) {?>
<section id="product">
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL); ?>">Home</a><span class="divider">/</span></li>
        <li class="active">Product</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
               <?php 
            
        	 
        	?>
      <!-- Product Details-->
      <div class="row">
        <div class="span5">
  
          <ul class="thumbnails mainimage">
              
              <?php if(($objProductsResult->productDefaultImage->recordText) ==""){?>
              <li class="span5"> 
           
              
                
                             
                            <img width="240" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/No-Image.png"alt="">
                             
                              
          
            
            </li>
              <?php }else{?> 
          <?php foreach ($albumImageResult as $image) {?>
            <li class="span5">
           
              <a  rel="position: 'inside' , showTitle: false, adjustX:-4, adjustY:-4" class="thumbnail cloud-zoom" href="<?php print(SITE_BASE_URL);?>imgs/<?php print($image->recordText);?>">
                 
                  
                <img alt="" src="<?php print(SITE_BASE_URL);?>imgs/<?php print($image->recordText);?>">
               
                
            
              </a>
            
            </li>
              <?php }}?>
            
          </ul>
           
          <ul class="thumbnails mainimage">
           <?php foreach ($albumImageResult as $image) {?>
            
           <li class="producthtumb">
           
              <a href="#"><span><span><img width="240" src="<?php print(SITE_BASE_URL);?>imgs/<?php print($image->recordText);?>" alt=""></span></span> </a>
            
            </li>
            
            <?php }?>
          
          </ul>
       <div class="image1" id="productImageWrapID_<?php print($PobjectID); ?>" style="display:none;">
        <img alt="" src="<?php print(SITE_BASE_URL);?>imgs/<?php print($objProductsResult->productDefaultImage->recordText);?>" height="50" width="50">
       </div>
        </div>
        <div class="span7">
          <div class="row">
            <div class="span7">
              <h1 class="productname"><?php print($objProductsResult->productName);?></h1>
              <div class="productprice">
                <div class="proldprice"><?php print(CURRENCY_CODE); ?><?php print($objProductsResult->unitPrice);?></div>
                <div class="prnewprice span2 margin-none"><?php print(CURRENCY_CODE); ?><?php print($objProductsResult->discountPrice);?></div>
<div class="pull-right"><?php if ($stockavalibility != 0){?> <span class="label label-success">Availability :</span> <?=$stockavalibility?> Items in stock <? }else{ echo '&nbsp;&nbsp;'.'<span class="label label-error">Not Available</span>'; }?></div>
              </div>
              <div class="quantitybox">
              	<input type="text" class="span1 selectqty" placeholder="Qty." name="" id="cartQty"  style="float:left">
              	<select class="selectsize">
                  <option>Select Color</option>
                
                  <option>Red</option>
                  <option>Green</option>
                  <option>Blue</option>
                  <option>Black</option>
                </select>
                <select class="selectqty crt_size" name="cartSize" id="cartSize">
                  <option>Size</option>
               <?php foreach ($Sizes as $Sindex=>$size){?>
             <option><?php print($size->size->name);?></option>
                  <?php }?>
                </select>
                <?php if($stockavalibility != 0 ){?>
              
                <a id="productInfo_<?php print($PobjectID); ?>" class="btn btnaddcrt btn-success pull-left" href="#">Add to Cart</a>
                <?php }else{?>
               
                 <p  class="btn btncrt btn-error" >Add to Cart</p>
                <?php } ?>
                <div class="links  productlinks">
                    
                  <a class="wishlist" href="wishlist.html">wishlist</a>
                  <?php if($objProductsResult->Comparestatus == 'Enabled'){?>
                  <a class="compare" href="compare.html">compare</a>
                  <?php }else{?>
                  <div class="emptybx"></div>
                  <?php }?>
                </div>
              </div>
              <div class="productdesc">
                <ul class="nav nav-tabs" id="myTab">
                  <li class="active"><a href="#description">Description</a></li>
                  <li><a href="#specification">Specification</a></li>
                  <li><a href="#review">Review</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="description">
              <?php print(htmlspecialchars_decode($objProductsResult->productDescription,ENT_QUOTES));?>
                  </div>
                  <div class="tab-pane " id="specification">
                    <ul class="productinfo">
                      <li>
                        <span class="productinfoleft"> Brand:</span> <?php print($objProductsResult->productBrand->brand_name);?> </li>
                      <li>
                        <span class="productinfoleft"> Product Code:</span> <?php print($objProductsResult->productCode);?> </li>
                      <li>
                        <span class="productinfoleft"> Reward Points:</span> 20 </li>
                      <li>
                        <span class="productinfoleft"> Availability: </span> <?php if ($stockavalibility != '') {?>In Stock <?php } else{?> Out Stock <?php }?></li>
                      <li>
                        <span class="productinfoleft"> Old Price: </span> <?php print(CURRENCY_CODE); ?><?php print($objProductsResult->discountPrice);?> </li>
                      <li>
                        <span class="productinfoleft"> Ex Tax: </span> $500.00 </li>
                     
                    </ul>
                  </div>
                  <?php 
                                $objReviews = new Reviews();
                                $objReviews->tb_name = 'tbl_reviews';
                                $ip = $objReviews->get_client_ip();
                    
                                
                                $objReviews->reviews_status = 'Approved';;
                                $objReviews->product_id = $productId;
                                $reviewResult = $objReviews->searchApproved();
                               
                               
                                //print_r($reviewResult);
                                
                                //$visited_client = $objReviews->getAllByIP($ip);
                               
                    
                    
                    
                    ?>
                  <div class="tab-pane" id="review">
                    <ul class="reveiw">
                        <?php if(count($reviewResult) != 0){
                              foreach($reviewResult As $indexre=>$reviewRes){
                        
?>
                      <li>
                        <h4 class="title"><?php print($reviewRes->reviews_subject)?><span class="date"><i class="icon-calendar"></i><?php print($reviewRes->added_on)?></span></h4>


                           
                        
                        <span class="reveiwdetails"><?php print($reviewRes->reviews_description)?> </span>
                        
                      </li>
                      <?php }}?>
                    </ul>
                    <?php  if(isset($_SESSION['SESS_CUSTOMER_INFO'])){
                                     $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
                                     $customerName = $customerInfo->firstName;
                                     $customerId= $customerInfo->id;
                                     
                                        $objReviews->product_id = $productId;
                                        $objReviews->IP = $ip;
                                        $objReviews->added_by = $customerId;
                                        $visited_client = $objReviews->search();
                                        
                                     if(count($visited_client)==0){
                                         $action = 'add';
                                     }else{
                                         $action ='edit';
                                     }
                                     
                                     
                                     
                                     ?>
                    <form class="form-vertical" id="review-submit-form" method = "post" action="<?php print(ADMIN_BASE_URL);?>modules/reviews/reviews_request.php">
                        
                             <fieldse class="rating">
                                
                                <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="5 stars">5 stars</label>
                                <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="4 stars">4 stars</label>
                                <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="3 stars">3 stars</label>
                                <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="2 stars">2 stars</label>
                                <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="1 stars">1 star</label>
                            </fieldse>
                        <div id="rate-error" style="color:red; margin-left: 300px; display:none">Please rate this  product</div>
                            
                       
                      <h3>Write a Review</h3>  

                      <fieldset>
                        <div class="control-group">
                          <label class="control-label">Title</label>
                          <div class="controls">
                            <input type="text" class="span3" name="reviews_subject"><div id="title-error" style="color:red;  display:none">title is required</div>
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Description</label>
                          <div class="controls">
                            <textarea rows="3"  class="span3" name="reviews_description"></textarea>
                          </div>
                          <div id="des-error" style="color:red;  display:none">description is required</div>
                          
                          <input type="hidden" name="ip" id="ip" value="<?php echo $ip; ?>"/>
                          <?php if($action == 'edit'){
                            foreach($visited_client As $vis){ ?>
                            <input type="hidden" name="id" id="id" value="<?php echo $vis->id; ?>"/>
                            <?php }} ?>
                          <input type = "hidden" name ="url" value="<?php echo $_SERVER['REQUEST_URI']?>" />
                          <input type="hidden" name="product_id" id="product_id" value="<?php echo $productId; ?>"/>
                          <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action?>"/>
                          <input type="hidden" name="added_by" id="customerName" value="<?php echo $customerId?>"/>
                          
                        </div>
                      </fieldset>
                      <input type="submit" class="btn btn-inverse" value="continue" name="submit" id="submit" >
                    </form><?php }?>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <!-- Product Description tab & comments-->
      
    </div>
  </section>
  <!--  Related Products-->
  <section id="related">
    <div class="container">
      <h1 class="headingfull"><span>Related Products</span></h1>
      <section id="thumbnails">
      <div class="sidewidt" style="padding:10px;">
        <ul class="thumbnails">
        			    <?php 
            
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            $objProducts->listingOrder = 'id DESC';
            //$objProducts->category = $CId;
            //$objProducts->isSpecialOffer = 'Yes';
//            echo $productBrandId;
//            exit();
                       
            $objProducts->productBrand = $productBrandId;
            $objProductsResult = $objProducts->search();
           //print_r($objProductsResult);
            foreach ($objProductsResult as $Pindex => $products) {

                if ($Pindex == 4) {
	               break;
                  }
          /*
            $PobjectID = $products->id;
            
            $objAlbumImage = new Image();
            $objAlbumImage->tb_name = 'tbl_images';
             
            $objAlbumImage->searchStr = "";
            $objAlbumImage->limit = "";
            //$objAlbumImage->listingOrder = 'recordListingId Asc';
            $objAlbumImage->imageObject = 'Product';
            $objAlbumImage->imageObjectKeyId = $PobjectID;
            $albumImageResult = $objAlbumImage->search();
            */
         
      ?>
        <li class="span3">
            
            
             <?php 
                     $text = $products->productName;
                    if($text != ''){ 
 
                    $product_N = $objProducts->limit_words($text,3);}
?>
                      
          <a href="product.html" class="prdocutname"><?php print($product_N);?></a>
          <div class="thumbnail">	
            <span data-original-title="" class="sale tooltip-test">Sale</span>
              <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">
                             
                             
                             <?php if(($products->productDefaultImage->recordText)!=""){?>
                        <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php print($products->id); ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span>
                                    
                                    <img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span> </a>
                                <?php }else{?>
                             
                             <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php print($products->id); ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span><img width="240" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/No-Image.png"alt=""></span></span> </a>
                             
                               <?php  }?>
                                
                         </div>
            <div class="caption">
              <div class="price pull-left">	
                <span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print($products->unitPrice);?></span>
                <span class="newprice"><?php print(CURRENCY_CODE); ?><?php print($products->discountPrice);?></span>
              </div>	
              <a id="productInfo_<?php print($products->id); ?>" href="#" class="btn pull-right add_item_to_cart_product1">Add to Cart</a>
              <div class="clearfix"></div>
               
            </div>
          </div>
        </li>
        <?php }?>
   
      </ul>
        </div>
      </section>
    </div>
  </section>
<?php } else {?>
<div class="container">
    <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL); ?>">Home</a><span class="divider">/</span></li>
        <li class="active">Store</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<div class="span12">
                   
          <?php if($objbrandName){?>
          <h1 class="productname"><?php print($objbrandName->brand_name);?>&nbsp;Collection</h1>
          <legend><?php print($objbrandName->brand_description);?></legend>
          <?php }else{?>
          <h1 class="productname">Browse All Store Products</h1>
          <?php }?>
         
          <section id="latest">
            <div class="row">
                  <?php 

                                     // set the pagination
                                     //-------------------------
                                     $searchQ        = "";
                                     $recLimit       = "";
                                     $orderStr       = "";
                                     
                                     $objPaggination = new Paggination();
                                     $rmax           = 12;
                                     $page           = "";
                                     if(isset($_GET['rmax'])){
                                     	$rmax		= 	$_GET['rmax'];
                                     }
                                     
                                     	
                                     if($rmax != ""){
                                     	$objPaggination->ResultsPerPage = $rmax;
                                     }
                                     
                                       if(isset($_GET['category_id'])){
                                       
                                     $categoryId	= $_GET['category_id'];
                                     }	
                                     if(isset($_GET['brand_id'])){
                                       
                                     $brandId	= $_GET['brand_id'];
                                     
                                     }	
                                     	
                                     if(isset($_GET['special_offers'])){
                                       
                                     $Special_offers	= $_GET['special_offers'];
                                     }		
                                     if(isset($_GET['is_featured'])){
                                       
                                     $is_feartured = $_GET['is_featured'];
                                   
                                     }
                                     
                                     if(isset($_GET['page'])){
                                     	$page	= $_GET['page'];
                                     }
                                     if($page == ''){
                                     	$page = 1;
                                     }
                                     
                                     if(isset($_GET['q'])){
                                     	$searchQ = $_GET['q'];
                                     }
                                     
                                     $objProducts = new Product();
                                     $objProducts->tb_name = "tbl_product";
                                     $objProducts->searchStr = $searchQ;
                                     $objProducts->category = $categoryId;
                                     $objProducts->productBrand =  $brandId;
                                     $objProducts->isSpecialOffer = $Special_offers;
                                     $objProducts->isFeatured = $is_feartured;
                                     //$objProducts->category = $categoryId;
                                     $totalResult = $objProducts->countRec();
                                
                                     $objPaggination->CurrentPage  = $page;
                                     $objPaggination->TotalResults = $totalResult;
                                     $paginationData               = $objPaggination->getPaggingData();
                                     $pageLimit1                   = $paginationData['MYSQL_LIMIT1'];
                                     $pageLimit2                   = $paginationData['MYSQL_LIMIT2'];
                                     $limit 			      = " LIMIT $pageLimit1,$pageLimit2";
                                     
                                     
                                     
            
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            $objProducts->listingOrder = 'id DESC';
            $objProducts->category = $categoryId;
            $objProducts->productBrand =  $brandId;
            $objProducts->isSpecialOffer = $Special_offers;
            $objProducts->isFeatured = $is_feartured;
            $objProducts->searchStr = $search;
            $objProducts->limit = $limit;
            //$objProducts->category = $categoryId;
            $objProductsResult = $objProducts->search();
            //print_r($objProductsResult);
            $pagginationDataResult        = $objPaggination;?>
                <div class="span12">
                <div class="sorting well">
                  <form class=" form-inline pull-left">
                      <div class="span3">
                    Sort By :
                    <select class="span2" id = "SortBy" name ="SortBy" onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" >
                      <option value = "">Default</option>
                      <option value = "Name">Name</option>
                      <option value = "Price">Price</option>
                      <option value = "Rating">Rating </option>
                      <option value = "Color">Color</option>
                      <option value = "Category" <?php if(!empty($categoryId)){ ?> selected='selected' <?php }?> >Category</option>
                       <option value = "Brand" <?php if(!empty($brandId)){ ?> selected='selected' <?php }?> >Brand</option>
<!--                       <option value = "product.html?special_offers=Yes" <?php if($Special_offers == 'Yes'){ ?> selected='selected' <?php }?>>Special Offers</option>
                       <option value = "product.html?is_featured=yes" <?php if($is_feartured == 'yes'){ ?> selected='selected' <?php }?>>Featured</option>-->
                    </select></div>
                    &nbsp;&nbsp;
                    
                    <select class="span1" style="display: none;">
                      <option>10</option>
                      <option>15</option>
                      <option>20</option>
                      <option>25</option>
                      <option>30</option>
                    </select>
                     <div class="span3">
                     <div id ="category_sort" <?php if(empty($categoryId)){?>style="display: none;"<?php } ?>>
                        Show:
                        &nbsp;&nbsp;<select onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" class="span2"id = "sort_by_category" name ="sort_by_category" > 
                      <option>--Please Select--</option>
                      <?php if (count($categoryInfo)){
                              foreach ($categoryInfo As $cIndex=>$categoryDetails){
                                  if($categoryDetails->category_name == 'Default'){
                                      
                                  }else{
                                  ?>
                      <option value="product.html?category_id=<?php print($categoryDetails->id)?>"  id='<?php print($categoryDetails->id)?>' data-bid='category'<?php if ($categoryDetails->id == $categoryId){ ?> selected='selected' <?php }?>><?php print($categoryDetails->category_name)?></option>
                      <?php }}}?>
                      
                     </select> </div>
                      
                    <div id ="brand_sort" <?php if(empty($brandId)){?>style="display: none;"<?php } ?>>
                        Show:
                        &nbsp;&nbsp;<select onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" class="span2"id = "sort_by_brand" name ="sort_by_brand"  >
                      <option>--Please Select--</option>
                      <?php if (count($brandInfo)){
                              foreach ($brandInfo As $bIndex=>$brandDetails){
                                  
                                  if($brandDetails->brand_name == 'Default'){
                                      
                                  }else{?>
                                  ?>
                      <option value="product.html?brand_id=<?php print($brandDetails->id)?>" id="<?php print($brandDetails->id)?>" data-bid='brand' <?php if ($brandDetails->id == $brandId){ ?> selected='selected' <?php }?>><?php print($brandDetails->brand_name)?></option>
                      <?php }}}?>
                        </select></div></div>
                     
                     
                    
                  </form>
                  <div class="btn-group pull-right">
                    <button id="list" class="btn"><i class="icon-th-list"></i>
                    </button>
                    <button id="grid" class="btn btn-success"><i class="icon-th icon-white"></i></button>
                  </div>
                </div></div>
                
                <?php if(count($objProductsResult)==0){?>
                <div class="span11">
                <div class="alert alert-success">
            
                    No Products to Display  </div></div>
                <?php }else{ ?>
              <div class="span12">
                
                <section id="thumbnails">
                  <ul class="thumbnails grid" style="display: block;">
                                   
       <?php    //print_r($pagginationDataResult);
            foreach ($objProductsResult as $Pindex => $products) {

                
          
            $PobjectID = $products->id;
            

            /*
            $objAlbumImage = new Image();
            $objAlbumImage->tb_name = 'tbl_images';
             
            $objAlbumImage->searchStr = "";
            $objAlbumImage->limit = "";
            //$objAlbumImage->listingOrder = 'recordListingId Asc';
            $objAlbumImage->imageObject = 'Product';
            $objAlbumImage->imageObjectKeyId = $PobjectID;
            $albumImageResult = $objAlbumImage->search();
         */
      ?>
                    <li class="span3">

                    <?php 
                     $text = $products->productName;
                    if($text != ''){ 
 
                    $product_N = $objProducts->limit_words($text,3);}
?>
                      
                    
                      <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>" class="prdocutname"><?php print($product_N);?></a>

                     
                      <div class="thumbnail">
                        <span <?php if($Special_offers == 'Yes'){ ?>class="offer tooltip-test"<?php }elseif($is_feartured == 'yes'){ ?> class ="featured tooltip-test" <?php }elseif($page == 1){ ?>class="latest tooltip-test"<?php }else{?>class="sale tooltip-test" <?php }?>data-original-title=""></span>
                         <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">
                             
                             
                             <?php if(($products->productDefaultImage->recordText)!=""){?>
                        <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span>
                                    
                                    <img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span> </a>
                                <?php }else{ ?>
                             
                             <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>"><span><span><img width="240" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/No-Image.png"alt=""></span></span> </a>
                             
                               <?php  }?>
                                
                         </div>
                        <div class="caption">
                          <div class="price pull-left">	
                            <span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print number_format(($products->unitPrice*CURRENCY_RATE),2);?></span>
                            <span class="newprice"><?php print(CURRENCY_CODE); ?><?php print($products->discountPrice);?></span>
                          </div>	
                          <a id="productInfo_<?php print($PobjectID); ?>" href="#" class="btn pull-right add_item_to_cart_product">Add to Cart</a>
                          <div class="clearfix"></div>
                           
                        </div>
                      </div>
                    </li>
                    <?php }?>
               
                  </ul>
                  <ul class="thumbnails list" style="display: none;">
                   <?php foreach ($objProductsResult as $Pindex => $products) { 
                   	$PobjectID = $products->id;
                   	?> 
                    <li class="span12">
                      <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>" class="prdocutname"><?php print($products->productName);?></a>
                      <div class="thumbnail">
                        <div class="row">
                          <div class="image" id="productImageWrapIDS_<?php print($PobjectID); ?>">
                          <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>" class="span3"><span><span><img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span></a>
                          </div>
                         <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">
                          <div class="details span6"><?php print(htmlspecialchars_decode($products->productSmallDescription,ENT_QUOTES));?></div>
                          <div class="caption pull-right margin-none">
                          	<table class="table table-striped table-bordered table-condensed">
                            <tbody>
                              <tr>
                              	<td>
                                <div class="price pull-right">	
                            		<span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print($products->unitPrice*CURRENCY_RATE);?></span>
                            		<span class="newprice"><?php print(CURRENCY_CODE); ?><?php print($products->discountPrice);?></span>
                          		</div>
                          		</td>
                              </tr>
                              <tr>
                              	<td><a id="productInfo_<?php print($PobjectID); ?>" class="btn pull-right add_item_to_cart_product1" href="#">Add to Cart</a></td>
                              </tr>
                              <tr>
                                <td>
                                  <span class="links pull-left">
                                    <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>&productBrandId=<?php echo $products->productBrand->id; ?>" class="info">info</a>
                                    <a href="wishlist.html" class="wishlist">wishlist</a>
                                    <a href="compare.html" class="compare">compare</a>
                                  </span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        </div>
                      </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <div class="pagination pull-right">
                    <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/paggination.tpl.php'); ?>
                  <!--  
                    <ul>
                      <li><a href="#">Prev</a>
                      </li>
                      <li class="active">
                        <a href="#">1</a>
                      </li>
                      <li><a href="#">2</a>
                      </li>
                      <li><a href="#">3</a>
                      </li>
                      <li><a href="#">4</a>
                      </li>
                      <li><a href="#">Next</a>
                      </li>
                      
                    </ul>
                    -->
                  </div>
                </section>
                </div><?php }?>
            </div>
          </section>
        </div>
</div>
</div>
<?php }?>
