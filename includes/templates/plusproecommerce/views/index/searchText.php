<script type="text/javascript">
$(document).ready(function(){
    $( "#SortBy" ).change(function () {
    
     if($("option:selected", this ).text() =="Category"){
       $("#category_sort").show();
    }else{
       $("#category_sort").hide();        
    }
     if($("option:selected", this ).text() =="Brand"){
       $("#brand_sort").show();
    }else{
       $("#brand_sort").hide();        
    }
})
});


function goToPage(page,elemid) {
    //console.log(elemid);
   
    //http://html5doctor.com/html5-custom-data-attributes/    
    // 'Getting' data-attributes using getAttribute
//var vg = document.getElementById(elemid);
//var vgq = vg.getAttribute('data-bid'); // fruitCount = '12'
      //console.log(vgq);


    if(page == 'product.html?is_featured=yes' || page == 'product.html?special_offers=Yes' || page == 'product.html?category_id='+elemid || page == 'product.html?brand_id='+elemid){
     window.location = page;
 }
    
}

</script>


<div class="container">
    <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL); ?>">Home</a><span class="divider">/</span></li>
        <li class="active">Search</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
                    
                    
                    <?php 

                                     // set the pagination
                                     //-------------------------
                                     $searchQ        = "";
                                     $recLimit       = "";
                                     $orderStr       = "";
                                     
                                     $objPaggination = new Paggination();
                                     $rmax           = 10;
                                     $page           = "";
                                     if(isset($_GET['rmax'])){
                                     	$rmax		= 	$_GET['rmax'];
                                     }
                                     if($_POST){
                                         
                                         $searchtextQ = $_POST['editbox_search'];
                                         if ($searchtextQ == 'Search here...'){
                                             $searchtext = '';
                                         }else{
                                             $searchtext = $searchtextQ;
                                         }
                                         
                                     }
                                     
                                     if($_GET['searchtext']){
                                         
                                         $searchtext = $_GET['searchtext'];
                                         echo $searchtext;
                                     }
                                     
                                     	
                                     if($rmax != ""){
                                     	$objPaggination->ResultsPerPage = $rmax;
                                     }
                                     
                                       if(isset($_GET['category_id'])){
                                       
                                     $categoryId	= $_GET['category_id'];
                                     }	
                                     if(isset($_GET['brand_id'])){
                                       
                                     $brandId	= $_GET['brand_id'];
                                     
                                     }	
                                     	
                                     if(isset($_GET['special_offers'])){
                                       
                                     $Special_offers	= $_GET['special_offers'];
                                     }	
                                     
                                     if(isset($_GET['is_featured'])){
                                       
                                     $is_feartured = $_GET['is_featured'];
                                   
                                     }
                                     
                                     
                                     if(isset($_GET['page'])){
                                     	$page	= $_GET['page'];
                                     }
                                     if($page == ''){
                                     	$page = 1;
                                     }
                                     
                                     if(isset($_GET['q'])){
                                     	$searchQ = $_GET['q'];
                                     }
                                     
                                     $objProducts = new Product();
                                     $objProducts->tb_name = "tbl_product";
                                     $objProducts->searchStr = $searchtext;
                                     $objProducts->category = $categoryId;
                                     $objProducts->productBrand =  $brandId;
                                     $objProducts->isSpecialOffer = $Special_offers;
                                     $objProducts->isFeatured = $is_feartured;
                                     //$objProducts->category = $categoryId;
                                     $totalResult = $objProducts->countRec();
                                
                                     $objPaggination->CurrentPage  = $page;
                                     $objPaggination->TotalResults = $totalResult;
                                     $paginationData               = $objPaggination->getPaggingData();
                                     $pageLimit1                   = $paginationData['MYSQL_LIMIT1'];
                                     $pageLimit2                   = $paginationData['MYSQL_LIMIT2'];
                                     $limit 			      = " LIMIT $pageLimit1,$pageLimit2";
                                     
                                     
                                     
            
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            $objProducts->listingOrder = 'id DESC';
            $objProducts->category = $categoryId;
            $objProducts->productBrand =  $brandId;
            $objProducts->isSpecialOffer = $Special_offers;
            $objProducts->isFeatured = $is_feartured;
            $objProducts->searchStr = $searchtext;
            $objProducts->limit = $limit;
            //$objProducts->category = $categoryId;
            $objProductsResult = $objProducts->search();
            //print_r($objProductsResult);
            $pagginationDataResult        = $objPaggination;?>
        	<div class="span12">
          <h1 class="productname">Search Results for <?php echo  $searchtext?></h1>
          
          <?php if($objbrandName){?>
          <legend><?php print($objbrandName->brand_name);?>&nbsp;Collection</legend>
          <?php }?>
         
          <section id="latest">
            <div class="row">
                  
                <div class="span12">
                <div class="sorting well">
                  <form class=" form-inline pull-left">
                      <div class="span3">
                    Sort By :
                    <select class="span2" id = "SortBy" name ="SortBy" onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" >
                      <option value = "">Default</option>
                      <option value = "Name">Name</option>
                      <option value = "Price">Price</option>
                      <option value = "Rating">Rating </option>
                      <option value = "Color">Color</option>
                      <option value = "Category" <?php if(!empty($categoryId)){ ?> selected='selected' <?php }?> >Category</option>
                       <option value = "Brand" <?php if(!empty($brandId)){ ?> selected='selected' <?php }?> >Brand</option>
                       <option value = "product.html?special_offers=Yes" <?php if($Special_offers == 'Yes'){ ?> selected='selected' <?php }?>>Special Offers</option>
                       <option value = "product.html?is_featured=yes" <?php if($is_feartured == 'yes'){ ?> selected='selected' <?php }?>>Featured</option>
                    </select></div>
                    &nbsp;&nbsp;
                    
                    <select class="span1" style="display: none;">
                      <option>10</option>
                      <option>15</option>
                      <option>20</option>
                      <option>25</option>
                      <option>30</option>
                    </select>
                     <div class="span3">
                     <div id ="category_sort" <?php if(empty($categoryId)){?>style="display: none;"<?php } ?>>
                        Show:
                        &nbsp;&nbsp;<select onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" class="span2"id = "sort_by_category" name ="sort_by_category" > 
                      <option>--Please Select--</option>
                      <?php if (count($categoryInfo)){
                              foreach ($categoryInfo As $cIndex=>$categoryDetails){?>
                      <option value="product.html?category_id=<?php print($categoryDetails->id)?>"  id='<?php print($categoryDetails->id)?>' data-bid='category'<?php if ($categoryDetails->id == $categoryId){ ?> selected='selected' <?php }?>><?php print($categoryDetails->category_name)?></option>
                      <?php }}?>
                      
                     </select> </div>
                      
                    <div id ="brand_sort" <?php if(empty($brandId)){?>style="display: none;"<?php } ?>>
                        Show:
                        &nbsp;&nbsp;<select onchange="javascript:goToPage(options[selectedIndex].value,options[selectedIndex].id)" class="span2"id = "sort_by_brand" name ="sort_by_brand"  >
                      <option>--Please Select--</option>
                      <?php if (count($brandInfo)){
                              foreach ($brandInfo As $bIndex=>$brandDetails){?>
                      <option value="product.html?brand_id=<?php print($brandDetails->id)?>" id="<?php print($brandDetails->id)?>" data-bid='brand' <?php if ($brandDetails->id == $brandId){ ?> selected='selected' <?php }?>><?php print($brandDetails->brand_name)?></option>
                      <?php }}?>
                        </select></div></div>
                     
                     
                    
                  </form>
                  <div class="btn-group pull-right">
                    <button id="list" class="btn"><i class="icon-th-list"></i>
                    </button>
                    <button id="grid" class="btn btn-success"><i class="icon-th icon-white"></i></button>
                  </div>
                </div></div>
                
                <?php if(count($objProductsResult)==0){?>
                <div class="span11">
                <div class="alert alert-success">
            
                    No Results to Display  </div></div>
                <?php }else{ ?>
              <div class="span12">
                
                <section id="thumbnails">
                  <ul class="thumbnails grid" style="display: block;">
                                   
       <?php    //print_r($pagginationDataResult);
            foreach ($objProductsResult as $Pindex => $products) {

                
          
            $PobjectID = $products->id;
            /*
            $objAlbumImage = new Image();
            $objAlbumImage->tb_name = 'tbl_images';
             
            $objAlbumImage->searchStr = "";
            $objAlbumImage->limit = "";
            //$objAlbumImage->listingOrder = 'recordListingId Asc';
            $objAlbumImage->imageObject = 'Product';
            $objAlbumImage->imageObjectKeyId = $PobjectID;
            $albumImageResult = $objAlbumImage->search();
         */
      ?>
                    <li class="span3">
                    
                      <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>" class="prdocutname"><?php 
                      
                      $productN = $products->productName;
                      $productN = substr($productN, 0, 25);                    
                      $productName = preg_replace('/ [^ ]*$/', ' ', $productN);
                      
                      print($productName);?></a>
                     
                      <div class="thumbnail">
                        <span class="sale tooltip-test" data-original-title="">Sale</span>
                         <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">
                        <?php if(($products->productDefaultImage->recordText)!=""){?>     
                       
                        <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>"><span><span><img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span> </a>
                         <?php }else{ ?>
                             
                             <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>"><span><span><img width="240" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/No-Image.png"alt=""></span></span> </a>
                             
                               <?php  }?>    </div>
                        <div class="caption">
                          <div class="price pull-left">	
                            <span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print($products->unitPrice);?></span>
                            <span class="newprice"><?php print(CURRENCY_CODE); ?><?php print($products->discountPrice);?></span>
                          </div>	
                          <a id="productInfo_<?php print($PobjectID); ?>" href="#" class="btn pull-right add_item_to_cart_product">Add to Cart</a>
                          <div class="clearfix"></div>
                           
                        </div>
                      </div>
                    </li>
                    <?php }?>
               
                  </ul>
                  <ul class="thumbnails list" style="display: none;">
                   <?php foreach ($objProductsResult as $Pindex => $products) { 
                   	$PobjectID = $products->id;
                   	?> 
                    <li class="span12">
                      <a href="#" class="prdocutname"><?php print($products->productName);?></a>
                      <div class="thumbnail">
                        <div class="row">
                          <div class="image" id="productImageWrapIDS_<?php print($PobjectID); ?>">
                          <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>" class="span3"><span><span><img width="240" src="<?php print(SITE_BASE_URL);?>includes/extLibs/mythumb.php?file=<?php print(SITE_BASE_URL);?>imgs/<?php print($products->productDefaultImage->recordText);?>&width=240&height=" alt=""></span></span></a>
                          </div>
                         <div class="image" id="productImageWrapID_<?php print($PobjectID); ?>">
                          <div class="details span6"><?php print(htmlspecialchars_decode($products->productSmallDescription,ENT_QUOTES));?></div>
                          <div class="caption pull-right margin-none">
                          	<table class="table table-striped table-bordered table-condensed">
                            <tbody>
                              <tr>
                              	<td>
                                <div class="price pull-right">	
                            		<span class="oldprice"><?php print(CURRENCY_CODE); ?><?php print($products->unitPrice);?></span>
                            		<span class="newprice"><?php print(CURRENCY_CODE); ?><?php print($products->discountPrice);?></span>
                          		</div>
                          		</td>
                              </tr>
                              <tr>
                              	<td><a id="productInfo_<?php print($PobjectID); ?>" class="btn pull-right add_item_to_cart_product1" href="#">Add to Cart</a></td>
                              </tr>
                              <tr>
                                <td>
                                  <span class="links pull-left">
                                    <a href="<?php print(SITE_BASE_URL);?>product.html?productId=<?php echo $PobjectID; ?>" class="info">info</a>
                                    <a href="wishlist.html" class="wishlist">wishlist</a>
                                    <a href="compare.html" class="compare">compare</a>
                                  </span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        </div>
                      </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <div class="pagination pull-right">
                    <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/paggination.tpl.php'); ?>
                  <!--  
                    <ul>
                      <li><a href="#">Prev</a>
                      </li>
                      <li class="active">
                        <a href="#">1</a>
                      </li>
                      <li><a href="#">2</a>
                      </li>
                      <li><a href="#">3</a>
                      </li>
                      <li><a href="#">4</a>
                      </li>
                      <li><a href="#">Next</a>
                      </li>
                      
                    </ul>
                    -->
                  </div>
                </section>
                </div><?php }?>
            </div>
          </section>
        </div>
</div>
</div>
