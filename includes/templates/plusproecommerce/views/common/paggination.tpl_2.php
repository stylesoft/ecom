<?php 
$totalResult 		= $pagginationDataResult->TotalResults;
$totalPages 		= $pagginationDataResult->TotalPages;
$currentPage 		= $pagginationDataResult->CurrentPage;
$pageParam   		= $pagginationDataResult->PageVarName;
$pageNumbers 		= $pagginationDataResult->numbers;
$prevPage    		= $pagginationDataResult->ResultArray['PREV_PAGE'];
$currentPage    	= $pagginationDataResult->ResultArray['CURRENT_PAGE'];
$nextPage    		= $pagginationDataResult->ResultArray['NEXT_PAGE'];
$displayRecordFrom  = $pagginationDataResult->ResultArray['START_OFFSET'];
$displayRecordTo  	= $pagginationDataResult->ResultArray['END_OFFSET'];
$firstPage          = 1;
$pageNumbers = ceil($totalResult / $rmax);
//echo $pageNumbers;
?>




<div class="pagination">
    <ul id="pagination">

           <?php if (isset($prevPage) && $prevPage != ""): ?>
        <li class="next">   <a class="prev" href="?q=<?php print($searchQ);?>&page=<?php print($prevPage);?>&category_id=<?php print($categoryId);?>&is_featured=<?php echo $is_feartured?>&special_offers=<?php echo $Special_offers?>&brand_id=<?php echo $brandId?>">&laquo;Previous</a></li> 
           <?php else: ?>
        <li class="next"><a class="prev active_page_no" href="#">&laquo;Previous</a></li>
	   <?php endif; ?>   
           
            <?php if(count($pageNumbers)>0){?>
              <?php for ($i = 1; $i <= $pageNumbers; $i++) {?>
	    <?php if ($i != $currentPage) { ?>
        <li><a href="?q=<?php print($searchQ);?>&page=<?php print($i);?>&category_id=<?php print($categoryId);?>&is_featured=<?php echo $is_feartured?>&special_offers=<?php echo $Special_offers?>&brand_id=<?php echo $brandId?>"><?php print($i);?></a></li>
        <?php } elseif($i == $currentPage) {?>
           
        <li><a class="active_page_no" href="#"><?php print($i);?></a></li>
           
           
            <?php }}} ?>
           
           
           <?php if (isset($nextPage) && $nextPage != ""): ?>
        <li class="next"><a class="next" href="?q=<?php print($searchQ);?>&page=<?php print($nextPage);?>&category_id=<?php print($categoryId);?>&is_featured=<?php echo $is_feartured?>&special_offers=<?php echo $Special_offers?>&brand_id=<?php echo $brandId?>">Next &raquo;</a></li>
           <?php else: ?>
        <li class="next"><a href="#" class="next active_page_no">Next &raquo;</a></li>
           <?php endif; ?>
           
           </ul>
            </div>
                        
                         