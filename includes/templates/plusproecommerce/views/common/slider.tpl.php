<?php
$objHomeBanner = new HomeBanner();
$objHomeBanner->tb_name = 'home_banner_images';

$orderStr = " recordListingID Asc ";
$objHomeBanner->bannerType = 'HomeBanners';
$objHomeBanner->listingOrder = $orderStr;
$homeBannerResult = $objHomeBanner->search();

//print_r($homeBannerResult);

$homePageContentObject                = new PageContent();
$homePageContentObject->tb_name       = 'tbl_page_contents';
$homePageMiddleContentDetails         = $homePageContentObject->getAllByPageIdAndType('1','COLUMN_CONTENT','homePage');


?> 


		
		
		
			<section class="slider">
		<div class="container">
        	<div class="row" style="margin-top:-11px;">
			<div class="slider-wrapper theme-default span8">
				<div id="slider" class="nivoSlider">
				   <?php if($homeBannerResult){?>
                <?php foreach ($homeBannerResult As $banner_index => $banner_img){?>
             <img src="<?php print(SITE_BASE_URL);?>imgs/<?php print($banner_img->recordText); ?>" data-thumb="<?php print(SITE_BASE_URL);?>imgs/<?php print($banner_img->recordText); ?>" alt="" data-transition="slideInLeft" />
					<?php }}?>
				</div>
				<div id="htmlcaption" class="nivo-html-caption"> <strong>This</strong> is an example of a <em>HTML</em> caption with
					<a href="#">a link</a>
                </div>
			</div>
            
            <div class="span4 banner">
            	<a href="<?php print($homePageMiddleContentDetails[0]->contentLink); ?>">
                <div class="s-desc">
                	<h1><?php print($homePageMiddleContentDetails[0]->title); ?></h1>
                    <div class="img-banner"><img title="" alt="" src="<?php print(SITE_BASE_URL); ?>imgs/<?php print($homePageMiddleContentDetails[0]->contentMainImage); ?>"></div>
                   <?php print(htmlspecialchars_decode($homePageMiddleContentDetails[0]->contents,ENT_QUOTES));?>
                 </div>
                </a>
            </div>
            </div>
		</div>
	</section>