<?php 
//require_once 'http://testing.plusproecommerce.com/bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'shoppingCart';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId = '';


$pageTitle = 'Shopping Cart';
$pageKeywords = 'Shopping Cart';
$pageDescription = 'Shopping Cart';


$CONTENT = '';
$cartContents = "";
$totalAmount = 0;

$objShippingMethod = new ShippingMethod();


$cartShippingMethod = getShippingMethod();
$cartContents = getCartContents();

//Product Settings..
$objProductSettings  = new ProductSettings();
$objProductSettings->tb_name = 'tbl_product_settings';
 
$ProductSettingsInfo = $objProductSettings->getProductSettings();
 
$VATValue= $ProductSettingsInfo->VATValue;
$guest_amount = $ProductSettingsInfo->guest_amount;

?>
<div aria-hidden="false" aria-labelledby="myModalLabel"
role="dialog" tabindex="-1" class="<?php if ($hide){?>modal fade in<?php } else{?>modal hide fade in<?php } ?>"
id="myModal">
  <div class="modal-header">
    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    <h3 id="myModalLabel">My Cart</h3>
  </div>
<div id="cartContentsNotificationsLoader" style="display: none;" align ="center">
              <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/ajax-loader.gif"></img>
              </div> 
  <div class="modal-body" id="hcart">
    <table class="table table-striped">
      <tbody>
        <tr>
          <th class="image">Image</th>
          <th class="name">Product Name</th>
          <th class="quantity">Quantity</th>
          <th class="price">Unit Price</th>
          <th class="total">Total</th>
          <th class="quantity">&nbsp;</th>
        </tr>
         <?php if($cartContents){?>
                              
                                       <?php foreach($cartContents As $cIndex=>$cartContent){
                                                $totalAmount = $totalAmount +  $cartContent->productSubTotal; 
                                                $unitPrice = ($cartContent->productDiscountPrice) ? $cartContent->productDiscountPrice :  $cartContent->unitPrice;  
                                       ?>
        <tr>
          <td class="image"><a href="#"><img width="50" height="50" src="<?php print(SITE_BASE_URL);?>includes/extLibs/imagesize.php?file=<?php print($cartContent->productImage->recordText);?>&maxw=50&maxh=50"  alt="product" title="<?php print($cartContent->productName); ?>"></a>
          <td class="name"><a href="#"><?php print($cartContent->productName); ?> <br><?php if($cartContent->color == ''){echo "red";} else{ print($cartContent->color);} ?><br><?php print($cartContent->productSize); ?></a></td>
          <td class="quantity"><input type="text" class="span1 cart_quantity_boxh" name="cartQty_<?php print($cartContent->productId); ?>" id="cartQty_<?php print($cartContent->productId); ?>" value="<?php print($cartContent->productQty); ?>" size="1"></td>
          <td class="price"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($unitPrice*CURRENCY_RATE))); ?></td>
          <td class="total"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productSubTotal*CURRENCY_RATE))); ?></td>
          <td class="quantity"><a href="#" class="removeItem" id="cartItemID_<?php print($cartContent->productId); ?>" onclick="return false;"><img alt="" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/remove.png" data-original-title="Remove" class="tooltip-test"></a></td>
        </tr>
        <?php }}?>
    
        <tr>
          <td class="image" colspan="6"><h4 class="pull-right margin-none">Total: <?php print(CURRENCY_CODE); ?> <?php print(priceFormateNumber($totalAmount*CURRENCY_RATE)); ?></h4></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
    <button data-dismiss="modal" class="btn">Continue Shoping</button>
    <button class="btn btn-primary" onclick="location.href='<?php if($guest_amount<$totalAmount or isset($_SESSION['SESS_CUSTOMER_INFO'])){ print(SITE_BASE_URL);?>checkout.html<?php } else {?><?php print(SITE_BASE_URL);?>checkout.html<?php }?>'">Place Order</button>
  </div>

</div>
