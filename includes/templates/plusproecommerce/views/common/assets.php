
<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,800,700,300' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/pluspro_fly_to_basket.js"></script>
<!-- styles -->
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/bootstrap.css" rel="stylesheet">
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/docs.css" rel="stylesheet">
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/js/google-code-prettify/prettify.css" rel="stylesheet">
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/style.css" rel="stylesheet">
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/rating.css" rel="stylesheet">

<!-- Slider -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/nivo-slider.css" type="text/css" media="screen" />

<!-- Icon -->
<link rel="stylesheet" type="text/css" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/font-awesome.min.css" />
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- fav and touch icons -->
<link rel="shortcut icon" href="<?php print(SITE_BASE_URL);?><?php print(SITE_FAV_ICON);?>">
<!-- Inline CSS 
================================================== -->
<style>
.thumbnails{
	margin-left:6px;
}
.thumbnails > li {
    float: left;
    margin-bottom: 20px;
    margin-right: 4px;
	margin-left:4px;
}
</style>