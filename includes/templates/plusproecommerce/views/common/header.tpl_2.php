<?php  
   
$objCategory   = new Category();
$objCategory->tb_name = 'tbl_category';
$CategoryInfo  = $objCategory->getAll();


$objBrand   = new Brand();
$objBrand->tb_name = 'tbl_brand';
$BrandInfo  = $objBrand->getAll();





?>

<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#frmCustomerLoginT").validate({
                    rules: {
                        email: {
                            required : true,
                            email : true
                        },
                		password: {
                    		required : true
                		} 

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "ajex/checkout/login.php",
                            data: $("#frmCustomerLoginT").serialize(), 
                            success: function(Res) {
                                if(Res == "success"){
                            		window.location.href='<?php print(SITE_BASE_URL); ?>checkout.php';
                                } else if(Res == "failed"){
                                	$('#error_notice').fadeIn(750);
                                    $('#error_notice').fadeOut(750);
                                }
                            },
                            error: function(Res) {
                            	$('#error_notice').fadeIn(750);
                                $('#error_notice').fadeOut(750);
                            }
                        });
                        
                        return false;
                    },
                    errorPlacement: function (error, elemenet) {
                        error.remove();
                     }
                }); 
        });
</script>
<!-- Jquery Validation : End -->




        <!-- Header Start -->
<header>
  <!-- Sticky Navbar Start -->
  <div id="main-nav" class="navbar navbar-fixed-top">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <ul class="nav">
          <li><a href="mailto:<?php print(SITE_CONTACT_EMAIL);?>"><i class="icon-envelope"></i> <?php print(SITE_CONTACT_EMAIL);?></a></li>
          <li><a href="#"><i class="icon-phone-sign"></i><?php print(SITE_CONTACT_NUM_2);?></a></li>          
      </ul>
      <nav style="height:0px" class="nav-collapse collapse">
        <ul class="nav pull-right">
          <li><a href="<?php print(SITE_BASE_URL); ?>"><i class="icon-home"></i></a></li>
          <li><a href="<?php print(SITE_BASE_URL); ?>shopping-cart.html">Shopping Cart</a></li>
          <li class="dropdown hover"><a data-toggle="" class="dropdown-toggle" href="<?php print(SITE_BASE_URL); ?>login.html">Login <b class="caret"></b></a>
          	<ul class="dropdown-menu topcart">
          	<!--<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // customer login....
        $("#frmCustomerLoginT").submit(function(e){
            $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
            function(data){   
                if(data == 'success'){
                    window.location.href='<?php print(SITE_BASE_URL); ?>checkout.php'; 
                } else if(data == 'failed'){
                    $('#error_notice').fadeIn(750);
                    $('#error_notice').fadeOut(750);
                }
                           
            }		
        );
            return false;
        });
        
        
     
        
    });

</script> -->
            	<li style="padding:10px;">
<!--  -->
            <div id="stylized" class="myform">
            <!--  -->
<div id="error_notice" style="display:none;" class="error_notice">Login failed!</div>
            	 <form name="frmCustomerLogin" id="frmCustomerLoginT" action="" method="post">
            		<input type="text" class="input-medium" placeholder="Username" name="email" id="email">
                  	<input type="password" class="input-medium" placeholder="Password" name="password" id="password">
                  	<input type="submit" class="btn btn-inverse" value="Login">
                  	</form>
<!--  -->
                  	</div>
                  	<!--  -->
            	</li>
          	</ul>
          </li>
          <li><a href="<?php print(SITE_BASE_URL); ?>register.html">Register</a></li>
          <li><a href="<?php print(SITE_BASE_URL); ?>contact-us.html">Feedback</a></li>
       </ul>
      </nav>
    </div>
  </div>
  <!--Sticky Navbar End -->
  
  <div class="header-white">
    <div class="container">
      <div class="row">
        <div class="span4">
          <a href="<?php print(SITE_BASE_URL); ?>" class="logo"><img title="Ecommerce" alt="Company Logo" src="<?php print(SITE_BASE_URL);?>imgs/<?php print(SITE_LOGO);?>"></a>
        </div>
        <div class="span8">
          <div class="row">
        
            <div class="pull-right logintext">Welcome 
             <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){
                    $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
                    $customerName = $customerInfo->firstName;
                    ?>
            <?php print($customerName);?> <?php } else {?> Guest<?php }?>,  you can <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){?> <a href="<?php print(SITE_BASE_URL); ?>logout.html">logout </a><?php } else{?><a href="<?php print(SITE_BASE_URL); ?>login.html">login </a><?php }?>
              or <a href="<?php print(SITE_BASE_URL); ?>register.html">create an account</a>
            </div>
          </div>
         <form class="form-search marginnull topsearch pull-right">
          <div class="span6 pull-left">
          <button value="Search" class="btn btn-inverse pull-right search" type="submit">Search</button>
<input type="text" class="span5 search-query search-icon-top pull-right" value="Search here..." onfocus="if (this.value=='Search here...') this.value='';" onblur="if (this.value=='') this.value='Search here...';">
</div>
          <div class="pull-right ml5"><a data-toggle="modal" href="#myModal" class="btn btn-info pull-right"><i class="icon-shopping-cart"></i> Cart(<span id="cart_contents"></span> )</a></div>
          </form>
        </div>
      </div>
    </div>
    <!-- Navigation Start -->
    <div  id="categorymenu">
      <div class="container">
        <nav class="subnav">
          <ul class="nav-pills categorymenu">
            <li><a <?php if ($pageType == "homePage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>">Home</a></li>
            <li><a <?php if ($pageType == "productPage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>product.html">Products</a>
              <div>
                
                    
                  <?php 
                
                  foreach($BrandInfo As $bIndex=>$Brand){
                      $index = $bIndex+1;
                     
                                            
                      if(($index%10) == 1){?>
                  
                  <ul class="arrow">
                     
                      <?php }?>
                      <li><a href="<?php print(SITE_BASE_URL); ?>product/<?php print($Brand->id)?>"><?php  print($Brand->brand_name)?></a></li>
                  
                  <?php if(($index%10) == 0 || count($BrandInfo) == $index ){?>
                    </ul>
                  <?php }}?>
                
                
              </div>
            </li>
            <li><a <?php if ($pageType == "categoryPage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>category.html">Stores</a>
              <div>
                <ul class="arrow">
                  <?php foreach($CategoryInfo As $cIndex=>$Category){?>
                  <li><a href="<?php print(SITE_BASE_URL); ?>category/<?php print($Category->id)?>"><?php  print($Category->category_name)?></a></li>
                  
                  <?php } ?>
                </ul>
              </div>
            </li>
            <li><a <?php if ($pageType == "shoppingCart") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>shopping-cart.html">Shopping Cart</a></li>
            <li><a href="<?php print(SITE_BASE_URL); ?>checkout.html">Checkout</a></li>
            
            <li><a href="#">My Account</a>
            <div>
                <ul class="arrow">
                  <li><a href="login.html">Login</a></li>
                  <li><a href="register.html">Register</a></li>
                  <li><a href="wishlist.html">Wishlist</a></li>
                </ul>
              </div>
            </li>
            
            
            <li><a <?php if ($pageType == 'contactUsPage') {?>class="active" <?php }?> href="<?php print(SITE_BASE_URL); ?>contact-us.html">Contacts</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- Navigation Ends -->
  </div>
</header>
<!-- Header Ends -->
