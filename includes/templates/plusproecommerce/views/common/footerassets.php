<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/google-code-prettify/prettify.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-transition.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-alert.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-modal.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-dropdown.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-scrollspy.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-tab.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-tooltip.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-popover.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-button.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-collapse.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-carousel.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-typeahead.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap-affix.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/application.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/respond.min.js"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/cloud-zoom.1.0.2.js"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.nivo.slider.js"></script>
<script defer src="<?php print(FRONT_LAYOUT_URL);?>contents/js/custom.js"></script>
<script type="text/javascript">
$(window).load(function () {
  $('#slider').nivoSlider();
});
</script>