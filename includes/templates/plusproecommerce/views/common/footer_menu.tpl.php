<footer id="footer">
  <section class="footersocial">
    <div class="container">
      <div class="row">
        <div class="span3 twitter">
          <h2>Quick Links</h2>
<?php
$objMenuPages = new Page();
$menuPages    = $objMenuPages->getAllByStatus('Exclude_from_menu');
?>
          <ul class="nav-list categories">
            <?php if ($menuPages) { ?>
	      <?php foreach ($menuPages As $mindex=>$menuObj) {?>
            <li><a href="<?php print(SITE_BASE_URL); ?>page/<?php print($menuObj->id); ?>/<?php print($objMenuPages->formateUrlString($menuObj->name)); ?>.html"><?php print($menuObj->name); ?></a></li>
            <?php }}?>
          </ul>
        </div>
        <div class="span3 twitter">
          <h2>Popular Brands</h2>
          <ul class="nav-list categories">
      
           <?php 
           $brandNmae = '';
           $i = 0;
            $objProducts = new Product();
            $objProducts->tb_name = "tbl_product";
            //$objProducts->listingOrder = 'id DESC';
            //$objProducts->category = $CId;
            //$objProducts->isFeatured = 'yes';
            $objProducts->listBrand = 'top-brands';
            $objProductsResult = $objProducts->search();
            //echo "<pre>";
           //print_r($objProductsResult);
           //echo "</pre>";
            foreach ($objProductsResult as $Pindex => $products) {

              
          
          
          if ($products->productBrand->brand_name !== $brandNmae) {
         
              
      ?>
      
            <li><a href="<?php print($products->id);?>"><?php print($products->productBrand->brand_name);?></a></li>
            <?php }
            if ($i == 4) {
            	break;
            }
            $brandNmae = $products->productBrand->brand_name;
            $i++;
            }?>
          
          </ul>
        </div>
        <div class="span3 contact">
          <h2>Contact Us </h2>
          <ul>
            <li class="phone"><?php print(SITE_CONTACT_NUM_2);?></li>
            <li class="mobile"><?php print(SITE_CONTACT_ADDR_2);?></li>
            <li class="email"><?php print(SITE_CONTACT_EMAIL);?></li>
          </ul>
        </div>
        <?php 
        $homePageContentObject                = new PageContent();
        $homePageContentObject->tb_name       = 'tbl_page_contents';
        $homePageGetInTouch                   = $homePageContentObject->getAllByPageIdAndType('1','TESTAMONIAL','homePage')
        ?>
        <div class="span3 newsletter">
          <h2><?php print($homePageGetInTouch[0]->title);?> </h2>
         <?php print(htmlspecialchars_decode($homePageGetInTouch[0]->contents,ENT_QUOTES));?>
          <div class="fl">
          <div class="control-group">
          <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
 <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/css/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/validationEngine/js/jquery.validationEngine.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/email.js"></script>
          <script>	
		$(document).ready(function() {			
			$("#formIDS").validationEngine();
		});
		
		
	</script>
            <form id="formIDS" class="form-horizontal">
              <div class="">
                <div class="input-prepend">
                  <input type="text" name="email" class="validate[required,custom[email]]" placeholder="Subscribe to Newsletter" id="inputIcon">
                  <button value="Subscribe" class="btn btn-inverse" type="submit">Sign in</button>
                </div>
              </div>
            </form>
          </div>
        </div>
          <div id="footersocial"> 
            <a href="<?php print(SITE_FACEBOOK);?>" title="Facebook" class="facebook">Facebook</a>
            <a href="<?php print(SITE_TWITTER);?>" title="Twitter" class="twitter">Twitter</a>
            <a href="<?php print(SITE_LINKEDIN);?>" title="Linkedin" class="linkedin">Linkedin</a>
            <a href="<?php print(SITE_OTHERSOCIALLINKS);?>" title="rss" class="rss">rss</a>
            <a href="<?php print(SITE_GOOGLEPLUS);?>" title="Googleplus" class="googleplus">Googleplus</a>
            <a href="#" title="Skype" class="skype">Skype</a>
            <a href="<?php print(SITE_PINTEREST);?>" title="Flickr" class="flickr">Flickr</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="copyrightbottom">
    <div class="container">
      <div class="row">
        <div class="span6">All rights are copyright to <?php print(SITE_NAME);?>.</div>
        <div class="span6 textright">Designed &amp; Developed by <?php print(SITE_NAME);?>.</div>
   	 </div>
    </div>
  </section> 
        <a id="gotop" href="#">Back to top</a>
</footer>

