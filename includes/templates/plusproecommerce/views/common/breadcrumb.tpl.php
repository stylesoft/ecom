<?php 
$productBrandObj = new Brand(); 
$productCategoryObj = new Category();	
?>

<ul class="breadcrumb">
	<li>
		<a href="<?php print(SITE_BASE_URL); ?>">Home</a>
		<span class="divider">/</span>
	</li>
	
	<?php if(isset($_GET['categoryId']) || isset($_GET['productCategoryId'])){ ?>
	<li>
		<a href="<?php print(SITE_BASE_URL . 'category.html'); ?>">All Categories</a>
		<span class="divider">/</span>
	</li>		
	<?php } else if(isset($_GET['productId']) || isset($_GET['brandId'])) { ?>
	<li>
		<a href="<?php print(SITE_BASE_URL . 'product.html'); ?>">All Products</a>
		<span class="divider">/</span>
	</li>
	<?php } ?>

	
	<?php if(isset($_GET['categoryId']) || (isset($_GET['productCategoryId']) && $_GET['productCategoryId'] != '')){ 

		$currentCategoryId = $_GET['productCategoryId'];
		if(isset($_GET['categoryId'])){
			$currentCategoryId = $_GET['categoryId'];
			}
		$categoryInfo = $productCategoryObj->getCategory($currentCategoryId); 
		
			if($categoryInfo->parent_category > 0){
			$parentCategoryInfo = $productCategoryObj->getCategory($categoryInfo->parent_category);
			?>
				<li>
					<a href="<?php print(SITE_BASE_URL . 'category/' . $categoryInfo->parent_category); ?>"><?php print($parentCategoryInfo->category_name); ?></a>
					<span class="divider">/</span>
				</li>
			<?php } 
			
			if(!isset($_GET['categoryId'])){  ?>
			<li>
				<a href="<?php print(SITE_BASE_URL . 'category/' . $categoryInfo->id); ?>"><?php print($categoryInfo->category_name); ?></a>
				<span class="divider">/</span>
			</li>
	<?php 	} 
	
	} else if(isset($_GET['productBrandId']) && $_GET['productBrandId'] != '') { 
		
		$brandInfo = $productBrandObj->getBrand($_GET['productBrandId']);
		?>
	<li>
		<a href="<?php print(SITE_BASE_URL . 'product/' . $brandInfo->id); ?>"><?php print($brandInfo->brand_name); ?></a>
		<span class="divider">/</span>
	</li>
	<?php } ?>
	
	<li class="active">
		<?php print ($pageTitle)?>
	</li>
</ul>