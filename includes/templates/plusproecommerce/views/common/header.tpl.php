<?php  
   
$objCategory   = new Category();
$objCategory->tb_name = 'tbl_category';
$CategoryInfo  = $objCategory->getAll();


$objBrand   = new Brand();
$objBrand->tb_name = 'tbl_brand';
$BrandInfo  = $objBrand->getAll();





?>






        <!-- Header Start -->
<header>
  <!-- Sticky Navbar Start -->
  <div id="main-nav" class="navbar navbar-fixed-top">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <ul class="nav">
          <li><a href="mailto:<?php print(SITE_CONTACT_EMAIL);?>"><i class="icon-envelope"></i> <?php print(SITE_CONTACT_EMAIL);?></a></li>
          <li><a href="#"><i class="icon-phone-sign"></i><?php print(SITE_CONTACT_NUM_2);?></a></li>          
      </ul>
      <nav style="height:0px" class="nav-collapse collapse">
        <ul class="nav pull-right">
          <li><a href="<?php print(SITE_BASE_URL); ?>"><i class="icon-home"></i></a></li>
          <li><a href="<?php print(SITE_BASE_URL); ?>shopping-cart.html">Shopping Cart</a></li>
           <?php if(!isset($_SESSION['SESS_CUSTOMER_INFO'])){
                    $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
                    $customerName = $customerInfo->firstName;
                    ?>
          <li class="dropdown hover"><a data-toggle="" class="dropdown-toggle" href="<?php print(SITE_BASE_URL); ?>login.html">Login <b class="caret"></b></a>
          	<ul class="dropdown-menu topcart">
          	<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // customer login....
        $("#frmCustomerLoginT").submit(function(e){
            $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
            function(data){   
                if(data == 'success'){
                    window.location.href='<?php print(SITE_BASE_URL); ?>'; 
                } else if(data == 'failed'){
                	$('#errormsg').css('display','block');
                    $('#error_notice').fadeIn(750);
                    $('#error_notice').fadeOut(750);
                }
                           
            }		
        );
            return false;
        });
        
        
     
        
    });

</script> 
            	<li style="padding:10px;">
            	 <div id="errormsg" style="display:none;" class="error_notice">Login failed!</div>
            	 <form name="frmCustomerLogin" id="frmCustomerLoginT" action="" method="post">
            		<input type="text" class="input-medium" placeholder="Username" name="email" id="email">
                  	<input type="password" class="input-medium" placeholder="Password" name="password" id="password">
                  	<input type="submit" class="btn btn-inverse" value="Login">
                  	</form>
            	</li>
          	</ul>
          </li>
          <li><a href="<?php print(SITE_BASE_URL); ?>register.html">Register</a></li>
          <?php } else {?>
          <li><a href="<?php echo SITE_BASE_URL;?>myaccount.html">My Account</a></li>
          <?php }?>
          
          <li><a href="<?php print(SITE_BASE_URL); ?>contact-us.html">Feedback</a></li>
       </ul>
      </nav>
    </div>
  </div>
  <!--Sticky Navbar End -->
  
  <div class="header-white">
    <div class="container">
      <div class="row">
        <div class="span4">
          <a href="<?php print(SITE_BASE_URL); ?>" class="logo"><img title="Ecommerce" alt="Company Logo" src="<?php print(SITE_BASE_URL);?>imgs/<?php print(SITE_LOGO);?>"></a>
        </div>
        <div class="span8">
          <div class="row">
        
            <div class="pull-right logintext">
             <a href="<?php print(SITE_BASE_URL);?>doConvertCurrency.php?id=1&curr=£">&pound;</a>
                             <a href="<?php print(SITE_BASE_URL);?>doConvertCurrency.php?id=3&curr=€"> &euro;</a> 
                             <a href="<?php print(SITE_BASE_URL);?>doConvertCurrency.php?id=2&curr='$'"> &#36; </a>
                             &nbsp; &nbsp;
            Welcome 
             <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){
                    $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
                    $customerName = $customerInfo->firstName;
                    ?>
            <?php print($customerName);?> <?php } else {?> Guest<?php }?> <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){?> <a href="<?php print(SITE_BASE_URL); ?>logout.html">logout </a><?php } else{?><a href="<?php print(SITE_BASE_URL); ?>login.html">login </a>
              or  you can <a href="<?php print(SITE_BASE_URL); ?>register.html">create an account</a><?php }?>
            </div>
          </div>
            <form class="form-search marginnull topsearch pull-right" method="post" action="<?php print(SITE_BASE_URL); ?>search.php">
          <div class="span6 pull-left">
          <button value="Search" class="btn btn-inverse pull-right search" type="submit">Search</button>
<input type="text" class="span5 search-query search-icon-top pull-right" value="Search here..." onfocus="if (this.value=='Search here...') this.value='';" onblur="if (this.value=='') this.value='Search here...';" name="editbox_search">
</div>
          <div class="pull-right ml5"><a data-toggle="modal" href="#myModal" class="btn btn-info pull-right"><i class="icon-shopping-cart"></i> Cart(<span id="cart_contents"></span> )</a></div>
          </form>
        </div>
      </div>
    </div>
    <!-- Navigation Start -->
    <div  id="categorymenu">
      <div class="container">
        <nav class="subnav">
          <ul class="nav-pills categorymenu">
            <li><a <?php if ($pageType == "homePage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>">Home</a></li>
            <li><a <?php if ($pageType == "productPage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>product.html">Products</a>
              <div>
                
                    
                  <?php 
                
                  foreach($BrandInfo As $bIndex=>$Brand){
                      $index = $bIndex+1;
                     
                                            
                      if(($index%10) == 1){?>
                  
                  <ul class="arrow">
                     
                      <?php }?>
                     <?php if($Brand->brand_name == 'Default'){?>
                      <li style="display: none"></li>  
                   <?php  }else{
?>
                      <li><a href="<?php print(SITE_BASE_URL); ?>product/<?php print($Brand->id)?>"><?php  print($Brand->brand_name)?></a></li>
                   <?php } ?>
                  <?php if(($index%10) == 0 || count($BrandInfo) == $index ){?>
                    </ul>
                  <?php }}?>
                
                
              </div>
            </li>
            <li><a <?php if ($pageType == "categoryPage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>category.html">Stores</a>
              <div>
                <ul class="arrow">
                  <?php foreach($CategoryInfo As $cIndex=>$Category){?>
                    
                    <?php if($Category->category_name == 'Default'){?>
                      <li style="display: none"></li>  
                   <?php  }else{
?>
                  <li><a href="<?php print(SITE_BASE_URL); ?>category/<?php print($Category->id)?>"><?php  print($Category->category_name)?></a></li>
                  
                   <?php }} ?>
                </ul>
              </div>
            </li>
            <li><a <?php if ($pageType == "specialOffers") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>offers.html">Special Offers</a></li>
            <li><a <?php if ($pageType == "featured") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>featured.html">Featured</a></li>
            <!-- <li><a <?php if ($pageType == "shoppingCart") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>shopping-cart.html">Shopping Cart</a></li> -->
            <!-- 
            <li><a <?php if ($pageType == "Checkout") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>checkout.html">Checkout</a></li>
            
            <li><a <?php if ($pageType == "Login") {?>class="active"<?php }?> href="<?php echo SITE_BASE_URL;?>myaccount.html" >My Account</a>
            <div>
                <ul class="arrow">
                  <li><a href="login.html">Login</a></li>
                  <li><a href="register.html">Register</a></li>
                  <li><a href="wishlist.html">Wishlist</a></li>
                </ul>
              </div>
            </li>
             -->
            
            <li><a <?php if ($pageType == 'contactUsPage') {?>class="active" <?php }?> href="<?php print(SITE_BASE_URL); ?>contact-us.html">Contacts</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- Navigation Ends -->
  </div>
</header>
<!-- Header Ends -->
