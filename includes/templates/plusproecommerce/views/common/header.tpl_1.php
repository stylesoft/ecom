      <!-- Header Start -->
<header>
  <!-- Sticky Navbar Start -->
  <div id="main-nav" class="navbar navbar-fixed-top">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <ul class="nav">
          <li><a href="mailto:<?php print(SITE_CONTACT_EMAIL);?>"><i class="icon-envelope"></i> <?php print(SITE_CONTACT_EMAIL);?></a></li>
          <li><a href="#"><i class="icon-phone-sign"></i><?php print(SITE_CONTACT_NUM_2);?></a></li>          
      </ul>
      <nav style="height:0px" class="nav-collapse collapse">
        <ul class="nav pull-right">
          <li><a href="<?php print(SITE_BASE_URL); ?>"><i class="icon-home"></i></a></li>
          <li><a href="<?php print(SITE_BASE_URL); ?>shopping-cart.html">Shopping Cart</a></li>
          <li class="dropdown hover"><a data-toggle="" class="dropdown-toggle" href="<?php print(SITE_BASE_URL); ?>login.html">Login <b class="caret"></b></a>
          	<ul class="dropdown-menu topcart">
          	<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // customer login....
        $("#frmCustomerLoginT").submit(function(e){
            $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
            function(data){   
                if(data == 'success'){
                    window.location.href='<?php print(SITE_BASE_URL); ?>checkout.php'; 
                } else if(data == 'failed'){
                    $('#error_notice').fadeIn(750);
                    $('#error_notice').fadeOut(750);
                }
                           
            }		
        );
            return false;
        });
        
        
     
        
    });

</script> 
            	<li style="padding:10px;">
            	 <form name="frmCustomerLogin" id="frmCustomerLoginT" action="" method="post">
            		<input type="text" class="input-medium" placeholder="Username" name="email" id="email">
                  	<input type="password" class="input-medium" placeholder="Password" name="password" id="password">
                  	<input type="submit" class="btn btn-inverse" value="Login">
                  	</form>
            	</li>
          	</ul>
          </li>
          <li><a href="<?php print(SITE_BASE_URL); ?>register.html">Register</a></li>
          <li><a href="<?php print(SITE_BASE_URL); ?>contact-us.html">Feedback</a></li>
       </ul>
      </nav>
    </div>
  </div>
  <!--Sticky Navbar End -->
  
  <div class="header-white">
    <div class="container">
      <div class="row">
        <div class="span4">
          <a href="<?php print(SITE_BASE_URL); ?>" class="logo"><img title="Ecommerce" alt="Company Logo" src="<?php print(SITE_BASE_URL);?>imgs/<?php print(SITE_LOGO);?>"></a>
        </div>
        <div class="span8">
          <div class="row">
        
            <div class="pull-right logintext">Welcome 
             <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){
                    $customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
                    $customerName = $customerInfo->firstName;
                    ?>
            <?php print($customerName);?> <?php } else {?> Guest<?php }?>,  you can <?php if(isset($_SESSION['SESS_CUSTOMER_INFO'])){?> <a href="<?php print(SITE_BASE_URL); ?>logout.html">logout </a><?php } else{?><a href="<?php print(SITE_BASE_URL); ?>login.html">login </a><?php }?>
              or <a href="<?php print(SITE_BASE_URL); ?>register.html">create an account</a>
            </div>
          </div>
         <form class="form-search marginnull topsearch pull-right">
          <div class="span6 pull-left">
          <button value="Search" class="btn btn-inverse pull-right search" type="submit">Search</button>
<input type="text" class="span5 search-query search-icon-top pull-right" value="Search here..." onfocus="if (this.value=='Search here...') this.value='';" onblur="if (this.value=='') this.value='Search here...';">
</div>
          <div class="pull-right ml5"><a data-toggle="modal" href="#myModal" class="btn btn-info pull-right"><i class="icon-shopping-cart"></i> Cart(<span id="cart_contents"></span> )</a></div>
          </form>
        </div>
      </div>
    </div>
    <!-- Navigation Start -->
    <div  id="categorymenu">
      <div class="container">
        <nav class="subnav">
          <ul class="nav-pills categorymenu">
            <li><a <?php if ($pageType == "homePage") {?>class="active"<?php }?> href="<?php print(SITE_BASE_URL); ?>">Home</a></li>
            <li><a <?php if ($pageType == "productPage") {?>class="active"<?php }?> href="product.html">Products</a>
              <div>
                <ul class="arrow">
                  <li class="category"><a href="product.html">Full width Product</a></li>
                  <li><a href="product.html"> Watch</a></li>
                  <li><a href="product.html"> Suits & shirts</a></li>
                  <li><a href="product.html"> Shoes</a></li>
                  <li><a href="product.html">Jackets & Coats</a></li>
                  <li><a href="product.html">Mice and Trackballs</a></li>
                  <li><a href="product.html">Laptops </a></li>
                </ul>
                <ul class="arrow">
                  <li class="category"><a href="product-sidebar.html">Product with Sidebar</a></li>
                  <li><a href="product-sidebar.html">Watch</a></li>
                  <li><a href="product-sidebar.html">Suits & shirts</a></li>
                  <li><a href="product-sidebar.html">Shoes</a></li>
                  <li><a href="product-sidebar.html">Jackets & Coats</a></li>
                  <li><a href="product-sidebar.html">Mice and Trackballs</a></li>
                  <li><a href="product-sidebar.html">Laptops </a></li>
                </ul>
              </div>
            </li>
            <li><a <?php if ($pageType == "categoryPage") {?>class="active"<?php }?> href="category.html">Stores</a>
              <div>
                <ul class="arrow">
                  <li><a class="bold" href="category.html">Full Width Category</a></li>
                  <li><a class="bold" href="category-sidebar.html">Category with Sidebar</a></li>
                  <li><a href="#">Mens</a></li>
                  <li><a href="#">Womens</a></li>
                  <li><a href="#">Childrens</a></li>
                  <li><a href="#">Home and Furniture</a></li>
                  <li><a href="#">Electric Appliances</a></li>
                </ul>
              </div>
            </li>
            <li><a <?php if ($pageType == "shoppingCart") {?>class="active"<?php }?> href="shopping-cart.html">Shopping Cart</a></li>
            <li><a href="checkout.html">Checkout</a></li>
            <li><a href="compare.html">Compare</a></li>
            <li><a href="#">My Account</a>
            <div>
                <ul class="arrow">
                  <li><a href="login.html">Login</a></li>
                  <li><a href="register.html">Register</a></li>
                  <li><a href="wishlist.html">Wishlist</a></li>
                </ul>
              </div>
            </li>
            <li><a href="#">Features</a>
            	<div>
                <ul class="arrow">
                  <li><a href="typography.html">Typography</a></li>
                  <li><a href="form-element.html">Form Elements</a></li>
                  <li><a href="buttons.html">Buttons</a></li>
                  <li><a href="javascripts.html">Javascripts</a></li>
                </ul>
              </div>
            </li>
            <li><a href="#">Speciality Pages</a>
            	<div>
                <ul class="arrow">
                  <li><a href="404-error.html">404 Error</a></li>
                  <li><a href="500-error.html">500 Error</a></li>
                </ul>
              </div>
            </li>
            <li><a <?php if ($pageType == 'contactUsPage') {?>class="active" <?php }?> href="<?php print(SITE_BASE_URL); ?>contact-us.html">Contacts</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- Navigation Ends -->
  </div>
</header>
<!-- Header Ends -->