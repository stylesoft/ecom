	<?php 
	$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
	$currentCustomerId   = $currentCustomerInfo->id;
	    $objCustomer = new Customer();
        $objCustomer->tb_name = 'tbl_customer';
        
                                                       
	$CustomerDetails = $objCustomer->getCustomer($currentCustomerId);
	
	$billingAddress = $CustomerDetails->addressBillingDtls[0];
	$shippingAddressInfo = $CustomerDetails->addressShippingDtls[0];
	//print_r($CustomerDetails);
	
	?>
	<?php if($currentCustomerInfo =='') {?>
<script type="text/javascript">
<!--
window.location.href='<?php print(SITE_BASE_URL); ?>login.html';
//-->
</script>
<?php } //com?>
	
        
        <?php $bojcountry = new Country();
      $cuntrySelected = $bojcountry->getCountry($billingAddress->country);
      $cuntrySelectedS = $bojcountry->getCountry($shippingAddressInfo->country);
       $resultsall = $bojcountry->getAll();
         
         $objRegion = new Region();
         $regionList = $objRegion->getAll();
         $regionListJSONObject = json_encode($regionList);
        //testing 
        
        // print_r($cuntrySelected);
        ?>

<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#frmBillingDetails").validate({
                	rules: {
                		 cmbTitle: {
                            required : true,
                            
                          }, 
                		  txtFirstName: {
                              required : true,
                              
                          },
                          txtLastName: {
                              required : true,
                              
                          },
                          txtEmail: {
                              required : true,
                              email : true
                          },
                    	txtBillingFirstName: {
                         required : true,
                            //email : true
                        },
                        txtBillingLastName: {
                            required : true,
                            //number : true
                        },
                        txtBillingPhone: {
                            required : true,
                            number : true
                        },
                        txtBillingCountry: {
                            required : true,
                            //number : true
                        },
                       
                        txtBillingAddress_1: {
                            required : true,
                            //number : true
                        },
                       
                        txtBillingTownCity: {
                            required : true,
                            //number : true
                        },
                        txtBillingPostalZIP: {
                            required : true,
                            //number : true
                        },

                        txtBillingCountyState: {
                         required : true,
                            //number : true
                        },
                        txtShippingFirstName: {
                            required : true,
                               //email : true
                           },
                           txtShippingLastName: {
                               required : true,
                               //number : true
                           },
                           txtShippingPhone: {
                               required : true,
                               number : true
                           },
                           txtShippingCountry: {
                               required : true,
                               //number : true
                           },
                           txtShippingLastName: {
                               required : true,
                               //number : true
                           },
                           txtShippingAddress_1: {
                               required : true,
                               //number : true
                           },
                          

                           txtShippingTownCity: {
                               required : true,
                               //number : true
                           },
                           txtShippingPostalZIP: {
                               required : true,
                               //number : true
                           },

                           txtShippingCountyState: {
                               required : true,
                               //number : true
                           }
                        

                    },
                    messages: {
                    	cmbTitle: "Please select a title",
                    	select01 : "Please select a country",
                    	//txtBillingCountyState : "Please select a region/state",
                    	chkRangeId : "Please accept the privacy policy"
                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL);?>ajex/checkout/editaccount.php",
                            data: $("#frmBillingDetails").serialize()+'&ajax=1', 
                            success: function(data) {
                                if(data == 'success'){
                           window.location.href='<?php print(SITE_BASE_URL); ?>myaccount.html';    
                                }else if(data == 'failed'){
                                    $('#error_notice').fadeIn(750);
                                    $('#error_notice').fadeOut(750);
                                }
                            }
                            
                        });
                        return false;
                    }
                    
                    
                }); 
        });
</script>
<script>
$(document).ready(function(){
	$("#select01").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>


<script>
$(document).ready(function(){
	$("#select011").change(function(){
			var country_id = $(this).find(":selected").attr("country_id1");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId1").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select022').find('option').remove();
			$('#select022').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select022').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>
<div class="container">
  <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            
		<div class="span3">
				<aside>
					<h1 class="headingfull"><span>My Account</span></h1>
				<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/accountsidebar.tpl.php'); ?>
				</aside>
	
			</div>
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
  <section id="featured">
  	 	<form name="frmBillingDetails" id="frmBillingDetails" action="<?php print(SITE_BASE_URL);?>ajex/checkout/editaccount.php" method="post">
     <div class="checkoutsteptitle">Personal Details
          </div>
          <div class="">
            <div class="row">
           <div class="form-horizontal">
                <fieldset>
                  <div class="span4">
                     <div class="control-group">
               <label class="control-label"><span class="red">*</span> Title:</label>
                <div class="controls">
                <select  id="cmbTitle" name="cmbTitle" class="emailinput">
                            <option value="">Select Title</option>
                            <option value="Mr" <?php if($CustomerDetails->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                            <option value="Mrs" <?php if($CustomerDetails->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                            <option value="Ms" <?php if($CustomerDetails->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                            <option value="Miss" <?php if($CustomerDetails->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                            <option value="Dr" <?php if($CustomerDetails->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                        </div>
                        </div>
                    <div class="control-group">
                  <label class="control-label"><span class="red">*</span> First Name:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" value="<?php print ($CustomerDetails->firstName);?>" name="txtFirstName" id="txtFirstName">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Last Name:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" value="<?php print ($CustomerDetails->lastName);?>" name="txtLastName" id="txtLastName">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Email:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge"  value="<?php print($CustomerDetails->email); ?>" name="txtEmail" id="txtEmail">
                  </div>
                </div>
                    
                  </div>
                </fieldset>
             </div>
            </div>
            
          </div>
    
    
  	 <div class="checkoutsteptitle">Billing Details
          </div>
          <div class="">
            <div class="row">
            <div class="form-horizontal">
                <fieldset>
                   <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingFirstName" id="txtBillingFirstName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->firstName);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingLastName" id="txtBillingLastName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->lastName);?>" <?php } ?> class="">
                      </div>
                    </div>
                   
                     <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingAddress_1" id="txtBillingAddress_1" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->address);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPhone" id="txtBillingPhone" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->phone);?>" <?php } ?> class="">
                      </div>
                    </div>
                    
                      <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select01" name="txtBillingCountry"  style="">
                          <option value=''>--Select a country--</option>
                            <option country_id="<?php print ($cuntrySelected->id);?>" value="<?php print ($cuntrySelected->id);?>" selected="selected"><?php echo $cuntrySelected->name;?></option>
                          <?php 
                          $repeatCountryId = '';
                          foreach ($resultsall as $index=>$country) {?>
                       <?php  if ($country->id!=$cuntrySelected->id and $country->id!=$repeatCountryId) {?>
                          <option country_id="<?php print ($country->id);?>" value="<?php print ($country->id);?>"><?php print ($country->name);?></option>
                        <?php } 
                        $repeatCountryId = $country->id;
                            }?>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                    <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtBillingCompanyName" id="txtBillingCompanyName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->houseNumber);?>" <?php } ?> class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtBillingAddress_2" id="txtBillingAddress_2" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->region);?>" <?php } ?>  class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingTownCity" id="txtBillingTownCity" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->city);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPostalZIP" id="txtBillingPostalZIP" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->postcode);?>" <?php } ?> class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                          <select id="select02" name="txtBillingCountyState" style="">
                          <?php if($billingAddress){?>
                           <option value='<?php print($billingAddress->state); ?>' selected="selected"><?php print($billingAddress->state); ?></option>
                          <?php } else{?>
                         <option value=''>Region / State:</option>
                         <?php }?>
                        </select>
                          <input type="hidden" name="txtBillingAddressId" id="txtBillingAddressId" value="<?php print($billingAddress->id);?>" />
                      </div>
                    </div>
                   
                  </div>
                </fieldset>
            </div>
            </div>
           
          </div>
  
  		
        <div class="checkoutsteptitle">Delivery Details
          </div>
          <div class="">
            <div class="row">
               <div class="form-horizontal">
                <fieldset>
                      <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingFirstName" id="txtShippingFirstName" type="text" value="<?php print($shippingAddressInfo->firstName);?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingLastName" id="txtShippingLastName" type="text" value="<?php print($shippingAddressInfo->lastName);?>" class="">
                      </div>
                    </div>
                   
                  
                   <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingAddress_1" id="txtShippingAddress_1" type="text" value="<?php  print($shippingAddressInfo->address);?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPhone" id="txtShippingPhone" type="text" value="<?php  print($shippingAddressInfo->phone);?>" class="">
                      </div>
                    </div>
                    
                      <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select011" name="txtShippingCountry"  style="">
                          <option value=''>--Select a country--</option>
                     <option country_id1="<?php echo $cuntrySelectedS->id;?>" value="<?php echo $cuntrySelectedS->id;?>" selected="selected"><?php  echo $cuntrySelectedS->name;?></option>

                            <?php foreach ($resultsall as $index=>$country) {
                            if ($country->id!=$cuntrySelectedS->id) {?>
         <option country_id1="<?php print ($country->id);?>" value="<?php print ($country->id);?>"><?php print ($country->name);?></option>
         <?php }}?>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                    <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtShippingCompanyName" id="txtShippingCompanyName" type="text" value="<?php print($shippingAddressInfo->houseNumber);?>" class="">
                      </div>
                    </div>
                   
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtShippingAddress_2" id="txtShippingAddress_2" type="text" value="<?php print($shippingAddressInfo->region);?>"  class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingTownCity" id="txtShippingTownCity" type="text" value="<?php print($shippingAddressInfo->city);?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPostalZIP" id="txtShippingPostalZIP" type="text" value="<?php print($shippingAddressInfo->postcode);?>" class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                          <select id="select022" name="txtShippingCountyState" style="">
                          <?php if($shippingAddressInfo){?>
                           <option value='<?php print($shippingAddressInfo->state);?>' selected="selected"><?php print($shippingAddressInfo->state);?></option>
                          <?php } else{?>
                         <option value=''>Region / State:</option>
                         <?php }?>
                        </select>
                      </div>
                    </div>
                    
                    <input type="hidden" name="txtShippingAddressId" id="txtShippingAddressId" value="<?php print($shippingAddressInfo->id);?>" />
                    
                  </div>
                </fieldset>
            </div>
            </div>
            
            
             <input value="Continue"   class="btn btn-success pull-right" type="submit">
            
            
            
          </div>
  
  
  </form>
  
  </section>
</div>
</div>
</div>