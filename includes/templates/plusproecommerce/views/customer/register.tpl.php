<!-- Shopping Cart -->
<?php 
$cartContents = "";
$totalAmount = 0;

$objShippingMethod = new ShippingMethod();


$cartShippingMethod = getShippingMethod();
$cartContents = getCartContents();

//Product Settings..
$objProductSettings  = new ProductSettings();
$objProductSettings->tb_name = 'tbl_product_settings';
 
$ProductSettingsInfo = $objProductSettings->getProductSettings();
 
$VATValue= $ProductSettingsInfo->VATValue;
$guest_amount = $ProductSettingsInfo->guest_amount;
?>

   <?php foreach($cartContents As $cIndex=>$cartContent){
    $totalAmount = $totalAmount +  $cartContent->productSubTotal; 
   }
    ?>
    
<?php
if ($guest_amount<$totalAmount or  isset($_SESSION['SESS_CUSTOMER_INFO'])) {?>

<script type="text/javascript">
<!--
window.location.href='<?php print(SITE_BASE_URL); ?>checkout.html';
//-->
</script>

<?php }
elseif($accountType =='Geust' ) {?>
<script type="text/javascript">

window.location.href='<?php print(SITE_BASE_URL); ?>checkout.html?page_type=checkout&accType=guest';

</script>
<?php } ?>

<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#customerRegistraionForm").validate({
                    rules: {
                    	cmbTitle: {
                            required : true,
                            
                        },
                        txtFirstName: {
                            required : true,
                            
                        },
                        txtLastName: {
                            required : true,
                            
                        },
                        txtEmail: {
                            required : true,
                            email : true
                        },
                        txtBillingPhone: {
                            required : true,
                            number : true
                        },
                        txtBillingAddress_1: {
                            required : true,
                            
                        },
                        txtBillingTownCity: {
                            required : true,
                            
                        },
                        txtBillingPostalZIP: {
                            required : true,
                            
                        },
                        txtBillingCountry: {
                            required : true,
                            
                        },
                        //txtBillingCountyState: {
                         //   required : true,
                            
                        //},
                        txtPassword: {
                            required : true,
                            
                        },
                        txtCPassword: {
                            required : true,
                            equalTo: "#txtPassword"
                        },
                        chkRangeId: {
                            required : true
                        }

                    },
                    messages: {
                    	cmbTitle: "Please select a title",
                    	txtBillingCountry : "Please select a country",
                    	//txtBillingCountyState : "Please select a region/state",
                    	chkRangeId : "Please accept the privacy policy"
                    },
		    //Submit Handler
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL);?>doRegister.php",
                            data: $("#customerRegistraionForm").serialize(), 
                            success: function(Res) {
                                if(Res == "email_failure"){
                                	$("#featured").hide('slow').after("<h1 style='color:#F10505;'>Email not sent!</h1>");    
                                } else if(Res == "failure"){
                                	$("#featured").hide('slow').after("<h1 style='color:#F10505;'>Registration failure!</h1>");
                                } else if(Res == "success"){
                                	$("#featured").hide('slow').after("<h1 style='color:#000;'>Successfully registered!</h1>");
                                }
				$(window).scrollTop(0);
                            	
                            },
                            error: function(Res) {
                                $("#featured").hide('slow').after("<h1 style='color:#000;'>Please submit again!</h1>");
                            }
                        });
                        return false;
                    }
                    //Submit Handler
                }); 
        });
</script>
<!-- Jquery Validation : End -->

<!--<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // form validation...............
    	errornotice = $("#formerror");
    	emptyerror = "This field is required.";
    	emailerror = "Please enter a valid e-mail.";
        userPasswordErrorC = "Password not matched.";   
            
            // customer login....
            $(".customerRegistration").submit(function(e){
                errornotice = $("#loginFormError");
                errornotice1 = $("#loginFormError1");
                var userEmail = $('#txtEmail').val();
                var userEmailError = false;
                
                var userPassword = $('#txtPassword').val();
                var userPasswordError = false;

                var userPassword1 = $('#txtCPassword').val();
                var userPasswordError1 = false;

                var billingFirstName = $('#txtFirstName').val();
                var billingFirstNameError = false;

                var billingLastName = $('#txtLastName').val();
                var billingLastNameError = false;

                var BillingPhone = $('#txtBillingPhone').val();
                var BillingPhoneError = false;

                var BillingAddress_1 = $('#txtBillingAddress_1').val();
                var BillingAddress_1Error = false;

                var BillingTownCity = $('#txtBillingTownCity').val();
                var BillingTownCityError = false;

                var BillingPostalZIP = $('#txtBillingPostalZIP').val();
                var BillingPostalZIPError = false;

                var BillingLastName = $('#txtBillingLastName').val();
                var BillingLastNameError = false;

                var BillingCountry = $('#select01').val();
                var BillingCountryError = false;

                var BillingState = $('#select02').val();
                var BillingStateError = false;

                var BillingTitle = $('#cmbTitle').val();
                var BillingTitleError = false;
                
                
                if ((userEmail == "") || (userEmail == emptyerror)) {
            		$('#txtEmail').addClass("error");
            		$('#txtEmail').val(emailerror);
            		errornotice.fadeIn(750);
                            userEmailError = true;
                        }
                if((userPassword == "") || (userPassword == emptyerror)) {
                    $('#txtPassword').addClass("error");
    		$('#txtPassword').val(emptyerror);
    		errornotice.fadeIn(750);
                    userPasswordError = true;
                }  

                if((userPassword1 != userPassword) || (userPassword1 == emptyerror) || (userPassword1 == "")) {
                    $('#txtCPassword').addClass("error");
    		$('#txtCPassword').val(userPasswordErrorC);
    		errornotice1.fadeIn(750);
                   userPasswordError = true;
                  return false;     
                } 

                if((billingFirstName == "") || (billingFirstName == emptyerror)) {
                    $('#txtFirstName').addClass("error");
    		$('#txtFirstName').val(emptyerror);
    		errornotice.fadeIn(750);
    		billingFirstNameError = true;
                } 


                if((billingLastName == "") || (billingLastName == emptyerror)) {
                    $('#txtLastName').addClass("error");
    		$('#txtLastName').val(emptyerror);
    		errornotice.fadeIn(750);
    		billingFirstNameError = true;
                }


                if((BillingPhone == "") || (BillingPhone == emptyerror)) {
                    $('#txtBillingPhone').addClass("error");
    		$('#txtBillingPhone').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingPhoneError = true;
                }  


                if((BillingAddress_1 == "") || (BillingAddress_1 == emptyerror)) {
                    $('#txtBillingAddress_1').addClass("error");
    		$('#txtBillingAddress_1').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingAddress_1Error = true;
                } 

                if((BillingTownCity == "") || (BillingTownCity == emptyerror)) {
                    $('#txtBillingTownCity').addClass("error");
    		$('#txtBillingTownCity').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingTownCityError = true;
                }


                if((BillingPostalZIP == "") || (BillingPostalZIP == emptyerror)) {
                    $('#txtBillingPostalZIP').addClass("error");
    		$('#txtBillingPostalZIP').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingPostalZIPError = true;
                }

                if((BillingLastName == "") || (BillingLastName == emptyerror)) {
                    $('#txtBillingLastName').addClass("error");
    		$('#txtBillingLastName').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingPostalZIPError = true;
                }

                if((BillingCountry == "") || (BillingCountry == emptyerror)) {
                    $('#select01').addClass("error");
    		$('#select01').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingCountryError = true;
                }


                if((BillingState == "") || (BillingState == emptyerror)) {
                    $('#select02').addClass("error");
    		$('#select02').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingStateError = true;
                }

                if(!$('input[name=chkRangeId]:checked').length) {
                	$('#errornoticeCheck').addClass("error");
            		$('#errornoticeCheck').val(emptyerror);
            		$('#errornoticeCheck').show("slow");
            		$("#errornoticeCheck").fadeOut(750);
            		errornotice.fadeIn(750);
            		BillingStateError = true;
                    
                }


                if((BillingTitle == "") || (BillingTitle == emptyerror)) {
                    $('#cmbTitle').addClass("error");
    		$('#cmbTitle').val(emptyerror);
    		errornotice.fadeIn(750);
    		BillingTitleError = true;
                }
              
                    
                   else {
                	return true;
                }
                            
                  
                return false;
            });
        
        
     
        
    });

</script> -->

<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/popup.js"></script>
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/popup.css" rel="stylesheet">
<!--  
<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
        
    	 
    });
    	</script>   
    	-->
      
<?php 
$objCountry = new Country();
$countryList = $objCountry->getAll();

$objRegion = new Region();
$regionList = $objRegion->getAll();
$regionListJSONObject = json_encode($regionList);

?>
<!-- Populate regions based on selected country : Start -->
<script>
$(document).ready(function(){
	$("#select01").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>
<!-- Populate regions based on selected country : End -->      
      
      
      	<div class="container">
    <!--<ul class="breadcrumb">
        <li>
          <a href="#">Home</a>
          <span class="divider">/</span>
        </li>
        <li class="active">Login</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            
		<!--Sidebar Starts-->
		 <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/sidebar.tpl.php'); ?>
            <!--sidebar Ends-->
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
  <section id="featured">
    <h1 class="productname">Register Account</h1>
    <p> If you already have an account with us, please login at the login page.</p>
    <!--  -->
            <div id="stylized" class="myform">
            <!--  -->
     <form id="customerRegistraionForm" class="form-horizontal customerRegistration" action="<?php print(SITE_BASE_URL);?>doRegister.php" method="post">
            <h3 class="heading3">Your Personal Details</h3>
            <div class="registerbox">
              <fieldset>
               <div class="control-group">
               <label class="control-label"><span class="red">*</span> Title:</label>
                <div class="controls">
                <select  id="cmbTitle" name="cmbTitle" class="emailinput">
                            <option value="">Select Title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Ms">Ms</option>
                            <option value="Miss">Miss</option>
                            <option value="Dr">Dr</option>
                        </select>
                        </div>
                        </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> First Name:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge"  name="txtFirstName" id="txtFirstName">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Last Name:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="txtLastName" id="txtLastName">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Email:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="txtEmail" id="txtEmail">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Telephone:</label>
                  <div class="controls">
                    <input type="text" id="txtBillingPhone" name="txtBillingPhone" class="input-xladasdasrge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Fax:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge">
                  </div>
                </div>
              </fieldset>
            </div>
            <hr>
            <h3 class="heading3">Your Address</h3>
            <div class="registerbox">
              <fieldset>
                <div class="control-group">
                  <label class="control-label"> Company:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingCompanyName" class="input-xlarge">
                  </div>
                </div>
                <!--<div class="control-group">
                  <label class="control-label"><span class="red">*</span> Last Name:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingLastName" id="txtBillingLastName" class="input-xlarge">
                  </div>
                </div>-->
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Address 1:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingAddress_1" id="txtBillingAddress_1" class="input-xlarge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Address 2:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingAddress_2" class="input-xlarge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    <span class="red">*</span>City:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingTownCity" id="txtBillingTownCity" class="input-xlarge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    <span class="red">*</span>Post Code:</label>
                  <div class="controls">
                    <input type="text" name="txtBillingPostalZIP" id="txtBillingPostalZIP" class="input-xlarge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="select01">
                    <span class="red">*</span>Country:</label>
                  <div class="controls">
                    <select class="span3" id="select01" name="txtBillingCountry">
                      <option value=''>Country:</option>
                       <?php foreach ($countryList as $index=>$country) { ?>
			                         <option country_id="<?php print($country->id); ?>" value="<?php echo $country->name;?>" <?php if($country->name == $billingAddressInfo->country) { ?> selected="selected" <?php } ?>><?php echo $country->name;?></option>
				                    <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    <!--<span class="red">*</span>-->Region / State:</label>
                  <div class="controls">
                    <select class="span3" id="select02" name="txtBillingCountyState">
                      <option value=''>Region / State:</option>

                    </select>
                  </div>
                </div>

		<div class="control-group">
                  <label class="control-label">Add region if not listed:</label>
                  <div class="controls">
                    <input type="text" name="txtAddCountryRegion" id="txtAddCountryRegion" class="input-xlarge">
                    <input type="hidden" id="txtAddCountryRegionCountryId" name="txtAddCountryRegionCountryId">
                  </div>
                </div>

              </fieldset>
            </div>
            <hr>
            <h3 class="heading3">Your Password</h3>
            <div class="registerbox">
              <fieldset>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Password:</label>
                  <div class="controls">
                    <input name="txtPassword" id="txtPassword" type="password" class="input-xlarge">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Password Confirm::</label>
                  <div class="controls">
                    <input name="txtCPassword" id="txtCPassword" type="password" class="input-xlarge">
                  </div>
                </div>
              </fieldset>
            </div>
            <hr>
            <h3 class="heading3">Newsletter</h3>
            <div class="registerbox">
              <fieldset>
                <div class="control-group">
                  <label class="control-label">Subscribe:</label>
                  <div class="controls">
                    <label class="checkbox inline">
                      <input type="radio" value="Enabled" name="Enabled" checked="checked">
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="radio" value="Disabled" name="Enabled">
                      No </label>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class="pull-right">
             <div id="errornoticeCheck" style="display:none;"> You must agree terms & condition to sign up</div>
              <label class="checkbox inline">
                <input type="checkbox" name="chkRangeId" value="option2">
              </label>
              I have read and agree to the <a id="button" href="#">Privacy Policy</a>
              &nbsp;
              <input name="customerRegister" id="customerRegister" type="submit" value="Continue" class="btn btn-inverse">
            </div>
          </form>
<!--  -->
          </div>
          <!--  -->
          <div class="clearfix"></div>     
  </section>
</div>
</div>
</div>


        	         <div id="popupContact">
<a id="popupContactClose"><span></span></a>
		<h1><?php echo $pageTitle2;?></h1>


		   <?php print($pageBody1);?>
	
	</div>
