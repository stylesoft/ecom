<?php $success = $_GET['suc']; ?>
<!-- Shopping Cart -->


<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
 <script>
        $(document).ready(function() {
                $("#frmCustomerLogin").validate({
                    rules: {
                        email: {
                            required : true,
                            email : true
                        },
                        txtPassword: {
                            required : true,
                            
                        },
                        txtCPassword: {
                            required : true,
                            equalTo: "#txtPassword"
                        }

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            <?php if($activation) { ?>
                            url: "<?php echo SITE_BASE_URL ?>ajex/checkout/forgetpassword.php",
                            <?php } else {?>
                            url: "<?php echo SITE_BASE_URL ?>ajex/checkout/activation.php",
                            <?php }?>
                            data: $("#frmCustomerLogin").serialize(), 
                            success: function(Res) {
                            	if(Res == "success"){
                            		 <?php if($activation) { ?>
                            		  $("#frmCustomerLogin").hide('slow'); 
                            		  $("#resutl").show('slow');
                            		<?php } else{?>
                            		  $("#frmCustomerLogin").hide('slow'); 
                            		  $("#resutlP").show('slow');
                            		<?php }?>
                                } else if(Res == "failed"){ 
                                	 <?php if($activation) { ?>
                           		  $("#frmCustomerLogin").hide('slow'); 
                           		  $("#resutlError").show('slow');
                           		<?php } else{?>
                           		  $("#frmCustomerLogin").hide('slow'); 
                           		  $("#resutlError1").show('slow');
                           		<?php }?>                                   
                               		//$('#error_notice').fadeIn(750);
					          //$('#errormsg').css('display','block');
                                    //$('#error_notice').fadeOut(750);
                                }
                            },
                            error: function(Res) {
                            	$('#errormsg').fadeIn(750);
                                $('#errormsg').fadeOut(750);
                            }
                        });
                        return false;
                    },
                }); 
        });
</script>

<!-- Jquery Validation : End -->


<!--<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // customer login....
        $("#frmCustomerLogin").submit(function(e){
            $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
            function(data){   
                if(data == 'success'){
                    window.location.href='<?php print(SITE_BASE_URL); ?>checkout.php'; 
                } else if(data == 'failed'){
                    $('#error_notice').fadeIn(750);
                    $('#error_notice').fadeOut(750);
                }
                           
            }		
        );
            return false;
        });
        
        
     
        
    });

</script> -->

	<div class="container">
    <!--<ul class="breadcrumb">
    
        <li>
          <a href="<?php print(SITE_BASE_URL); ?>">Home</a>
          <span class="divider">/</span>
        </li>
        <li class="active">Login</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            
	<div class="span3">
				<aside>
					<h1 class="headingfull"><span>My Account</span></h1>
				<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/accountsidebar.tpl.php'); ?>
				</aside>
	
			</div>
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
    <section id="featured">
    <h1 class="productname"> <?php if ($activation == '') {?>Send an E-mail to re-set password<?php } else{?> Re-set Password<?php }?></h1>
    <section class="returncustomer2">
    <?php if ($activation == '') {?>
            <h2 class="heading2">Example (email@youremail.com)</h2>
            <p>If you do not remember your password, please fill in the field below and please request a new one. It will be sent to the email address you specify.</p>
            <?php } else{?>
            
            <?php }?>
            <div class="loginbox">
             <div id="resutl" style="display:none;">your password has reset</div>
             <div id="resutlP" style="display:none;">Sent an email to reset your password check your email</div>
              <div id="resutlError1" style="display:none; color:red;">There is no registered account with this email </div>
              <form class="form-vertical" name="frmCustomerLogin" id="frmCustomerLogin" action="" method="post">
                <fieldset>
                 <?php if ($activation == '') {?>
                 <div class="control-group">
                      <label class="control-label">E-Mail</label>
                      <div class="controls">
                        <input name="email" id="email" type="text" value="" class="">
                      </div>
                    </div>
                    <?php  } else if ($activation != '') {?>
                  <div class="control-group">
                    <label class="control-label">New Password:</label>
                    <div class="controls">
                      <input type="password" name="txtPassword" id="txtPassword" class="span3">
                      <input type="hidden" name="id" id="id" value="<?php echo $id;?>" class="span3">
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label">Confirm Password:</label>
                    <div class="controls">
                      <input type="password" name="txtCPassword" id="txtCPassword" class="span3">
                    </div>
                  </div>
                  <?php }?>
                  <!--<a href="#" class="">Forgotten Password</a>-->
                 <input value="Submit" id="button-login" name="button-login" class="btn btn-inverse" type="submit"><br>
                  <!--   <a class="btn btn-inverse" href="#">Login</a>-->
                </fieldset>
              </form>
            </div>
          </section>
  </section>
</div>
</div>
</div>
