	<?php 
	$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
	$currentCustomerId   = $currentCustomerInfo->id;
	    $objCustomer = new Customer();
        $objCustomer->tb_name = 'tbl_customer';
        
                                                       
	$CustomerDetails = $objCustomer->getCustomer($currentCustomerId);
	
	$billingAddress = $CustomerDetails->addressBillingDtls[0];
	$shippingAddressInfo = $CustomerDetails->addressShippingDtls[0];
	//print_r($CustomerDetails);
	
	?>
	<?php if($currentCustomerInfo =='') {?>
<script type="text/javascript">
<!--
window.location.href='<?php print(SITE_BASE_URL); ?>login.html';
//-->
</script>
<?php } //com?>
	<?php $bojcountry = new Country();
      $cuntrySelectedB = $bojcountry->getCountry($billingAddress->country);
      $cuntrySelectedS = $bojcountry->getCountry($shippingAddressInfo->country);
        //testing 
        
        // print_r($cuntrySelected);
        ?>
<div class="container">
  <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
           
			<div class="span3">
				<aside>
					<h1 class="headingfull"><span>My Account</span></h1>
				<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/accountsidebar.tpl.php'); ?>
				</aside>
	
			</div>
			
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
  <section id="featured">
   	
    <div class="checkoutsteptitle">Personal Details
          </div>
          <div class="">
            <div class="row">
              <form class="form-horizontal">
                <fieldset>
                  <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name : </label>
                      <div class="controls form_text">
                      	<?php print ($CustomerDetails->firstName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name : </label>
                      <div class="controls form_text">
                        <?php print ($CustomerDetails->lastName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">E-Mail : </label>
                      <div class="controls form_text">
                        <?php print ($CustomerDetails->email);?>
                      </div>
                    </div>
                   
                  </div>
                </fieldset>
              </form>
            </div>
          
             <!--<div class="span1 pull-right"><a class="btn btn-success ">Continue</a></div>  
             <div class="span1 pull-right" ><a class="btn btn-success">Back</a></div>-->
             <br><br>
          </div>
    
    
    
    <div class="checkoutsteptitle">Billing Details
          </div>
          <div class="">
            <div class="row">
              <form class="form-horizontal">
                <fieldset>
               <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name : </label>
                      <div class="controls form_text">
                      	<?php print($billingAddress->firstName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name : </label>
                      <div class="controls form_text">
                        <?php print($billingAddress->lastName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">E-Mail : </label>
                      <div class="controls form_text">
                        <?php print ($CustomerDetails->email);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone : </label>
                      <div class="controls form_text">
                     <?php print($billingAddress->phone);?>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Company : </label>
                      <div class="controls form_text">
                       <?php print($billingAddress->houseNumber);?>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                  
                    <div class="control-group">
                      <label class="control-label">Address 1 : </label>
                      <div class="controls form_text">
                        <?php print($billingAddress->address);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Address 2 : </label>
                      <div class="controls form_text">
                     <?php print($billingAddress->region);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City : </label>
                      <div class="controls form_text"> 
                       <?php print($billingAddress->city);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code : </label>
                      <div class="controls form_text">
                      <?php print($billingAddress->postcode);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Country : </label>
                      <div class="controls form_text">
                       <?php echo $cuntrySelectedB->name;?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Region  /<br>
                      
                         State  :  </label>
                      <div class="controls form_text">
                      <?php echo $billingAddress->state;?>
                     	
                      </div>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
          
             <!--<div class="span1 pull-right"><a class="btn btn-success ">Continue</a></div>  
             <div class="span1 pull-right" ><a class="btn btn-success">Back</a></div>-->
             <br><br>
          </div>
          
          
           <div class="checkoutsteptitle">Delivery Details
          </div>
          <div class="">
            <div class="row">
              <form class="form-horizontal">
                <fieldset>
               <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name : </label>
                      <div class="controls form_text">
                      	<?php print($shippingAddressInfo->firstName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name : </label>
                      <div class="controls form_text">
                        <?php print($shippingAddressInfo->lastName);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">E-Mail : </label>
                      <div class="controls form_text">
                        <?php print ($CustomerDetails->email);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone : </label>
                      <div class="controls form_text">
                     <?php print($shippingAddressInfo->phone);?>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Company : </label>
                      <div class="controls form_text">
                       <?php print($shippingAddressInfo->houseNumber);?>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                    
                    <div class="control-group">
                      <label class="control-label">Address 1 : </label>
                      <div class="controls form_text">
                        <?php print($shippingAddressInfo->address);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Address 2 : </label>
                      <div class="controls form_text">
                     <?php print($shippingAddressInfo->region);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City : </label>
                      <div class="controls form_text"> 
                       <?php print($shippingAddressInfo->city);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code : </label>
                      <div class="controls form_text">
                      <?php print($shippingAddressInfo->postcode);?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Country : </label>
                      <div class="controls form_text">
                       <?php echo $cuntrySelectedS->name;?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Region  /<br>
                      
                         State  :  </label>
                      <div class="controls form_text">
                      <?php echo $shippingAddressInfo->state;?>
                     	
                      </div>
                    </div><br><br><div align="right"><a class="btn btn-inverse" href="<?php print(SITE_BASE_URL);?>editaccount.html" >Edite Details</a></div>
                  </div>
                  
                   
                </fieldset>
              </form>
            </div>
          
             <!--<div class="span1 pull-right"><a class="btn btn-success ">Continue</a></div>  
             <div class="span1 pull-right" ><a class="btn btn-success">Back</a></div>-->
             <br><br>
          </div>
    
  </section>
</div>
</div>
</div>