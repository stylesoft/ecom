<?php $success = $_GET['suc']; ?>
<!-- Shopping Cart -->


<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#frmCustomerLogin").validate({
                    rules: {
                        email: {
                            required : true,
                            email : true
                        },
                		password: {
                    		required : true
                		} 

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "ajex/checkout/login.php",
                            data: $("#frmCustomerLogin").serialize(), 
                            success: function(Res) {
                            	if(Res == "success"){
                            		window.location.href='<?php print(SITE_BASE_URL); ?>';
                                }else if(Res == "success_visited"){
                                    console.log('yes');
                            		window.location.href='<?php print($product_url); ?>';
                                }  else if(Res == "failed"){                                        
                               		//$('#error_notice').fadeIn(750);
					$('#errormsg').css('display','block');
                                    //$('#error_notice').fadeOut(750);
                                }
                            },
                            error: function(Res) {
                            	$('#errormsg').fadeIn(750);
                                $('#errormsg').fadeOut(750);
                            }
                        });
                        return false;
                    },
                }); 
        });
</script>
<!-- Jquery Validation : End -->


<!--<script type="text/javascript">
    //Accordian Function
    $(document).ready(function(){
    
        // customer login....
        $("#frmCustomerLogin").submit(function(e){
            $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
            function(data){   
                if(data == 'success'){
                    window.location.href='<?php print(SITE_BASE_URL); ?>checkout.php'; 
                } else if(data == 'failed'){
                    $('#error_notice').fadeIn(750);
                    $('#error_notice').fadeOut(750);
                }
                           
            }		
        );
            return false;
        });
        
        
     
        
    });

</script> -->

	<div class="container">
    <!--<ul class="breadcrumb">
    
        <li>
          <a href="<?php print(SITE_BASE_URL); ?>">Home</a>
          <span class="divider">/</span>
        </li>
        <li class="active">Login</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            
			
			<div class="span3">
				<aside>
					<h1 class="headingfull"><span>My Account</span></h1>
				<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/accountsidebar.tpl.php'); ?>
				</aside>
	
			</div>
			
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
  <section id="featured">
    <?php 
    if (isset($_GET['suc'])) {
    
    if ($success=='suc') {?>
        <h1>Registration Success</h1>
        <?php } else {?>
        <h1>Registration Failed</h1>
        <?php }
        }?>
    <h1 class="productname">Login</h1>
    <section class="newcustomer">
            <h2 class="heading2">New Customer </h2>
            <div class="loginbox">
              <h4 class="heading4"> Register Account</h4>
              <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
              <br>
              <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
              <br>
              <br>
              <a class="btn btn-inverse" href="register.html">Continue</a>
            </div>
          </section>
          <section class="returncustomer">
            <h2 class="heading2">Returning Customer </h2>
             <div id="errormsg" style="display:none;" class="error_notice">Login failed!</div>
            <div class="loginbox">
              <h4 class="heading4">I am a returning customer</h4>
              <!--  -->
            <div id="stylized" class="myform">
            <!--  -->
              <form class="form-vertical" name="frmCustomerLogin" id="frmCustomerLogin" action="" method="post">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label">E-Mail Address:</label>
                    <div class="controls">
                      <input type="text" class="span3" name="email" id="email">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Password:</label>
                    <div class="controls">
                      <input type="password" class="span3" name="password" id="password">
                    </div>
                  </div>
                  <a href="<?php echo SITE_BASE_URL;?>forgetpassword.html" class="">Forgotten Password</a>
                  <br>
                  <br>
                   <input value="Login" id="button-login" name="button-login" class="btn btn-inverse" type="submit"><br>
                   <!-- 
                  <a class="btn btn-inverse" value="Login" id="button-login" name="button-login" href="#">Login</a>
                   -->
                   
                   <?php if($product_url != ''){?>
                   <input type = "hidden" name ="product_url" value="<?php echo $product_url ?>" />
                   <?php } ?>
                   
                   
                </fieldset>
              </form>
<!--  -->
              </div>
              <!--  -->
            </div>
          </section>
  </section>
</div>
</div>
</div>
