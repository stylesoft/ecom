	<?php if($currentCustomerInfo =='') {?>
<script type="text/javascript">
<!--
window.location.href='<?php print(SITE_BASE_URL); ?>login.html';
//-->
</script>
<?php } //com?>
<div class="container">
    <?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
		<div class="row">
        	<!--Sidebar Starts-->
            
			<div class="span3">
				<aside>
					<h1 class="headingfull"><span>My Account</span></h1>
				<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/accountsidebar.tpl.php'); ?>
				</aside>
	
			</div>
            <!--sidebar Ends-->
            <div class="span9">
  <!-- Featured Product-->
  
  <section id="featured">
   	
     <h1 class="productname">Order History</h1>
      <div class="cart-info">
            <table class="table table-striped table-bordered">
              <tbody><tr>
               
                <th class="name">Order No</th>
                <th class="name">Date On</th>
                <th class="model">Pay By</th>
                <th class="quantity">Amount</th>
               
              </tr>
              <?php 
              if(count($orderResult) >0){
            foreach($orderResult As $rowIndex=>$orderData) {?>
              <tr>
               
                <td class="name"><?php print($orderData->id);?></td>
                <td class="name"><?php print($orderData->orderDate);?></td>
                <td class="model"><?php print($orderData->paymentMethod);?></td>
                <td class="quantity"><?php print(CURRENCY_CODE); ?><?php print($orderData->grandTotal*CURRENCY_RATE);?></td>
              
                
              </tr>
                 <?php }}?>
            
             
            </tbody></table>
           
          </div>
    
  </section>
</div>
</div>
</div>