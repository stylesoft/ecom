<?php $logerror = $_GET['log']?>
<script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery-ui.js"></script>
<script type="text/javascript">

//Accordian Function
$(document).ready(function(){
    
    <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] != '' ){?>
    $( "#checkout" ).accordion({
heightStyle: "content",
header: "> div > .checkout-heading",
collapsible: false,
active: 5
});
<?php } elseif(isset($_SESSION['check'])){?>
$( "#checkout" ).accordion({
heightStyle: "content",
header: "> div > .checkout-heading",
collapsible: false,
active: 2
});       	
<?php } elseif(isset($_SESSION['SESS_CUSTOMER_INFO'])){?>
$( "#checkout" ).accordion({
heightStyle: "content",
header: "> div > .checkout-heading",
collapsible: false,
active: 1
});
<?php } else {?>
 $( "#checkout" ).accordion({
heightStyle: "content",
header: "> div > .checkout-heading",
collapsible: false
});            
<?php } ?>
        
        // form validation...............
    	errornotice = $("#formerror");
    	emptyerror = "This field is required.";
    	emailerror = "Please enter a valid e-mail.";
            
            
            // customer login....
            $("#frmCustomerLogin").submit(function(e){
                errornotice = $("#loginFormError");
                var userEmail = $('#email').val();
                var userEmailError = false;
                var userPassword = $('#password').val();
                var userPasswordError = false;
                
                if ((userEmail == "") || (userEmail == emptyerror)) {
    		$('#email').addClass("error");
    		$('#email').val(emailerror);
    		errornotice.fadeIn(750);
                    userEmailError = true;
                }
                if((userPassword == "") || (userPassword == emptyerror)) {
                    $('#password').addClass("error");
    		$('#password').val(emailerror);
    		errornotice.fadeIn(750);
                    userPasswordError = true;
                }  
                    
                    
                if(userEmailError == false && userPasswordError == false){
                        errornotice.fadeOut(750);
                        $.post('ajex/checkout/login.php',$(this).serialize()+'&ajax=1',
    			function(data){   
                               if(data == 'success'){
                                   window.location.reload();
                                    e.preventDefault();
                                    delta = 1;
                                    $('#checkout').accordion('option', 'active',($('#checkout').accordion('option','active') + delta  )); 
                               } else if(data == 'failed'){
                                   $('#loginFormErrorInvalid').fadeIn(750);
                               }
                               
                            }		
                        );
                    } else {
                        $('#loginFormErrorInvalid').fadeOut(750);
                    }      
                            
                  
                return false;
            });
            
            
             // Billing Details ....
            $("#frmBillingDetails").submit(function(e){
            
            	    errornotice = $("#billingFormError");
                        var billingTitle = $('#cmbBillingTitle').val();
                        var billingTitleError = false;

                        var billingFirstName = $('#txtBillingFirstName').val();
                        var billingFirstNameError = false;

                        var billingLastName = $('#txtBillingLastName').val();
                        var billingLastNameError = false;



                        var billingAddress1 = $('#txtBillingAddress_1').val();
                        var billingAddress1Error = false;

                        var billingTownCity = $('#txtBillingTownCity').val();
                        var billingTownCityError = false;

                        var billingCountry = $('#txtBillingCountry').val();
                        var billingCountryError = false;
                        
                        
                        if ((billingTitle == "") || (billingTitle == emptyerror)) {
                            $('#cmbBillingTitle').addClass("error");
                            $('#cmbBillingTitle').val(emptyerror);
                            errornotice.fadeIn(750);
                            billingTitleError = true;
                        }


                        if ((billingFirstName == "") || (billingFirstName == emptyerror)) {
                                $('#txtBillingFirstName').addClass("error");
                                $('#txtBillingFirstName').val(emptyerror);
                                errornotice.fadeIn(750);
                                billingFirstNameError = true;
                        }


              
                        if ((billingLastName == "") || (billingLastName == emptyerror)) {
                                $('#txtBillingLastName').addClass("error");
                                $('#txtBillingLastName').val(emptyerror);
                                errornotice.fadeIn(750);
                                billingLastNameError = true;
                        }


                        if ((billingAddress1 == "") || (billingAddress1 == emptyerror)) {
                            $('#txtBillingAddress_1').addClass("error");
                            $('#txtBillingAddress_1').val(emptyerror);
                            errornotice.fadeIn(750);
                            billingAddress1Error = true;
                        }


                        if ((billingTownCity == "") || (billingTownCity == emptyerror)) {
                            $('#txtBillingTownCity').addClass("error");
                            $('#txtBillingTownCity').val(emptyerror);
                            errornotice.fadeIn(750);
                            billingTownCityError = true;
                        }


                        if ((billingCountry == "") || (billingCountry == emptyerror)) {
                            $('#txtBillingCountry').addClass("error");
                            $('#txtBillingCountry').val(emptyerror);
                            errornotice.fadeIn(750);
                            billingCountryError = true;
                        }

            
            
            if(billingTitleError == false && billingFirstNameError == false && billingLastNameError == false && billingAddress1Error == false && billingTownCityError == false && billingCountryError == false){
                errornotice.fadeOut(750);
                        $.post('ajex/checkout/billing-details.php',$(this).serialize()+'&ajax=1',
    			function(data){
                               if(data == 'success'){
                            	   window.location.reload();
                                    e.preventDefault();
                                    delta = 1;
                                    $('#checkout').accordion('option', 'active',($('#checkout').accordion('option','active') + delta  ));   
                               }
                            }		
                        );
                  } 
                            
                            
                return false;
            });
            
            
            // Delivery Details ....
            $("#frmDeliveryDetails").submit(function(e){
            
            		    errornotice = $("#deliveryDetailsFormError");
                        var shippingTitle = $('#cmbShippingTitle').val();
                        var shippingTitleError = false;

                        var shippingFirstName = $('#txtShippingFirstName').val();
                        var shippingFirstNameError = false;

                        var shippingLastName = $('#txtShippingLastName').val();
                        var shippingLastNameError = false;

                        var shippingAddress1 = $('#txtShippingAddress_1').val();
                        var shippingAddress1Error = false;


                        var shippingTownCity = $('#txtShippingTownCity').val();
                        var shippingTownCityError = false;

                        var shippingCountry = $('#txtShippingCountry').val();
                        var shippingCountryError = false;
                        
                         if ((shippingTitle == "") || (shippingTitle == emptyerror)) {
                            $('#cmbShippingTitle').addClass("error");
                            $('#cmbShippingTitle').val(emptyerror);
                            errornotice.fadeIn(750);
                            shippingTitleError = true;
                        }


                        if ((shippingFirstName == "") || (shippingFirstName == emptyerror)) {
                                $('#txtShippingFirstName').addClass("error");
                                $('#txtShippingFirstName').val(emptyerror);
                                errornotice.fadeIn(750);
                                shippingFirstNameError = true;
                        }


              
                        if ((shippingLastName == "") || (shippingLastName == emptyerror)) {
                                $('#txtShippingLastName').addClass("error");
                                $('#txtShippingLastName').val(emptyerror);
                                errornotice.fadeIn(750);
                                shippingLastNameError = true;
                        }


                        if ((shippingAddress1 == "") || (shippingAddress1 == emptyerror)) {
                            $('#txtShippingAddress_1').addClass("error");
                            $('#txtShippingAddress_1').val(emptyerror);
                            errornotice.fadeIn(750);
                            shippingAddress1Error = true;
                        }


                        if ((shippingTownCity == "") || (shippingTownCity == emptyerror)) {
                            $('#txtShippingTownCity').addClass("error");
                            $('#txtShippingTownCity').val(emptyerror);
                            errornotice.fadeIn(750);
                            shippingTownCityError = true;
                        }


                        if ((shippingCountry == "") || (shippingCountry == emptyerror)) {
                            $('#txtShippingCountry').addClass("error");
                            $('#txtShippingCountry').val(emptyerror);
                            errornotice.fadeIn(750);
                            shippingCountryError = true;
                        }
                        
                        if(shippingTitleError == false && shippingFirstNameError == false && shippingLastNameError == false && shippingAddress1Error == false && shippingTownCityError == false && shippingCountryError == false){
                            errornotice.fadeOut(750);
                            $.post('ajex/checkout/delivery-details.php',$(this).serialize()+'&ajax=1',
                                function(data){   
                                if(data == 'success'){
                                        e.preventDefault();
                                        delta = 1;
                                        $('#checkout').accordion('option', 'active',($('#checkout').accordion('option','active') + delta  ));   
                                }
                                }		
                            );
                        }   
                            
                return false;
            });
            
            // Delivery Methods
            $("#frmDeliveryMethods").submit(function(e){
            
                     errornotice = $("#deliveryMethodFormError");
            
                     if ($("input[name='chkRangeId']:checked").length > 0){   
                        errornotice.fadeOut(750);
                        $.post('ajex/checkout/delivery-method.php',$(this).serialize()+'&ajax=1',
    			function(data){ 
                               if(data == 'success'){
                                    e.preventDefault();
                                    delta = 1;
                                    $('#checkout').accordion('option', 'active',($('#checkout').accordion('option','active') + delta  ));   
                               }
                            }		
                        );
                           } else{
                errornotice.fadeIn(750);
            } 
                            
                return false;
            });
            
            
            
            // Payment Methods
             $("#frmPaymentMethods").submit(function(e){
            	  errornotice = $("#paymentMethodFormError");
            	  if ($("input[name='chkTermsConditions']:checked").length > 0){
                        $.post('ajex/checkout/payment-methods.php',$(this).serialize()+'&ajax=1',
    			function(data){   
                               if(data == 'success'){
                                   window.location.reload();
                                    e.preventDefault();
                                    delta = 1;
                                    $('#checkout').accordion('option', 'active',($('#checkout').accordion('option','active') + delta  ));   
                               }
                            }		
                        );
            	  } else{
            		  errornotice.fadeIn(750);
            	  }
                return false;
            });
            
            // new customer options....
            // Payment Methods
            $("#frmNewCustomer").submit(function(e){
                   window.location.href='register.php'; 
                   return false;
            });
            
            
            // Clears any fields in the form when the user clicks on them
            $(":input").focus(function() {
                    if ($(this).hasClass("error")) {
                            $(this).val("");
                            $(this).removeClass("error");
                    }

            });
        
});

</script> 

<?php $bojcountry = new Country();
       $resultsall = $bojcountry->getAll();
         
         $objRegion = new Region();
         $regionList = $objRegion->getAll();
         $regionListJSONObject = json_encode($regionList);
        
        ?>
        <!-- Populate regions based on selected country : Start -->
<script>
$(document).ready(function(){
	$("#select01").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>

<script>
$(document).ready(function(){
	$("#select011").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select022').find('option').remove();
			$('#select022').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select022').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>
<section id="checkout">
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="#">Home</a><span class="divider">/</span></li>
        <li class="active">Checkout</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <h1 class="productname">Summer Offer Best Product (<?php echo count($cartContents);?>)</h1>
      <div class="checkoutsteptitle checkout-heading">Step 1: Checkout Options<a class="modify">Modify</a>
          </div>
           
          <div class="checkoutstep " style="display: none;">
          
            <section class="newcustomer ">
              <h3 class="heading3">New Customer </h3>
              <div class="loginbox">
                <label class="inline">
                  <input type="radio" value="option1" style="">
                  Register Account </label>
                <br>
                <label class="inline">
                  <input type="radio" value="option1" style="">
                  Guest Checkout </label>
                <p><br>
                  By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                <br>
                <a class="btn btn-inverse" href="<?php print(SITE_BASE_URL); ?>register.html">Continue</a>
              </div>
            </section>
            <section class="returncustomer">
              <h3 class="heading3">Returning Customer </h3>
              <div class="loginbox">
                <form class="form-vertical" name="frmCustomerLogin" id="frmCustomerLogin" action="" method="post">
                  <fieldset>
                    <div class="control-group">
                      <label class="control-label">E-Mail Address:</label>
                      <div class="controls">
                        <input type="text" name="email" id="name="email"" class="span3">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Password:</label>
                      <div class="controls">
                        <input name="password" id="password" value="" type="password" class="span3">
                      </div>
                    </div>
                    <a href="#" class="">Forgotten Password</a>
                    <br>
                    <br>
                     <input value="Login" id="button-login" name="button-login" class="btn btn-inverse" type="submit">
                    
                  </fieldset>
                </form>
              </div>
            </section>
         
          </div>
          <div class="checkoutsteptitle checkout-heading">Step 2: Billing Details<a class="modify">Modify</a>
          </div>
          <div class="checkoutstep" style="display: none;">
            
            <div class="row">
              <form class="form-horizontal" name="frmBillingDetails" id="frmBillingDetails" action="" method="post">
                <fieldset>
                  <div class="span4">
                         <div class="control-group">
                      <label class="control-label">Title<span class="red">*</span></label>
                      <div class="controls">
                        <select id="selectError" name="cmbBillingTitle" style="">
                          <option>Please Select</option>
                         <option value="Mr" <?php if($billingAddress->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                            <option value="Mrs" <?php if($billingAddress->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                            <option value="Ms" <?php if($billingAddress->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                            <option value="Miss" <?php if($billingAddress->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                            <option value="Dr" <?php if($billingAddress->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingFirstName" id="txtBillingFirstName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->firstName);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingLastName" id="txtBillingLastName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->lastName);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPhone" id="txtBillingPhone" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->phone);?>" <?php } ?> class="">
                      </div>
                    </div>
                       <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtBillingCompanyName" id="txtBillingCompanyName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->houseNumber);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingAddress_1" id="txtBillingAddress_1" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->address);?>" <?php } ?> class="">
                      </div>
                    </div>
                  </div>
                  <div class="span4">
                
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtBillingAddress_2" id="txtBillingAddress_2" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->region);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingTownCity" id="txtBillingTownCity" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->city);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPostalZIP" id="txtBillingPostalZIP" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->postcode);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select01" name="txtBillingCountry" style="">
                          <option>--Select a country--</option>
                        <option country_id="<?php print ($billingAddress->id);?>" value="<?php echo $billingAddress->country;?>" selected="selected"><?php echo $billingAddress->country;?></option>
                           <?php foreach ($resultsall as $index=>$country) {
                             if ($country->name!=$billingAddress->country) {?>
                    <option country_id="<?php print ($country->id);?>" value="<?php print ($country->name);?>"><?php print ($country->name);?></option>
         <?php }}?>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select02" name="txtBillingCountyState" style="">
                         <option value=''>Region / State:</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </fieldset>
                            <div style="clear: both; padding-top: 15px; border-top: 1px solid #DDDDDD;">
    <input name="shipping_address" value="1" id="shipping" checked="checked" type="checkbox">
    <label for="shipping">My delivery and billing addresses are the same.</label>
  <br>
  <br>
  <br>
</div>

  <input type="hidden" name="txtBillingAddressId" id="txtBillingAddressId" value="<?php print($billingAddress->id);?>" />
                <input value="Continue" id="billing_details" name="btnBillingDetails" class="btn btn-success pull-right" type="submit">
              </form>
            </div>
          
          </div>
          <div class="checkoutsteptitle checkout-heading">Step 3: Delivery Details<a class="modify">Modify</a>
          </div>
          <div class="checkoutstep" style="display: none;">
          
            <div class="row">
              <form class="form-horizontal" name="frmDeliveryDetails" id="frmDeliveryDetails" action="" method="post">
                <fieldset>
                  <div class="span4">
                          <div class="control-group">
                      <label class="control-label">Title<span class="red">*</span></label>
                      <div class="controls">
                        <select id="selectError"  name="cmbShippingTitle" style="">
                         <option>Please Select</option>
                        <option value="Mr" <?php if($shippingAddressInfo->title == 'Mr'){?> selected="selected" <?php } ?>>Mr</option>
                        <option value="Mrs" <?php if($shippingAddressInfo->title == 'Mrs'){?> selected="selected" <?php } ?>>Mrs</option>
                        <option value="Ms" <?php if($shippingAddressInfo->title == 'Ms'){?> selected="selected" <?php } ?>>Ms</option>
                        <option value="Miss" <?php if($shippingAddressInfo->title == 'Miss'){?> selected="selected" <?php } ?>>Miss</option>
                        <option value="Dr" <?php if($shippingAddressInfo->title == 'Dr'){?> selected="selected" <?php } ?>>Dr</option>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input  name="txtShippingFirstName" id="txtShippingFirstName" type="text" value="<?php if ($_SESSION['check'] != ''){print($billingAddress->firstName);} else {print($shippingAddressInfo->firstName);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingLastName" id="txtShippingLastName" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->lastName); } else{ print($shippingAddressInfo->lastName);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPhone" id="txtShippingPhone" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->phone); } else { print($shippingAddressInfo->phone);}?>" class="">
                      </div>
                    </div>  
                      <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtShippingCompanyName" id="txtShippingLastName" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->houseNumber); } else {print($shippingAddressInfo->houseNumber);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingAddress_1" id="txtShippingAddress_1" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->address); } else { print($shippingAddressInfo->address);}?>" class="">
                      </div>
                    </div>
                  </div>
                  <div class="span4">
                  
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtShippingAddress_2" id="txtShippingAddress_2" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->region); } else {print($shippingAddressInfo->region);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingTownCity" id="txtShippingTownCity" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->city); } else {print($shippingAddressInfo->city);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPostalZIP" id="txtShippingPostalZIP" type="text" value="<?php if ($_SESSION['check'] != ''){ print($billingAddress->postcode); } else {print($shippingAddressInfo->postcode);}?>" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select011" name="txtShippingCountry" style="">
                     <option value="">--Select a country--</option>
                     <option country_id="<?php if ($_SESSION['check'] != ''){ print($billingAddress->id); } else {echo $shippingAddressInfo->id;}?>" value="<?php echo $shippingAddressInfo->country;?>" selected="selected"><?php if ($_SESSION['check'] != ''){ print($billingAddress->country); } else {echo $shippingAddressInfo->country;}?></option>

          <?php foreach ($resultsall as $index=>$country) {
       if ($country->name!=$billingAddress->country) {
      	?>
         <option country_id="<?php print ($country->id);?>" value="<?php print ($country->name);?>"><?php print ($country->name);?></option>
         <?php }}?>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select022" name="txtShippingCountyState" style="">
                           <option value=''>Region / State:</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </fieldset>
                 <input value="Continue" id="delivery_details" name="btnDelivery_details" class="btn btn-success pull-right" type="submit">
            <input type="hidden" name="txtShippingAddressId" id="txtShippingAddressId" value="<?php print($shippingAddressInfo->id);?>" />
              </form>
            </div>
          </div>
          <div class="checkoutsteptitle checkout-heading">Step 4: Delivery Method<a class="modify">Modify</a>
          </div>
          <div class="checkoutstep" style="display: none;">
            <p>Please select the preferred shipping method to use on this order..</p>
                <?php   if($cartContents){?>
          <?php foreach($cartContents As $cIndex=>$cartContent){
          
          	$productWeight = $cartContent->productInfo-> productWeight;
          	$unit  = $cartContent->productInfo-> weightUnit;
          	$productQty    = $cartContent->productQty;
          	
          	if($unit=="kg"){
          	
          		$productWeight = $productWeight *1000;
          	
          	}
          	
          	$row_weight    = $productWeight * $productQty;
          	$totalWeight  = $totalWeight + $row_weight;
          	}
          	
          	?>
          	 <?php }?>
          	 <form name="frmDeliveryMethods" id="frmDeliveryMethods" action="" method="post">
          	  <?php foreach($shippingCarrierRanges As $cIndex=>$shippingCarrierRange){?>

           <?php 
         $MaxWeight = $shippingCarrierRange->rangeTo;
        if($MaxWeight >= $totalWeight or empty($MaxWeight)){
        ?>
            <label class="inline">
              <input type="radio" name="chkRangeId" value="<?php print($shippingCarrierRange->id);?>" <?php if($existingShippingMethod == $shippingCarrierRange->id){?> checked="checked" <?php } ?> style="">
              <?php print($shippingCarrierRange->carrierType->carrierType);?> <?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber($shippingCarrierRange->rangePrice*CURRENCY_RATE));?> &nbsp; (<?php print($shippingCarrierRange->carrierType->delay);?>)</label>
              <?php }}?>
            <textarea name="shippComment" id="shippComment" rows="3"><?php if(isset($_SESSION["shippingComments"])){?> <?php print($_SESSION["shippingComments"]);?> <?php } ?></textarea>
            <br>
            <input value="Continue" id="button-shipping-method" name="button-shipping-method" class="btn btn-success pull-right" type="submit">
            </form>
          </div>
          <div class="checkoutsteptitle checkout-heading">Step 5: Payment  Method<a class="modify">Modify</a>
          </div>
         
         <div class="checkoutstep" style="display: none;">
            <p>Please select the preferred payment method to use on this order.</p>
            <form name="frmPaymentMethods" id="frmPaymentMethods" action="" method="post">
            <label class=" inline">
              <input name="payment_method" value="cash" id="cod" <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] == 'cash' ){?> checked="checked" <?php } ?>  type="radio">
              Cash On Delivery</label>
                  <label class=" inline">
             <input name="payment_method" value="paypal" id="paypal" <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] == 'paypal' ){?> checked="checked" <?php } ?>  type="radio">
              <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/paypal.png"  alt="PAYPAL" /></label>
                  <label class=" inline">
            <input name="payment_method" value="sage" id="sage" <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] == 'sage' ){?> checked="checked" <?php } ?> type="radio">
             <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/sage.png" /></label>
              <label class=" inline">
            <input name="payment_method" value="SecureTrading" id="secu"  <?php if(isset($_SESSION["selectedPaymentMethod"]) && $_SESSION["selectedPaymentMethod"] == 'SecureTrading' ){?> checked="checked" <?php } ?> type="radio">
             <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/secure-trade.png"  /></label>
            <br>
            <div class="pull-right">
              <div class="privacy">I have read and agree to the <a id="button" href="#">Privacy Policy</a>
              <input name="chkTermsConditions" id="chkTermsConditions" value="1" type="checkbox"><br>
            <input value="Continue" id="button-payment-method" name="button-payment-method" class="btn btn-success pull-right" type="submit">
           </div>
              </div>
              </form>
            </div>
          
         
          <div class="checkoutsteptitle down checkout-heading">Step 6: Confirm Order<a class="modify">Modify</a>
          </div>
          <div class="checkoutstep" style="display: none;">
            <form name="orderConfirm" id="orderConfirm" action="order.html" method="post">  
            <div class="cart-info">
                <table class="table table-striped table-bordered">
                  <tbody><tr>
                    <th width="14%" class="image">Image</th>
                    <th width="21%" class="name">Product Name</th>
                    <th width="14%" class="model">Model</th>
                    <th width="12%" class="quantity">Quantity</th>
                    <th width="15%" class="price">Unit Price</th>
                    <th width="13%" class="total">Total</th>
                    <th width="11%" class="total">Edit</th>
                  </tr>
                    <?php if($cartContents){?>
                        <?php foreach($cartContents As $cIndex=>$cartContent){
                                $totalAmount = $totalAmount +  $cartContent->productSubTotal; 
                        ?>   
             
                  <tr>
                    <td class="image"><a href="#"><img width="50" height="50" src="<?php print(SITE_BASE_URL);?>includes/extLibs/imagesize.php?file=<?php print($cartContent->productImage->recordText);?>&maxw=40&maxh=40" alt="product" title="<?php print($cartContent->productName); ?>"></a></td>
                    <td class="name"><a href="#"><?php print($cartContent->productName); ?></a></td>
                    <td class="model"><?php print($cartContent->productInfo->productCode); ?></td>
                    <td class="quantity"><input type="text" class="span1" name="quantity[40]" value="<?php print($cartContent->productQty); ?>" size="1"></td>
                    <td class="price"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productUnitePrice*CURRENCY_RATE))); ?></td>
                    <td class="total"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber(($cartContent->productUnitePrice*CURRENCY_RATE))); ?></td>
                    <td class="total"><a href="#"><img alt="" src="<?php print(FRONT_LAYOUT_URL);?>contents/images/remove.png" data-original-title="Remove" class="tooltip-test"></a></td>
                  </tr>
                  <?php }}?>
                <?php 
                 $result = count($cartContents);
                if ($result>0) {
                	$vat = 21.0;
                }
                else {
                	$vat = "0.00";
                }
                ?>
                </tbody></table>
              </div>
            <div class="row">
        <div class="pull-right">
          <div class="span4 pull-right">
            <table class="table table-striped table-bordered ">
              <tbody><tr>
                <td><span class="extra bold">Sub-Total :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber($totalAmount*CURRENCY_RATE)); ?></span></td>
              </tr>
              <tr>
                <td><span class="extra bold">Shipping Amount <?php if($cartShippingMethod){?> <span>(<?php print($cartShippingMethod->carrierType->carrierType);?>)</span><?php } ?> :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?><?php if($totalAmount>0){?> <?php if($cartShippingMethod){?> <?php print(priceFormateNumber($cartShippingMethod->rangePrice*CURRENCY_RATE));?> <?php } } else {?> 0.00 <?php } ?></span></td>
              </tr>
              <tr>
             
                <td><span class="extra bold">VAT (17.5%) :</span></td>
                <td><span class="bold"><?php print(CURRENCY_CODE); ?> <?php echo $vat;?></span></td>
              </tr>
              <tr>
                <td><span class="extra bold totalamout">Total :</span></td>
                <td><span class="bold totalamout"><?php print(CURRENCY_CODE); ?>
           <?php 
                                if($cartShippingMethod){
                                    $shippingCost = $cartShippingMethod->rangePrice*CURRENCY_RATE;
                                } else {
                                    $shippingCost = 0;
                                }
                                if($totalAmount>0){
                                $finalAmount = $totalAmount + $shippingCost+$vat;
                                }
                                print(priceFormateNumber($finalAmount*CURRENCY_RATE));
                                ?></span></td>
              </tr>
            </tbody></table><br>
            
             <input value="CheckOut" id="button-confirm" class="btn btn-success pull-right" type="submit" />
            <input type="submit" class="btn pull-right mr10" value="Continue Shopping">
          </div>
        </div>
      </div>
      </form>
          </div>
     </div>
  </section>
  <script type="text/javascript" src="<?php print(FRONT_LAYOUT_URL);?>contents/js/popup.js"></script>
<link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/popup1.css" rel="stylesheet">
    	         <div id="popupContact">
<a id="popupContactClose"><span></span></a>
		<h1><?php echo $pageTitle2;?></h1>


		   <?php print($pageBodyC);?>
	
	</div>
