<?php $bojcountry = new Country();
/*
if ($_SESSION['check'] != ''){
$cuntrySelected = $bojcountry->getCountry($billingAddress->country);
}else{
	$cuntrySelected = $bojcountry->getCountry($shippingAddressInfo->country);
}
*/
       $resultsall = $bojcountry->getAll();
         
         $objRegion = new Region();
         $regionList = $objRegion->getAll();
         $regionListJSONObject = json_encode($regionList);
        
        ?>

<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
 <script>
       
        $(document).ready(function() {
                $("#frmDeliveryDetails").validate({
                	rules: {
                		cmbTitle: {
                            required : true,
                            
                        },
                        txtEmail: {
                            required : true,
                            email : true
                        }, 
                		txtShippingFirstName: {
                         required : true,
                            //email : true
                        },
                        txtShippingLastName: {
                            required : true,
                            //number : true
                        },
                        txtShippingPhone: {
                            required : true,
                            number : true
                        },
                        txtShippingCountry: {
                            required : true,
                            //number : true
                        },
                        txtShippingLastName: {
                            required : true,
                            //number : true
                        },
                        txtShippingAddress_1: {
                            required : true,
                            //number : true
                        },
                       

                        txtShippingTownCity: {
                            required : true,
                            //number : true
                        },
                        txtShippingPostalZIP: {
                            required : true,
                            //number : true
                        },

                        txtShippingCountyState: {
                            required : true,
                            //number : true
                        }
                        

                    },
                    messages: {
                    	cmbTitle: "Please select a title",
                    	select01 : "Please select a country",
                    	//txtBillingCountyState : "Please select a region/state",
                    	chkRangeId : "Please accept the privacy policy"
                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL); ?>ajex/checkout/delivery-details-guest.php",
                            data: $("#frmDeliveryDetails").serialize()+'&ajax=1', 
                            success: function(data) {
                                if(data == 'success'){
                           window.location.href='<?php print(SITE_BASE_URL); ?>checkout/4/checkout.html?page_type=checkout&pageId=4';    
                                }else if(data == 'failed'){
                                	$('#error_notice').show();
                                    //$('#error_notice').fadeIn(10000);
                                    $('#error_notice').fadeOut(1000);
                                    
                                    
                                }
                            }
                            
                        });
                        return false;
                    }
                    
                    
                }); 
        });
</script>

<!-- Jquery Validation : End -->

<script>
$(document).ready(function(){
	$("#select01").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>	
	
	
	<section id="checkout">
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL);?>">Home</a><span class="divider">/</span></li>
        <li class="active">Checkout</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <h1 class="productname">(Guest Account) Delivery Details</h1>
     
          <div id="error_notice" style="display:none; color:red;">Email address exists</div>
          <div class="checkoutsteptitle">
          </div>
          <div class="">
            <div class="row">
              <form class="form-horizontal" name="frmDeliveryDetails" id="frmDeliveryDetails" action="" method="post">
                <fieldset>
                  <div class="span4">
                     <div class="control-group">
               <label class="control-label"><span class="red">*</span> Title:</label>
                <div class="controls">
                <select  id="cmbTitle" name="cmbTitle" class="emailinput">
                            <option value="">Select Title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Ms">Ms</option>
                            <option value="Miss">Miss</option>
                            <option value="Dr">Dr</option>
                        </select>
                        </div>
                        </div>
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingFirstName" id="txtShippingFirstName" type="text" value="" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingLastName" id="txtShippingLastName" type="text" value="" class="">
                      </div>
                    </div>
                   
                  
                   <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingAddress_1" id="txtShippingAddress_1" type="text" value="" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPhone" id="txtShippingPhone" type="text" value="" class="">
                      </div>
                    </div>
                    
                      <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select01" name="txtShippingCountry"  style="">
                          <option value="">--Select a country--</option>
                     

                            <?php foreach ($resultsall as $index=>$country) {
                             ?>
         <option country_id="<?php print ($country->id);?>" value="<?php print ($country->id);?>"><?php print ($country->name);?></option>
         <?php }?>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                  <div class="control-group">
                  <label class="control-label"><span class="red">*</span> Email:</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="txtEmail" id="txtEmail">
                  </div>
                </div>
                
                    <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtShippingCompanyName" id="txtShippingCompanyName" type="text" value="" class="">
                      </div>
                    </div>
                   
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtShippingAddress_2" id="txtShippingAddress_2" type="text" value=""  class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingTownCity" id="txtShippingTownCity" type="text" value="" class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtShippingPostalZIP" id="txtShippingPostalZIP" type="text" value="" class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                          <select id="select02" name="txtShippingCountyState" style="">
                         <option value=''>Region / State:</option>
                        </select>
                      </div>
                    </div>
                    
                  
                    
                  </div>
                </fieldset>
                <input type="hidden" name="txtShippingAddressId" id="txtShippingAddressId" value="" />
                 <input type="hidden" name="txtPassword" id="txtPassword" value="123456" />
                <input value="Continue" id="delivery_details"  class="btn btn-success pull-right" type="submit">
            
             <div class="span1 pull-right" ><a href="<?php print(SITE_BASE_URL); ?>shopping-cart.html" class="btn btn-success">Back</a></div>
             </form>
            </div>
           
          </div>
          
          
     </div>
  </section>

