<!-- Jquery Validation :Start -->
<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#frmDeliveryMethods").validate({
                    rules: { 
                    	chkRangeId: {
                            required : true,
                            //email : true
                        },
                        password: {
                            required : true,
                            //number : true
                        }

                    },
                    messages: {
                    	cmbTitle: "Please select a title",
                    	txtBillingCountry : "Please select a country",
                    	//txtBillingCountyState : "Please select a region/state",
                    	chkRangeId : "Please select the delivery method"
                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL);?>ajex/checkout/delivery-method.php",
                            data: $("#frmDeliveryMethods").serialize()+'&ajax=1', 
                            success: function(data) {
                                if(data == 'success'){
                           window.location.href='<?php print(SITE_BASE_URL); ?>checkout/5/checkout.html?page_type=checkout&pageId=5';    
                                }else if(data == 'failed'){
                                    $('#error_notice').fadeIn(750);
                                    $('#error_notice').fadeOut(750);
                                }
                            }
                            
                        });
                        return false;
                    }
                }); 
        });
</script>
<!-- Jquery Validation : End -->
	
	<section id="checkout">
  <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php echo SITE_BASE_URL ;?>">Home</a><span class="divider">/</span></li>
        <li class="active">Checkout</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <h1 class="productname">Step 4: Shipping Method</h1>
     
          
          <div class="checkoutsteptitle">
          </div>
    <div class="checkoutstep2" >
            <p>Please select the preferred shipping method to use on this order..</p>
            <?php   if($cartContents){?>
          <?php foreach($cartContents As $cIndex=>$cartContent){
          
          	$productWeight = $cartContent->productInfo-> productWeight;
          	$unit  = $cartContent->productInfo-> weightUnit;
          	$productQty    = $cartContent->productQty;
          	
          	if($unit=="kg"){
          	
          		$productWeight = $productWeight *1000;
          	
          	}
          	
          	$row_weight    = $productWeight * $productQty;
          	$totalWeight  = $totalWeight + $row_weight;
          	}
	}
	
	$objSubcribe = new ExcludedShipping();
	$objSubcribe->tb_name = 'tbl_shipping_excluded';
	
	$objSubcribe->searchStrCountry = $shippingAddressInfo->country;
	//$objSubcribe->limit = $recLimit;
	//$objSubcribe->listingOrder = $orderStr;
	$subcribeResult = $objSubcribe->search();
	//print_r($subcribeResult);
          	?>
          	<form name="frmDeliveryMethods" id="frmDeliveryMethods" action="" method="post">
          	  <?php foreach($shippingCarrierRanges As $cIndex=>$shippingCarrierRange){
          	  //echo $shippingCarrierRange->carrierType->id;
          	  	?>
               <?php if ($shippingCarrierRange->carrierType->id != $subcribeResult[$cIndex]->shipping_id) {?>
           <?php 
         $MaxWeight = $shippingCarrierRange->rangeTo;
        if($MaxWeight >= $totalWeight or empty($MaxWeight)){
        ?>
            <label class="inline">
              <input type="radio" name="chkRangeId" id="chkRangeId" value="<?php print($shippingCarrierRange->id);?>" <?php if($existingShippingMethod == $shippingCarrierRange->id){?> checked="checked" <?php } ?> style="">
              <?php print($shippingCarrierRange->carrierType->carrierType);?> <?php print(CURRENCY_CODE); ?><?php print(priceFormateNumber($shippingCarrierRange->rangePrice*CURRENCY_RATE));?> &nbsp; (<?php print($shippingCarrierRange->carrierType->delay);?>)</label>
              <?php }}}?>
            <textarea name="shippComment" id="shippComment" rows="3"><?php if(isset($_SESSION["shippingComments"])){?> <?php print($_SESSION["shippingComments"]);?> <?php } ?></textarea>
            <br>
            <input value="Continue" id="button-shipping-method" name="button-shipping-method" class="btn btn-success pull-right" type="submit">
            
             <div class="span1 pull-right" ><a href="<?php print(SITE_BASE_URL); ?>checkout/3/checkout.html?page_type=checkout&pageId=3" class="btn btn-success">Back</a></div>
              </form>
          </div>
  </div>
    
  </section>
