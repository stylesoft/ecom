<?php $bojcountry = new Country();
      $cuntrySelected = $bojcountry->getCountry($billingAddress->country);
       $resultsall = $bojcountry->getAll();
         
         $objRegion = new Region();
         $regionList = $objRegion->getAll();
         $regionListJSONObject = json_encode($regionList);
        //testing 
        
        // print_r($cuntrySelected);
        ?>

<link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/jqueryValidation/js/jquery.validate.js" type="text/javascript"></script>
<script>
        $(document).ready(function() {
                $("#frmBillingDetails").validate({
                	rules: { 
                    	txtBillingFirstName: {
                         required : true,
                            //email : true
                        },
                        txtBillingLastName: {
                            required : true,
                            //number : true
                        },
                        txtBillingPhone: {
                            required : true,
                            number : true
                        },
                        txtBillingCountry: {
                            required : true,
                            //number : true
                        },
                       
                        txtBillingAddress_1: {
                            required : true,
                            //number : true
                        },
                       
                        txtBillingTownCity: {
                            required : true,
                            //number : true
                        },
                        txtBillingPostalZIP: {
                            required : true,
                            //number : true
                        },

                        txtBillingCountyState: {
                         required : true,
                            //number : true
                        }
                        

                    },
                    messages: {
                    	cmbTitle: "Please select a title",
                    	select01 : "Please select a country",
                    	//txtBillingCountyState : "Please select a region/state",
                    	chkRangeId : "Please accept the privacy policy"
                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php print(SITE_BASE_URL);?>ajex/checkout/billing-details.php",
                            data: $("#frmBillingDetails").serialize()+'&ajax=1', 
                            success: function(data) {
                                if(data == 'success'){
                           window.location.href='<?php print(SITE_BASE_URL); ?>checkout/3/checkout.html?page_type=checkout&pageId=3';    
                                }else if(data == 'failed'){
                                    $('#error_notice').fadeIn(750);
                                    $('#error_notice').fadeOut(750);
                                }
                            }
                            
                        });
                        return false;
                    }
                    
                    
                }); 
        });
</script>
<script>
$(document).ready(function(){
	$("#select01").change(function(){
			var country_id = $(this).find(":selected").attr("country_id");
			var regionList = <?php print($regionListJSONObject); ?>;

			//add the country id to the hidden field
			$("#txtAddCountryRegionCountryId").val(country_id);

			//Get the regions of the selected country
			var regionsOfSelectedCountry = $.grep(regionList,function(object, index){
				return object.country_id == country_id;
			});

			$('#select02').find('option').remove();
			$('#select02').append( new Option("Region / State:","",false,false) );


			//populate the combobox with regions
			var defaultSelected = false;
		    var nowSelected     = false;
			var text = "";
			var val = "";	
					
			for(var i=0; i < regionsOfSelectedCountry.length; i++){
				text = regionsOfSelectedCountry[i].name;
				val = regionsOfSelectedCountry[i].name;
				$('#select02').append( new Option(text,val,defaultSelected,nowSelected) );
			}

	});
});
</script>	
	

	
	<section id="checkout">
    <div class="container">
      <!--<ul class="breadcrumb">
        <li><a href="<?php print(SITE_BASE_URL);?>">Home</a><span class="divider">/</span></li>
        <li class="active">Checkout</li>
      </ul>-->
<?php require_once(FRONT_LAYOUT_VIEW_PATH . 'common/breadcrumb.tpl.php'); ?>
      <h1 class="productname">Step 2: Billing Details</h1>
     
          
          <div class="checkoutsteptitle">
          </div>
          <div class="">
            <div class="row">
              <form class="form-horizontal" name="frmBillingDetails" id="frmBillingDetails" action="" method="post">
                <fieldset>
                  <div class="span4">
                    <div class="control-group">
                      <label class="control-label">First Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingFirstName" id="txtBillingFirstName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->firstName);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Last Name<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingLastName" id="txtBillingLastName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->lastName);?>" <?php } ?> class="">
                      </div>
                    </div>
                   
                     <div class="control-group">
                      <label class="control-label">Address 1<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingAddress_1" id="txtBillingAddress_1" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->address);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Telephone<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPhone" id="txtBillingPhone" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->phone);?>" <?php } ?> class="">
                      </div>
                    </div>
                    
                      <div class="control-group">
                      <label class="control-label">Country<span class="red">*</span></label>
                      <div class="controls">
                        <select id="select01" name="txtBillingCountry"  style="">
                          <option value=''>--Select a country--</option>
                            <option country_id="<?php print ($cuntrySelected->id);?>" value="<?php print ($cuntrySelected->id);?>" selected="selected"><?php echo $cuntrySelected->name;?></option>
                          <?php foreach ($resultsall as $index=>$country) {?>
                       <?php  if ($country->id!=$cuntrySelected->id) {?>
                          <option country_id="<?php print ($country->id);?>" value="<?php print ($country->id);?>"><?php print ($country->name);?></option>
                        <?php } }?>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                  <div class="span4">
                    <div class="control-group">
                      <label class="control-label">Company</label>
                      <div class="controls">
                        <input name="txtBillingCompanyName" id="txtBillingCompanyName" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->houseNumber);?>" <?php } ?> class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Address 2</label>
                      <div class="controls">
                        <input name="txtBillingAddress_2" id="txtBillingAddress_2" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->region);?>" <?php } ?>  class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">City<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingTownCity" id="txtBillingTownCity" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->city);?>" <?php } ?> class="">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Post Code<span class="red">*</span></label>
                      <div class="controls">
                        <input name="txtBillingPostalZIP" id="txtBillingPostalZIP" type="text" <?php if($billingAddress){?> value="<?php print($billingAddress->postcode);?>" <?php } ?> class="">
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Region / State<span class="red">*</span></label>
                      <div class="controls">
                          <select id="select02" name="txtBillingCountyState" style="">
                          <?php if($billingAddress){?>
                           <option value='<?php print($billingAddress->state); ?>' selected="selected"><?php print($billingAddress->state); ?></option>
                          <?php } else{?>
                         <option value=''>Region / State:</option>
                         <?php }?>
                        </select>
                      </div>
                    </div>
                   
                  </div>
                   
                </fieldset>
                <input name="shipping_address" value="1" id="shipping" checked="checked" type="checkbox">
                <label for="shipping">My delivery and billing addresses are the same.</label>
              <input type="hidden" name="txtBillingAddressId" id="txtBillingAddressId" value="<?php print($billingAddress->id);?>" />
             <input value="Continue" id="billing_details" name="btnBillingDetails" class="btn btn-success pull-right" type="submit">
             <div class="span1 pull-right" ><a href="<?php print(SITE_BASE_URL); ?>checkout.html" class="btn btn-success">Back</a></div>
             </form>
            </div>
           
          </div>
          
          
     </div>
  </section>
