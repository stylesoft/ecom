

<div id="steps">
  
  <h1>Checkout :: <?php print($pageTitle);?> </h1>
  
  
  <!-- Steps Container -->
  <div id="navi_blocks">
	<ul>
        <li><a  href="<?php print(SITE_BASE_URL);?>checkout.html?step=summary">Summary</a></li>
        <li><a href="<?php print(SITE_BASE_URL);?>checkout.html?step=login">Login</a></li>
        <li><a href="<?php print(SITE_BASE_URL);?>checkout.html?step=address">Address</a></li>
        <li><a   href="<?php print(SITE_BASE_URL);?>checkout.html?step=shipping">Shipping</a></li>
        <li><a class="active_step" href="<?php print(SITE_BASE_URL);?>checkout.html?step=payment">Payment</a></li>
	</ul>
    
</div>
<!-- Steps Container End-->

    
<div align="center"> 
  
    <img src="<?php print(FRONT_LAYOUT_URL);?>contents/images/ajax-loader_cart.gif" />
    <BR/>
    <p><?php print(htmlspecialchars_decode($pageBody,ENT_QUOTES));?></p>
    <BR/>
    
<?php 
    	$cartSubTotal  		= 0;
    	$cartShippingAmount = 15;
    	$cartFinalAmount    = 0;
    	$productPriceCurrency = '';
    	$productPrice = '';
   		foreach ($cartContents As $cIndex => $cartContent) {
   			$productPrice = $cartContent->productUnitePrice * $cartContent->productQty;
   			$cartSubTotal = $cartSubTotal + $productPrice;
  		}
  		
  		$cartFinalAmount = $cartSubTotal + $cartShippingAmount;
   ?>
   
   
	
	<form name="myform" action="https://payments.securetrading.net/process/payments/choice" method="post" id="myform" >
<input name="currencyiso3a" type="hidden" value="GBP" />
<input name="mainamount" type="hidden" value="<?php print($cartFinalAmount);?>" />
<input name="version" type="hidden" value="1" />
<input name="sitereference" type="hidden" value="abc123" />
 <input type="hidden" name="orderreference" value="<?php print($orderId);?>" />
  <input type="hidden" name="singlepay" value="1" />
  
  <input type="hidden" size="30" id="billingfirstname" name="billingfirstname" value="<?php print($customerFirstname);?>"/>
  <input type="hidden" size="30" id="billinglastname" name="billinglastname" value="<?php print($customerLastname);?>"/>
  <input type="hidden" size="30" id="billingemail" name="billingemail" value="<?php print($customerEmailAddress);?>"/>
  <!--  House number of name -->
  <input type="hidden" size="60" id="billingpremise" name="billingpremise" value="<?php print($customerAddress1);?>"/>
  <!--  road -->
  <input type="hidden" size="60" id="billingstreet" name="billingstreet" value="<?php print($customerAddress2);?>" />
   <!--  post code or zip code  -->
   <input type="hidden" size="30" id="billingpostcode" name="billingpostcode" value="<?php print($customerZip);?>" />
   
   <!--  country  -->
   <input type="hidden" size="30" id="billingcountryiso2a" name="billingcountryiso2a" value="LK" />
   

  

 
</form>

  
</div>

</div>
 <!-- Steps -->
 
 <script type='text/javascript'>
window.onload = function () {
	document.myform.submit();
}
</script> 
  