<?php

require_once '../../bootstrap.php';

$selectedShippingRangeId = $_POST['chkRangeId'];
$shippingCustomerComments = $_POST['shippComment'];

if ($selectedShippingRangeId != '') {

    $objCarrierRange = new CarrierRange();

    $selectedShippingRangeInfo = $objCarrierRange->getCarrierRange($selectedShippingRangeId);
    $_SESSION["shippingComments"] = $shippingCustomerComments;
    $_SESSION["shippingMethod"] = serialize($selectedShippingRangeInfo);
    print("success");
} else {
    print("failed");
}
?>