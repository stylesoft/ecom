<?php
require_once '../../bootstrap.php';

$customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
$customerId   = $customerInfo->id;

// Shipping Address .................
$shippingAddressId = ($_POST['txtShippingAddressId'] != '' ? $_POST['txtShippingAddressId'] : ""); //$_POST['txtShippingAddressId'];
$shippingAddressTitle = ($_POST['cmbShippingTitle'] != '' ? $_POST['cmbShippingTitle'] : ""); //$_POST['cmbShippingTitle'];
$shippingAddressFirstName = ($_POST['txtShippingFirstName'] != '' ? $_POST['txtShippingFirstName'] : ""); //$_POST['txtShippingFirstName'];
$shippingAddressLastName = ($_POST['txtShippingLastName'] != '' ? $_POST['txtShippingLastName'] : ""); //$_POST['txtShippingLastName'];
$shippingAddressCompanyName = ($_POST['txtShippingCompanyName'] != '' ? $_POST['txtShippingCompanyName'] : ""); //$_POST['txtShippingCompanyName'];
$shippingAddressPhoneNumber = ($_POST['txtShippingPhone'] != '' ? $_POST['txtShippingPhone'] : ""); //$_POST['txtShippingPhone'];
$shippingAddressAddress1 = ($_POST['txtShippingAddress_1'] != '' ? $_POST['txtShippingAddress_1'] : ""); //$_POST['txtShippingAddress_1'];
$shippingAddressAddress2 = ($_POST['txtShippingAddress_2'] != '' ? $_POST['txtShippingAddress_2'] : ""); //$_POST['txtShippingAddress_2'];
$shippingAddressTown = ($_POST['txtShippingTownCity'] != '' ? $_POST['txtShippingTownCity'] : ""); //$_POST['txtShippingTownCity'];
$shippingAddressCounty = ($_POST['txtShippingCountyState'] != '' ? $_POST['txtShippingCountyState'] : ""); //$_POST['txtShippingCountyState'];
$shippingAddressPostal = ($_POST['txtShippingPostalZIP'] != '' ? $_POST['txtShippingPostalZIP'] : ""); //$_POST['txtShippingPostalZIP'];
$shippingAddressCountry = ($_POST['txtShippingCountry'] != '' ? $_POST['txtShippingCountry'] : ""); //$_POST['txtShippingCountry'];

// save the Your Shipping Address
$objCustomerAddress = new Address();
$objCustomerAddress->id = $shippingAddressId;
$objCustomerAddress->title = $shippingAddressTitle;
$objCustomerAddress->firstName = $shippingAddressFirstName;
$objCustomerAddress->lastName = $shippingAddressLastName;
$objCustomerAddress->houseNumber = $shippingAddressCompanyName;
$objCustomerAddress->phone = $shippingAddressPhoneNumber;
$objCustomerAddress->address = $shippingAddressAddress1;
$objCustomerAddress->region = $shippingAddressAddress2;
$objCustomerAddress->city = $shippingAddressTown;
$objCustomerAddress->state = $shippingAddressCounty;
$objCustomerAddress->postcode = $shippingAddressPostal;
$objCustomerAddress->country = $shippingAddressCountry;
$objCustomerAddress->membersId = $customerId;
$objCustomerAddress->addressObjectType = 'CUSTOMER';
$objCustomerAddress->addressType = 'SHIPPING';

$isShippingAddressUpdated   = false;
if($customerId != ""){
    if($shippingAddressId == ""){
       $isShippingAddressUpdated = $objCustomerAddress->addAddress();
       $isShippingAddressUpdated   = true;
    } else {
       $isShippingAddressUpdated = $objCustomerAddress->updateAddress();
       $isShippingAddressUpdated   = true;
    }
}

if ($isShippingAddressUpdated == true) {
   print("success");
} else {
    print("failed");
}

?>