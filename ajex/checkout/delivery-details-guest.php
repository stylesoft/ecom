<?php
require_once '../../bootstrap.php';
$siteEmailAddress = SITE_EMAIL;
require "../../includes/extLibs/class.phpmailer.php";

//$customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];

// Shipping Address .................
$shippingAddressId = ($_POST['txtShippingAddressId'] != '' ? $_POST['txtShippingAddressId'] : ""); //$_POST['txtShippingAddressId'];
$shippingAddressTitle = ($_POST['cmbTitle'] != '' ? $_POST['cmbTitle'] : ""); //$_POST['cmbShippingTitle'];
$shippingAddressFirstName = ($_POST['txtShippingFirstName'] != '' ? $_POST['txtShippingFirstName'] : ""); //$_POST['txtShippingFirstName'];
$shippingAddressLastName = ($_POST['txtShippingLastName'] != '' ? $_POST['txtShippingLastName'] : ""); //$_POST['txtShippingLastName'];
$shippingAddressCompanyName = ($_POST['txtShippingCompanyName'] != '' ? $_POST['txtShippingCompanyName'] : ""); //$_POST['txtShippingCompanyName'];
$shippingAddressPhoneNumber = ($_POST['txtShippingPhone'] != '' ? $_POST['txtShippingPhone'] : ""); //$_POST['txtShippingPhone'];
$shippingAddressAddress1 = ($_POST['txtShippingAddress_1'] != '' ? $_POST['txtShippingAddress_1'] : ""); //$_POST['txtShippingAddress_1'];
$shippingAddressAddress2 = ($_POST['txtShippingAddress_2'] != '' ? $_POST['txtShippingAddress_2'] : ""); //$_POST['txtShippingAddress_2'];
$shippingAddressTown = ($_POST['txtShippingTownCity'] != '' ? $_POST['txtShippingTownCity'] : ""); //$_POST['txtShippingTownCity'];
$shippingAddressCounty = ($_POST['txtShippingCountyState'] != '' ? $_POST['txtShippingCountyState'] : ""); //$_POST['txtShippingCountyState'];
$shippingAddressPostal = ($_POST['txtShippingPostalZIP'] != '' ? $_POST['txtShippingPostalZIP'] : ""); //$_POST['txtShippingPostalZIP'];
$shippingAddressCountry = ($_POST['txtShippingCountry'] != '' ? $_POST['txtShippingCountry'] : ""); //$_POST['txtShippingCountry'];

// personal details ...........
$id = '';
$title = $shippingAddressTitle;
$firstName = $shippingAddressFirstName;
$lastName = $shippingAddressLastName;
$email = $_POST['txtEmail'];
$password = $_POST['txtPassword'];
$status = 'Enabled';
$addedDate = getCurrentDateTime();
$lastLoginDate = '';
$lastLoginFrom = '';

$objCustomer = new Customer();
$objCustomer->tb_name = 'tbl_customer';
$objCustomer->searchStrEmail = $email;
$customerInfoEmail = $objCustomer->search();

$objCustomer->id = $id;
$objCustomer->title = $title;
$objCustomer->firstName = $firstName;
$objCustomer->lastName = $lastName;
$objCustomer->email = $email;
$objCustomer->password = md5($password);
$objCustomer->status = $status;
$objCustomer->addedDate = $addedDate;
$objCustomer->lastLoginDate = $lastLoginDate;
$objCustomer->lastLoginFrom = $lastLoginFrom;

if ($customerInfoEmail[0]->email != $email) {

$customerId = $objCustomer->addCustomer();
} elseif($customerInfoEmail[0]->email == $email){
	echo failed;
	exit;
}

$customerInfo = $objCustomer->loginCustomer($email, md5($password));
if ($customerInfo) {

	session_regenerate_id();
	$_SESSION['SESS_CUSTOMER_INFO'] = $customerInfo;
	$_SESSION['IS_CUSTOMER_LOGIN'] = 'Yes';
	$_SESSION['IS_GUEST_LOGIN'] = 'Yes';
	session_write_close();
}
//$customerId   = $customerInfo->id;

if ($customerId) {


// save the Your Shipping Address
$objCustomerAddress = new Address();
$objCustomerAddress->id = $shippingAddressId;
$objCustomerAddress->title = $shippingAddressTitle;
$objCustomerAddress->firstName = $shippingAddressFirstName;
$objCustomerAddress->lastName = $shippingAddressLastName;
$objCustomerAddress->houseNumber = $shippingAddressCompanyName;
$objCustomerAddress->phone = $shippingAddressPhoneNumber;
$objCustomerAddress->address = $shippingAddressAddress1;
$objCustomerAddress->region = $shippingAddressAddress2;
$objCustomerAddress->city = $shippingAddressTown;
$objCustomerAddress->state = $shippingAddressCounty;
$objCustomerAddress->postcode = $shippingAddressPostal;
$objCustomerAddress->country = $shippingAddressCountry;
$objCustomerAddress->membersId = $customerId;
$objCustomerAddress->addressObjectType = 'CUSTOMER';
$objCustomerAddress->addressType = 'SHIPPING';

$isBillingAddressUpdated   = false;
 
		$isBillingAddressUpdated = $objCustomerAddress->addAddress();
		$isBillingAddressUpdated   = true;

    
    $objMailTemplate = new MailTemplate();
    $templateInfo    = $objMailTemplate->getTemplate('1');
    $fromMail = $templateInfo->fromMail;
    $fromName = $templateInfo->fromName;
    $mailSubject = $templateInfo->mailSubject;
    $mailText    = $templateInfo->mailText;
    
    $mailTemplate = str_replace("[customer_name]",$firstName,$mailText);
    $mailTemplate = str_replace("[user_name]",$email,$mailTemplate);
    $mailTemplate = str_replace("[user_password]",$password,$mailTemplate);
    
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->AddReplyTo($fromMail, $fromName);
    $mail->AddAddress($email);
    $mail->SetFrom($fromMail, $fromName);
    $mail->Subject = $mailSubject;
    $mail->MsgHTML($mailTemplate);
    if($mail->Send()){
    	echo "success";
    	exit;
    }
    //header('Location: login.php?suc=suc');
    //echo "success";
    //exit;	
} else {
	echo "failed";
	exit;
}

?>
