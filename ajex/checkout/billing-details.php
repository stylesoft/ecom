<?php
session_start();
require_once '../../bootstrap.php';

$customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
$customerId   = $customerInfo->id;


if ($_POST['shipping_address'] == ''){
	//$deliveryadd = $_POST['shipping_address'];
	
	unset($_SESSION['check']);
}else{
$deliveryadd = $_POST['shipping_address'];
$_SESSION['check'] = $deliveryadd;
}


$billingAddressId = ($_POST['txtBillingAddressId'] != '' ? $_POST['txtBillingAddressId'] : ""); //$_POST['txtBillingAddressId'];
$billingAddressTitle = ($_POST['cmbBillingTitle'] != '' ? $_POST['cmbBillingTitle'] : ""); //$_POST['cmbBillingTitle'];
$billingAddressFirstName = ($_POST['txtBillingFirstName'] != '' ? $_POST['txtBillingFirstName'] : ""); //$_POST['txtBillingFirstName'];
$billingAddressLastName = ($_POST['txtBillingLastName'] != '' ? $_POST['txtBillingLastName'] : ""); //$_POST['txtBillingLastName'];
$billingAddressCompanyName = ($_POST['txtBillingCompanyName'] != '' ? $_POST['txtBillingCompanyName'] : ""); //$_POST['txtBillingCompanyName'];
$billingAddressPhoneNumber = ($_POST['txtBillingPhone'] != '' ? $_POST['txtBillingPhone'] : ""); //$_POST['txtBillingPhone'];
$billingAddressAddress1 = ($_POST['txtBillingAddress_1'] != '' ? $_POST['txtBillingAddress_1'] : ""); //$_POST['txtBillingAddress_1'];
$billingAddressAddress2 = ($_POST['txtBillingAddress_2'] != '' ? $_POST['txtBillingAddress_2'] : ""); //$_POST['txtBillingAddress_2'];
$billingAddressTown = ($_POST['txtBillingTownCity'] != '' ? $_POST['txtBillingTownCity'] : ""); //$_POST['txtBillingTownCity'];
$billingAddressCounty = ($_POST['txtBillingCountyState'] != '' ? $_POST['txtBillingCountyState'] : ""); //$_POST['txtBillingCountyState'];
$billingAddressPostal = ($_POST['txtBillingPostalZIP'] != '' ? $_POST['txtBillingPostalZIP'] : ""); //$_POST['txtBillingPostalZIP'];
$billingAddressCountry = ($_POST['txtBillingCountry'] != '' ? $_POST['txtBillingCountry'] : ""); //$_POST['txtBillingCountry'];

// save the Your Billing Address
$objCustomerAddress = new Address();
$objCustomerAddress->id = $billingAddressId;
$objCustomerAddress->title = $billingAddressTitle;
$objCustomerAddress->firstName = $billingAddressFirstName;
$objCustomerAddress->lastName = $billingAddressLastName;
$objCustomerAddress->houseNumber = $billingAddressCompanyName;
$objCustomerAddress->phone = $billingAddressPhoneNumber;
$objCustomerAddress->address = $billingAddressAddress1;
$objCustomerAddress->region = $billingAddressAddress2;
$objCustomerAddress->city = $billingAddressTown;
$objCustomerAddress->state = $billingAddressCounty;
$objCustomerAddress->postcode = $billingAddressPostal;
$objCustomerAddress->country = $billingAddressCountry;
$objCustomerAddress->membersId = $customerId;
$objCustomerAddress->addressObjectType = 'CUSTOMER';
$objCustomerAddress->addressType = 'BILLING';

$isBillingAddressUpdated   = false;
if($customerId != ""){
    if($billingAddressId == ""){
       $isBillingAddressUpdated = $objCustomerAddress->addAddress();
       $isBillingAddressUpdated   = true;
    } else {
       $isBillingAddressUpdated = $objCustomerAddress->updateAddress();
       $isBillingAddressUpdated   = true;
    }
    
}



if ($isBillingAddressUpdated == true) {
   print("success");
} else {
    print("failed");
}
?>