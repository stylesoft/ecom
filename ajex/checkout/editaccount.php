<?php
require_once '../../bootstrap.php';
$customerInfo = $_SESSION['SESS_CUSTOMER_INFO'];
$customerId   = $customerInfo->id;


// personal details ...........
$id = $customerId;
$title = $_POST['cmbTitle'];
$firstName = $_POST['txtFirstName'];
$lastName = $_POST['txtLastName'];
$email = $_POST['txtEmail'];
//$password = $_POST['txtPassword'];
//$status = 'Enabled';
//$addedDate = getCurrentDateTime();
$lastLoginDate = '';
$lastLoginFrom = '';

$objCustomer = new Customer();
$objCustomer->tb_name = 'tbl_customer'; 
$CustomerDetails = $objCustomer->getCustomer($customerId);

//$password = $CustomerDetails->password;
$status = $CustomerDetails->status;
$addedDate = $CustomerDetails->addedDate;


$objCustomer->id = $id;
$objCustomer->title = $title;
$objCustomer->firstName = $firstName;
$objCustomer->lastName = $lastName;
$objCustomer->email = $email;
//$objCustomer->password = md5($password);
//$objCustomer->status = $status;
$objCustomer->addedDate = $addedDate;
$objCustomer->lastLoginDate = $lastLoginDate;
$objCustomer->lastLoginFrom = $lastLoginFrom;

$customerupdate   = false;
$customerupdate = $objCustomer->editCustomer();
if ($customerupdate != '') {
	$customerupdate = true;
}




// Billing Address .................
$billingAddressId = ($_POST['txtBillingAddressId'] != '' ? $_POST['txtBillingAddressId'] : ""); //$_POST['txtBillingAddressId'];
$billingAddressTitle = ($title != '' ? $title : ""); //$_POST['cmbBillingTitle'];
$billingAddressFirstName = ($_POST['txtBillingFirstName'] != '' ? $_POST['txtBillingFirstName'] : ""); //$_POST['txtBillingFirstName'];
$billingAddressLastName = ($_POST['txtBillingLastName'] != '' ? $_POST['txtBillingLastName'] : ""); //$_POST['txtBillingLastName'];
$billingAddressCompanyName = ($_POST['txtBillingCompanyName'] != '' ? $_POST['txtBillingCompanyName'] : ""); //$_POST['txtBillingCompanyName'];
$billingAddressPhoneNumber = ($_POST['txtBillingPhone'] != '' ? $_POST['txtBillingPhone'] : ""); //$_POST['txtBillingPhone'];
$billingAddressAddress1 = ($_POST['txtBillingAddress_1'] != '' ? $_POST['txtBillingAddress_1'] : ""); //$_POST['txtBillingAddress_1'];
$billingAddressAddress2 = ($_POST['txtBillingAddress_2'] != '' ? $_POST['txtBillingAddress_2'] : ""); //$_POST['txtBillingAddress_2'];
$billingAddressTown = ($_POST['txtBillingTownCity'] != '' ? $_POST['txtBillingTownCity'] : ""); //$_POST['txtBillingTownCity'];
$billingAddressCounty = ($_POST['txtBillingCountyState'] != '' ? $_POST['txtBillingCountyState'] : ""); //$_POST['txtBillingCountyState'];
$billingAddressPostal = ($_POST['txtBillingPostalZIP'] != '' ? $_POST['txtBillingPostalZIP'] : ""); //$_POST['txtBillingPostalZIP'];
$billingAddressCountry = ($_POST['txtBillingCountry'] != '' ? $_POST['txtBillingCountry'] : ""); //$_POST['txtBillingCountry'];

// save the Your Billing Address
$objCustomerAddress = new Address();
$objCustomerAddress->id = $billingAddressId;
$objCustomerAddress->title = $billingAddressTitle;
$objCustomerAddress->firstName = $billingAddressFirstName;
$objCustomerAddress->lastName = $billingAddressLastName;
$objCustomerAddress->houseNumber = $billingAddressCompanyName;
$objCustomerAddress->phone = $billingAddressPhoneNumber;
$objCustomerAddress->address = $billingAddressAddress1;
$objCustomerAddress->region = $billingAddressAddress2;
$objCustomerAddress->city = $billingAddressTown;
$objCustomerAddress->state = $billingAddressCounty;
$objCustomerAddress->postcode = $billingAddressPostal;
$objCustomerAddress->country = $billingAddressCountry;
$objCustomerAddress->membersId = $customerId;
$objCustomerAddress->addressObjectType = 'CUSTOMER';
$objCustomerAddress->addressType = 'BILLING';

$isBillingAddressUpdated   = false;
if($customerId != ""){
	
		$isBillingAddressUpdated = $objCustomerAddress->updateAddress();
		$isBillingAddressUpdated   = true;
	

}



// Shipping Address .................
$shippingAddressId = ($_POST['txtShippingAddressId'] != '' ? $_POST['txtShippingAddressId'] : ""); //$_POST['txtShippingAddressId'];
$shippingAddressTitle = ($title != '' ? $title : ""); //$_POST['cmbShippingTitle'];
$shippingAddressFirstName = ($_POST['txtShippingFirstName'] != '' ? $_POST['txtShippingFirstName'] : ""); //$_POST['txtShippingFirstName'];
$shippingAddressLastName = ($_POST['txtShippingLastName'] != '' ? $_POST['txtShippingLastName'] : ""); //$_POST['txtShippingLastName'];
$shippingAddressCompanyName = ($_POST['txtShippingCompanyName'] != '' ? $_POST['txtShippingCompanyName'] : ""); //$_POST['txtShippingCompanyName'];
$shippingAddressPhoneNumber = ($_POST['txtShippingPhone'] != '' ? $_POST['txtShippingPhone'] : ""); //$_POST['txtShippingPhone'];
$shippingAddressAddress1 = ($_POST['txtShippingAddress_1'] != '' ? $_POST['txtShippingAddress_1'] : ""); //$_POST['txtShippingAddress_1'];
$shippingAddressAddress2 = ($_POST['txtShippingAddress_2'] != '' ? $_POST['txtShippingAddress_2'] : ""); //$_POST['txtShippingAddress_2'];
$shippingAddressTown = ($_POST['txtShippingTownCity'] != '' ? $_POST['txtShippingTownCity'] : ""); //$_POST['txtShippingTownCity'];
$shippingAddressCounty = ($_POST['txtShippingCountyState'] != '' ? $_POST['txtShippingCountyState'] : ""); //$_POST['txtShippingCountyState'];
$shippingAddressPostal = ($_POST['txtShippingPostalZIP'] != '' ? $_POST['txtShippingPostalZIP'] : ""); //$_POST['txtShippingPostalZIP'];
$shippingAddressCountry = ($_POST['txtShippingCountry'] != '' ? $_POST['txtShippingCountry'] : ""); //$_POST['txtShippingCountry'];

// save the Your Shipping Address
$objCustomerAddress1 = new Address();
$objCustomerAddress1->id = $shippingAddressId;
$objCustomerAddress1->title = $shippingAddressTitle;
$objCustomerAddress1->firstName = $shippingAddressFirstName;
$objCustomerAddress1->lastName = $shippingAddressLastName;
$objCustomerAddress1->houseNumber = $shippingAddressCompanyName;
$objCustomerAddress1->phone = $shippingAddressPhoneNumber;
$objCustomerAddress1->address = $shippingAddressAddress1;
$objCustomerAddress1->region = $shippingAddressAddress2;
$objCustomerAddress1->city = $shippingAddressTown;
$objCustomerAddress1->state = $shippingAddressCounty;
$objCustomerAddress1->postcode = $shippingAddressPostal;
$objCustomerAddress1->country = $shippingAddressCountry;
$objCustomerAddress1->membersId = $customerId;
$objCustomerAddress1->addressObjectType = 'CUSTOMER';
$objCustomerAddress1->addressType = 'SHIPPING';

$isShippingAddressUpdated   = false;
if($customerId != ""){

		$isShippingAddressUpdated = $objCustomerAddress1->updateAddress();
		$isShippingAddressUpdated   = true;
	}



if ($isBillingAddressUpdated == true and $isShippingAddressUpdated == true) {
	if ($customerupdate == true) {
		print("success");
	}
	
	
} else {
	print("failed");
}


?>