<?php 
require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = 'featured';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$categoryId         = '';
$is_feartured   =   'yes';
$Special_offers = '';


// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************

$category = new Category();
$category->tb_name = 'tbl_category';
$categoryInfo = $category->getAll();

$brand = new Brand();
$brand->tb_name = 'tbl_brand';
$brandInfo = $brand->getAll();

$DisplaySettings = new ProductDisplaySettings();
$DisplaySettings->tb_name = 'tbl_product_display_settings';
$DisplayInfo  = $DisplaySettings->getProductDisplaySettings();


	$pageTitle = 'Featured Products';
	$pageKeywords = 'Featured Products';
	$pageDescription = 'Featured Products';
	



$CONTENT = '';
$LAYOUT = FRONT_LAYOUT_PATH."default_layout.tpl.php";

$CONTENT = FRONT_LAYOUT_VIEW_PATH."index/featured.tpl.php";

require_once $LAYOUT;

?>
