	<footer>
		<section id="footer-top">
			<div class="wrap">
		<h4>Register today and put your childcare in safe hands.</h4>
<p class="single">Fill out the form below to register your details with us so we can start hunting for the right person to take care of your little ones!</p>
               

				<!-- Begin Subscribe Form -->
	            <form id="subscribe" action="#" method="post">
                	<input class="subscribe" type="text" name="subscribe-email" value="" id="subscribe-email" placeholder="Your email" />
	                <input type="submit" name="submit" value="" />
	            </form><!-- End Subscribe Form -->
              <ul class="social-networks person">
									<li><a href="<?php echo SITE_TWITTER;?>" target="_blank" class="twitter"></a></li>
									<li><a href="<?php echo SITE_GOOGLEPLUS;?>" target="_blank" class="linkedin"></a></li>
									<li><a href="<?php echo SITE_LINKEDIN;?>" target="_blank" class="email"></a></li>
								</ul>

			</div>
		</section>

		<section id="footer-bottom">
			<div class="wrap">
				 Copyright &copy; &nbsp;<?php print(SITE_NAME);?>&nbsp; <?php print(Date("Y"));?>. Web Design by <a href="<?php echo SITE_BASE_URL;?>" target="_blank" >Singernet</a>   &nbsp;&nbsp;|&nbsp;&nbsp;Hosting by <a href="http://plusprodemos.com/" target="_blank" >PlusPro</a>
			</div>
			<div class="wrap" id="footer-links-div">
				<ul>
				<li><a href="<?php print(SITE_BASE_URL);?>page/45/terms_and_conditions.html" target="_blank">Terms & Conditions</a> &nbsp;&nbsp;|&nbsp;&nbsp;</li>
				<li><a href="<?php print(SITE_BASE_URL);?>page/46/privacy_policy.html" target="_blank">Privacy Policy</a> &nbsp;&nbsp;|&nbsp;&nbsp;</li>
				<li><a href="<?php print(SITE_BASE_URL);?>page/47/cookies.html" target="_blank">Cookies</a></li>
				</ul>
			</div>
		</section>
	</footer>
