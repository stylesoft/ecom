<?php
require_once 'bootstrap.php';
// eddidby my change
// page variables........
$pageId = '';
$pageType = '';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$newsId = "";
$contentBanner = "";
$rightTopBanner = "";
$rightbottomBanner = "";
$bottomBanner = "";
$sideBarPageId = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();

//*********************************
// product category id
$productCategoryId = "";
$productId = "";

if ($_GET) {

    if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }


    if (isset($_GET['page_type'])) {
        $pageType = $_GET['page_type'];
    }

    if (isset($_GET['c'])) {
        $productCategoryId = $_GET['c'];
    }

    if (isset($_GET['productId'])) {
        $productId = $_GET['productId'];
    }

    if ($pageId) {
        $pageId = $_GET['pageId'];


        if ($pageType == 'mainPage' || $pageType == 'shoppingCart' || $pageType == 'registration' || $pageType == 'login' || $pageType =='checkout' || $pageType =='shipping-and-billing' || $pageType =='payment-method' || $pageType =='myaccount' || $pageType == 'myorder' || $pageType == 'paypal' || $pageType == 'wish_list' || $pageType == 'search') {
            $selectedPageInfo = $pageObject->getPage($pageId);
            $sideBarPageId = 'p' . $pageId;
        } else if ($pageType == 'contactUsPage') {
            $selectedPageInfo = $contactPageObject->getContactPage($pageId);
            $sideBarPageId = 'c' . $pageId;
        }elseif($pageType == 'sub_pages'){
			$selectedPageInfo 		= $subPageObject->getSubPage($pageId);
	}elseif($pageType == 'sec-sub-page'){
			$selectedPageInfo 		= $secSubPageObject->getSecSubPage($pageId);
	}
	
	
	
        $pageId = $selectedPageInfo->id;
        $pageTitle = $selectedPageInfo->title;
        $pageKeywords = $selectedPageInfo->keywords;
        $pageDescription = $selectedPageInfo->description;
        $pageBody = $selectedPageInfo->body;
        $pageListingID = $selectedPageInfo->listingId;
        $pageName = $selectedPageInfo->name;
        $pageIsLive = $selectedPageInfo->live;
        $isContactBoxEnable = $selectedPageInfo->isContact;
    }
} else {
    $pageId = 1;
    $pageType = 'homePage';
    $selectedPageInfo = $homePageObject->getHomePage($pageId);
    $pageId = $selectedPageInfo->id;
    $pageTitle = $selectedPageInfo->title;
    $pageKeywords = $selectedPageInfo->keywords;
    $pageDescription = $selectedPageInfo->description;
    $pageBody = $selectedPageInfo->body;
    $pageListingID = null; //$selectedPageInfo->listingId;
    $pageName = $selectedPageInfo->name;
    $pageIsLive = null; //$selectedPageInfo->live;
    $isContactBoxEnable = null; //$selectedPageInfo->isContact;

    $sideBarPageId = 'h' . $pageId;
}

$CONTENT = '';
if ($pageType == 'homePage') {
    $CONTENT = FRONT_LAYOUT_VIEW_PATH . "index/index.tpl.php";
} 


else {
    $CONTENT = FRONT_LAYOUT_VIEW_PATH . "index/default.tpl.php";
}


$teamPageInfo = $pageObject->getPage('12');
$pageBody1 = $teamPageInfo->body;
$pageTitle2 = $teamPageInfo1->title;



$selectedcontact = $contactPageObject->getContactPage('1');
$pagecontact = $selectedcontact->title;

// e-ecommerce sites don't change it....
if ($pageType == 'productPage') {
    if($productId != ""){
            $objProduct = new Product();
            $objProduct->tb_name = 'tbl_product';
            $productInfo = $objProduct->getProduct($productId);
		
            $pageTitle 				= $productInfo->productName;
            $pageKeywords 			= $productInfo->keywords;
	        $pageDescription        = $productInfo->productSmallDescription;
	        //$pageCode				= $productInfo->productCode;
            
            $objCategory = new Category();
            $categoryInfo                       = $productInfo->category;
            $productCategoryID                  = $categoryInfo->id;
            $parentCategoryID                   = $categoryInfo->parent_category;
            $firstParentCategoryID = $objCategory->getParentCategory($parentCategoryID);
            
            
            $objProduct 				= new Product();
			$objProduct->tb_name 		= 'tbl_product';
		
			$recLimit    = " LIMIT 0,3";
			
			$objProduct->searchStr      = '';
			$objProduct->limit          = $recLimit;
			$objProduct->category       = '';
			$objProduct->listingOrder   = ' display_order Asc';
			$objProduct->isFeatured     = 'yes';
			$customersWhoViewedThisItemAlsoViewed     = $objProduct->search();
	
    }   
}elseif($pageType == 'categoryPage' || $pageType == 'subCategoryPage' || $pageType == 'search') {
    
    
    // selected category info.........
    $categoryGroup = array();
    $currentCategoryId = "";
    $subcategoryId     = "";
    $productType       = "";
    
    $categoryName           = "";
    $categoryDescription    = "";
    $subCategoryName        = "";
    $secSubCategoryName     = "";
    
    
    if($_GET['categoryId']){
        $categoryId = $_GET['categoryId'];
         $objCategory = new Category();
         $categoryInfo        = $objCategory->getCategory($categoryId);
         $categoryName        = $categoryInfo->category_name;
         $categoryDescription    = $categoryInfo->category_description;
         
         $pageTitle = $categoryName;
         $pageKeywords = $categoryDescription;
         $pageDescription = $categoryDescription;
    }
    
    if ($_GET['id'] !=''){
    	$categoryId = $_GET['id'];
    	$objCategory = new Category();
    	$categoryInfo        = $objCategory->getCategory($categoryId);
    	$categoryName        = $categoryInfo->category_name;
    	$categoryDescription    = $categoryInfo->category_description;
    	 
    	$pageTitle = 'Category';
    	$pageKeywords = $categoryDescription;
    	$pageDescription = $categoryDescription;
    }
    
   
    if($_GET['subcategoryId']){
        $subcategoryId = $_GET['subcategoryId'];
       
        $objCategory = new Category();
        $subCategoryInfo        = $objCategory->getCategory($subcategoryId);
        $subCategoryName        = $subCategoryInfo->category_name;
        $subCategoryDescription = $subCategoryInfo->category_description;
        $pageTitle = $subCategoryName;
        $pageKeywords = $subCategoryName;
        $pageDescription = $subCategoryDescription;
        
    }
    
    if($_GET['productType']){
        $productType = $_GET['productType'];
        
        $objCategory = new Category();
        $secSubCategoryInfo         = $objCategory->getCategory($productType);
        $secSubCategoryName         = $secSubCategoryInfo->category_name;
        
    }
    

    if($productType != ""){
        $currentCategoryId = $productType;
    }elseif($subcategoryId != ""){
        $currentCategoryId = $subcategoryId;
    } else {
        $currentCategoryId = $categoryId;
    }
    
    

    if($currentCategoryId){
    $objCategory = new Category();
    $selectedCategoryInfo  = $objCategory->getCategory($currentCategoryId);
    }
    
    // get the sub categories into the array
    if($selectedCategoryInfo->subCategory){
        
        foreach($selectedCategoryInfo->subCategory As $sIndex=>$subCategory){

             if($subCategory->subCategory){
                 
                 foreach ($subCategory->subCategory As $subIndex=>$secSubCategory){
                     $currentCategoryId  = $secSubCategory->id;
                     array_push($categoryGroup, $currentCategoryId);
                 }
             } else {
                 $currentCategoryId  = $subCategory->id;
                 array_push($categoryGroup, $currentCategoryId);
             }
        }
    } else {
        
        $productCategoryId = $currentCategoryId;
        
    }

    
    // set the pagination
    //-------------------------
                $searchQ        = "";
		$recLimit       = "";
		$orderStr       = "";
                
		$objPaggination = new Paggination();
		$rmax           = "";
		$page           = "";
		if(isset($_GET['rmax'])){
		$rmax		= 	$_GET['rmax'];
		}
		
					
		if($rmax != ""){
			$objPaggination->ResultsPerPage = $rmax;
		}
					
					
		
		
		if(isset($_GET['page'])){			
			$page	= $_GET['page'];
		}
		if($page == ''){
			$page = 1;
		}
		
		if(isset($_GET['q'])){
			$searchQ = $_GET['q'];
		}
                
                $objProduct = new Product();
		$objProduct->tb_name = 'tbl_product';
		
		$objProduct->searchStr      = $searchQ;
		$objProduct->category       = $productCategoryId;
                $objProduct->categoryGroup  = $categoryGroup;
		$totalResult                = $objProduct->countRec();
		
		//-------------------
		$objPaggination->CurrentPage  = $page;
		$objPaggination->TotalResults = $totalResult;
		$paginationData               = $objPaggination->getPaggingData();
		$pageLimit1                   = $paginationData['MYSQL_LIMIT1'];
		$pageLimit2                   = $paginationData['MYSQL_LIMIT2'];
		$limit 			      = " LIMIT $pageLimit1,$pageLimit2";
                $pagginationDataResult        = $objPaggination;
        
    // get the category product....
    //--------------------------------
                $objProduct = new Product();
		$objProduct->tb_name = 'tbl_product';
               
                 if($productKey == 'top-sellers'){
                    $objProduct->listParam      = $productKey;
                 }
		
                
		$objProduct->searchStr      = $searchQ;
		$objProduct->category       = $productCategoryId;
                $objProduct->categoryGroup  = $categoryGroup;
		$objProduct->limit          = $limit;
		$objProduct->listingOrder   = $orderStr;
                
                 if($productKey == 'specials'){
                    $objProduct->isSpecialOffer       = 'Yes';
                 }
		$productResult              = $objProduct->search();
                
    

} elseif($pageType == 'shoppingCart'){
    
                $cartContents = "";
                $totalAmount   = 0;
                $objShippingMethod  = new ShippingMethod();
               
                
                $cartShippingMethod = getShippingMethod();
                
                //$shippiongAmount = 15;
		        $cartContents   = getCartContents();
		
		
		
		
		$CONTENT = FRONT_LAYOUT_VIEW_PATH."cart/shopping-cart.tpl.php";
    
}elseif($pageType == 'wish_list'){
        $customerId      = "";
        if(isset($_SESSION['IS_CUSTOMER_LOGIN'])){
            if($_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
                    $currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
                    $customerId   = $currentCustomerInfo->id;
                  } else {
                      header('Location: login.html');
                  }
       } else {
           header('Location: login.html');
       }
        $objWishList = new WishList();
        $myWishListContents = $objWishList->getAllByCustomerId($customerId);
        $CONTENT = FRONT_LAYOUT_VIEW_PATH."wishlist/my-wish-list.tpl.php";
}elseif($pageType == 'registration'){
    
		$CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/registration.tpl.php";
    
} elseif($pageType == 'login'){
		$CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/login.tpl.php";
    
}elseif($pageType == 'checkout'){
    
  
                $checkoutstep = "";
                $activePage  = "";
                $checkoutstep = $_GET['step'];

                if($checkoutstep == ""){
                    $checkoutstep = "summary";
                }
    
                
                if($checkoutstep == 'summary'){
                    
                    $cartContents = "";
                    $totalAmount   = 0;

                    $objShippingMethod  = new ShippingMethod();
                    $shippingMethods    = $objShippingMethod->getAllShippingMethod();

                    $cartShippingMethod = getShippingMethod();

                    $cartContents   = getCartContents();
                    
                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."checkout/summary.tpl.php";
                } elseif($checkoutstep == 'login'){
                    
                    if(isset($_SESSION['IS_CUSTOMER_LOGIN'])){
			if($_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
                             header('Location: checkout.html?step=address');
                        }
                        
                     }
                     
                     $nextpage  = "checkout.html?step=address";
                    
                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."checkout/login.tpl.php";
                }elseif($checkoutstep == 'address'){
                    
                    
                    if($_POST){
                        
                        if($_POST['btnCustomerRegistration']){
                                
                                $currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
				$currentCustomerId   = $currentCustomerInfo->id;
				$objCustomer = new Customer();
				$objCustomer->tb_name = ' tbl_customer';
				$customerInfo = $objCustomer->getCustomer($currentCustomerId);	
                                
                                
                                $billingAddress             = $customerInfo->addressBillingDtls[0];
                                $shippingAddress             = $customerInfo->addressShippingDtls[0];
                                
                                $customerId = $customerInfo->id;
                                $billingAddressId = $billingAddress->id;
                                $shippingAddressId = $shippingAddress->id;
                            
                                // ------- billing address ----------------------
                                
                                    $billingAddressTitle            = $_POST['cmbBillingTitle'];
                                    $billingAddressFirstName        = $_POST['txtBillingFirstName'];
                                    $billingAddressLastName         = $_POST['txtBillingLastName'];
                                    $billingAddressCompanyName      = $_POST['txtBillingCompanyName'];
                                    $billingAddressPhoneNumber      = $_POST['txtBillingPhone'];
                                    $billingAddressAddress1         = $_POST['txtBillingAddress_1'];
                                    $billingAddressAddress2         = $_POST['txtBillingAddress_2'];
                                    $billingAddressTown             = $_POST['txtBillingTownCity'];
                                    $billingAddressCounty           = $_POST['txtBillingCountyState'];
                                    $billingAddressPostal           = $_POST['txtBillingPostalZIP'];
                                    $billingAddressCountry          = $_POST['txtBillingCountry'];

                                    $objCustomerAddress = new Address();
                                    $objCustomerAddress->id = $billingAddressId;
                                    $objCustomerAddress->title = $billingAddressTitle;
                                    $objCustomerAddress->firstName = $billingAddressFirstName;
                                    $objCustomerAddress->lastName = $billingAddressLastName;
                                    $objCustomerAddress->houseNumber = $billingAddressCompanyName;
                                    $objCustomerAddress->phone  = $billingAddressPhoneNumber;
                                    $objCustomerAddress->address = $billingAddressAddress1;
                                    $objCustomerAddress->region  = $billingAddressAddress2;
                                    $objCustomerAddress->city    = $billingAddressTown;
                                    $objCustomerAddress->state   = $billingAddressCounty;
                                    $objCustomerAddress->postcode = $billingAddressPostal;
                                    $objCustomerAddress->country  = $billingAddressCountry;
                                    $objCustomerAddress->membersId = $customerId;
                                    $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                    $objCustomerAddress->addressType = 'BILLING';
                                    if($billingAddressId){
                                        $objCustomerAddress->updateAddress();
                                    } else {
                                        $objCustomerAddress->addAddress();
                                    }
                            
                               // ---- shipping address -----------------------
                                    $shippingAddressTitle            = $_POST['cmbShippingTitle'];
                                    $shippingAddressFirstName        = $_POST['txtShippingFirstName'];
                                    $shippingAddressLastName         = $_POST['txtShippingLastName'];
                                    $shippingAddressCompanyName      = $_POST['txtShippingCompanyName'];
                                    $shippingAddressPhoneNumber      = $_POST['txtShippingPhone'];
                                    $shippingAddressAddress1         = $_POST['txtShippingAddress_1'];
                                    $shippingAddressAddress2         = $_POST['txtShippingAddress_2'];
                                    $shippingAddressTown             = $_POST['txtShippingTownCity'];
                                    $shippingAddressCounty           = $_POST['txtShippingCountyState'];
                                    $shippingAddressPostal           = $_POST['txtShippingPostalZIP'];
                                    $shippingAddressCountry          = $_POST['txtShippingCountry'];
                                
                                     $objCustomerAddress = new Address();
                                    $objCustomerAddress->id = $shippingAddressId;
                                    $objCustomerAddress->title = $shippingAddressTitle;
                                    $objCustomerAddress->firstName = $shippingAddressFirstName;
                                    $objCustomerAddress->lastName = $shippingAddressLastName;
                                    $objCustomerAddress->houseNumber = $shippingAddressCompanyName;
                                    $objCustomerAddress->phone  = $shippingAddressPhoneNumber;
                                    $objCustomerAddress->address = $shippingAddressAddress1;
                                    $objCustomerAddress->region  = $shippingAddressAddress2;
                                    $objCustomerAddress->city    = $shippingAddressTown;
                                    $objCustomerAddress->state   = $shippingAddressCounty;
                                    $objCustomerAddress->postcode = $shippingAddressPostal;
                                    $objCustomerAddress->country  = $shippingAddressCountry;
                                    $objCustomerAddress->membersId = $customerId;
                                    $objCustomerAddress->addressObjectType = 'CUSTOMER';
                                    $objCustomerAddress->addressType = 'SHIPPING';
                                    if($shippingAddressId){
                                        $objCustomerAddress->updateAddress();
                                    } else {
                                        $objCustomerAddress->addAddress();
                                    }
                            
                                    
                                    header('Location: checkout.html?step=address');
                        }
                    }
                    
                    
                    
                    if(isset($_SESSION['IS_CUSTOMER_LOGIN'])){
			if($_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
				$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
				$currentCustomerId   = $currentCustomerInfo->id;
				$objCustomer = new Customer();
				$objCustomer->tb_name = ' tbl_customer';
				$customerInfo = $objCustomer->getCustomer($currentCustomerId);	
                                
                                
                                $billingAddress             = $customerInfo->addressBillingDtls;
                                $shippingAddress             = $customerInfo->addressShippingDtls;
            
                                $billingAddressInfo             = $billingAddress[0];
                                $shippingAddressInfo            = $shippingAddress[0];
                                
                                
                                  //--------------------
                                $orderStr       = 'order_date'." ".'Asc';  
                                $objOrder = new Order();
                                $objOrder->tb_name = 'tbl_order';
                                $objOrder->customerId = $currentCustomerId;

                                $objOrder->listingOrder   = $orderStr;
                                $orderResult              = $objOrder->search();
                                //--------------------
                                
				
			}
		} else {
			header('Location: checkout.html?step=login');
		}
                    
                    
                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."checkout/address.tpl.php";
                 }elseif($checkoutstep == 'shipping'){
                     
                    
                     $isError = $_GET['error'];
                     
                     $cartContents   = getCartContents();
                     
                     $totalWeight   = 0;
                     
                     foreach($cartContents As $cartContent){
                        
                         
                         $productWeight = $cartContent->productWeight;
                         $productQty    = $cartContent->productQty;
                         $row_weight    = $productWeight * $productQty;
                         $totalWeight   = $totalWeight + $row_weight;
                     }
                    
                     $objCarrierRange = new CarrierRange();
                     $shippingCarrierRanges = $objCarrierRange->getAllByRange($totalWeight);
                     
                     
                     
                     if($_POST['htn_go_to_payment']){
              
                         
                         $termsAgreed = $_POST['shippingTermsAgree'];
                         if($termsAgreed == 'Yes'){
                         $selectedShippingRangeId = $_POST['chkRangeId'];
                         if($selectedShippingRangeId != ''){
                         $selectedShippingRangeInfo = $objCarrierRange->getCarrierRange($selectedShippingRangeId);
                         
                         $_SESSION["shippingMethod"] = serialize($selectedShippingRangeInfo);
                         
                         header('Location: checkout.html?step=payment');
                         } else {
                             header('Location: checkout.html?step=shipping&error=carrier');
                         }
                         } else {
                            header('Location: checkout.html?step=shipping&error=terms');
                         }
                         
                     }
                     
                     $existingShippingMethod = null;
                     $exitsShippingMethod = unserialize($_SESSION["shippingMethod"]);
                     if($exitsShippingMethod){
                         $existingShippingMethod = $exitsShippingMethod->id;
                     }
                     
                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."checkout/shipping.tpl.php";
                } else {
                    
                     $isError = $_GET['error'];
                    
                    if($_POST){
                        
                        if($_POST['paymentMethod']){
                            
                            
                            $selectedPaymentMethod = $_POST['rdPaymentType'];
                            
                            if($selectedPaymentMethod == ""){
                                header('Location: checkout.html?step=payment&error=paymentMethod');
                            } elseif($selectedPaymentMethod == "BankTransfer"){
                                 header('Location: order.html?paymentMethod=BankTransfer');
                            }elseif($selectedPaymentMethod == "SecureTrading"){
                                header('Location: order.html?paymentMethod=SecureTrading');
                            }elseif($selectedPaymentMethod == "PayPal"){
                                header('Location: order.html?paymentMethod=PayPal');
                            }
                            
                            
                        }
                        
                   }
                    
                    
                    
                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."checkout/payment.tpl.php";
                }
    
    
  
                
                
                
    
} elseif($pageType == 'shipping-and-billing'){
    
                if(!isset($_SESSION['IS_CUSTOMER_LOGIN'])){
			if(!$_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
				header('Location: login.html');
			}
		} else {
                    
                    
                                $currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
				$currentCustomerId   = $currentCustomerInfo->id;
				$objCustomer = new Customer();
				$objCustomer->tb_name = ' tbl_customer';
				$customerInfo = $objCustomer->getCustomer($currentCustomerId);	
                                
                                
                                $billingAddress             = $customerInfo->addressBillingDtls;
                                $shippingAddress             = $customerInfo->addressShippingDtls;
            
                                $billingAddressInfo             = $billingAddress[0];
                                $shippingAddressInfo            = $shippingAddress[0];
                    
                }
    
                
		$CONTENT = FRONT_LAYOUT_VIEW_PATH."cart/shipping-and-billing-address.tpl.php";
    
} elseif($pageType =='payment-method'){
                $CONTENT = FRONT_LAYOUT_VIEW_PATH."cart/payment-method.tpl.php";
}elseif($pageType =='myaccount'){
    
    $action = "";
    $addressInfo = "";

                if(isset($_SESSION['IS_CUSTOMER_LOGIN'])){
			if($_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
				$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
				$currentCustomerId   = $currentCustomerInfo->id;
				$objCustomer = new Customer();
				$objCustomer->tb_name = ' tbl_customer';
				$customerInfo = $objCustomer->getCustomer($currentCustomerId);	
                                
                                
                                $billingAddress             = $customerInfo->addressBillingDtls;
                                $shippingAddress             = $customerInfo->addressShippingDtls;
            
                                $billingAddressInfo             = $billingAddress[0];
                                $shippingAddressInfo            = $shippingAddress[0];
                                
                                if($_GET['action']){
                                   $action = $_GET['action']; 
                                   $customerAddressType = null;
                                   if($action == 'billing-address'){
                                      $addressInfo = $billingAddressInfo;
                                      $customerAddressType = 'BILLING';
                                   }elseif($action == 'shipping-address'){
                                      $addressInfo = $shippingAddressInfo;
                                      $customerAddressType = 'SHIPPING';
                                   }
                                   $CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/address.tpl.php";
                                } else {
                                
                                    //--------------------
                                    $orderStr       = 'order_date'." ".'Asc';  
                                    $objOrder = new Order();
                                    $objOrder->tb_name = 'tbl_order';
                                    $objOrder->customerId = $currentCustomerId;

                                    $objOrder->listingOrder   = $orderStr;
                                    $orderResult              = $objOrder->search();
                                    //--------------------

                                    $CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/myaccount.tpl.php";
                                }
			}
		} else {
			header('Location: login.html');
		}
} elseif($pageType =='myorder'){
    
    $orderReference = $_GET['orderReference'];
    $objOrder = new Order();
    $orderDetails = $objOrder->getOrder($orderReference);
    /*
    print("<pre>");
    print_r($orderDetails);
    print("</pre>");
    exit;
     * 
     */

            $customerId = $orderDetails->customerId;
            $order_date = $orderDetails->orderDate;
            $order_status = $orderDetails->orderStatus;
            $payment_status = $orderDetails->paymentStatus;
            $sub_total = $orderDetails->subTotal;
            $shipping_amount = $orderDetails->shippingAmount;
            $grand_total = $orderDetails->grandTotal;
            $payment_method = $orderDetails->paymentMethod;
    

    
    $CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/myorder.tpl.php";
    
}elseif($pageType =='myorders'){
    
    
    
     $action = "";
    $addressInfo = "";

                if(isset($_SESSION['IS_CUSTOMER_LOGIN'])){
			if($_SESSION['IS_CUSTOMER_LOGIN'] == 'Yes'){
				$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
				$currentCustomerId   = $currentCustomerInfo->id;
				$objCustomer = new Customer();
				$objCustomer->tb_name = ' tbl_customer';
				$customerInfo = $objCustomer->getCustomer($currentCustomerId);	
                              
                                //--------------------
                                    $orderStr       = 'order_date'." ".'Asc';  
                                    $objOrder = new Order();
                                    $objOrder->tb_name = 'tbl_order';
                                    $objOrder->customerId = $currentCustomerId;

                                    $objOrder->listingOrder   = $orderStr;
                                    $orderResult              = $objOrder->search();
                                    //--------------------
                                
                                $CONTENT = FRONT_LAYOUT_VIEW_PATH."customer/myorders.tpl.php";
			}
		} else {
			header('Location: login.html');
		}
    
   
    
} elseif($pageType =='paypal'){
    
    
    $pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo  = null;
		
			$selectedPageInfo 		= $pageObject->getPage($pageId);
		
		$pageId 				= $selectedPageInfo->id;
		$pageTitle 				= $selectedPageInfo->title;
		$pageKeywords 			= $selectedPageInfo->keywords;
		$pageDescription 		= $selectedPageInfo->description;
		$pageBody 				= $selectedPageInfo->body;
		$pageListingID 			= $selectedPageInfo->listingId;
		$pageName 				= $selectedPageInfo->name;
		$pageIsLive 			= $selectedPageInfo->live;
		$isContactBoxEnable 	= $selectedPageInfo->isContact;

$finalCurrency = 'GBP';
 $shippingCost  = "12.00";

$cartContents   = getCartContents();


$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
$currentCustomerId   = $currentCustomerInfo->id;
$objCustomer = new Customer();
$objCustomer->tb_name = ' tbl_customer';
$customerInfo = $objCustomer->getCustomer($currentCustomerId);	

$objAddress = new Address();
$billingAddresses = $objAddress->getAllByMember($currentCustomerId, 'BILLING');

foreach($billingAddresses As $bIndex=>$billingAddress){
$customerFirstname      = $billingAddress->firstName;
$customerLastname       = $billingAddress->lastName;
$customerAddress1       = $billingAddress->address;
$customerAddress2       = $billingAddress->region;
$customerCity           = $billingAddress->city;
$customerState          = $billingAddress->state;
$customerZip            = $billingAddress->postcode;
$customerCountry        = 'Bedfordshire';//$billingAddress->country;
}

$customerEmailAddress   = $customerInfo->email;

$currentDateTime = date( 'Y-m-d H:i:s');

$currentCustomerInfo = $_SESSION['SESS_CUSTOMER_INFO']; 
$currentCustomerId   = $currentCustomerInfo->id;
$arrCartContents = getCartContents();

$subTotal = 0;
$shippiongAmount = 15;
$grandTotal = 0;
  foreach($arrCartContents As $cIndex=>$cartContent){
      
     $productUnitePrice = $cartContent->productUnitePrice;
     $productQty = $cartContent->productQty;
     
     $subTotal = $subTotal + ($productUnitePrice * $productQty);
        
  }
    
 $grandTotal = $subTotal + $shippiongAmount;
    
    $CONTENT = FRONT_LAYOUT_VIEW_PATH."cart/paypal.tpl.php";
} 

$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";


require_once $LAYOUT;
?>